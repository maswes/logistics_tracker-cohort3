# Contribution guidelines #

As a contributor, you're responsible for quality.  Please ensure that what is working stays working so the next student finds it better than you left it.

# Who do I talk to for more information? #

As of 2015, UCSD Professor Ryan Kastner is the POC for this repository

# Overview #

Logistics tracker is a project to read Radio-Frequency Identification (RFID) tags and transmit the tag information over a wireless channel. It is divided into three parts: 

  1. RFID
  2. WiFi_driver
  3. FMCOMMS

The project is organized around the concept of an inventory control system, such as in a retail company.  A large retail company may contain hundreds or thousands of stores in which consumers can shop and make purchases.  Purchases are recorded at the point-of-sale.  Today, this point of sale often consists of purchases being unloaded from a cart and one-by-one scanned past a bar-code reader.  It is at this point that the sale of an item is recorded.

Typically such companies have distribution centers that are capable of pushing new merchandise to retail stores as purchases are made and as local (i.e. shop) inventories are drawn down. Some companies may also integrate portions of their inventory system directly with those of suppliers and/or shipping companies. This speeds the time between which a product is manufactured and the time at which it is available for purchase by a customer, thereby reducing the costs of maintaining a large inventory.  The RFID portion consists of a front-end and a back-end.

This capstone project is designed to model a system in which point-of-sale either augments or replaces the barcode scanner.  Merchandise identified by an RFID tag using an RFID reader could be purchased without having to be individually handled and scanned, or even unloaded from the shopping cart.  Requiring fewer items to be handled individually, or even handled at all, offers the promise of a faster checkout experience, requiring fewer checkout locations. 

The RFID portion represents the point-of-sale, in which the product is identified.  The FMCOMMS represents that portion in which the inventory change created by the sale is communicated back from one of many retail locations to distribution centers and corporate offices in real-time.  Of course, most such retail companies would use the Internet, private lines leased from telecommunications companies or even dial-up phone lines to transmit such information.  Ignoring this little bit of reality permitted us to incorporate into our project the concept of transmitting this information via RF.  The communications between the retail stop and distribution centers is represented by the FMCOMMS portion of our project, so named because it leverages a software-defined radio (SDR) evaluation board from Analog Devices called the FMCOMMS3 evaluation board.  The WES program was interested in having a capstone team evaluate the FMCOMMS board for suitability as a hardware platform.  After learning that the current SDR board used by the program (the Chilipepper by Toyon) was no longer in production, we decided to incorporate an evaluation of this into our project.

The FMCOMMS3 evaluation board is designed to be an auxiliary card hosted on another board that contains processing logic either in the form of an FPGA or a combination of an FPGA and a microprocessor such as is found in the Zynq System-On-a-Chip (SOC).  The Zed Board project board, by AVNET, contains a Zynq SOC as well as an FPGA Mezzanine Card (FMC) interface that connects to the FMCOMMS board.  Two Zed Boards, each with an FMCOMMS SDR board, formed two ends of an RF communications link.  One end was modeled as a a point-of-sale and interfaced with an RFID reader.  The other end was modeled as a back-end office, such as a distribution center or corporate office.  The intent of the project is to use RFID to detect and identify a product and to transmit some information about the product to the back-end.  In our project when a product was detected, a camera took a picture of the product, and that image would be sent to the back-end.

The need to connect the RFID front-end to the Zed Board and also to connect the back-end Zed Board to a computer on which the received photograph could be stored required an interface.  This introduces the third project component: the WiFi driver.  The WiFi driver is designed to be hosted in a Zed Board running Linux within its processing system (i.e., within its ARM core processor). The WiFi driver is designed to communicate with the RFID reader on the front-end and the computer on the back-end via WiFi. It was also designed to forward information sent by the RFID reader from the Zynq's processing system to the programmable logic where it would be transmitted via the FMCOMMS3 SDR to the back-end.  At the back-end, the WiFi driver would similarly read data received from the SDR and transmit it via WiFi to the computer on which the picture of the product would be stored.

The WiFi driver also represented our risk mitigation strategy.  From the beginning we understood that incorporating both the RFID and FMCOMMS aspects into a single three-person project represented high risk.  Therefore the WiFi was designed such that if necessary the WiFi driver could instead be used to send data between the Zed Boards using either WiFi or simply a standard wired Ethernet connection.

### **1. RFID**

Provides the RFID front-end implementation for the Logistics Tracker.

### What is this repository for?

* Maintains the source code for the RFID front-end implementation running in Raspian Linux on the R-Pi B+

### How do I get setup?

This implementation uses the LinkSprite Cottonwood RFID reader via TTL serial and incorporates an LCD and several LEDs to provide status indication.  Commenting out the LCD and LED code will not affect the RFID implementation.   In order to get setup, pull the src tree and place it in your home directory.   Ensure you have python 2.7 or later installed in Raspian.   Look at the import header to see what other library support you need and download them.  Everything is being actively supported and shouldn't be hard to find.
  


### **2. WiFi Driver**

This is a virtual WiFi driver. It has two parts, the kernel driver and the
user driver.

The kernel driver is called mac80211_serial, and the user driver is called
user_driver.

The kernel driver is based on mac80211_hwsim. It compiles as a loadable
kernel module. When the kernel module is loaded, it creates a wlan interface
that can be used by the operating system as a WiFi device. However, this
wlan interface doesn't not transmit or receive from hardware. Instead,
it opens a netlink socket that can be used by a user application. When
the operating system wants to tranmit an 802.11 (WiFi) packet, 
the packet will be written by this kernel driver to the netlink socket. 
A user application can listen on the socket for the 802.11 packet. When
the user application receives the 802.11 packet, it can transmit it via
a mechanism of its choosing. For example, when the command "iw wlan0 scan"
is issued on the command line, the operating system will send an 802.11
packet to the wlan0 interface. The packet will go to this kernel driver
which will then transmit the 802.11 packet over the netlink socket. The
802.11 packet will be received by the user application listening on the socket
and the user application can then transmit the packet using any mechanism.
The user application can send the packet to hardware to transmit the packet
over the air. The user application will also listen for 802.11 packets
When it receives an 802.11 packet, it will send the packet to the netlink
socket. The packet will be received by this kernel driver and propagated
up the networking stack.

In this program, a user driver is provided. This user driver demonstrates
the communication with the kernel driver over the netlink socket. This user
driver listens on the netlink socket for packets from the kernel driver.
When the user driver receives an 802.11  packet from the kernel driver, 
it broadcasts the packet as a UDP packet to IP address 255.255.255.255. 
The user driver also listens for broadcast UDP packets with destination address
255.255.255.255. When it receives a UDP packet to 255.255.255.255, it sends
the payload over the netlink socket to the kernel driver. In this way,
broadcast UDP packets are used to simulate a WiFi network. As far as the
operating system is concerned, the wlan0 interface presented by the 
kernel driver is a WiFi driver, and all applications used with WiFi drivers
work. For example, hostapd can be used to host an access point, wpa_supplicant
can be used to connect to an access point, iw can be used to scan for access
points, and dns_masq can be used to host a DNS server.

To compile the kernel driver, the source code for the linux distribution
for the device must be used. The files in 
/kernel_driver/drivers/net/wireless are copied to the drivers/net/wireless
folder of the kernel source code. Notice that in the Kconfig and Makefile
entries are added for mac80211_serial. The .config file is copied to the 
root of the kernel source code. The kernel is the compiled as normal. After
the kernel is compiled, the kernel driver (mac80211_serial) is compiled
with this command: "make drivers/net/wireless/mac80211_serial.ko".

uImage for the kernel and the mac80211_serial.ko module is then copied to the
device. "insmod mac80211_serial.ko" is run to install the kernel module.

To compile the user driver, the user driver is copied to the device.
The netlink library is installed by running "sudo apt-get install libnl-3-dev".
Then the user driver is compiled by running "source compile.sh" to run the
compile script. To run the user driver, run 
"user_driver <ethernet interface> <ip address>" where <ethernet interface> is
the name of the ethernet interface that the UDP packets should be broadcast on
and <ip address> is the IP address of that interface. For example, 
"user_driver eth0 10.0.2.15".

## Instructions ##

### Setting up the device for Linux ###
* Full instructions can be found here: http://wiki.analog.com/resources/tools-software/linux-software/zynq_images
* Download and flash the image to a 8GB or larger SD card by following these directions. http://wiki.analog.com/resources/tools-software/linux-software/zynq_images/windows_hosts
* After the image has been flashed, open the SD card in the file explorer and copy the contents of the folder zynq-zed-adv7511-fmcomms1 to the root folder. (http://wiki.analog.com/resources/tools-software/linux-software/zynq_images#preparing_the_image)
* Ensure that the jumpers are set to boot from the SD card.
* Plug in the HDMI monitor, and USB keyboard and mouse, connect the FMCOMMS1 board, and turn the device on.
### Getting the SDK ###
* Download the SDK from: http://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2014-4.html
* * Only the SDK is needed
Getting the kernel source code
* Download the analog devices code
* * git clone https://github.com/analogdevicesinc/linux.git
* * cd linux
* * git checkout xcomm_zynq
* Or download from this repo
* * git clone https://username@bitbucket.org/logisticstracker/linux.git
### Compiling the Kernel ###
* Follow the steps here to set up the build environment: http://www.wiki.xilinx.com/Install+Xilinx+Tools
* Follow the steps here for the steps to build the kernel: http://wiki.analog.com/resources/tools-software/linux-drivers/platforms/zynq?s[]=zedboard&s[]=fmcomms1#build_and_install_the_kernel_image
* export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
* export ARCH=arm
* source /opt/Xilinx/SDK/2014.4/settings64.sh
* (optional if .config already exists, required otherwise) make zynq_xcomm_adv7511_defconfig
* make uImage LOADADDR=0x00008000
### Compiling an application with libnl3 ###
* gcc myprogram.cpp -I /usr/include/libnl3 -lnl-3 -lnl-genl-3



### **3. FMCOMMS**

This is the Software Defined Radio (SDR) section of the Logistics Tracker application, intended to form the longer distance relay for moving RFID data to and from back-end processing.

### What does this repository contain? ###

The FMCOMMS portion of the logistics tracker project leverages the demonstration projects provided by Analog Devices.  Analog Devices provides demonstration HDL projects for a variety of hardware platforms, including the Zed Board.  In addition, Analog Devices provides two types of projects for the processing system side of the Zynq: a Linux-based version and a "no-OS" (no operating system) version.  These demonstration projects initialize all the Analog Device integrated circuits and then transmit and receive I/Q sinusoids.  The sinusoids are not transmitted from the processing system but rather are generated within an Analog Devices IC on the FMCOMMS evaluation board.  However the received signal does propagate to the processing system. All of these Analog Device repositories are available online via GitHub, but snapshots of the HDL and no-OS repositories are provided as zip files in our repository. Specifically, the snapshots are the releases of these repositories designed for Vivado 2014.2. Within the logistics_tracker-cohort3 capstone repository, these ZIP files are located at fmcomms\analog_devices\core_repository.  Note that this capstone transitioned from the FMCOMMS1 evaluation board to the FMCOMMS3 evaluation board.  The ZIP files contain projects for both evaluation boards.  For instructions on using the Analog Devices FMCOMMS1 board demonstration project, consult the Wiki http://wiki.analog.com/resources/eval/user-guides/ad-fmcomms1-ebz.  For instructions on using the Analog Devices FMCOMMS3 board consult http://wiki.analog.com/resources/eval/user-guides/ad-fmcomms3-ebz.

Also contained in the FMCOMMS portion is documentation on the FMCOMMS evaluation boards, particularly on the Analog Device integrated circuits that each evaluation board is designed to showcase.  Within the logistics_tracker-cohort3 capstone repository, these files can be found at fmcomms\analog_devices\docts.

Finally, this repository contains the Simulink models and the PL/PS projects for a implementing QPSK transceiver within the Zynq.  Within the logistics_tracker-cohort3 capstone repository, these files can be found at fmcomms\projects.  Models/Projects are organized into 'matlab', 'pl' and 'ps' sub-directories.  Each contains more than a few models/projects as the capstone evolved and more knowledge was gained.  The tools, particularly Vivado, did not always make it easy to re-organize, re-locate, re-name or save a project as a new project.  Therefore, it is appropriate to identify the projects for each that represent the final stages for each of the models/projects.  

The final Simulink model for a synthesizable QPSK transmitter can be found at fmcomms\projects\matlab\final\tx\2015_06_09\10_brk05.  Although variations of a QPSK receiver model exist, no such final project can be identified, as progress had not reached the point of developing a synthesizable receiver.   

The final HDL project for the capstone can be found at fmcomms\projects\pl\fmc3\cap02_preKaosTxIla.   This project leverages IP cores designed to aid integration of QPSK transmitters and receivers into the FMCOMMS demonstration projects.  They are written in Verilog, and are located at fmcomms\projects\pl\kaos.  They include kaos_tx_dma_ifc03, axi_ad9361_dac_dma, kaos_rx_9361_ifc, kaos_rx_proxy and kaos_rx_dma_ifc01.  The QPSK TX IP core, created from the synthesized Simulink QPSK TX model, is also contained in the fmcomms\projects\pl\kaos directory.

The final PS project is comprised of cap2015_06_09a_sw, with the supporting hardware and board-support projects 'cap2015_06_09a_hw' and 'cap2015_06_09a_sw_bsp'.  All three are located in the fmcomms\projects\ps\workspace_fmc3 directory.  The cap2015_06_09a_sw project initializes the FMCOMMS3 board and sends a repeating saw-tooth signal to the programmable logic via DMA.