## Documents for Logistics Tracker ##

### What is this repository for? ###

This repo holds the documentation for the Logistics Tracker application to include

* Milestones
* Presentations done over the quarter in class as well as supporting documentation for those presentations

The project is broken down into 3 sections and most documentation parallels this

* [RFID](https://bitbucket.org/logisticstracker/fmcomms) - Front-end detection of passive RFID tags
* [Tailored Linux Stack](https://bitbucket.org/logisticstracker/fmcomms) - tailored Linux IP stack that supports wireless 1:n communication.  This allow the RFID front-end to communicate with the Zedboard SDR
* [SDR](https://bitbucket.org/logisticstracker/fmcomms) - Software Defined Radio providing a relay over RF to a back-end host interface

### How do I get set up? ###

This repo should only contain documentation so there is not setup per se.  It's designed be be pulled separately from the other trees containing source code as it tends to be larger and not pulled as frequently.  
### Contribution guidelines ###

Enhancing the project means adding documentation.   Please keep things tidy and don't add source code here.

### Who do I talk to? ###

Questions beyond the 2015 academic year should be addressed to Ryan Kastner at UCSD.