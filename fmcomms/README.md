# README #

This is the Software Defined Radio (SDR) section of the Logistics Tracker application used to form the longer distance relay for moving RFID data to and from back-end processing.

### What is this repository for? ###

* Contains the run-ready ADC reference implementation for the FMCOMMS1 board.  This allows you to get familiar with the FMCOMMS1 implementation without the need to build it up from scratch
* Contains source code and accompanying project files for modifying the SDR in Xilinx Vivado development environment.  This project extens the FMCOMMS1 reference implementation and implements the RF TX and RX chains to modulate QPSK.
* Current support is for Vivado 2014.2
 
### How do I get set up? ###

Each project contains instructions on how to get setup.   When possible, we've zipped the entire project and committed it such that it can be easily resurrected into its original directory but on your machine.  A short set of instructions (or video) once unzipped can be used to get you fully operational.  This assumes you know how to work in the Xilinx Vivado environment.  

### Contribution guidelines ###

As a contributor, you're responsible for quality.  Please ensure that everything stays working so the next guys finds it better than you left it.

### Who do I talk to? ###

After the 2015 academic year, UCSD professor, Ryan Kastner is the POC.