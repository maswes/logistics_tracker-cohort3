
For instructions on using the Analog Devices FMCOMMS1 board consult the following Wiki:

	http://wiki.analog.com/resources/eval/user-guides/ad-fmcomms1-ebz


For instructions on using the Analog Devices FMCOMMS3 board consult the following Wiki:

	http://wiki.analog.com/resources/eval/user-guides/ad-fmcomms3-ebz

Directories in this repository:
===============================

analog_devices
 
  - Contains code and documents provides by Analog Devices

analog_devices -> core_repository

  - Contains two zip files:  

      - hdl-hdl_2014_r2.zip: A zip of the Analog Devices HDL Github repository 
                             (https://github.com/analogdevicesinc/hdl/tree/hdl_2014_r2).
                             Specifically, this file contains HDL projects for each relevant
                             Analog Devices' integrated circuit (e.g., the AD9361, AD9122,
                             AD9643, etc.) and FMCOMMS evaluation boards (FMCOMMS1, FMCOMMS2,
                             FMCOMMS3, FMCOMMS4, etc.) for select Xilinx (Zynq) and Altera 
                             platforms (e.g., Zed Board).

                             The Vivado projects contained in this snapshot are designed
                             specifically for Vivado 2014.2.  This is the most recent
                             snapshot available as of Spring 2015. Analog Devices tries to
                             stay current with the Xilinx and Altera toolchains. A snapshot
                             designed for Vivado 2015.1 is expected in late June 2015).

                             This capstone project originally evaluated the FMCOMMS1 board.
                             Integration proved very challenging.  We switched to the
                             FMCOMMS3 evaluation board very late in the quarter. The FMCOMMS3 
                             evaluation board uses the FMCOMMS2 project contained in this 
                             repository.  We found the FMCOMMS2/3 to be a much more straight-forward
                             integration effort, albeit with parts implemented directly in Verilog.

                             Therefore, depending upon whether you are using the FMCOMMS1 or FMCOMMS3 
                             evaluation board, use either the FMCOMMS1 or FMCOMMS2 project
                             from this repository.


      - no-OS-2014_R2.zip:   A zip of the Analog Devices ARM Processing System Github repository 
                             (https://github.com/analogdevicesinc/no-OS/tree/2014_R2).
                             Specifically, this file contains projects for each relevant
                             Analog Devices' integrated circuit and FMCOMMS evaluation boards. 

                             The Vivado projects contained in this snapshot are designed
                             specifically for Xilinx SDK 2014.2.  This is the most recent
                             snapshot available as of Spring 2015. Analog Devices tries to
                             stay current with the Xilinx and Altera toolchains. A snapshot
                             designed for Xilinx SDK 2015.1 is expected in late June 2015).

                             Depending on whether you are using the FMCOMMS1 or FMCOMMS3 evaluation
                             board, use either the 'fmcomms1' folder or the 'ad9361\sw' folder
                             from this repository

analog_devices -> docts

  -  Contains datasheets on the Analog Devices integrated circuits used on both the FMCOMMS1 and FMCOMMS3 boards.
     Also contains register mappings to be used when using these evaluation boards.  DO NOT USE THE REGISTER MAPS
     CONTAINED IN THE DATASHEETS!!!  The registers are remapped by the Analog Device hard cores in their HDL projects.
     This fact made creating a Vivado project from scratch rather than leveraging the Analog Devices example projects 
     a daunting proposition, given the ICs required a non-trivial setup process. The register maps in this folder are 
     taken from the Analog Device Wiki sites given at the top of this README file.

projects

  - In this folder are Simulink models, Vivado projects and Xilinx SDK projects related to both the FMCOMMS1 and FMCOMMS3 boards.

projects -> pl -> fmc3 -> cap02_preKaosTxIla

  - This is the primary current Vivado 2014.2 project (targets the FMCOMMS3 board)

projects -> pl -> kaos

  -  This folder contains Vivado projects for IP cores created and added into cap02_preKaosTxIla (targets the FMCOMMS3 board).

projects -> matlab -> final -> tx -> 2015_06_09 -> 10_brk05

  - This folder contains the latest Simulink for a QPSK transmitter.  It is a modified version of the MathWorks 
    QPSK Tx example.  It is the code used to create the projects/pl/kaos/kaos_qpsk_tx02 project to create a QPSK Tx IP core.