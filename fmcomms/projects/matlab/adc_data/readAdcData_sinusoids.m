clc; clear; close all;

signal = xlsread('sinusoids.xlsx');

sig1 = signal(1:4:end);
sig2 = signal(2:4:end);
sig3 = signal(3:4:end);
sig4 = signal(4:4:end);

%% Plot 1st sinusoid
subplot(4, 1, 1);
plot(sig1);
hold on;
subplot(4, 1, 1);
plot(sig2);
hold off;

subplot(4, 1, 2);
plot(sig2);

%% Plot 2nd sinusoid
subplot(4, 1, 3);
plot(sig3);
hold on;
subplot(4, 1, 3);
plot(sig4);
hold off;

subplot(4, 1, 4);
plot(sig4);