clc; clear; close all;

% signal = xlsread('stepUpStepDown.xlsx');
% 
% sig1 = signal(1:4:end);
% sig2 = signal(2:4:end);
% sig3 = signal(3:4:end);
% sig4 = signal(4:4:end);
% 
% %% Plot 1st sinusoid
% subplot(4, 1, 1);
% plot(sig1);
% % hold on;
% % subplot(4, 1, 1);
% % plot(sig2);
% % hold off;
% 
% %% Plot 2nd sinusoid
% subplot(4, 1, 2);
% plot(sig2);
% 
% %% Plot 3rd sinusoid
% subplot(4, 1, 3);
% plot(sig3);
% % hold on;
% % subplot(4, 1, 3);
% % plot(sig4);
% % hold off;
% 
% %% Plot 4th sinusoid
% subplot(4, 1, 4);
% plot(sig4);
% 
% min1   = min(sig1);
% max1   = max(sig1);
% range1 = max1 - min1;
% 
% min2   = min(sig2);
% max2   = max(sig2);
% range2 = max2 - min2;
% 
% min3   = min(sig3);
% max3   = max(sig3);
% range3 = max3 - min3;
% 
% min4   = min(sig4);
% max4   = max(sig4);
% range4 = max4 - min4;
% 
% fprintf(1, 'Range of signal 1 = %10d\n', range1);
% fprintf(1, 'Range of signal 2 = %10d\n', range2);
% fprintf(1, 'Range of signal 3 = %10d\n', range3);
% fprintf(1, 'Range of signal 4 = %10d\n', range4);
% 
% 
% xlswrite('stepUpStepDownSig1.xlsx', sig1);
% xlswrite('stepUpStepDownSig2.xlsx', sig2);
% xlswrite('stepUpStepDownSig3.xlsx', sig3);
% xlswrite('stepUpStepDownSig4.xlsx', sig4);
% 
% sineSig = xlsread('sineSig4.xlsx');
% figure;
% plot(sineSig);
% 

sig1 = xlsread('stepUpStepDownSig1.xlsx');
sig2 = xlsread('stepUpStepDownSig2.xlsx');
sig3 = xlsread('stepUpStepDownSig3.xlsx');
sig4 = xlsread('stepUpStepDownSig4.xlsx');

figure;

%% Plot 1st sinusoid
subplot(4, 1, 1);
plot(sig1);
% hold on;
% subplot(4, 1, 1);
% plot(sig2);
% hold off;

subplot(4, 1, 2);
plot(sig2);

%% Plot 2nd sinusoid
subplot(4, 1, 3);
plot(sig3);
% hold on;
% subplot(4, 1, 3);
% plot(sig4);
% hold off;

subplot(4, 1, 4);
plot(sig4);

