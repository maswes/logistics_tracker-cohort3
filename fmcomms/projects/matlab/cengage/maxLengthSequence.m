
pn      = [1;0;0;0;1;0;1;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;1;1;1;1;1;1;1;0;1;0;1;0;1;0;0;1;1;0;0;1;1;1;0;1;1;1;0;1;0;0;1;0;1;1;0;0;0;1;1;0;1;1;1;1;0;1;1;0;1;0;1;1;0;1;1;0;0;1;0;0;1;0;0;0;1;1;1;0;0;0;0;1;0;1;1;1;1;1;0;0;1;0;1;0;1;1;1;0;0;1;1;0;1;0;0;0;1;0;0;1;1;1];
pn      = 2 .* pn;
pn      = pn - 1;
pnLen   = length(pn);
pn      = pn';
pnFixed = [zeros(1, 126) pn zeros(1, 126)];
pnShift = [pn zeros(1, 252)];
outLen  = 2 * pnLen - 1;
out     = zeros(1, outLen);

for i = 1:outLen
    
    total   = sum(pnFixed .* pnShift);
    pnShift = [0 pnShift(1:end-1)];
    out(i)  = total;
end

plot(out);

