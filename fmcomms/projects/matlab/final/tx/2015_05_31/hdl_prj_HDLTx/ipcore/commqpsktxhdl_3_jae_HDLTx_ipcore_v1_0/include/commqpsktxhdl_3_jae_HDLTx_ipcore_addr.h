/*
 * File Name:         hdl_prj_HDLTx\ipcore\commqpsktxhdl_3_jae_HDLTx_ipcore_v1_0\include\commqpsktxhdl_3_jae_HDLTx_ipcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-31 20:07:35
*/

#ifndef COMMQPSKTXHDL_3_JAE_HDLTX_IPCORE_H_
#define COMMQPSKTXHDL_3_JAE_HDLTX_IPCORE_H_

#define  IPCore_Reset_commqpsktxhdl_3_jae_HDLTx_ipcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_commqpsktxhdl_3_jae_HDLTx_ipcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* COMMQPSKTXHDL_3_JAE_HDLTX_IPCORE_H_ */
