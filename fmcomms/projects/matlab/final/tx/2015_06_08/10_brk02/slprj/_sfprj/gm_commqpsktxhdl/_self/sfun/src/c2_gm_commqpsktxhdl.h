#ifndef __c2_gm_commqpsktxhdl_h__
#define __c2_gm_commqpsktxhdl_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_gm_commqpsktxhdlInstanceStruct
#define typedef_SFc2_gm_commqpsktxhdlInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  uint8_T c2_tp_Pack_Preamble0;
  uint8_T c2_tp_Pack_Preamble1;
  uint8_T c2_tp_Append_Data;
  boolean_T c2_isStable;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_gm_commqpsktxhdl;
  uint8_T c2_is_c2_gm_commqpsktxhdl;
  uint8_T c2_temporalCounter_i1;
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
  boolean_T *c2_load_data;
  uint8_T *c2_preamble_addr;
} SFc2_gm_commqpsktxhdlInstanceStruct;

#endif                                 /*typedef_SFc2_gm_commqpsktxhdlInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_gm_commqpsktxhdl_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c2_gm_commqpsktxhdl_get_check_sum(mxArray *plhs[]);
extern void c2_gm_commqpsktxhdl_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
