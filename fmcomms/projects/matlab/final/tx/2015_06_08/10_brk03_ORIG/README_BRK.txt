This folder, C:\capstone\projects\matlab\final\tx\2015_06_08\10_brk03_ORIG, contained
the same contents as the C:\capstone\projects\matlab\final\tx\2015_06_08\10_brk03
folder before the latter was modified.  

In other words, if you wanted to unwind the latter, you could copy the 
contents of the former into the latter.  
