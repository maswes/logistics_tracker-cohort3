/*
 * File Name:         hdl_prj\ipcore\commqpsktxhdl_ipcore_v1_0\include\commqpsktxhdl_ipcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-09 14:26:13
*/

#ifndef COMMQPSKTXHDL_IPCORE_H_
#define COMMQPSKTXHDL_IPCORE_H_

#define  IPCore_Reset_commqpsktxhdl_ipcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_commqpsktxhdl_ipcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* COMMQPSKTXHDL_IPCORE_H_ */
