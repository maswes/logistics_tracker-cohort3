/*
 * File Name:         kaos_tx_9361_ifc_hdl_prj\ipcore\kaos_tx_9361_ifc_ipcore_v1_0\include\kaos_tx_9361_ifc_ipcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-28 01:39:46
*/

#ifndef KAOS_TX_9361_IFC_IPCORE_H_
#define KAOS_TX_9361_IFC_IPCORE_H_

#define  IPCore_Reset_kaos_tx_9361_ifc_ipcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_kaos_tx_9361_ifc_ipcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* KAOS_TX_9361_IFC_IPCORE_H_ */
