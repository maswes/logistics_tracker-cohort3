/* Include files */

#include <stddef.h>
#include "blas.h"
#include "sdrzfreqcalibFMC23_cgxe.h"
#include "m_Hc1X60vKnc326iDURq7NcH.h"
#include <string.h>
#include "mwmathutil.h"

/* Type Definitions */

/* Named Constants */
#define BasebandSampleRate             (245760.0)
#define EnableBurstMode                (false)
#define LostSamplesOutputPort          (true)
#define BypassUserLogic                (true)

/* Variable Declarations */

/* Variable Definitions */
static emlrtMCInfo emlrtMCI = { 20, 34, "eml_error",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_error.m"
};

static emlrtMCInfo b_emlrtMCI = { 171, 28, "validateattributes",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"
};

static emlrtMCInfo c_emlrtMCI = { 162, 28, "validateattributes",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"
};

static emlrtMCInfo d_emlrtMCI = { 322, 28, "validateattributes",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"
};

static emlrtMCInfo e_emlrtMCI = { 288, 28, "validateattributes",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"
};

static emlrtMCInfo f_emlrtMCI = { 16, 13, "eml_warning",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_warning.m"
};

static emlrtMCInfo g_emlrtMCI = { 16, 5, "eml_warning",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_warning.m"
};

static emlrtMCInfo h_emlrtMCI = { 1, 1, "SystemCore",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\shared\\system\\coder\\+matlab\\+system\\+coder\\SystemCore.p"
};

static emlrtMCInfo i_emlrtMCI = { 1, 1, "SDRSystemBase",
  "C:\\MATLAB\\SupportPackages\\R2014b\\sdrpluginbase\\toolbox\\shared\\sdr\\sdrplug\\sdrpluginbase\\host\\+comm\\+internal\\SDRSystemBase.p"
};

static emlrtMCInfo j_emlrtMCI = { 16, 1, "error",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\error.m" };

static emlrtMCInfo k_emlrtMCI = { 13, 17, "error",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\lang\\error.m" };

static emlrtMCInfo l_emlrtMCI = { 28, 19, "assert",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\assert.m"
};

static emlrtMCInfo m_emlrtMCI = { 87, 9, "eml_int_forloop_overflow_check",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"
};

static emlrtMCInfo n_emlrtMCI = { 86, 15, "eml_int_forloop_overflow_check",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"
};

static emlrtECInfo emlrtECI = { 2, 4, 1, "", "" };

static emlrtECInfo b_emlrtECI = { 2, 17, 1, "", "" };

static emlrtECInfo c_emlrtECI = { 2, 18, 1, "", "" };

static emlrtECInfo d_emlrtECI = { 2, 19, 1, "", "" };

static emlrtECInfo e_emlrtECI = { 2, 20, 1, "", "" };

static emlrtECInfo f_emlrtECI = { 2, 21, 1, "", "" };

static emlrtECInfo g_emlrtECI = { 2, 22, 1, "", "" };

static emlrtECInfo h_emlrtECI = { 2, 23, 1, "", "" };

static emlrtECInfo i_emlrtECI = { 2, 24, 1, "", "" };

static emlrtBCInfo emlrtBCI = { 0, 255, 17, 9, "char", "char",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\strfun\\char.m",
  2 };

static emlrtBCInfo b_emlrtBCI = { 1, 1024, 1, 1, "", "SDRSystemBase",
  "C:\\MATLAB\\SupportPackages\\R2014b\\sdrpluginbase\\toolbox\\shared\\sdr\\sdrplug\\sdrpluginbase\\host\\+comm\\+internal\\SDRSystemBase.p",
  0 };

static emlrtECInfo j_emlrtECI = { -1, 1, 1, "SDRSystemBase",
  "C:\\MATLAB\\SupportPackages\\R2014b\\sdrpluginbase\\toolbox\\shared\\sdr\\sdrplug\\sdrpluginbase\\host\\+comm\\+internal\\SDRSystemBase.p"
};

static emlrtBCInfo c_emlrtBCI = { 1, 1024, 1, 1, "", "SDRTxZynqFMC23Base",
  "C:\\MATLAB\\SupportPackages\\R2014b\\xilinxzynqbasedradio\\toolbox\\shared\\sdr\\sdrz\\sdrz\\+comm\\+internal\\SDRTxZynqFMC23Base.p",
  0 };

static emlrtBCInfo d_emlrtBCI = { MIN_int32_T, MAX_int32_T, 1, 1, "",
  "SDRTxZynqFMC23Base",
  "C:\\MATLAB\\SupportPackages\\R2014b\\xilinxzynqbasedradio\\toolbox\\shared\\sdr\\sdrz\\sdrz\\+comm\\+internal\\SDRTxZynqFMC23Base.p",
  0 };

static emlrtBCInfo e_emlrtBCI = { 1, 5120, 1, 1, "", "SDRTxZynqFMC23Base",
  "C:\\MATLAB\\SupportPackages\\R2014b\\xilinxzynqbasedradio\\toolbox\\shared\\sdr\\sdrz\\sdrz\\+comm\\+internal\\SDRTxZynqFMC23Base.p",
  0 };

static emlrtBCInfo f_emlrtBCI = { -1, -1, 1, 1, "", "PUP",
  "C:\\MATLAB\\SupportPackages\\R2014b\\sdrpluginbase\\toolbox\\shared\\sdr\\sdrplug\\sdrpluginbase\\host\\+sdrplugin\\PUP.p",
  0 };

static emlrtECInfo k_emlrtECI = { 1, -1, -1, "", "" };

static emlrtECInfo l_emlrtECI = { 2, -1, -1, "", "" };

/* Function Declarations */
static void mw__internal__call__autoinference(real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_dimModes, real_T
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_data[], int32_T
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_sizes[2], real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_dType, real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_complexity, real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed, char_T
  infoCache_RestoreInfo_DispatcherInfo_objTypeName[31], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize, sIvmHumfM4VG8K4LjAjoqqB
  infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_data[8],
  sIvmHumfM4VG8K4LjAjoqqB_size
  infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes[8], char_T
  infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[22], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex, real_T
  infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[], int32_T
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[2], real_T
  infoCache_RestoreInfo_cgxeChksum[4], s7UBIGHSehQY1gCsIQWwr5C
  infoCache_VerificationInfo_checksums[4], real_T
  infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4], char_T
  infoCache_slVer[3]);
static comm_internal_SDRTxZC706FMC23SL *SDRTxZC706FMC23SL_SDRTxZC706FMC23SL
  (InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance,
   comm_internal_SDRTxZC706FMC23SL *obj);
static void b_rand(InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance, real_T
                   r[10]);
static void eml_error(void);
static void SDRTxZynqFMC23Base_set_CenterFrequency
  (comm_internal_SDRTxZC706FMC23SL *obj, real_T b_val);
static void SDRTxZynqFMC23Base_set_Attenuation(comm_internal_SDRTxZC706FMC23SL
  *obj, real_T b_val[2]);
static void SDRSystemBase_setupImpl(comm_internal_SDRTxZC706FMC23SL *obj);
static void SDRTxZynqFMC23Base_initStaticProps(comm_internal_SDRTxZC706FMC23SL
  *obj);
static void SDRSystemBase_packPropertyListLocal(comm_internal_SDRTxZC706FMC23SL *
  obj, int32_T *psize, uint8_T pbytes[1024]);
static real_T eml_switch_helper(char_T expr_data[], int32_T expr_sizes[2]);
static void SDRTxZynqFMC23Base_packCreationGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes[1024]);
static void SDRTxZynqFMC23Base_packProperty(comm_internal_SDRTxZC706FMC23SL *obj,
  char_T what_data[], int32_T what_sizes[2], int32_T *psize, uint8_T
  pbytes_data[], int32_T *pbytes_sizes);
static int32_T SDRTxZynqFMC23Base_getIntEncoding(comm_internal_SDRTxZC706FMC23SL
  *obj, char_T what_data[], int32_T what_sizes[2]);
static real_T b_eml_switch_helper(char_T expr_data[], int32_T expr_sizes[2]);
static boolean_T eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T b_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T c_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T d_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T e_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T f_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T g_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T h_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T i_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T j_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T k_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T l_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static boolean_T m_eml_strcmp(char_T a_data[], int32_T a_sizes[2]);
static void PUP_packProperty(int32_T intenc, uint8_T packedVal[16], uint8_T
  pbytes[24]);
static real_T SDRTxZynqFMC23Base_enumToInt(comm_internal_SDRTxZC706FMC23SL *obj,
  char_T what_data[], int32_T what_sizes[2]);
static void SDRTxZynqFMC23Base_packNontunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes_data[],
   int32_T *pbytes_sizes);
static void SDRSystemBase_packPropertyList(comm_internal_SDRTxZC706FMC23SL *obj,
  int32_T *psize, uint8_T pbytes[1024]);
static void deblank(char_T x[15], char_T y_data[], int32_T y_sizes[2]);
static void b_SDRSystemBase_packPropertyList(comm_internal_SDRTxZC706FMC23SL
  *obj, int32_T *psize, uint8_T pbytes[1024]);
static void b_deblank(char_T x[21], char_T y_data[], int32_T y_sizes[2]);
static void SDRTxZynqFMC23Base_packTunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes[1024]);
static void sdr_setupImpl(int32_T creationArgsSize, uint8_T creationArgs_data[],
  int32_T creationArgs_sizes, int32_T nonTunablePropsSize, uint8_T
  nonTunableProps_data[], int32_T nonTunableProps_sizes, int32_T
  tunablePropsSize, uint8_T tunableProps[1024], int32_T *driverHandle,
  SDRPluginStatusT *errStat, char_T errId[1024], char_T errStr[1024]);
static void SDRTxZynqFMC23Base_setNontunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj);
static void SDRSystemBase_setPropertyList(comm_internal_SDRTxZC706FMC23SL *obj);
static void SDRTxZynqFMC23Base_setTunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj);
static boolean_T SDRTxZynqFMC23SL_stepImpl(comm_internal_SDRTxZC706FMC23SL *obj,
  creal_T d[8192]);
static void SDRSystemBase_updateTunable(comm_internal_SDRTxZC706FMC23SL *obj);
static boolean_T all(boolean_T x_data[], int32_T x_sizes);
static void sdr_txStepImplDouble(int32_T driverHandle, int32_T dataSize, real_T
  data[16384], int32_T *metaDataSize, uint8_T metaData[5120], SDRPluginStatusT
  *errStat, char_T errId[1024], char_T errStr[1024]);
static void SDRSystemBase_releaseImpl(comm_internal_SDRTxZC706FMC23SL *obj);
static boolean_T SDRSystemBase_isConnected(comm_internal_SDRTxZC706FMC23SL *obj);
static void cast(b_sIvmHumfM4VG8K4LjAjoqqB x[8], sIvmHumfM4VG8K4LjAjoqqB y_data
                 [8], sIvmHumfM4VG8K4LjAjoqqB_size y_elems_sizes[8]);
static void cgxe_mdl_start(InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance);
static void cgxe_mdl_initialize(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance);
static void cgxe_mdl_outputs(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance);
static void cgxe_mdl_update(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance);
static void cgxe_mdl_terminate(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance);
static const mxArray *mw__internal__name__resolution__fcn(void);
static void info_helper(const mxArray **info);
static const mxArray *emlrt_marshallOut(const char * u);
static const mxArray *b_emlrt_marshallOut(const uint32_T u);
static void b_info_helper(const mxArray **info);
static void c_info_helper(const mxArray **info);
static void d_info_helper(const mxArray **info);
static const mxArray *mw__internal__autoInference__fcn(void);
static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2]);
static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance);
static uint32_T emlrt_marshallIn(const mxArray *b_method, const char_T
  *identifier);
static uint32_T b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static boolean_T c_emlrt_marshallIn(const mxArray *b_method_not_empty, const
  char_T *identifier);
static boolean_T d_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static uint32_T e_emlrt_marshallIn(const mxArray *d_state, const char_T
  *identifier);
static uint32_T f_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static void g_emlrt_marshallIn(const mxArray *d_state, const char_T *identifier,
  uint32_T y[625]);
static void h_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint32_T y[625]);
static void i_emlrt_marshallIn(const mxArray *d_state, const char_T *identifier,
  uint32_T y[2]);
static void j_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint32_T y[2]);
static void k_emlrt_marshallIn(const mxArray *b_sysobj, const char_T *identifier,
  comm_internal_SDRTxZC706FMC23SL *y);
static void l_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, comm_internal_SDRTxZC706FMC23SL *y);
static void m_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[11]);
static void n_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[14]);
static int32_T o_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static void p_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[1024]);
static void q_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[6]);
static real_T r_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static void s_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[2]);
static void t_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[256]);
static void u_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[12]);
static void v_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[13]);
static void w_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[22]);
static void mw__internal__setSimState__fcn(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance, const mxArray *st);
static const mxArray *message(const mxArray *b, emlrtMCInfo *location);
static void error(const mxArray *b, emlrtMCInfo *location);
static void b_error(const mxArray *b, const mxArray *c, emlrtMCInfo *location);
static void warning(const mxArray *b, emlrtMCInfo *location);
static const mxArray *b_message(const mxArray *b, const mxArray *c, emlrtMCInfo *
  location);
static void sdrplugin_internal_privsetupsession(const mxArray *b, emlrtMCInfo
  *location);
static void isempty(const mxArray *b, emlrtMCInfo *location);
static uint32_T x_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId);
static boolean_T y_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static void ab_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint32_T ret[625]);
static void bb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint32_T ret[2]);
static void cb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[11]);
static void db_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[14]);
static int32_T eb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId);
static void fb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[1024]);
static void gb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[6]);
static real_T hb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static void ib_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[2]);
static void jb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[256]);
static void kb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[12]);
static void lb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[13]);
static void mb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[22]);
static real_T eml_rand_mt19937ar(uint32_T d_state[625]);

/* Function Definitions */
static void mw__internal__call__autoinference(real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_dimModes, real_T
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_data[], int32_T
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_sizes[2], real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_dType, real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_complexity, real_T
  *infoCache_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed, char_T
  infoCache_RestoreInfo_DispatcherInfo_objTypeName[31], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize, sIvmHumfM4VG8K4LjAjoqqB
  infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_data[8],
  sIvmHumfM4VG8K4LjAjoqqB_size
  infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes[8], char_T
  infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[22], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex, real_T
  infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[], int32_T
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[2], real_T
  infoCache_RestoreInfo_cgxeChksum[4], s7UBIGHSehQY1gCsIQWwr5C
  infoCache_VerificationInfo_checksums[4], real_T
  infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4], char_T
  infoCache_slVer[3])
{
  int8_T Ports_dims_data[4];
  int32_T i0;
  static int8_T iv0[4] = { 1, 2, 1, 1 };

  sIvmHumfM4VG8K4LjAjoqqB_size unusedOutput_elems_sizes[8];
  sIvmHumfM4VG8K4LjAjoqqB unusedOutput_data[8];
  static b_sIvmHumfM4VG8K4LjAjoqqB rv0[8] = { { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0,
      'a', 0.0, 'a', 0.0 }, { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0, 'a', 0.0, 'a',
      0.0 }, { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0, 'a', 0.0, 'a', 0.0 }, { 'a', {
        0.0, 1.0, 2.0 }, 0.0, 0.0, 'a', 0.0, 'a', 0.0 }, { 'a', { 0.0, 1.0, 2.0
      }, 0.0, 0.0, 'a', 0.0, 'a', 0.0 }, { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0, 'a',
      0.0, 'a', 0.0 }, { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0, 'a', 0.0, 'a', 0.0 },
    { 'a', { 0.0, 1.0, 2.0 }, 0.0, 0.0, 'a', 0.0, 'a', 0.0 } };

  sIvmHumfM4VG8K4LjAjoqqB_size persisVarDWork_elems_sizes[8];
  static char_T cv0[5] = { 's', 't', 'a', 't', 'e' };

  sIvmHumfM4VG8K4LjAjoqqB persisVarDWork_data[8];
  static char_T cv1[22] = { 'W', 'C', 'H', 'P', 'o', '3', 'U', 'R', 'm', 'p',
    'r', 'x', 'p', 'U', '0', 'r', 'p', 'G', '3', 'D', 'E', 'D' };

  static char_T cv2[15] = { 's', 't', 'a', 't', 'e', '_', 'n', 'o', 't', '_',
    'e', 'm', 'p', 't', 'y' };

  static char_T cv3[22] = { '8', 'O', '7', 'o', 'w', 'q', '0', 'e', 'X', 'Z',
    'r', 'l', '3', 'k', '2', 'X', 'V', 'X', 'L', 'k', 'a', 'D' };

  static int8_T iv1[4] = { 2, 2, 2, 1 };

  static char_T cv4[22] = { '1', 'S', 'J', 'f', 'U', 'z', 'x', 'k', 'k', '6',
    '7', 'I', 'u', 'C', 'X', '1', 'z', 'u', 'p', 'Q', 'g', 'G' };

  static int16_T iv2[4] = { 625, 2, 625, 1 };

  static char_T cv5[22] = { 'X', 'L', 'X', 'c', 'K', 'r', 'V', 'W', 'E', 'T',
    'd', 'l', 'O', 'n', 'p', '6', 'F', 'X', 'O', 'x', 'D', 'C' };

  static char_T cv6[6] = { 'm', 'e', 't', 'h', 'o', 'd' };

  static char_T cv7[16] = { 'm', 'e', 't', 'h', 'o', 'd', '_', 'n', 'o', 't',
    '_', 'e', 'm', 'p', 't', 'y' };

  int8_T t2_Ports_dims_data[4];
  char_T t2_objTypeName[31];
  static char_T cv8[31] = { 'c', 'o', 'm', 'm', '_', 'i', 'n', 't', 'e', 'r',
    'n', 'a', 'l', '_', 'S', 'D', 'R', 'T', 'x', 'Z', 'C', '7', '0', '6', 'F',
    'M', 'C', '2', '3', 'S', 'L' };

  char_T t2_sysObjChksum[22];
  static char_T cv9[22] = { 'P', '8', 'p', 'b', 'o', 'c', 'd', 'D', 'U', '4',
    'h', 'g', '5', '7', 'e', 'w', 'Y', 'M', 'V', 'R', 'v', 'F' };

  int8_T t1_DispatcherInfo_Ports_dims_data[4];
  char_T t1_DispatcherInfo_objTypeName[31];
  char_T t1_DispatcherInfo_sysObjChksum[22];
  uint32_T t1_cgxeChksum[4];
  static uint32_T uv0[4] = { 2131908541U, 3223019714U, 1952094914U, 3844393129U
  };

  static uint32_T t3_chksum[4] = { 2736638490U, 3060638183U, 1603028534U,
    4102735264U };

  s7UBIGHSehQY1gCsIQWwr5C checksums[4];
  static int32_T t4_chksum[4] = { 725019261, 9350207, 1380543125, 1962202275 };

  static uint32_T t5_chksum[4] = { 2329432643U, 1966713464U, 3591173899U,
    2781510642U };

  static uint32_T t6_chksum[4] = { 1865002805U, 2764452104U, 3024785176U,
    1498073510U };

  s7UBIGHSehQY1gCsIQWwr5C t0_checksums[4];
  uint32_T b_t6_chksum[4];
  static uint32_T t8_codeGenChksum[4] = { 907274058U, 3220786148U, 779473529U,
    2769831971U };

  static char_T cv10[3] = { '8', '.', '4' };

  emlrtDimSizeGeqCheckFastR2012b(4, 3, &emlrtECI, emlrtRootTLSGlobal);
  for (i0 = 0; i0 < 4; i0++) {
    Ports_dims_data[i0] = iv0[i0];
  }

  cast(rv0, unusedOutput_data, unusedOutput_elems_sizes);
  emlrtDimSizeGeqCheckFastR2012b(16, 5, &b_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[0].names[0] = 1;
  persisVarDWork_elems_sizes[0].names[1] = 5;
  for (i0 = 0; i0 < 5; i0++) {
    persisVarDWork_data[0].names[i0] = cv0[i0];
  }

  persisVarDWork_elems_sizes[0].dims[0] = 1;
  persisVarDWork_elems_sizes[0].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[0].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[0].dType = 7.0;
  persisVarDWork_data[0].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &b_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[0].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[0].dTypeName[1] = 0;
  persisVarDWork_data[0].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[0].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[0].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[0].dTypeChksum[i0] = cv1[i0];
  }

  persisVarDWork_data[0].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 15, &c_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[1].names[0] = 1;
  persisVarDWork_elems_sizes[1].names[1] = 15;
  for (i0 = 0; i0 < 15; i0++) {
    persisVarDWork_data[1].names[i0] = cv2[i0];
  }

  persisVarDWork_elems_sizes[1].dims[0] = 1;
  persisVarDWork_elems_sizes[1].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[1].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[1].dType = 8.0;
  persisVarDWork_data[1].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &c_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[1].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[1].dTypeName[1] = 0;
  persisVarDWork_data[1].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[1].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[1].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[1].dTypeChksum[i0] = cv3[i0];
  }

  persisVarDWork_data[1].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 5, &d_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[2].names[0] = 1;
  persisVarDWork_elems_sizes[2].names[1] = 5;
  for (i0 = 0; i0 < 5; i0++) {
    persisVarDWork_data[2].names[i0] = cv0[i0];
  }

  persisVarDWork_elems_sizes[2].dims[0] = 1;
  persisVarDWork_elems_sizes[2].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[2].dims[i0] = (real_T)iv1[i0];
  }

  persisVarDWork_data[2].dType = 7.0;
  persisVarDWork_data[2].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &d_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[2].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[2].dTypeName[1] = 0;
  persisVarDWork_data[2].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[2].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[2].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[2].dTypeChksum[i0] = cv4[i0];
  }

  persisVarDWork_data[2].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 15, &e_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[3].names[0] = 1;
  persisVarDWork_elems_sizes[3].names[1] = 15;
  for (i0 = 0; i0 < 15; i0++) {
    persisVarDWork_data[3].names[i0] = cv2[i0];
  }

  persisVarDWork_elems_sizes[3].dims[0] = 1;
  persisVarDWork_elems_sizes[3].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[3].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[3].dType = 8.0;
  persisVarDWork_data[3].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &e_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[3].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[3].dTypeName[1] = 0;
  persisVarDWork_data[3].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[3].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[3].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[3].dTypeChksum[i0] = cv3[i0];
  }

  persisVarDWork_data[3].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 5, &f_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[4].names[0] = 1;
  persisVarDWork_elems_sizes[4].names[1] = 5;
  for (i0 = 0; i0 < 5; i0++) {
    persisVarDWork_data[4].names[i0] = cv0[i0];
  }

  persisVarDWork_elems_sizes[4].dims[0] = 1;
  persisVarDWork_elems_sizes[4].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[4].dims[i0] = (real_T)iv2[i0];
  }

  persisVarDWork_data[4].dType = 7.0;
  persisVarDWork_data[4].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &f_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[4].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[4].dTypeName[1] = 0;
  persisVarDWork_data[4].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[4].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[4].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[4].dTypeChksum[i0] = cv5[i0];
  }

  persisVarDWork_data[4].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 15, &g_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[5].names[0] = 1;
  persisVarDWork_elems_sizes[5].names[1] = 15;
  for (i0 = 0; i0 < 15; i0++) {
    persisVarDWork_data[5].names[i0] = cv2[i0];
  }

  persisVarDWork_elems_sizes[5].dims[0] = 1;
  persisVarDWork_elems_sizes[5].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[5].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[5].dType = 8.0;
  persisVarDWork_data[5].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &g_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[5].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[5].dTypeName[1] = 0;
  persisVarDWork_data[5].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[5].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[5].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[5].dTypeChksum[i0] = cv3[i0];
  }

  persisVarDWork_data[5].complexity = 0.0;
  emlrtDimSizeGeqCheckFastR2012b(16, 6, &h_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[6].names[0] = 1;
  persisVarDWork_elems_sizes[6].names[1] = 6;
  for (i0 = 0; i0 < 6; i0++) {
    persisVarDWork_data[6].names[i0] = cv6[i0];
  }

  persisVarDWork_elems_sizes[6].dims[0] = 1;
  persisVarDWork_elems_sizes[6].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[6].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[6].dType = 7.0;
  persisVarDWork_data[6].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &h_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[6].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[6].dTypeName[1] = 0;
  persisVarDWork_data[6].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[6].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[6].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[6].dTypeChksum[i0] = cv1[i0];
  }

  persisVarDWork_data[6].complexity = 0.0;
  persisVarDWork_elems_sizes[7].names[0] = 1;
  persisVarDWork_elems_sizes[7].names[1] = 16;
  for (i0 = 0; i0 < 16; i0++) {
    persisVarDWork_data[7].names[i0] = cv7[i0];
  }

  persisVarDWork_elems_sizes[7].dims[0] = 1;
  persisVarDWork_elems_sizes[7].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    persisVarDWork_data[7].dims[i0] = (real_T)iv0[i0];
  }

  persisVarDWork_data[7].dType = 8.0;
  persisVarDWork_data[7].dTypeSize = -1.0;
  emlrtDimSizeGeqCheckFastR2012b(1, 0, &i_emlrtECI, emlrtRootTLSGlobal);
  persisVarDWork_elems_sizes[7].dTypeName[0] = 1;
  persisVarDWork_elems_sizes[7].dTypeName[1] = 0;
  persisVarDWork_data[7].dTypeIndex = 0.0;
  persisVarDWork_elems_sizes[7].dTypeChksum[0] = 1;
  persisVarDWork_elems_sizes[7].dTypeChksum[1] = 22;
  for (i0 = 0; i0 < 22; i0++) {
    persisVarDWork_data[7].dTypeChksum[i0] = cv3[i0];
  }

  persisVarDWork_data[7].complexity = 0.0;
  for (i0 = 0; i0 < 4; i0++) {
    t2_Ports_dims_data[i0] = Ports_dims_data[i0];
  }

  for (i0 = 0; i0 < 31; i0++) {
    t2_objTypeName[i0] = cv8[i0];
  }

  for (i0 = 0; i0 < 8; i0++) {
    unusedOutput_elems_sizes[i0] = persisVarDWork_elems_sizes[i0];
    unusedOutput_data[i0] = persisVarDWork_data[i0];
  }

  for (i0 = 0; i0 < 22; i0++) {
    t2_sysObjChksum[i0] = cv9[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t1_DispatcherInfo_Ports_dims_data[i0] = t2_Ports_dims_data[i0];
  }

  for (i0 = 0; i0 < 31; i0++) {
    t1_DispatcherInfo_objTypeName[i0] = t2_objTypeName[i0];
  }

  for (i0 = 0; i0 < 8; i0++) {
    persisVarDWork_elems_sizes[i0] = unusedOutput_elems_sizes[i0];
    persisVarDWork_data[i0] = unusedOutput_data[i0];
  }

  for (i0 = 0; i0 < 22; i0++) {
    t1_DispatcherInfo_sysObjChksum[i0] = t2_sysObjChksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t1_cgxeChksum[i0] = uv0[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[0].chksum[i0] = (real_T)t3_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[1].chksum[i0] = (real_T)t4_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[2].chksum[i0] = (real_T)t5_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[3].chksum[i0] = (real_T)t6_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t0_checksums[i0] = checksums[i0];
    b_t6_chksum[i0] = t8_codeGenChksum[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_Ports_dimModes = 0.0;
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_sizes[0] = 1;
  infoCache_RestoreInfo_DispatcherInfo_Ports_dims_sizes[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_Ports_dims_data[i0] = (real_T)
      t1_DispatcherInfo_Ports_dims_data[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_Ports_dType = 8.0;
  *infoCache_RestoreInfo_DispatcherInfo_Ports_complexity = 0.0;
  *infoCache_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed = 0.0;
  for (i0 = 0; i0 < 31; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_objTypeName[i0] =
      t1_DispatcherInfo_objTypeName[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize = 3400.0;
  for (i0 = 0; i0 < 8; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes[i0] =
      persisVarDWork_elems_sizes[i0];
    infoCache_RestoreInfo_DispatcherInfo_persisVarDWork_data[i0] =
      persisVarDWork_data[i0];
  }

  for (i0 = 0; i0 < 22; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[i0] =
      t1_DispatcherInfo_sysObjChksum[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex = 1.0;
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[0] = 1;
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[1] = 1;
  infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[0] = 0.0;
  for (i0 = 0; i0 < 4; i0++) {
    infoCache_RestoreInfo_cgxeChksum[i0] = (real_T)t1_cgxeChksum[i0];
    infoCache_VerificationInfo_checksums[i0] = t0_checksums[i0];
    infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[i0] = (real_T)
      b_t6_chksum[i0];
  }

  for (i0 = 0; i0 < 3; i0++) {
    infoCache_slVer[i0] = cv10[i0];
  }
}

static comm_internal_SDRTxZC706FMC23SL *SDRTxZC706FMC23SL_SDRTxZC706FMC23SL
  (InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance,
   comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  comm_internal_SDRTxZC706FMC23SL *c_obj;
  int32_T k;
  static int16_T iv3[256] = { 111, 382, -87, -200, 147, 711, -127, -41, -18, 645,
    -286, 36, -312, 346, -555, -194, -601, -119, -722, -543, -642, -472, -555,
    -646, -303, -404, -66, -312, 221, 45, 393, 197, 488, 413, 403, 346, 219, 241,
    -74, -93, -343, -373, -550, -680, -582, -755, -450, -689, -146, -356, 221,
    51, 569, 515, 759, 795, 735, 870, 463, 609, 20, 136, -487, -488, -885, -1009,
    -1048, -1303, -883, -1191, -418, -712, 243, 69, 897, 885, 1340, 1516, 1394,
    1692, 1002, 1336, 228, 468, -715, -662, -1549, -1740, -1980, -2378, -1819,
    -2329, -1028, -1494, 217, -55, 1583, 1620, 2635, 2995, 2980, 3584, 2377,
    3045, 853, 1368, -1275, -1121, -3435, -3755, -4926, -5708, -5080, -6156,
    -3454, -4542, 40, -710, 5065, 4969, 10903, 11669, 16581, 18243, 21085, 23486,
    23573, 26391, 23573, 26391, 21085, 23486, 16581, 18243, 10903, 11669, 5065,
    4969, 40, -710, -3454, -4542, -5080, -6156, -4926, -5708, -3435, -3755,
    -1275, -1121, 853, 1368, 2377, 3045, 2980, 3584, 2635, 2995, 1583, 1620, 217,
    -55, -1028, -1494, -1819, -2329, -1980, -2378, -1549, -1740, -715, -662, 228,
    468, 1002, 1336, 1394, 1692, 1340, 1516, 897, 885, 243, 69, -418, -712, -883,
    -1191, -1048, -1303, -885, -1009, -487, -488, 20, 136, 463, 609, 735, 870,
    759, 795, 569, 515, 221, 51, -146, -356, -450, -689, -582, -755, -550, -680,
    -343, -373, -74, -93, 219, 241, 403, 346, 488, 413, 393, 197, 221, 45, -66,
    -312, -303, -404, -555, -646, -642, -472, -722, -543, -601, -119, -555, -194,
    -312, 346, -286, 36, -18, 645, -127, -41, 147, 711, -87, -200, 111, 382 };

  static int32_T iv4[12] = { 737280000, 737280000, 184320000, 184320000,
    61440000, 61440000, 30720000, 30720000, 15360000, 15360000, 3840000, 3840000
  };

  static char_T cv11[13] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i', 'l',
    't', 'e', 'r' };

  static char_T cv12[22] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i', 'l',
    't', 'e', 'r', 'F', 'r', 'o', 'm', 'O', 't', 'h', 'e', 'r' };

  real_T r[10];
  static char_T cv13[4] = { 'S', 'D', 'R', '_' };

  real_T d0;
  b_obj = obj;

  /*  SDRTxZC706FMC23SL Internal-use object */
  /*  Copyright 2014, The MathWorks, Inc. */
  c_obj = b_obj;
  c_obj->DataIsComplex = true;
  c_obj->NumChannels = 2.0;
  c_obj->CenterFrequency = 2.4E+9;
  for (k = 0; k < 2; k++) {
    c_obj->Attenuation[k] = 10000.0;
  }

  for (k = 0; k < 2; k++) {
    c_obj->FIRCoefficientSize[k] = 128.0;
  }

  for (k = 0; k < 256; k++) {
    c_obj->FIRCoefficients[k] = (real_T)iv3[k];
  }

  for (k = 0; k < 2; k++) {
    c_obj->FIRGain[k] = -12.0 * (real_T)k;
  }

  for (k = 0; k < 2; k++) {
    c_obj->FIRDecimInterpFactor[k] = 4.0;
  }

  for (k = 0; k < 2; k++) {
    c_obj->AnalogFilterCutoff[k] = 2.56E+6;
  }

  for (k = 0; k < 12; k++) {
    c_obj->FilterPathRates[k] = (real_T)iv4[k];
  }

  for (k = 0; k < 13; k++) {
    c_obj->FilterDesignTypeForTx[k] = cv11[k];
  }

  for (k = 0; k < 22; k++) {
    c_obj->FilterDesignTypeForRx[k] = cv12[k];
  }

  c_obj->pLastTunablePackedSize = 0;
  for (k = 0; k < 1024; k++) {
    c_obj->pLastTunablePackedBuffer[k] = 0;
  }

  c_obj->isInitialized = false;
  c_obj->isReleased = false;
  b_rand(moduleInstance, r);
  for (k = 0; k < 10; k++) {
    r[k] = 48.0 + muDoubleScalarFloor(r[k] * 10.0);
  }

  for (k = 0; k < 10; k++) {
    emlrtDynamicBoundsCheckFastR2012b((int32_T)r[k], 0, 255, &emlrtBCI,
      emlrtRootTLSGlobal);
  }

  for (k = 0; k < 4; k++) {
    c_obj->prequester[k] = cv13[k];
  }

  for (k = 0; k < 10; k++) {
    d0 = muDoubleScalarFloor(r[k]);
    if (muDoubleScalarIsNaN(d0) || muDoubleScalarIsInf(d0)) {
      d0 = 0.0;
    } else {
      d0 = muDoubleScalarRem(d0, 256.0);
    }

    if (d0 < 0.0) {
      c_obj->prequester[k + 4] = (char_T)(int8_T)-(int8_T)(uint8_T)-d0;
    } else {
      c_obj->prequester[k + 4] = (char_T)(int8_T)(uint8_T)d0;
    }
  }

  c_obj->pCenterFrequency = c_obj->CenterFrequency;
  for (k = 0; k < 2; k++) {
    c_obj->pAttenuation[k] = c_obj->Attenuation[k];
  }

  return b_obj;
}

static void b_rand(InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance, real_T
                   r[10])
{
  int32_T k;
  real_T d1;
  for (k = 0; k < 10; k++) {
    d1 = eml_rand_mt19937ar(moduleInstance->c_state);
    r[k] = d1;
  }
}

static void eml_error(void)
{
  const mxArray *y;
  static const int32_T iv5[2] = { 1, 37 };

  const mxArray *m0;
  char_T cv14[37];
  int32_T i1;
  static char_T cv15[37] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T', 'L',
    'A', 'B', ':', 'r', 'a', 'n', 'd', '_', 'i', 'n', 'v', 'a', 'l', 'i', 'd',
    'T', 'w', 'i', 's', 't', 'e', 'r', 'S', 't', 'a', 't', 'e' };

  y = NULL;
  m0 = emlrtCreateCharArray(2, iv5);
  for (i1 = 0; i1 < 37; i1++) {
    cv14[i1] = cv15[i1];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 37, m0, cv14);
  emlrtAssign(&y, m0);
  error(message(y, &emlrtMCI), &emlrtMCI);
}

static void SDRTxZynqFMC23Base_set_CenterFrequency
  (comm_internal_SDRTxZC706FMC23SL *obj, real_T b_val)
{
  boolean_T p;
  const mxArray *y;
  static const int32_T iv6[2] = { 1, 52 };

  const mxArray *m1;
  char_T cv16[52];
  int32_T i2;
  static char_T cv17[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'N', 'a', 'N' };

  const mxArray *b_y;
  static const int32_T iv7[2] = { 1, 39 };

  char_T cv18[39];
  static char_T cv19[39] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'C',
    'e', 'n', 't', 'e', 'r', 'F', 'r', 'e', 'q', 'u', 'e', 'n', 'c', 'y', ' ',
    't', 'o', ' ', 'b', 'e', ' ', 'n', 'o', 'n', '-', 'N', 'a', 'N', '.' };

  const mxArray *c_y;
  static const int32_T iv8[2] = { 1, 52 };

  static char_T cv20[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'F', 'i', 'n', 'i', 't', 'e' };

  const mxArray *d_y;
  static const int32_T iv9[2] = { 1, 38 };

  char_T cv21[38];
  static char_T cv22[38] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'C',
    'e', 'n', 't', 'e', 'r', 'F', 'r', 'e', 'q', 'u', 'e', 'n', 'c', 'y', ' ',
    't', 'o', ' ', 'b', 'e', ' ', 'f', 'i', 'n', 'i', 't', 'e', '.' };

  const mxArray *e_y;
  static const int32_T iv10[2] = { 1, 53 };

  char_T cv23[53];
  static char_T cv24[53] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'G', 'r', 'e', 'a', 't', 'e', 'r', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *f_y;
  static const int32_T iv11[2] = { 1, 75 };

  char_T cv25[75];
  static char_T cv26[75] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'C',
    'e', 'n', 't', 'e', 'r', 'F', 'r', 'e', 'q', 'u', 'e', 'n', 'c', 'y', ' ',
    't', 'o', ' ', 'b', 'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ',
    'w', 'i', 't', 'h', ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e',
    ' ', 'v', 'a', 'l', 'u', 'e', 's', ' ', '>', '=', ' ', '7', '0', '0', '0',
    '0', '0', '0', '0', '.' };

  const mxArray *g_y;
  static const int32_T iv12[2] = { 1, 50 };

  char_T cv27[50];
  static char_T cv28[50] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'L', 'e', 's', 's', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *h_y;
  static const int32_T iv13[2] = { 1, 77 };

  char_T cv29[77];
  static char_T cv30[77] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'C',
    'e', 'n', 't', 'e', 'r', 'F', 'r', 'e', 'q', 'u', 'e', 'n', 'c', 'y', ' ',
    't', 'o', ' ', 'b', 'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ',
    'w', 'i', 't', 'h', ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e',
    ' ', 'v', 'a', 'l', 'u', 'e', 's', ' ', '<', '=', ' ', '6', '0', '0', '0',
    '0', '0', '0', '0', '0', '0', '.' };

  p = true;
  if (muDoubleScalarIsNaN(b_val)) {
    p = false;
  }

  if (!p) {
    y = NULL;
    m1 = emlrtCreateCharArray(2, iv6);
    for (i2 = 0; i2 < 52; i2++) {
      cv16[i2] = cv17[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m1, cv16);
    emlrtAssign(&y, m1);
    b_y = NULL;
    m1 = emlrtCreateCharArray(2, iv7);
    for (i2 = 0; i2 < 39; i2++) {
      cv18[i2] = cv19[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 39, m1, cv18);
    emlrtAssign(&b_y, m1);
    b_error(y, b_y, &b_emlrtMCI);
  }

  p = true;
  if (!(!muDoubleScalarIsInf(b_val) && !muDoubleScalarIsNaN(b_val))) {
    p = false;
  }

  if (!p) {
    c_y = NULL;
    m1 = emlrtCreateCharArray(2, iv8);
    for (i2 = 0; i2 < 52; i2++) {
      cv16[i2] = cv20[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m1, cv16);
    emlrtAssign(&c_y, m1);
    d_y = NULL;
    m1 = emlrtCreateCharArray(2, iv9);
    for (i2 = 0; i2 < 38; i2++) {
      cv21[i2] = cv22[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 38, m1, cv21);
    emlrtAssign(&d_y, m1);
    b_error(c_y, d_y, &c_emlrtMCI);
  }

  p = true;
  if (!(b_val >= 7.0E+7)) {
    p = false;
  }

  if (!p) {
    e_y = NULL;
    m1 = emlrtCreateCharArray(2, iv10);
    for (i2 = 0; i2 < 53; i2++) {
      cv23[i2] = cv24[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 53, m1, cv23);
    emlrtAssign(&e_y, m1);
    f_y = NULL;
    m1 = emlrtCreateCharArray(2, iv11);
    for (i2 = 0; i2 < 75; i2++) {
      cv25[i2] = cv26[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 75, m1, cv25);
    emlrtAssign(&f_y, m1);
    b_error(e_y, f_y, &d_emlrtMCI);
  }

  p = true;
  if (!(b_val <= 6.0E+9)) {
    p = false;
  }

  if (!p) {
    g_y = NULL;
    m1 = emlrtCreateCharArray(2, iv12);
    for (i2 = 0; i2 < 50; i2++) {
      cv27[i2] = cv28[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 50, m1, cv27);
    emlrtAssign(&g_y, m1);
    h_y = NULL;
    m1 = emlrtCreateCharArray(2, iv13);
    for (i2 = 0; i2 < 77; i2++) {
      cv29[i2] = cv30[i2];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 77, m1, cv29);
    emlrtAssign(&h_y, m1);
    b_error(g_y, h_y, &e_emlrtMCI);
  }

  obj->CenterFrequency = b_val;
}

static void SDRTxZynqFMC23Base_set_Attenuation(comm_internal_SDRTxZC706FMC23SL
  *obj, real_T b_val[2])
{
  boolean_T p;
  int32_T k;
  boolean_T exitg4;
  const mxArray *y;
  static const int32_T iv14[2] = { 1, 52 };

  const mxArray *m2;
  char_T cv31[52];
  static char_T cv32[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'N', 'a', 'N' };

  const mxArray *b_y;
  static const int32_T iv15[2] = { 1, 35 };

  char_T cv33[35];
  static char_T cv34[35] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'A',
    't', 't', 'e', 'n', 'u', 'a', 't', 'i', 'o', 'n', ' ', 't', 'o', ' ', 'b',
    'e', ' ', 'n', 'o', 'n', '-', 'N', 'a', 'N', '.' };

  boolean_T exitg3;
  const mxArray *c_y;
  static const int32_T iv16[2] = { 1, 52 };

  static char_T cv35[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'F', 'i', 'n', 'i', 't', 'e' };

  const mxArray *d_y;
  static const int32_T iv17[2] = { 1, 34 };

  char_T cv36[34];
  static char_T cv37[34] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'A',
    't', 't', 'e', 'n', 'u', 'a', 't', 'i', 'o', 'n', ' ', 't', 'o', ' ', 'b',
    'e', ' ', 'f', 'i', 'n', 'i', 't', 'e', '.' };

  boolean_T exitg2;
  const mxArray *e_y;
  static const int32_T iv18[2] = { 1, 53 };

  char_T cv38[53];
  static char_T cv39[53] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'G', 'r', 'e', 'a', 't', 'e', 'r', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *f_y;
  static const int32_T iv19[2] = { 1, 64 };

  char_T cv40[64];
  static char_T cv41[64] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'A',
    't', 't', 'e', 'n', 'u', 'a', 't', 'i', 'o', 'n', ' ', 't', 'o', ' ', 'b',
    'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ', 'w', 'i', 't', 'h',
    ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e', ' ', 'v', 'a', 'l',
    'u', 'e', 's', ' ', '>', '=', ' ', '0', '.' };

  boolean_T exitg1;
  const mxArray *g_y;
  static const int32_T iv20[2] = { 1, 50 };

  char_T cv42[50];
  static char_T cv43[50] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'L', 'e', 's', 's', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *h_y;
  static const int32_T iv21[2] = { 1, 68 };

  char_T cv44[68];
  static char_T cv45[68] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'A',
    't', 't', 'e', 'n', 'u', 'a', 't', 'i', 'o', 'n', ' ', 't', 'o', ' ', 'b',
    'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ', 'w', 'i', 't', 'h',
    ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e', ' ', 'v', 'a', 'l',
    'u', 'e', 's', ' ', '<', '=', ' ', '8', '9', '7', '5', '0', '.' };

  p = true;
  k = 0;
  exitg4 = false;
  while (exitg4 == false && k < 2) {
    if (!muDoubleScalarIsNaN(b_val[k])) {
      k++;
    } else {
      p = false;
      exitg4 = true;
    }
  }

  if (!p) {
    y = NULL;
    m2 = emlrtCreateCharArray(2, iv14);
    for (k = 0; k < 52; k++) {
      cv31[k] = cv32[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m2, cv31);
    emlrtAssign(&y, m2);
    b_y = NULL;
    m2 = emlrtCreateCharArray(2, iv15);
    for (k = 0; k < 35; k++) {
      cv33[k] = cv34[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 35, m2, cv33);
    emlrtAssign(&b_y, m2);
    b_error(y, b_y, &b_emlrtMCI);
  }

  p = true;
  k = 0;
  exitg3 = false;
  while (exitg3 == false && k < 2) {
    if (!muDoubleScalarIsInf(b_val[k]) && !muDoubleScalarIsNaN(b_val[k])) {
      k++;
    } else {
      p = false;
      exitg3 = true;
    }
  }

  if (!p) {
    c_y = NULL;
    m2 = emlrtCreateCharArray(2, iv16);
    for (k = 0; k < 52; k++) {
      cv31[k] = cv35[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m2, cv31);
    emlrtAssign(&c_y, m2);
    d_y = NULL;
    m2 = emlrtCreateCharArray(2, iv17);
    for (k = 0; k < 34; k++) {
      cv36[k] = cv37[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 34, m2, cv36);
    emlrtAssign(&d_y, m2);
    b_error(c_y, d_y, &c_emlrtMCI);
  }

  p = true;
  k = 0;
  exitg2 = false;
  while (exitg2 == false && k < 2) {
    if (b_val[k] >= 0.0) {
      k++;
    } else {
      p = false;
      exitg2 = true;
    }
  }

  if (!p) {
    e_y = NULL;
    m2 = emlrtCreateCharArray(2, iv18);
    for (k = 0; k < 53; k++) {
      cv38[k] = cv39[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 53, m2, cv38);
    emlrtAssign(&e_y, m2);
    f_y = NULL;
    m2 = emlrtCreateCharArray(2, iv19);
    for (k = 0; k < 64; k++) {
      cv40[k] = cv41[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 64, m2, cv40);
    emlrtAssign(&f_y, m2);
    b_error(e_y, f_y, &d_emlrtMCI);
  }

  p = true;
  k = 0;
  exitg1 = false;
  while (exitg1 == false && k < 2) {
    if (b_val[k] <= 89750.0) {
      k++;
    } else {
      p = false;
      exitg1 = true;
    }
  }

  if (!p) {
    g_y = NULL;
    m2 = emlrtCreateCharArray(2, iv20);
    for (k = 0; k < 50; k++) {
      cv42[k] = cv43[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 50, m2, cv42);
    emlrtAssign(&g_y, m2);
    h_y = NULL;
    m2 = emlrtCreateCharArray(2, iv21);
    for (k = 0; k < 68; k++) {
      cv44[k] = cv45[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 68, m2, cv44);
    emlrtAssign(&h_y, m2);
    b_error(g_y, h_y, &e_emlrtMCI);
  }

  for (k = 0; k < 2; k++) {
    obj->Attenuation[k] = b_val[k];
  }
}

static void SDRSystemBase_setupImpl(comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  const mxArray *y;
  static const int32_T iv22[2] = { 1, 20 };

  const mxArray *m3;
  char_T cv46[20];
  int32_T i3;
  static char_T cv47[20] = { 'Z', 'C', '7', '0', '6', ' ', 'a', 'n', 'd', ' ',
    'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3' };

  uint8_T pb1[1024];
  int32_T ps1;
  uint8_T pb2[1024];
  int32_T ps2;
  int32_T loop_ub;
  int32_T b_loop_ub;
  int32_T npb_sizes;
  uint8_T npb_data[2048];
  int32_T nps;
  uint8_T tpb[1024];
  int32_T tps;
  uint8_T pb1_data[2048];
  int64_T i4;
  char_T errStr[1024];
  char_T errId[1024];
  SDRPluginStatusT errStat;
  const mxArray *b_y;
  static const int32_T iv23[2] = { 1, 1024 };

  const mxArray *c_y;
  static const int32_T iv24[2] = { 1, 1024 };

  const mxArray *varargin_1;
  b_obj = obj;
  y = NULL;
  m3 = emlrtCreateCharArray(2, iv22);
  for (i3 = 0; i3 < 20; i3++) {
    cv46[i3] = cv47[i3];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 20, m3, cv46);
  emlrtAssign(&y, m3);
  sdrplugin_internal_privsetupsession(y, &i_emlrtMCI);
  SDRTxZynqFMC23Base_initStaticProps(b_obj);
  SDRSystemBase_packPropertyListLocal(b_obj, &ps1, pb1);
  SDRTxZynqFMC23Base_packCreationGroupProps(b_obj, &ps2, pb2);
  if (1 > ps1) {
    loop_ub = 0;
  } else {
    loop_ub = emlrtDynamicBoundsCheckFastR2012b(ps1, 1, 1024, &b_emlrtBCI,
      emlrtRootTLSGlobal);
  }

  if (1 > ps2) {
    b_loop_ub = 0;
  } else {
    b_loop_ub = emlrtDynamicBoundsCheckFastR2012b(ps2, 1, 1024, &b_emlrtBCI,
      emlrtRootTLSGlobal);
  }

  SDRTxZynqFMC23Base_packNontunableGroupProps(b_obj, &nps, npb_data, &npb_sizes);
  b_obj->pCenterFrequency = b_obj->CenterFrequency;
  for (i3 = 0; i3 < 2; i3++) {
    b_obj->pAttenuation[i3] = b_obj->Attenuation[i3];
  }

  SDRTxZynqFMC23Base_packTunableGroupProps(b_obj, &tps, tpb);
  for (i3 = 0; i3 < loop_ub; i3++) {
    pb1_data[i3] = pb1[i3];
  }

  for (i3 = 0; i3 < b_loop_ub; i3++) {
    pb1_data[i3 + loop_ub] = pb2[i3];
  }

  i4 = (int64_T)ps1 + (int64_T)ps2;
  if (i4 > 2147483647LL) {
    i4 = 2147483647LL;
  } else {
    if (i4 < -2147483648LL) {
      i4 = -2147483648LL;
    }
  }

  sdr_setupImpl((int32_T)i4, pb1_data, loop_ub + b_loop_ub, nps, npb_data,
                npb_sizes, tps, tpb, &i3, &errStat, errId, errStr);
  b_obj->pDriverHandle = i3;
  if (errStat == SDRDriverError) {
    b_y = NULL;
    m3 = emlrtCreateCharArray(2, iv23);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m3, errId);
    emlrtAssign(&b_y, m3);
    c_y = NULL;
    m3 = emlrtCreateCharArray(2, iv24);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m3, errStr);
    emlrtAssign(&c_y, m3);
    varargin_1 = b_message(b_y, c_y, &i_emlrtMCI);
    isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
    error(emlrtAlias(varargin_1), &j_emlrtMCI);
    emlrtDestroyArray(&varargin_1);
  }

  SDRTxZynqFMC23Base_setNontunableGroupProps(b_obj);
  SDRTxZynqFMC23Base_setTunableGroupProps(b_obj);
}

static void SDRTxZynqFMC23Base_initStaticProps(comm_internal_SDRTxZC706FMC23SL
  *obj)
{
  int32_T i5;
  static char_T cv48[11] = { '1', '9', '2', '.', '1', '6', '8', '.', '3', '.',
    '2' };

  comm_internal_SDRTxZC706FMC23SL *b_obj;
  real_T b_val;
  boolean_T p;
  const mxArray *y;
  static const int32_T iv25[2] = { 1, 52 };

  const mxArray *m4;
  char_T cv49[52];
  static char_T cv50[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'N', 'o', 'n', 'N', 'a', 'N' };

  const mxArray *b_y;
  static const int32_T iv26[2] = { 1, 37 };

  char_T cv51[37];
  static char_T cv52[37] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'N',
    'u', 'm', 'H', 'W', 'C', 'h', 'a', 'n', 'n', 'e', 'l', 's', ' ', 't', 'o',
    ' ', 'b', 'e', ' ', 'n', 'o', 'n', '-', 'N', 'a', 'N', '.' };

  const mxArray *c_y;
  static const int32_T iv27[2] = { 1, 52 };

  static char_T cv53[52] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'e', 'x',
    'p', 'e', 'c', 't', 'e', 'd', 'F', 'i', 'n', 'i', 't', 'e' };

  const mxArray *d_y;
  static const int32_T iv28[2] = { 1, 36 };

  char_T cv54[36];
  static char_T cv55[36] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'N',
    'u', 'm', 'H', 'W', 'C', 'h', 'a', 'n', 'n', 'e', 'l', 's', ' ', 't', 'o',
    ' ', 'b', 'e', ' ', 'f', 'i', 'n', 'i', 't', 'e', '.' };

  const mxArray *e_y;
  static const int32_T iv29[2] = { 1, 53 };

  char_T cv56[53];
  static char_T cv57[53] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'G', 'r', 'e', 'a', 't', 'e', 'r', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *f_y;
  static const int32_T iv30[2] = { 1, 66 };

  char_T cv58[66];
  static char_T cv59[66] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'N',
    'u', 'm', 'H', 'W', 'C', 'h', 'a', 'n', 'n', 'e', 'l', 's', ' ', 't', 'o',
    ' ', 'b', 'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ', 'w', 'i',
    't', 'h', ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e', ' ', 'v',
    'a', 'l', 'u', 'e', 's', ' ', '>', '=', ' ', '2', '.' };

  const mxArray *g_y;
  static const int32_T iv31[2] = { 1, 50 };

  char_T cv60[50];
  static char_T cv61[50] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 'X', 'i', 'l',
    'i', 'n', 'x', ' ', 'Z', 'y', 'n', 'q', ' ', 'a', 'n', 'd', ' ', 'A', 'D',
    'I', ' ', 'F', 'M', 'C', 'O', 'M', 'M', 'S', '2', '/', '3', ':', 'n', 'o',
    't', 'L', 'e', 's', 's', 'E', 'q', 'u', 'a', 'l' };

  const mxArray *h_y;
  static const int32_T iv32[2] = { 1, 66 };

  static char_T cv62[66] = { 'E', 'x', 'p', 'e', 'c', 't', 'e', 'd', ' ', 'N',
    'u', 'm', 'H', 'W', 'C', 'h', 'a', 'n', 'n', 'e', 'l', 's', ' ', 't', 'o',
    ' ', 'b', 'e', ' ', 'a', 'n', ' ', 'a', 'r', 'r', 'a', 'y', ' ', 'w', 'i',
    't', 'h', ' ', 'a', 'l', 'l', ' ', 'o', 'f', ' ', 't', 'h', 'e', ' ', 'v',
    'a', 'l', 'u', 'e', 's', ' ', '<', '=', ' ', '2', '.' };

  static char_T cv63[6] = { 'd', 'o', 'u', 'b', 'l', 'e' };

  static int16_T c_val[256] = { 55, 140, -53, -49, 41, 223, -124, -25, -124, 123,
    -316, -129, -400, -140, -580, -402, -646, -475, -711, -653, -634, -630, -506,
    -607, -263, -398, -10, -188, 255, 104, 424, 300, 491, 439, 401, 396, 201,
    247, -84, -33, -351, -309, -540, -543, -565, -608, -423, -505, -126, -212,
    234, 165, 562, 544, 738, 775, 700, 790, 429, 538, -5, 91, -490, -448, -865,
    -893, -1006, -1106, -833, -973, -372, -511, 266, 181, 890, 891, 1301, 1398,
    1333, 1500, 935, 1122, 178, 319, -732, -689, -1523, -1607, -1916, -2109,
    -1730, -1980, -943, -1165, 274, 161, 1589, 1642, 2584, 2806, 2879, 3219,
    2253, 2606, 740, 981, -1343, -1323, -3433, -3686, -4846, -5343, -4938, -5559,
    -3280, -3836, 202, -68, 5173, 5385, 10925, 11742, 16509, 17938, 20931, 22860,
    23373, 25581, 23373, 25581, 20931, 22860, 16509, 17938, 10925, 11742, 5173,
    5385, 202, -68, -3280, -3836, -4938, -5559, -4846, -5343, -3433, -3686,
    -1343, -1323, 740, 981, 2253, 2606, 2879, 3219, 2584, 2806, 1589, 1642, 274,
    161, -943, -1165, -1730, -1980, -1916, -2109, -1523, -1607, -732, -689, 178,
    319, 935, 1122, 1333, 1500, 1301, 1398, 890, 891, 266, 181, -372, -511, -833,
    -973, -1006, -1106, -865, -893, -490, -448, -5, 91, 429, 538, 700, 790, 738,
    775, 562, 544, 234, 165, -126, -212, -423, -505, -565, -608, -540, -543,
    -351, -309, -84, -33, 201, 247, 401, 396, 491, 439, 424, 300, 255, 104, -10,
    -188, -263, -398, -506, -607, -634, -630, -711, -653, -646, -475, -580, -402,
    -400, -140, -316, -129, -124, 123, -124, -25, 41, 223, -53, -49, 55, 140 };

  static int32_T d_val[12] = { 754974720, 754974720, 11796480, 11796480, 3932160,
    3932160, 1966080, 1966080, 983040, 983040, 245760, 245760 };

  static char_T filterKind[13] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i',
    'l', 't', 'e', 'r' };

  static char_T cv64[22] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i', 'l',
    't', 'e', 'r', 'F', 'r', 'o', 'm', 'O', 't', 'h', 'e', 'r' };

  for (i5 = 0; i5 < 11; i5++) {
    obj->RadioAddress[i5] = cv48[i5];
  }

  b_obj = obj;
  b_val = obj->NumChannels;
  p = true;
  if (muDoubleScalarIsNaN(b_val)) {
    p = false;
  }

  if (!p) {
    y = NULL;
    m4 = emlrtCreateCharArray(2, iv25);
    for (i5 = 0; i5 < 52; i5++) {
      cv49[i5] = cv50[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m4, cv49);
    emlrtAssign(&y, m4);
    b_y = NULL;
    m4 = emlrtCreateCharArray(2, iv26);
    for (i5 = 0; i5 < 37; i5++) {
      cv51[i5] = cv52[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 37, m4, cv51);
    emlrtAssign(&b_y, m4);
    b_error(y, b_y, &b_emlrtMCI);
  }

  p = true;
  if (!(!muDoubleScalarIsInf(b_val) && !muDoubleScalarIsNaN(b_val))) {
    p = false;
  }

  if (!p) {
    c_y = NULL;
    m4 = emlrtCreateCharArray(2, iv27);
    for (i5 = 0; i5 < 52; i5++) {
      cv49[i5] = cv53[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 52, m4, cv49);
    emlrtAssign(&c_y, m4);
    d_y = NULL;
    m4 = emlrtCreateCharArray(2, iv28);
    for (i5 = 0; i5 < 36; i5++) {
      cv54[i5] = cv55[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 36, m4, cv54);
    emlrtAssign(&d_y, m4);
    b_error(c_y, d_y, &c_emlrtMCI);
  }

  p = true;
  if (!(b_val >= 2.0)) {
    p = false;
  }

  if (!p) {
    e_y = NULL;
    m4 = emlrtCreateCharArray(2, iv29);
    for (i5 = 0; i5 < 53; i5++) {
      cv56[i5] = cv57[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 53, m4, cv56);
    emlrtAssign(&e_y, m4);
    f_y = NULL;
    m4 = emlrtCreateCharArray(2, iv30);
    for (i5 = 0; i5 < 66; i5++) {
      cv58[i5] = cv59[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 66, m4, cv58);
    emlrtAssign(&f_y, m4);
    b_error(e_y, f_y, &d_emlrtMCI);
  }

  p = true;
  if (!(b_val <= 2.0)) {
    p = false;
  }

  if (!p) {
    g_y = NULL;
    m4 = emlrtCreateCharArray(2, iv31);
    for (i5 = 0; i5 < 50; i5++) {
      cv60[i5] = cv61[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 50, m4, cv60);
    emlrtAssign(&g_y, m4);
    h_y = NULL;
    m4 = emlrtCreateCharArray(2, iv32);
    for (i5 = 0; i5 < 66; i5++) {
      cv58[i5] = cv62[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 66, m4, cv58);
    emlrtAssign(&h_y, m4);
    b_error(g_y, h_y, &e_emlrtMCI);
  }

  b_obj->NumHWChannels = b_val;
  for (i5 = 0; i5 < 6; i5++) {
    obj->OutputDataType[i5] = cv63[i5];
  }

  b_obj = obj;
  b_obj->NumChannels = 2.0;
  b_obj = obj;
  b_obj->SamplesPerFrame = 4096.0;
  if (BasebandSampleRate != obj->FilterPathRates[10]) {
    b_obj = obj;
    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 2; i5++) {
      b_obj->FIRCoefficientSize[i5] = 128.0;
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 256; i5++) {
      b_obj->FIRCoefficients[i5] = (real_T)c_val[i5];
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 2; i5++) {
      b_obj->FIRGain[i5] = -12.0 * (real_T)i5;
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 2; i5++) {
      b_obj->FIRDecimInterpFactor[i5] = 4.0;
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 2; i5++) {
      b_obj->AnalogFilterCutoff[i5] = 163840.0;
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 12; i5++) {
      b_obj->FilterPathRates[i5] = (real_T)d_val[i5];
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 13; i5++) {
      b_obj->FilterDesignTypeForTx[i5] = filterKind[i5];
    }

    if (b_obj->isInitialized && !b_obj->isReleased) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      b_obj->TunablePropsChanged = true;
    }

    for (i5 = 0; i5 < 22; i5++) {
      b_obj->FilterDesignTypeForRx[i5] = cv64[i5];
    }
  }
}

static void SDRSystemBase_packPropertyListLocal(comm_internal_SDRTxZC706FMC23SL *
  obj, int32_T *psize, uint8_T pbytes[1024])
{
  int32_T ncols;
  char_T y_data[12];
  int32_T tmp_data[132];
  int32_T p;
  char_T x[12];
  static char_T propList[48] = { 'R', 's', 'r', 'd', 'a', 'd', 'e', 'r', 'd',
    'r', 'q', 'i', 'i', 'B', 'u', 'v', 'o', 'l', 'e', 'e', 'A', 'o', 's', 'r',
    'd', 'c', 't', 'L', 'd', 'k', 'e', 'i', 'r', 'T', 'r', 'b', 'e', 'y', ' ',
    ' ', 's', 'p', ' ', ' ', 's', 'e', ' ', ' ' };

  boolean_T exitg2;
  static boolean_T bv0[128] = { false, false, false, false, false, false, false,
    false, false, true, true, true, true, true, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, true, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false
  };

  boolean_T b0;
  int32_T loop_ub;
  int32_T y_sizes[2];
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T intenc;
  char_T b_val[11];
  uint8_T intval[11];
  uint8_T y[11];
  uint8_T b_y[4];
  uint8_T c_y[4];
  int32_T ps;
  uint8_T d_y[19];
  int32_T pb_sizes;
  uint8_T pb_data[132];
  boolean_T err2;
  boolean_T b_bool;
  int32_T exitg1;
  static char_T cv65[12] = { 's', 'd', 'r', 'B', 'l', 'o', 'c', 'k', 'T', 'y',
    'p', 'e' };

  int32_T c_bool;
  const mxArray *e_y;
  static const int32_T iv33[2] = { 1, 44 };

  const mxArray *m5;
  char_T cv66[44];
  static char_T cv67[44] = { 'b', 'a', 'd', ' ', 'p', 'r', 'o', 'p', ' ', 'n',
    'a', 'm', 'e', ' ', 'i', 'n', ' ', 'S', 't', 'r', 'i', 'n', 'g', 'S', 'e',
    't', ' ', 't', 'o', ' ', 'i', 'n', 't', ' ', 'c', 'o', 'n', 'v', 'e', 'r',
    's', 'i', 'o', 'n' };

  uint8_T f_y[4];
  uint8_T g_y[12];
  char_T c_val[14];
  uint8_T b_intval[14];
  uint8_T h_y[14];
  uint8_T i_y[22];
  uint8_T j_y[124];
  static uint8_T d_val[124] = { 67U, 58U, 92U, 77U, 65U, 84U, 76U, 65U, 66U, 92U,
    83U, 117U, 112U, 112U, 111U, 114U, 116U, 80U, 97U, 99U, 107U, 97U, 103U,
    101U, 115U, 92U, 82U, 50U, 48U, 49U, 52U, 98U, 92U, 115U, 100U, 114U, 112U,
    108U, 117U, 103U, 105U, 110U, 98U, 97U, 115U, 101U, 92U, 116U, 111U, 111U,
    108U, 98U, 111U, 120U, 92U, 115U, 104U, 97U, 114U, 101U, 100U, 92U, 115U,
    100U, 114U, 92U, 115U, 100U, 114U, 112U, 108U, 117U, 103U, 92U, 115U, 100U,
    114U, 112U, 108U, 117U, 103U, 105U, 110U, 98U, 97U, 115U, 101U, 92U, 104U,
    111U, 115U, 116U, 92U, 100U, 101U, 114U, 105U, 118U, 101U, 100U, 92U, 98U,
    105U, 110U, 92U, 119U, 105U, 110U, 54U, 52U, 92U, 108U, 105U, 98U, 109U,
    119U, 115U, 100U, 114U, 101U, 109U, 98U, 101U, 100U };

  uint8_T k_y[132];
  int64_T i6;
  const mxArray *l_y;
  const mxArray *m_y;
  static const int32_T iv34[2] = { 1, 51 };

  const mxArray *n_y;
  const mxArray *o_y;
  static const int32_T iv35[2] = { 1, 40 };

  char_T cv68[40];
  char_T cv69[51];
  char_T u_data[21];
  int32_T u_sizes[2];
  char_T b_u_data[21];
  int32_T b_u_sizes[2];
  static char_T cv70[51] = { 'p', 'r', 'o', 'p', 'e', 'r', 't', 'y', ' ', 'd',
    'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'h', 'a', 'v', 'e', ' ', 'a', 'n',
    ' ', 'i', 'n', 't', ' ', 'e', 'n', 'c', 'o', 'd', 'i', 'n', 'g', ' ', 'f',
    'u', 'n', 'c', 't', 'i', 'o', 'n', ':', ' ', '%', 's' };

  static char_T cv71[40] = { 'p', 'r', 'o', 'p', 'e', 'r', 't', 'y', ' ', 'd',
    'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'h', 'a', 'v', 'e', ' ', 'p', 'a',
    'c', 'k', ' ', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ':', ' ', '%', 's' };

  *psize = 0;
  for (ncols = 0; ncols < 1024; ncols++) {
    pbytes[ncols] = 0;
  }

  for (p = 0; p < 4; p++) {
    for (ncols = 0; ncols < 12; ncols++) {
      x[ncols] = propList[p + (ncols << 2)];
    }

    ncols = 12;
    exitg2 = false;
    while (exitg2 == false && ncols > 0) {
      if (!bv0[(int32_T)x[ncols - 1]]) {
        b0 = false;
      } else {
        b0 = true;
      }

      if (b0) {
        ncols--;
      } else {
        exitg2 = true;
      }
    }

    if (1 > ncols) {
      loop_ub = 0;
    } else {
      loop_ub = ncols;
    }

    y_sizes[0] = 1;
    y_sizes[1] = loop_ub;
    for (ncols = 0; ncols < loop_ub; ncols++) {
      y_data[ncols] = x[ncols];
    }

    b_obj = obj;
    switch ((int32_T)eml_switch_helper(y_data, y_sizes)) {
     case 0:
      intenc = 65792;
      break;

     case 1:
      intenc = 65793;
      break;

     case 2:
      intenc = 65794;
      break;

     case 3:
      intenc = 65795;
      break;

     default:
      l_y = NULL;
      m5 = emlrtCreateCharArray(2, iv34);
      for (ncols = 0; ncols < 51; ncols++) {
        cv69[ncols] = cv70[ncols];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m5, cv69);
      emlrtAssign(&l_y, m5);
      u_sizes[0] = 1;
      u_sizes[1] = loop_ub;
      for (ncols = 0; ncols < loop_ub; ncols++) {
        u_data[ncols] = y_data[ncols];
      }

      m_y = NULL;
      m5 = emlrtCreateCharArray(2, u_sizes);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m5, (char_T *)
        &u_data);
      emlrtAssign(&m_y, m5);
      b_error(l_y, m_y, &j_emlrtMCI);
      break;
    }

    switch ((int32_T)eml_switch_helper(y_data, y_sizes)) {
     case 0:
      for (ncols = 0; ncols < 11; ncols++) {
        b_val[ncols] = b_obj->RadioAddress[ncols];
      }

      for (ncols = 0; ncols < 11; ncols++) {
        intval[ncols] = (uint8_T)b_val[ncols];
      }

      memcpy(&y[0], &intval[0], (size_t)11 * sizeof(uint8_T));
      memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
      ncols = 11;
      memcpy(&c_y[0], &ncols, (size_t)4 * sizeof(uint8_T));
      ps = 19;
      for (ncols = 0; ncols < 4; ncols++) {
        d_y[ncols] = b_y[ncols];
      }

      for (ncols = 0; ncols < 4; ncols++) {
        d_y[ncols + 4] = c_y[ncols];
      }

      for (ncols = 0; ncols < 11; ncols++) {
        d_y[ncols + 8] = y[ncols];
      }

      pb_sizes = 19;
      for (ncols = 0; ncols < 19; ncols++) {
        pb_data[ncols] = d_y[ncols];
      }
      break;

     case 1:
      ps = 0;
      err2 = false;
      b_bool = false;
      if (loop_ub == 12) {
        ncols = 0;
        do {
          exitg1 = 0;
          if (ncols <= 11) {
            if (x[ncols] != cv65[ncols]) {
              exitg1 = 1;
            } else {
              ncols++;
            }
          } else {
            b_bool = true;
            exitg1 = 1;
          }
        } while (exitg1 == 0);
      }

      if (b_bool) {
        c_bool = 0;
      } else {
        c_bool = -1;
      }

      switch (c_bool) {
       case 0:
        ps = 2;
        break;

       default:
        err2 = true;
        break;
      }

      if (err2) {
        e_y = NULL;
        m5 = emlrtCreateCharArray(2, iv33);
        for (ncols = 0; ncols < 44; ncols++) {
          cv66[ncols] = cv67[ncols];
        }

        emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 44, m5, cv66);
        emlrtAssign(&e_y, m5);
        error(e_y, &j_emlrtMCI);
      }

      memcpy(&b_y[0], &ps, (size_t)4 * sizeof(uint8_T));
      memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
      ncols = 4;
      memcpy(&f_y[0], &ncols, (size_t)4 * sizeof(uint8_T));
      ps = 12;
      for (ncols = 0; ncols < 4; ncols++) {
        g_y[ncols] = c_y[ncols];
      }

      for (ncols = 0; ncols < 4; ncols++) {
        g_y[ncols + 4] = f_y[ncols];
      }

      for (ncols = 0; ncols < 4; ncols++) {
        g_y[ncols + 8] = b_y[ncols];
      }

      pb_sizes = 12;
      for (ncols = 0; ncols < 12; ncols++) {
        pb_data[ncols] = g_y[ncols];
      }
      break;

     case 2:
      for (ncols = 0; ncols < 14; ncols++) {
        c_val[ncols] = b_obj->prequester[ncols];
      }

      for (ncols = 0; ncols < 14; ncols++) {
        b_intval[ncols] = (uint8_T)c_val[ncols];
      }

      memcpy(&h_y[0], &b_intval[0], (size_t)14 * sizeof(uint8_T));
      memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
      ncols = 14;
      memcpy(&c_y[0], &ncols, (size_t)4 * sizeof(uint8_T));
      ps = 22;
      for (ncols = 0; ncols < 4; ncols++) {
        i_y[ncols] = b_y[ncols];
      }

      for (ncols = 0; ncols < 4; ncols++) {
        i_y[ncols + 4] = c_y[ncols];
      }

      for (ncols = 0; ncols < 14; ncols++) {
        i_y[ncols + 8] = h_y[ncols];
      }

      pb_sizes = 22;
      for (ncols = 0; ncols < 22; ncols++) {
        pb_data[ncols] = i_y[ncols];
      }
      break;

     case 3:
      memcpy(&j_y[0], &d_val[0], (size_t)124 * sizeof(uint8_T));
      memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
      ncols = 124;
      memcpy(&c_y[0], &ncols, (size_t)4 * sizeof(uint8_T));
      ps = 132;
      for (ncols = 0; ncols < 4; ncols++) {
        k_y[ncols] = b_y[ncols];
      }

      for (ncols = 0; ncols < 4; ncols++) {
        k_y[ncols + 4] = c_y[ncols];
      }

      for (ncols = 0; ncols < 124; ncols++) {
        k_y[ncols + 8] = j_y[ncols];
      }

      pb_sizes = 132;
      for (ncols = 0; ncols < 132; ncols++) {
        pb_data[ncols] = k_y[ncols];
      }
      break;

     default:
      n_y = NULL;
      m5 = emlrtCreateCharArray(2, iv35);
      for (ncols = 0; ncols < 40; ncols++) {
        cv68[ncols] = cv71[ncols];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 40, m5, cv68);
      emlrtAssign(&n_y, m5);
      b_u_sizes[0] = 1;
      b_u_sizes[1] = loop_ub;
      for (ncols = 0; ncols < loop_ub; ncols++) {
        b_u_data[ncols] = y_data[ncols];
      }

      o_y = NULL;
      m5 = emlrtCreateCharArray(2, b_u_sizes);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, b_u_sizes[1], m5, (char_T *)
        &b_u_data);
      emlrtAssign(&o_y, m5);
      b_error(n_y, o_y, &j_emlrtMCI);
      break;
    }

    for (ncols = 0; ncols < ps; ncols++) {
      i6 = (int64_T)*psize + (int64_T)(1 + ncols);
      if (i6 > 2147483647LL) {
        i6 = 2147483647LL;
      } else {
        if (i6 < -2147483648LL) {
          i6 = -2147483648LL;
        }
      }

      tmp_data[ncols] = emlrtDynamicBoundsCheckFastR2012b((int32_T)i6, 1, 1024,
        &b_emlrtBCI, emlrtRootTLSGlobal);
    }

    emlrtSizeEqCheck1DFastR2012b(ps, pb_sizes, &j_emlrtECI, emlrtRootTLSGlobal);
    for (ncols = 0; ncols < ps; ncols++) {
      pbytes[tmp_data[ncols] - 1] = pb_data[ncols];
    }

    i6 = (int64_T)*psize + (int64_T)ps;
    if (i6 > 2147483647LL) {
      i6 = 2147483647LL;
    } else {
      if (i6 < -2147483648LL) {
        i6 = -2147483648LL;
      }
    }

    *psize = (int32_T)i6;
  }
}

static real_T eml_switch_helper(char_T expr_data[], int32_T expr_sizes[2])
{
  real_T b_index;
  boolean_T b_bool;
  int32_T k;
  int32_T exitg4;
  static char_T cv72[12] = { 'R', 'a', 'd', 'i', 'o', 'A', 'd', 'd', 'r', 'e',
    's', 's' };

  int32_T exitg3;
  static char_T cv73[12] = { 's', 'd', 'r', 'B', 'l', 'o', 'c', 'k', 'T', 'y',
    'p', 'e' };

  int32_T exitg2;
  static char_T cv74[9] = { 'r', 'e', 'q', 'u', 'e', 's', 't', 'e', 'r' };

  int32_T exitg1;
  static char_T cv75[9] = { 'd', 'r', 'i', 'v', 'e', 'r', 'L', 'i', 'b' };

  b_bool = false;
  if (expr_sizes[1] == 12) {
    k = 0;
    do {
      exitg4 = 0;
      if (k <= 11) {
        if (expr_data[k] != cv72[k]) {
          exitg4 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg4 = 1;
      }
    } while (exitg4 == 0);
  }

  if (b_bool) {
    b_index = 0.0;
  } else {
    b_bool = false;
    if (expr_sizes[1] == 12) {
      k = 0;
      do {
        exitg3 = 0;
        if (k <= 11) {
          if (expr_data[k] != cv73[k]) {
            exitg3 = 1;
          } else {
            k++;
          }
        } else {
          b_bool = true;
          exitg3 = 1;
        }
      } while (exitg3 == 0);
    }

    if (b_bool) {
      b_index = 1.0;
    } else {
      b_bool = false;
      if (expr_sizes[1] == 9) {
        k = 0;
        do {
          exitg2 = 0;
          if (k <= 8) {
            if (expr_data[k] != cv74[k]) {
              exitg2 = 1;
            } else {
              k++;
            }
          } else {
            b_bool = true;
            exitg2 = 1;
          }
        } while (exitg2 == 0);
      }

      if (b_bool) {
        b_index = 2.0;
      } else {
        b_bool = false;
        if (expr_sizes[1] == 9) {
          k = 0;
          do {
            exitg1 = 0;
            if (k <= 8) {
              if (expr_data[k] != cv75[k]) {
                exitg1 = 1;
              } else {
                k++;
              }
            } else {
              b_bool = true;
              exitg1 = 1;
            }
          } while (exitg1 == 0);
        }

        if (b_bool) {
          b_index = 3.0;
        } else {
          b_index = -1.0;
        }
      }
    }
  }

  return b_index;
}

static void SDRTxZynqFMC23Base_packCreationGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes[1024])
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T i7;
  int32_T tmp_data[520];
  int32_T p;
  char_T x[16];
  static char_T propList[80] = { 's', 's', 'I', 'd', 'c', 'u', 'd', 'P', 'a',
    't', 'b', 'r', 'A', 't', 'r', 'D', 'E', 'd', 'a', 'l', 'e', 'x', 'd', 'U',
    'U', 'v', 'e', 'r', 'D', 'D', 'i', 'c', 'e', 'P', 'P', 'c', 'u', 's', 'P',
    'P', 'e', 't', 's', 'o', 'o', 'L', 'i', ' ', 'r', 'r', 'i', 'o', ' ', 't',
    't', 's', 'n', ' ', ' ', ' ', 't', 'M', ' ', ' ', ' ', ' ', 'o', ' ', ' ',
    ' ', ' ', 'd', ' ', ' ', ' ', ' ', 'e', ' ', ' ', ' ' };

  int32_T ncols;
  boolean_T exitg1;
  static boolean_T bv1[128] = { false, false, false, false, false, false, false,
    false, false, true, true, true, true, true, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, true, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false
  };

  boolean_T b1;
  char_T x_data[16];
  int32_T x_sizes[2];
  int32_T pb_sizes;
  uint8_T pb_data[520];
  int64_T i8;
  b_obj = obj;
  *psize = 0;
  for (i7 = 0; i7 < 1024; i7++) {
    pbytes[i7] = 0;
  }

  for (p = 0; p < 5; p++) {
    for (i7 = 0; i7 < 16; i7++) {
      x[i7] = propList[p + 5 * i7];
    }

    ncols = 16;
    exitg1 = false;
    while (exitg1 == false && ncols > 0) {
      if (!bv1[(int32_T)x[ncols - 1]]) {
        b1 = false;
      } else {
        b1 = true;
      }

      if (b1) {
        ncols--;
      } else {
        exitg1 = true;
      }
    }

    if (1 > ncols) {
      ncols = 0;
    }

    x_sizes[0] = 1;
    x_sizes[1] = ncols;
    for (i7 = 0; i7 < ncols; i7++) {
      x_data[i7] = x[i7];
    }

    SDRTxZynqFMC23Base_packProperty(b_obj, x_data, x_sizes, &ncols, pb_data,
      &pb_sizes);
    for (i7 = 0; i7 < ncols; i7++) {
      i8 = (int64_T)*psize + (int64_T)(1 + i7);
      if (i8 > 2147483647LL) {
        i8 = 2147483647LL;
      } else {
        if (i8 < -2147483648LL) {
          i8 = -2147483648LL;
        }
      }

      tmp_data[i7] = emlrtDynamicBoundsCheckFastR2012b((int32_T)i8, 1, 1024,
        &b_emlrtBCI, emlrtRootTLSGlobal);
    }

    emlrtSizeEqCheck1DFastR2012b(ncols, pb_sizes, &j_emlrtECI,
      emlrtRootTLSGlobal);
    for (i7 = 0; i7 < ncols; i7++) {
      pbytes[tmp_data[i7] - 1] = pb_data[i7];
    }

    i8 = (int64_T)*psize + (int64_T)ncols;
    if (i8 > 2147483647LL) {
      i8 = 2147483647LL;
    } else {
      if (i8 < -2147483648LL) {
        i8 = -2147483648LL;
      }
    }

    *psize = (int32_T)i8;
  }
}

static void SDRTxZynqFMC23Base_packProperty(comm_internal_SDRTxZC706FMC23SL *obj,
  char_T what_data[], int32_T what_sizes[2], int32_T *psize, uint8_T
  pbytes_data[], int32_T *pbytes_sizes)
{
  int32_T intenc;
  uint8_T y[15];
  static uint8_T b_val[15] = { 122U, 121U, 110U, 113U, 84U, 114U, 97U, 110U,
    115U, 109U, 105U, 116U, 116U, 101U, 114U };

  uint8_T b_y[4];
  int32_T intval;
  uint8_T c_y[4];
  uint8_T d_y[23];
  int32_T i9;
  real_T c_val;
  uint8_T e_y[4];
  uint8_T f_y[12];
  uint8_T g_y[11];
  static uint8_T b_intval[11] = { 49U, 57U, 50U, 46U, 49U, 54U, 56U, 46U, 51U,
    46U, 50U };

  uint8_T h_y[19];
  uint8_T i_y[9];
  boolean_T d_val;
  uint8_T j_y[8];
  uint8_T k_y[16];
  uint64_T c_intval;
  real_T e_val[2];
  uint32_T d_intval[2];
  uint32_T x;
  uint16_T e_intval[2];
  uint16_T b_u0;
  real_T f_val[256];
  int16_T f_intval[256];
  int16_T i10;
  uint8_T l_y[512];
  uint8_T m_y[520];
  int32_T g_intval[2];
  real_T g_val[12];
  uint32_T h_intval[12];
  uint8_T n_y[48];
  uint8_T o_y[56];
  uint32_T b_x[4];
  uint8_T p_y[24];
  int32_T c_x[4];
  const mxArray *q_y;
  const mxArray *r_y;
  const mxArray *m6;
  static const int32_T iv36[2] = { 1, 40 };

  char_T cv76[40];
  char_T u_data[21];
  int32_T u_sizes[2];
  static char_T cv77[40] = { 'p', 'r', 'o', 'p', 'e', 'r', 't', 'y', ' ', 'd',
    'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'h', 'a', 'v', 'e', ' ', 'p', 'a',
    'c', 'k', ' ', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ':', ' ', '%', 's' };

  intenc = SDRTxZynqFMC23Base_getIntEncoding(obj, what_data, what_sizes);
  switch ((int32_T)b_eml_switch_helper(what_data, what_sizes)) {
   case 0:
    memcpy(&y[0], &b_val[0], (size_t)15 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 15;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 23;
    for (i9 = 0; i9 < 4; i9++) {
      d_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      d_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 15; i9++) {
      d_y[i9 + 8] = y[i9];
    }

    *pbytes_sizes = 23;
    for (i9 = 0; i9 < 23; i9++) {
      pbytes_data[i9] = d_y[i9];
    }
    break;

   case 1:
    c_val = SDRTxZynqFMC23Base_enumToInt(obj, what_data, what_sizes);
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 2:
    memcpy(&g_y[0], &b_intval[0], (size_t)11 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 11;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 19;
    for (i9 = 0; i9 < 4; i9++) {
      h_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      h_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 11; i9++) {
      h_y[i9 + 8] = g_y[i9];
    }

    *pbytes_sizes = 19;
    for (i9 = 0; i9 < 19; i9++) {
      pbytes_data[i9] = h_y[i9];
    }
    break;

   case 3:
    intval = -1;
    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 4:
    intval = -1;
    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 5:
    c_val = SDRTxZynqFMC23Base_enumToInt(obj, what_data, what_sizes);
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 6:
    c_val = obj->SamplesPerFrame;
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 7:
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 1;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 9;
    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9 + 4] = c_y[i9];
    }

    i_y[8] = 0;
    *pbytes_sizes = 9;
    for (i9 = 0; i9 < 9; i9++) {
      pbytes_data[i9] = i_y[i9];
    }
    break;

   case 8:
    intval = 20;
    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 9:
    d_val = obj->DataIsComplex;
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 1;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 9;
    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9 + 4] = c_y[i9];
    }

    if (d_val) {
      i_y[8] = 1;
    } else {
      i_y[8] = 0;
    }

    *pbytes_sizes = 9;
    for (i9 = 0; i9 < 9; i9++) {
      pbytes_data[i9] = i_y[i9];
    }
    break;

   case 10:
    c_val = obj->NumChannels;
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 11:
    c_val = 245760.0;
    memcpy(&j_y[0], &c_val, (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 12:
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 1;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 9;
    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9 + 4] = c_y[i9];
    }

    i_y[8] = 0;
    *pbytes_sizes = 9;
    for (i9 = 0; i9 < 9; i9++) {
      pbytes_data[i9] = i_y[i9];
    }
    break;

   case 13:
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 1;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 9;
    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      i_y[i9 + 4] = c_y[i9];
    }

    i_y[8] = 1;
    *pbytes_sizes = 9;
    for (i9 = 0; i9 < 9; i9++) {
      pbytes_data[i9] = i_y[i9];
    }
    break;

   case 14:
    c_val = obj->pCenterFrequency;
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 1.8446744073709552E+19) {
      if (c_val >= 0.0) {
        c_intval = (uint64_T)c_val;
      } else {
        c_intval = 0ULL;
      }
    } else if (c_val >= 1.8446744073709552E+19) {
      c_intval = MAX_uint64_T;
    } else {
      c_intval = 0ULL;
    }

    memcpy(&j_y[0], &c_intval, (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 15:
    for (i9 = 0; i9 < 2; i9++) {
      e_val[i9] = obj->pAttenuation[i9];
    }

    for (i9 = 0; i9 < 2; i9++) {
      c_val = muDoubleScalarRound(e_val[i9]);
      if (c_val < 4.294967296E+9) {
        if (c_val >= 0.0) {
          x = (uint32_T)c_val;
        } else {
          x = 0U;
        }
      } else if (c_val >= 4.294967296E+9) {
        x = MAX_uint32_T;
      } else {
        x = 0U;
      }

      d_intval[i9] = x;
    }

    memcpy(&j_y[0], &d_intval[0], (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 16:
    x = 245760U;
    memcpy(&b_y[0], &x, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 17:
    c_val = obj->NumHWChannels;
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 18:
    for (i9 = 0; i9 < 2; i9++) {
      e_val[i9] = obj->FIRCoefficientSize[i9];
    }

    for (i9 = 0; i9 < 2; i9++) {
      c_val = muDoubleScalarRound(e_val[i9]);
      if (c_val < 65536.0) {
        if (c_val >= 0.0) {
          b_u0 = (uint16_T)c_val;
        } else {
          b_u0 = 0;
        }
      } else if (c_val >= 65536.0) {
        b_u0 = MAX_uint16_T;
      } else {
        b_u0 = 0;
      }

      e_intval[i9] = b_u0;
    }

    memcpy(&b_y[0], &e_intval[0], (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 19:
    for (i9 = 0; i9 < 256; i9++) {
      f_val[i9] = obj->FIRCoefficients[i9];
    }

    for (i9 = 0; i9 < 256; i9++) {
      c_val = muDoubleScalarRound(f_val[i9]);
      if (c_val < 32768.0) {
        if (c_val >= -32768.0) {
          i10 = (int16_T)c_val;
        } else {
          i10 = MIN_int16_T;
        }
      } else if (c_val >= 32768.0) {
        i10 = MAX_int16_T;
      } else {
        i10 = 0;
      }

      f_intval[i9] = i10;
    }

    memcpy(&l_y[0], &f_intval[0], (size_t)512 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 512;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 520;
    for (i9 = 0; i9 < 4; i9++) {
      m_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      m_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 512; i9++) {
      m_y[i9 + 8] = l_y[i9];
    }

    *pbytes_sizes = 520;
    for (i9 = 0; i9 < 520; i9++) {
      pbytes_data[i9] = m_y[i9];
    }
    break;

   case 20:
    for (i9 = 0; i9 < 2; i9++) {
      e_val[i9] = obj->FIRGain[i9];
    }

    for (i9 = 0; i9 < 2; i9++) {
      c_val = muDoubleScalarRound(e_val[i9]);
      if (c_val < 2.147483648E+9) {
        if (c_val >= -2.147483648E+9) {
          intval = (int32_T)c_val;
        } else {
          intval = MIN_int32_T;
        }
      } else if (c_val >= 2.147483648E+9) {
        intval = MAX_int32_T;
      } else {
        intval = 0;
      }

      g_intval[i9] = intval;
    }

    memcpy(&j_y[0], &g_intval[0], (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 21:
    for (i9 = 0; i9 < 2; i9++) {
      e_val[i9] = obj->FIRDecimInterpFactor[i9];
    }

    for (i9 = 0; i9 < 2; i9++) {
      c_val = muDoubleScalarRound(e_val[i9]);
      if (c_val < 4.294967296E+9) {
        if (c_val >= 0.0) {
          x = (uint32_T)c_val;
        } else {
          x = 0U;
        }
      } else if (c_val >= 4.294967296E+9) {
        x = MAX_uint32_T;
      } else {
        x = 0U;
      }

      d_intval[i9] = x;
    }

    memcpy(&j_y[0], &d_intval[0], (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 22:
    for (i9 = 0; i9 < 2; i9++) {
      e_val[i9] = obj->AnalogFilterCutoff[i9];
    }

    for (i9 = 0; i9 < 2; i9++) {
      c_val = muDoubleScalarRound(e_val[i9]);
      if (c_val < 4.294967296E+9) {
        if (c_val >= 0.0) {
          x = (uint32_T)c_val;
        } else {
          x = 0U;
        }
      } else if (c_val >= 4.294967296E+9) {
        x = MAX_uint32_T;
      } else {
        x = 0U;
      }

      d_intval[i9] = x;
    }

    memcpy(&j_y[0], &d_intval[0], (size_t)8 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 8;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 16;
    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      k_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 8; i9++) {
      k_y[i9 + 8] = j_y[i9];
    }

    *pbytes_sizes = 16;
    for (i9 = 0; i9 < 16; i9++) {
      pbytes_data[i9] = k_y[i9];
    }
    break;

   case 23:
    for (i9 = 0; i9 < 12; i9++) {
      g_val[i9] = obj->FilterPathRates[i9];
    }

    for (i9 = 0; i9 < 12; i9++) {
      c_val = muDoubleScalarRound(g_val[i9]);
      if (c_val < 4.294967296E+9) {
        if (c_val >= 0.0) {
          x = (uint32_T)c_val;
        } else {
          x = 0U;
        }
      } else if (c_val >= 4.294967296E+9) {
        x = MAX_uint32_T;
      } else {
        x = 0U;
      }

      h_intval[i9] = x;
    }

    memcpy(&n_y[0], &h_intval[0], (size_t)48 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 48;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 56;
    for (i9 = 0; i9 < 4; i9++) {
      o_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      o_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 48; i9++) {
      o_y[i9 + 8] = n_y[i9];
    }

    *pbytes_sizes = 56;
    for (i9 = 0; i9 < 56; i9++) {
      pbytes_data[i9] = o_y[i9];
    }
    break;

   case 24:
    c_val = SDRTxZynqFMC23Base_enumToInt(obj, what_data, what_sizes);
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 25:
    c_val = SDRTxZynqFMC23Base_enumToInt(obj, what_data, what_sizes);
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 26:
    c_val = SDRTxZynqFMC23Base_enumToInt(obj, what_data, what_sizes);
    c_val = muDoubleScalarRound(c_val);
    if (c_val < 2.147483648E+9) {
      if (c_val >= -2.147483648E+9) {
        intval = (int32_T)c_val;
      } else {
        intval = MIN_int32_T;
      }
    } else if (c_val >= 2.147483648E+9) {
      intval = MAX_int32_T;
    } else {
      intval = 0;
    }

    memcpy(&b_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    memcpy(&c_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 4;
    memcpy(&e_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 12;
    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9] = c_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 4] = e_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      f_y[i9 + 8] = b_y[i9];
    }

    *pbytes_sizes = 12;
    for (i9 = 0; i9 < 12; i9++) {
      pbytes_data[i9] = f_y[i9];
    }
    break;

   case 27:
    for (i9 = 0; i9 < 4; i9++) {
      b_x[i9] = 1000000U;
    }

    memcpy(&k_y[0], &b_x[0], (size_t)16 * sizeof(uint8_T));
    memcpy(&b_y[0], &intenc, (size_t)4 * sizeof(uint8_T));
    intval = 16;
    memcpy(&c_y[0], &intval, (size_t)4 * sizeof(uint8_T));
    *psize = 24;
    for (i9 = 0; i9 < 4; i9++) {
      p_y[i9] = b_y[i9];
    }

    for (i9 = 0; i9 < 4; i9++) {
      p_y[i9 + 4] = c_y[i9];
    }

    for (i9 = 0; i9 < 16; i9++) {
      p_y[i9 + 8] = k_y[i9];
    }

    *pbytes_sizes = 24;
    for (i9 = 0; i9 < 24; i9++) {
      pbytes_data[i9] = p_y[i9];
    }
    break;

   case 28:
    for (i9 = 0; i9 < 4; i9++) {
      b_x[i9] = 0U;
    }

    memcpy(&k_y[0], &b_x[0], (size_t)16 * sizeof(uint8_T));
    PUP_packProperty(intenc, k_y, p_y);
    *psize = 24;
    *pbytes_sizes = 24;
    for (i9 = 0; i9 < 24; i9++) {
      pbytes_data[i9] = p_y[i9];
    }
    break;

   case 29:
    for (i9 = 0; i9 < 4; i9++) {
      c_x[i9] = 250000;
    }

    memcpy(&k_y[0], &c_x[0], (size_t)16 * sizeof(uint8_T));
    PUP_packProperty(intenc, k_y, p_y);
    *psize = 24;
    *pbytes_sizes = 24;
    for (i9 = 0; i9 < 24; i9++) {
      pbytes_data[i9] = p_y[i9];
    }
    break;

   default:
    q_y = NULL;
    m6 = emlrtCreateCharArray(2, iv36);
    for (i9 = 0; i9 < 40; i9++) {
      cv76[i9] = cv77[i9];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 40, m6, cv76);
    emlrtAssign(&q_y, m6);
    u_sizes[0] = 1;
    u_sizes[1] = what_sizes[1];
    intval = what_sizes[0] * what_sizes[1];
    for (i9 = 0; i9 < intval; i9++) {
      u_data[i9] = what_data[i9];
    }

    r_y = NULL;
    m6 = emlrtCreateCharArray(2, u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m6, (char_T *)
      &u_data);
    emlrtAssign(&r_y, m6);
    b_error(q_y, r_y, &j_emlrtMCI);
    break;
  }
}

static int32_T SDRTxZynqFMC23Base_getIntEncoding(comm_internal_SDRTxZC706FMC23SL
  *obj, char_T what_data[], int32_T what_sizes[2])
{
  int32_T intenc;
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m7;
  static const int32_T iv37[2] = { 1, 51 };

  char_T cv78[51];
  int32_T i11;
  int32_T loop_ub;
  char_T u_data[21];
  int32_T u_sizes[2];
  static char_T cv79[51] = { 'p', 'r', 'o', 'p', 'e', 'r', 't', 'y', ' ', 'd',
    'o', 'e', 's', ' ', 'n', 'o', 't', ' ', 'h', 'a', 'v', 'e', ' ', 'a', 'n',
    ' ', 'i', 'n', 't', ' ', 'e', 'n', 'c', 'o', 'd', 'i', 'n', 'g', ' ', 'f',
    'u', 'n', 'c', 't', 'i', 'o', 'n', ':', ' ', '%', 's' };

  (void)obj;
  switch ((int32_T)b_eml_switch_helper(what_data, what_sizes)) {
   case 0:
    intenc = 66048;
    break;

   case 1:
    intenc = 66049;
    break;

   case 2:
    intenc = 131328;
    break;

   case 3:
    intenc = 131329;
    break;

   case 4:
    intenc = 131330;
    break;

   case 5:
    intenc = 196609;
    break;

   case 6:
    intenc = 196610;
    break;

   case 7:
    intenc = 196611;
    break;

   case 8:
    intenc = 196612;
    break;

   case 9:
    intenc = 196613;
    break;

   case 10:
    intenc = 196614;
    break;

   case 11:
    intenc = 196615;
    break;

   case 12:
    intenc = 196864;
    break;

   case 13:
    intenc = 262659;
    break;

   case 14:
    intenc = 328193;
    break;

   case 15:
    intenc = 328194;
    break;

   case 16:
    intenc = 328195;
    break;

   case 17:
    intenc = 328196;
    break;

   case 18:
    intenc = 328463;
    break;

   case 19:
    intenc = 328464;
    break;

   case 20:
    intenc = 328465;
    break;

   case 21:
    intenc = 328466;
    break;

   case 22:
    intenc = 328467;
    break;

   case 23:
    intenc = 328468;
    break;

   case 24:
    intenc = 328469;
    break;

   case 25:
    intenc = 328470;
    break;

   case 26:
    intenc = 328197;
    break;

   case 27:
    intenc = 328198;
    break;

   case 28:
    intenc = 328199;
    break;

   case 29:
    intenc = 328200;
    break;

   default:
    y = NULL;
    m7 = emlrtCreateCharArray(2, iv37);
    for (i11 = 0; i11 < 51; i11++) {
      cv78[i11] = cv79[i11];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m7, cv78);
    emlrtAssign(&y, m7);
    u_sizes[0] = 1;
    u_sizes[1] = what_sizes[1];
    loop_ub = what_sizes[0] * what_sizes[1];
    for (i11 = 0; i11 < loop_ub; i11++) {
      u_data[i11] = what_data[i11];
    }

    b_y = NULL;
    m7 = emlrtCreateCharArray(2, u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m7, (char_T *)
      &u_data);
    emlrtAssign(&b_y, m7);
    b_error(y, b_y, &j_emlrtMCI);
    break;
  }

  return intenc;
}

static real_T b_eml_switch_helper(char_T expr_data[], int32_T expr_sizes[2])
{
  real_T b_index;
  boolean_T b_bool;
  int32_T k;
  int32_T exitg17;
  static char_T cv80[15] = { 'B', 'y', 'p', 'a', 's', 's', 'U', 's', 'e', 'r',
    'L', 'o', 'g', 'i', 'c' };

  int32_T exitg16;
  static char_T cv81[15] = { 'C', 'e', 'n', 't', 'e', 'r', 'F', 'r', 'e', 'q',
    'u', 'e', 'n', 'c', 'y' };

  int32_T exitg15;
  static char_T cv82[11] = { 'A', 't', 't', 'e', 'n', 'u', 'a', 't', 'i', 'o',
    'n' };

  int32_T exitg14;
  static char_T cv83[18] = { 'B', 'a', 's', 'e', 'b', 'a', 'n', 'd', 'S', 'a',
    'm', 'p', 'l', 'e', 'R', 'a', 't', 'e' };

  int32_T exitg13;
  static char_T cv84[13] = { 'N', 'u', 'm', 'H', 'W', 'C', 'h', 'a', 'n', 'n',
    'e', 'l', 's' };

  int32_T exitg12;
  static char_T cv85[18] = { 'F', 'I', 'R', 'C', 'o', 'e', 'f', 'f', 'i', 'c',
    'i', 'e', 'n', 't', 'S', 'i', 'z', 'e' };

  int32_T exitg11;
  static char_T cv86[15] = { 'F', 'I', 'R', 'C', 'o', 'e', 'f', 'f', 'i', 'c',
    'i', 'e', 'n', 't', 's' };

  int32_T exitg10;
  static char_T cv87[7] = { 'F', 'I', 'R', 'G', 'a', 'i', 'n' };

  int32_T exitg9;
  static char_T cv88[20] = { 'F', 'I', 'R', 'D', 'e', 'c', 'i', 'm', 'I', 'n',
    't', 'e', 'r', 'p', 'F', 'a', 'c', 't', 'o', 'r' };

  int32_T exitg8;
  static char_T cv89[18] = { 'A', 'n', 'a', 'l', 'o', 'g', 'F', 'i', 'l', 't',
    'e', 'r', 'C', 'u', 't', 'o', 'f', 'f' };

  int32_T exitg7;
  static char_T cv90[15] = { 'F', 'i', 'l', 't', 'e', 'r', 'P', 'a', 't', 'h',
    'R', 'a', 't', 'e', 's' };

  int32_T exitg6;
  static char_T cv91[21] = { 'F', 'i', 'l', 't', 'e', 'r', 'D', 'e', 's', 'i',
    'g', 'n', 'T', 'y', 'p', 'e', 'F', 'o', 'r', 'T', 'x' };

  int32_T exitg5;
  static char_T cv92[21] = { 'F', 'i', 'l', 't', 'e', 'r', 'D', 'e', 's', 'i',
    'g', 'n', 'T', 'y', 'p', 'e', 'F', 'o', 'r', 'R', 'x' };

  int32_T exitg4;
  static char_T cv93[16] = { 'D', 'a', 't', 'a', 'S', 'o', 'u', 'r', 'c', 'e',
    'S', 'e', 'l', 'e', 'c', 't' };

  int32_T exitg3;
  static char_T cv94[12] = { 'D', 'D', 'S', 'F', 'r', 'e', 'q', 'u', 'e', 'n',
    'c', 'y' };

  int32_T exitg2;
  static char_T cv95[8] = { 'D', 'D', 'S', 'P', 'h', 'a', 's', 'e' };

  int32_T exitg1;
  static char_T cv96[8] = { 'D', 'D', 'S', 'S', 'c', 'a', 'l', 'e' };

  if (eml_strcmp(expr_data, expr_sizes)) {
    b_index = 0.0;
  } else if (b_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 1.0;
  } else if (c_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 2.0;
  } else if (d_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 3.0;
  } else if (e_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 4.0;
  } else if (f_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 5.0;
  } else if (g_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 6.0;
  } else if (h_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 7.0;
  } else if (i_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 8.0;
  } else if (j_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 9.0;
  } else if (k_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 10.0;
  } else if (l_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 11.0;
  } else if (m_eml_strcmp(expr_data, expr_sizes)) {
    b_index = 12.0;
  } else {
    b_bool = false;
    if (expr_sizes[1] == 15) {
      k = 0;
      do {
        exitg17 = 0;
        if (k <= 14) {
          if (expr_data[k] != cv80[k]) {
            exitg17 = 1;
          } else {
            k++;
          }
        } else {
          b_bool = true;
          exitg17 = 1;
        }
      } while (exitg17 == 0);
    }

    if (b_bool) {
      b_index = 13.0;
    } else {
      b_bool = false;
      if (expr_sizes[1] == 15) {
        k = 0;
        do {
          exitg16 = 0;
          if (k <= 14) {
            if (expr_data[k] != cv81[k]) {
              exitg16 = 1;
            } else {
              k++;
            }
          } else {
            b_bool = true;
            exitg16 = 1;
          }
        } while (exitg16 == 0);
      }

      if (b_bool) {
        b_index = 14.0;
      } else {
        b_bool = false;
        if (expr_sizes[1] == 11) {
          k = 0;
          do {
            exitg15 = 0;
            if (k <= 10) {
              if (expr_data[k] != cv82[k]) {
                exitg15 = 1;
              } else {
                k++;
              }
            } else {
              b_bool = true;
              exitg15 = 1;
            }
          } while (exitg15 == 0);
        }

        if (b_bool) {
          b_index = 15.0;
        } else {
          b_bool = false;
          if (expr_sizes[1] == 18) {
            k = 0;
            do {
              exitg14 = 0;
              if (k <= 17) {
                if (expr_data[k] != cv83[k]) {
                  exitg14 = 1;
                } else {
                  k++;
                }
              } else {
                b_bool = true;
                exitg14 = 1;
              }
            } while (exitg14 == 0);
          }

          if (b_bool) {
            b_index = 16.0;
          } else {
            b_bool = false;
            if (expr_sizes[1] == 13) {
              k = 0;
              do {
                exitg13 = 0;
                if (k <= 12) {
                  if (expr_data[k] != cv84[k]) {
                    exitg13 = 1;
                  } else {
                    k++;
                  }
                } else {
                  b_bool = true;
                  exitg13 = 1;
                }
              } while (exitg13 == 0);
            }

            if (b_bool) {
              b_index = 17.0;
            } else {
              b_bool = false;
              if (expr_sizes[1] == 18) {
                k = 0;
                do {
                  exitg12 = 0;
                  if (k <= 17) {
                    if (expr_data[k] != cv85[k]) {
                      exitg12 = 1;
                    } else {
                      k++;
                    }
                  } else {
                    b_bool = true;
                    exitg12 = 1;
                  }
                } while (exitg12 == 0);
              }

              if (b_bool) {
                b_index = 18.0;
              } else {
                b_bool = false;
                if (expr_sizes[1] == 15) {
                  k = 0;
                  do {
                    exitg11 = 0;
                    if (k <= 14) {
                      if (expr_data[k] != cv86[k]) {
                        exitg11 = 1;
                      } else {
                        k++;
                      }
                    } else {
                      b_bool = true;
                      exitg11 = 1;
                    }
                  } while (exitg11 == 0);
                }

                if (b_bool) {
                  b_index = 19.0;
                } else {
                  b_bool = false;
                  if (expr_sizes[1] == 7) {
                    k = 0;
                    do {
                      exitg10 = 0;
                      if (k <= 6) {
                        if (expr_data[k] != cv87[k]) {
                          exitg10 = 1;
                        } else {
                          k++;
                        }
                      } else {
                        b_bool = true;
                        exitg10 = 1;
                      }
                    } while (exitg10 == 0);
                  }

                  if (b_bool) {
                    b_index = 20.0;
                  } else {
                    b_bool = false;
                    if (expr_sizes[1] == 20) {
                      k = 0;
                      do {
                        exitg9 = 0;
                        if (k <= 19) {
                          if (expr_data[k] != cv88[k]) {
                            exitg9 = 1;
                          } else {
                            k++;
                          }
                        } else {
                          b_bool = true;
                          exitg9 = 1;
                        }
                      } while (exitg9 == 0);
                    }

                    if (b_bool) {
                      b_index = 21.0;
                    } else {
                      b_bool = false;
                      if (expr_sizes[1] == 18) {
                        k = 0;
                        do {
                          exitg8 = 0;
                          if (k <= 17) {
                            if (expr_data[k] != cv89[k]) {
                              exitg8 = 1;
                            } else {
                              k++;
                            }
                          } else {
                            b_bool = true;
                            exitg8 = 1;
                          }
                        } while (exitg8 == 0);
                      }

                      if (b_bool) {
                        b_index = 22.0;
                      } else {
                        b_bool = false;
                        if (expr_sizes[1] == 15) {
                          k = 0;
                          do {
                            exitg7 = 0;
                            if (k <= 14) {
                              if (expr_data[k] != cv90[k]) {
                                exitg7 = 1;
                              } else {
                                k++;
                              }
                            } else {
                              b_bool = true;
                              exitg7 = 1;
                            }
                          } while (exitg7 == 0);
                        }

                        if (b_bool) {
                          b_index = 23.0;
                        } else {
                          b_bool = false;
                          if (expr_sizes[1] == 21) {
                            k = 0;
                            do {
                              exitg6 = 0;
                              if (k <= 20) {
                                if (expr_data[k] != cv91[k]) {
                                  exitg6 = 1;
                                } else {
                                  k++;
                                }
                              } else {
                                b_bool = true;
                                exitg6 = 1;
                              }
                            } while (exitg6 == 0);
                          }

                          if (b_bool) {
                            b_index = 24.0;
                          } else {
                            b_bool = false;
                            if (expr_sizes[1] == 21) {
                              k = 0;
                              do {
                                exitg5 = 0;
                                if (k <= 20) {
                                  if (expr_data[k] != cv92[k]) {
                                    exitg5 = 1;
                                  } else {
                                    k++;
                                  }
                                } else {
                                  b_bool = true;
                                  exitg5 = 1;
                                }
                              } while (exitg5 == 0);
                            }

                            if (b_bool) {
                              b_index = 25.0;
                            } else {
                              b_bool = false;
                              if (expr_sizes[1] == 16) {
                                k = 0;
                                do {
                                  exitg4 = 0;
                                  if (k <= 15) {
                                    if (expr_data[k] != cv93[k]) {
                                      exitg4 = 1;
                                    } else {
                                      k++;
                                    }
                                  } else {
                                    b_bool = true;
                                    exitg4 = 1;
                                  }
                                } while (exitg4 == 0);
                              }

                              if (b_bool) {
                                b_index = 26.0;
                              } else {
                                b_bool = false;
                                if (expr_sizes[1] == 12) {
                                  k = 0;
                                  do {
                                    exitg3 = 0;
                                    if (k <= 11) {
                                      if (expr_data[k] != cv94[k]) {
                                        exitg3 = 1;
                                      } else {
                                        k++;
                                      }
                                    } else {
                                      b_bool = true;
                                      exitg3 = 1;
                                    }
                                  } while (exitg3 == 0);
                                }

                                if (b_bool) {
                                  b_index = 27.0;
                                } else {
                                  b_bool = false;
                                  if (expr_sizes[1] == 8) {
                                    k = 0;
                                    do {
                                      exitg2 = 0;
                                      if (k <= 7) {
                                        if (expr_data[k] != cv95[k]) {
                                          exitg2 = 1;
                                        } else {
                                          k++;
                                        }
                                      } else {
                                        b_bool = true;
                                        exitg2 = 1;
                                      }
                                    } while (exitg2 == 0);
                                  }

                                  if (b_bool) {
                                    b_index = 28.0;
                                  } else {
                                    b_bool = false;
                                    if (expr_sizes[1] == 8) {
                                      k = 0;
                                      do {
                                        exitg1 = 0;
                                        if (k <= 7) {
                                          if (expr_data[k] != cv96[k]) {
                                            exitg1 = 1;
                                          } else {
                                            k++;
                                          }
                                        } else {
                                          b_bool = true;
                                          exitg1 = 1;
                                        }
                                      } while (exitg1 == 0);
                                    }

                                    if (b_bool) {
                                      b_index = 29.0;
                                    } else {
                                      b_index = -1.0;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return b_index;
}

static boolean_T eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv97[13] = { 's', 'u', 'b', 'D', 'e', 'v', 'i', 'c', 'e', 'L',
    'i', 's', 't' };

  b_bool = false;
  if (a_sizes[1] == 13) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 12) {
        if (a_data[k] != cv97[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T b_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv98[16] = { 's', 'd', 'r', 'E', 'x', 'e', 'c', 'u', 't', 'i',
    'o', 'n', 'M', 'o', 'd', 'e' };

  b_bool = false;
  if (a_sizes[1] == 16) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 15) {
        if (a_data[k] != cv98[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T c_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv99[9] = { 'I', 'P', 'A', 'd', 'd', 'r', 'e', 's', 's' };

  b_bool = false;
  if (a_sizes[1] == 9) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 8) {
        if (a_data[k] != cv99[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T d_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv100[11] = { 'd', 'a', 't', 'a', 'U', 'D', 'P', 'P', 'o', 'r',
    't' };

  b_bool = false;
  if (a_sizes[1] == 11) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 10) {
        if (a_data[k] != cv100[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T e_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv101[11] = { 'c', 't', 'r', 'l', 'U', 'D', 'P', 'P', 'o', 'r',
    't' };

  b_bool = false;
  if (a_sizes[1] == 11) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 10) {
        if (a_data[k] != cv101[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T f_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv102[14] = { 'O', 'u', 't', 'p', 'u', 't', 'D', 'a', 't', 'a',
    'T', 'y', 'p', 'e' };

  b_bool = false;
  if (a_sizes[1] == 14) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 13) {
        if (a_data[k] != cv102[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T g_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv103[15] = { 'S', 'a', 'm', 'p', 'l', 'e', 's', 'P', 'e', 'r',
    'F', 'r', 'a', 'm', 'e' };

  b_bool = false;
  if (a_sizes[1] == 15) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 14) {
        if (a_data[k] != cv103[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T h_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv104[15] = { 'E', 'n', 'a', 'b', 'l', 'e', 'B', 'u', 'r', 's',
    't', 'M', 'o', 'd', 'e' };

  b_bool = false;
  if (a_sizes[1] == 15) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 14) {
        if (a_data[k] != cv104[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T i_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv105[16] = { 'N', 'u', 'm', 'F', 'r', 'a', 'm', 'e', 's', 'I',
    'n', 'B', 'u', 'r', 's', 't' };

  b_bool = false;
  if (a_sizes[1] == 16) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 15) {
        if (a_data[k] != cv105[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T j_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv106[13] = { 'D', 'a', 't', 'a', 'I', 's', 'C', 'o', 'm', 'p',
    'l', 'e', 'x' };

  b_bool = false;
  if (a_sizes[1] == 13) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 12) {
        if (a_data[k] != cv106[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T k_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv107[11] = { 'N', 'u', 'm', 'C', 'h', 'a', 'n', 'n', 'e', 'l',
    's' };

  b_bool = false;
  if (a_sizes[1] == 11) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 10) {
        if (a_data[k] != cv107[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T l_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv108[10] = { 'S', 'a', 'm', 'p', 'l', 'e', 'R', 'a', 't', 'e' };

  b_bool = false;
  if (a_sizes[1] == 10) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 9) {
        if (a_data[k] != cv108[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static boolean_T m_eml_strcmp(char_T a_data[], int32_T a_sizes[2])
{
  boolean_T b_bool;
  int32_T k;
  int32_T exitg1;
  static char_T cv109[11] = { 'L', 'o', 's', 't', 'S', 'a', 'm', 'p', 'l', 'e',
    's' };

  b_bool = false;
  if (a_sizes[1] == 11) {
    k = 0;
    do {
      exitg1 = 0;
      if (k <= 10) {
        if (a_data[k] != cv109[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

static void PUP_packProperty(int32_T intenc, uint8_T packedVal[16], uint8_T
  pbytes[24])
{
  uint8_T y[4];
  int32_T x;
  uint8_T b_y[4];
  memcpy(&y[0], &intenc, (size_t)4 * sizeof(uint8_T));
  x = 16;
  memcpy(&b_y[0], &x, (size_t)4 * sizeof(uint8_T));
  for (x = 0; x < 4; x++) {
    pbytes[x] = y[x];
  }

  for (x = 0; x < 4; x++) {
    pbytes[x + 4] = b_y[x];
  }

  for (x = 0; x < 16; x++) {
    pbytes[x + 8] = packedVal[x];
  }
}

static real_T SDRTxZynqFMC23Base_enumToInt(comm_internal_SDRTxZC706FMC23SL *obj,
  char_T what_data[], int32_T what_sizes[2])
{
  real_T intval;
  boolean_T err1;
  boolean_T err2;
  boolean_T b_bool;
  int32_T k;
  int32_T exitg9;
  static char_T cv110[16] = { 's', 'd', 'r', 'E', 'x', 'e', 'c', 'u', 't', 'i',
    'o', 'n', 'M', 'o', 'd', 'e' };

  int32_T exitg8;
  static char_T cv111[14] = { 'O', 'u', 't', 'p', 'u', 't', 'D', 'a', 't', 'a',
    'T', 'y', 'p', 'e' };

  int32_T exitg7;
  static char_T cv112[21] = { 'F', 'i', 'l', 't', 'e', 'r', 'D', 'e', 's', 'i',
    'g', 'n', 'T', 'y', 'p', 'e', 'F', 'o', 'r', 'T', 'x' };

  int32_T exitg6;
  static char_T cv113[21] = { 'F', 'i', 'l', 't', 'e', 'r', 'D', 'e', 's', 'i',
    'g', 'n', 'T', 'y', 'p', 'e', 'F', 'o', 'r', 'R', 'x' };

  int32_T exitg5;
  static char_T cv114[16] = { 'D', 'a', 't', 'a', 'S', 'o', 'u', 'r', 'c', 'e',
    'S', 'e', 'l', 'e', 'c', 't' };

  char_T expr[6];
  int32_T exitg4;
  static char_T cv115[6] = { 's', 'i', 'n', 'g', 'l', 'e' };

  int32_T exitg3;
  static char_T cv116[6] = { 'd', 'o', 'u', 'b', 'l', 'e' };

  char_T b_expr[13];
  int32_T exitg2;
  static char_T cv117[13] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i', 'l',
    't', 'e', 'r' };

  int32_T c_bool;
  char_T c_expr[22];
  int32_T exitg1;
  static char_T cv118[22] = { 'D', 'e', 'f', 'a', 'u', 'l', 't', 'F', 'i', 'l',
    't', 'e', 'r', 'F', 'r', 'o', 'm', 'O', 't', 'h', 'e', 'r' };

  int32_T d_bool;
  const mxArray *y;
  static const int32_T iv38[2] = { 1, 45 };

  const mxArray *m8;
  char_T cv119[45];
  static char_T cv120[45] = { 'b', 'a', 'd', ' ', 's', 't', 'r', 'i', 'n', 'g',
    ' ', 'v', 'a', 'l', ' ', 'i', 'n', ' ', 'S', 't', 'r', 'i', 'n', 'g', 'S',
    'e', 't', ' ', 't', 'o', ' ', 'i', 'n', 't', ' ', 'c', 'o', 'n', 'v', 'e',
    'r', 's', 'i', 'o', 'n' };

  const mxArray *b_y;
  static const int32_T iv39[2] = { 1, 44 };

  char_T cv121[44];
  static char_T cv122[44] = { 'b', 'a', 'd', ' ', 'p', 'r', 'o', 'p', ' ', 'n',
    'a', 'm', 'e', ' ', 'i', 'n', ' ', 'S', 't', 'r', 'i', 'n', 'g', 'S', 'e',
    't', ' ', 't', 'o', ' ', 'i', 'n', 't', ' ', 'c', 'o', 'n', 'v', 'e', 'r',
    's', 'i', 'o', 'n' };

  intval = 0.0;
  err1 = false;
  err2 = false;
  b_bool = false;
  if (what_sizes[1] == 16) {
    k = 0;
    do {
      exitg9 = 0;
      if (k <= 15) {
        if (what_data[k] != cv110[k]) {
          exitg9 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg9 = 1;
      }
    } while (exitg9 == 0);
  }

  if (b_bool) {
    k = 0;
  } else {
    b_bool = false;
    if (what_sizes[1] == 14) {
      k = 0;
      do {
        exitg8 = 0;
        if (k <= 13) {
          if (what_data[k] != cv111[k]) {
            exitg8 = 1;
          } else {
            k++;
          }
        } else {
          b_bool = true;
          exitg8 = 1;
        }
      } while (exitg8 == 0);
    }

    if (b_bool) {
      k = 1;
    } else {
      b_bool = false;
      if (what_sizes[1] == 21) {
        k = 0;
        do {
          exitg7 = 0;
          if (k <= 20) {
            if (what_data[k] != cv112[k]) {
              exitg7 = 1;
            } else {
              k++;
            }
          } else {
            b_bool = true;
            exitg7 = 1;
          }
        } while (exitg7 == 0);
      }

      if (b_bool) {
        k = 2;
      } else {
        b_bool = false;
        if (what_sizes[1] == 21) {
          k = 0;
          do {
            exitg6 = 0;
            if (k <= 20) {
              if (what_data[k] != cv113[k]) {
                exitg6 = 1;
              } else {
                k++;
              }
            } else {
              b_bool = true;
              exitg6 = 1;
            }
          } while (exitg6 == 0);
        }

        if (b_bool) {
          k = 3;
        } else {
          b_bool = false;
          if (what_sizes[1] == 16) {
            k = 0;
            do {
              exitg5 = 0;
              if (k <= 15) {
                if (what_data[k] != cv114[k]) {
                  exitg5 = 1;
                } else {
                  k++;
                }
              } else {
                b_bool = true;
                exitg5 = 1;
              }
            } while (exitg5 == 0);
          }

          if (b_bool) {
            k = 4;
          } else {
            k = -1;
          }
        }
      }
    }
  }

  switch (k) {
   case 0:
    intval = 1.0;
    break;

   case 1:
    for (k = 0; k < 6; k++) {
      expr[k] = obj->OutputDataType[k];
    }

    b_bool = false;
    k = 0;
    do {
      exitg4 = 0;
      if (k < 6) {
        if (expr[k] != cv115[k]) {
          exitg4 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg4 = 1;
      }
    } while (exitg4 == 0);

    if (b_bool) {
      k = 1;
    } else {
      b_bool = false;
      k = 0;
      do {
        exitg3 = 0;
        if (k < 6) {
          if (expr[k] != cv116[k]) {
            exitg3 = 1;
          } else {
            k++;
          }
        } else {
          b_bool = true;
          exitg3 = 1;
        }
      } while (exitg3 == 0);

      if (b_bool) {
        k = 2;
      } else {
        k = -1;
      }
    }

    switch (k) {
     case 0:
      intval = 3.0;
      break;

     case 1:
      intval = 4.0;
      break;

     case 2:
      intval = 5.0;
      break;

     default:
      err1 = true;
      break;
    }
    break;

   case 2:
    for (k = 0; k < 13; k++) {
      b_expr[k] = obj->FilterDesignTypeForTx[k];
    }

    b_bool = false;
    k = 0;
    do {
      exitg2 = 0;
      if (k < 13) {
        if (b_expr[k] != cv117[k]) {
          exitg2 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    if (b_bool) {
      c_bool = 0;
    } else {
      c_bool = -1;
    }

    switch (c_bool) {
     case 0:
      break;

     case 1:
      intval = 1.0;
      break;

     case 2:
      intval = 2.0;
      break;

     case 3:
      intval = 3.0;
      break;

     case 4:
      intval = -1.0;
      break;

     default:
      err1 = true;
      break;
    }
    break;

   case 3:
    for (k = 0; k < 22; k++) {
      c_expr[k] = obj->FilterDesignTypeForRx[k];
    }

    b_bool = false;
    k = 0;
    do {
      exitg1 = 0;
      if (k < 22) {
        if (c_expr[k] != cv118[k]) {
          exitg1 = 1;
        } else {
          k++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);

    if (b_bool) {
      d_bool = 1;
    } else {
      d_bool = -1;
    }

    switch (d_bool) {
     case 0:
      break;

     case 1:
      intval = 1.0;
      break;

     case 2:
      intval = 2.0;
      break;

     case 3:
      intval = 3.0;
      break;

     case 4:
      intval = -1.0;
      break;

     default:
      err1 = true;
      break;
    }
    break;

   case 4:
    break;

   default:
    err2 = true;
    break;
  }

  if (err1) {
    y = NULL;
    m8 = emlrtCreateCharArray(2, iv38);
    for (k = 0; k < 45; k++) {
      cv119[k] = cv120[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m8, cv119);
    emlrtAssign(&y, m8);
    error(y, &j_emlrtMCI);
  }

  if (err2) {
    b_y = NULL;
    m8 = emlrtCreateCharArray(2, iv39);
    for (k = 0; k < 44; k++) {
      cv121[k] = cv122[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 44, m8, cv121);
    emlrtAssign(&b_y, m8);
    error(b_y, &j_emlrtMCI);
  }

  return intval;
}

static void SDRTxZynqFMC23Base_packNontunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes_data[],
   int32_T *pbytes_sizes)
{
  uint8_T pb1[1024];
  int32_T ps1;
  uint8_T pb2[1024];
  int32_T ps2;
  int64_T i12;
  int32_T loop_ub;
  SDRSystemBase_packPropertyList(obj, &ps1, pb1);
  b_SDRSystemBase_packPropertyList(obj, &ps2, pb2);
  i12 = (int64_T)ps1 + (int64_T)ps2;
  if (i12 > 2147483647LL) {
    i12 = 2147483647LL;
  } else {
    if (i12 < -2147483648LL) {
      i12 = -2147483648LL;
    }
  }

  *psize = (int32_T)i12;
  if (1 > ps1) {
    loop_ub = 0;
  } else {
    loop_ub = emlrtDynamicBoundsCheckFastR2012b(ps1, 1, 1024, &c_emlrtBCI,
      emlrtRootTLSGlobal);
  }

  if (1 > ps2) {
    ps1 = 0;
  } else {
    ps1 = emlrtDynamicBoundsCheckFastR2012b(ps2, 1, 1024, &c_emlrtBCI,
      emlrtRootTLSGlobal);
  }

  *pbytes_sizes = loop_ub + ps1;
  for (ps2 = 0; ps2 < loop_ub; ps2++) {
    pbytes_data[ps2] = pb1[ps2];
  }

  for (ps2 = 0; ps2 < ps1; ps2++) {
    pbytes_data[ps2 + loop_ub] = pb2[ps2];
  }
}

static void SDRSystemBase_packPropertyList(comm_internal_SDRTxZC706FMC23SL *obj,
  int32_T *psize, uint8_T pbytes[1024])
{
  int32_T i13;
  int32_T tmp_data[520];
  int32_T p;
  char_T propList[15];
  static char_T b_propList[75] = { 'O', 'S', 'D', 'N', 'S', 'u', 'a', 'a', 'u',
    'a', 't', 'm', 't', 'm', 'm', 'p', 'p', 'a', 'C', 'p', 'u', 'l', 'I', 'h',
    'l', 't', 'e', 's', 'a', 'e', 'D', 's', 'C', 'n', 'R', 'a', 'P', 'o', 'n',
    'a', 't', 'e', 'm', 'e', 't', 'a', 'r', 'p', 'l', 'e', 'T', 'F', 'l', 's',
    ' ', 'y', 'r', 'e', ' ', ' ', 'p', 'a', 'x', ' ', ' ', 'e', 'm', ' ', ' ',
    ' ', ' ', 'e', ' ', ' ', ' ' };

  int32_T tmp_sizes[2];
  char_T b_tmp_data[15];
  int32_T pb_sizes;
  uint8_T pb_data[520];
  int32_T ps;
  int64_T i14;
  *psize = 0;
  for (i13 = 0; i13 < 1024; i13++) {
    pbytes[i13] = 0;
  }

  for (p = 0; p < 5; p++) {
    for (i13 = 0; i13 < 15; i13++) {
      propList[i13] = b_propList[p + 5 * i13];
    }

    deblank(propList, b_tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(obj, b_tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    for (i13 = 0; i13 < ps; i13++) {
      i14 = (int64_T)*psize + (int64_T)(1 + i13);
      if (i14 > 2147483647LL) {
        i14 = 2147483647LL;
      } else {
        if (i14 < -2147483648LL) {
          i14 = -2147483648LL;
        }
      }

      tmp_data[i13] = emlrtDynamicBoundsCheckFastR2012b((int32_T)i14, 1, 1024,
        &b_emlrtBCI, emlrtRootTLSGlobal);
    }

    emlrtSizeEqCheck1DFastR2012b(ps, pb_sizes, &j_emlrtECI, emlrtRootTLSGlobal);
    for (i13 = 0; i13 < ps; i13++) {
      pbytes[tmp_data[i13] - 1] = pb_data[i13];
    }

    i14 = (int64_T)*psize + (int64_T)ps;
    if (i14 > 2147483647LL) {
      i14 = 2147483647LL;
    } else {
      if (i14 < -2147483648LL) {
        i14 = -2147483648LL;
      }
    }

    *psize = (int32_T)i14;
  }
}

static void deblank(char_T x[15], char_T y_data[], int32_T y_sizes[2])
{
  int32_T ncols;
  boolean_T exitg1;
  static boolean_T bv2[128] = { false, false, false, false, false, false, false,
    false, false, true, true, true, true, true, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, true, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false
  };

  boolean_T b2;
  int32_T i15;
  ncols = 15;
  exitg1 = false;
  while (exitg1 == false && ncols > 0) {
    if (!bv2[(uint8_T)x[ncols - 1]]) {
      b2 = false;
    } else {
      b2 = true;
    }

    if (b2) {
      ncols--;
    } else {
      exitg1 = true;
    }
  }

  if (1 > ncols) {
    ncols = 0;
  }

  y_sizes[0] = 1;
  y_sizes[1] = ncols;
  for (i15 = 0; i15 < ncols; i15++) {
    y_data[i15] = x[i15];
  }
}

static void b_SDRSystemBase_packPropertyList(comm_internal_SDRTxZC706FMC23SL
  *obj, int32_T *psize, uint8_T pbytes[1024])
{
  int32_T i16;
  int32_T tmp_data[520];
  int32_T p;
  char_T propList[21];
  static char_T b_propList[357] = { 'E', 'N', 'B', 'B', 'N', 'F', 'F', 'F', 'F',
    'A', 'F', 'F', 'F', 'D', 'D', 'D', 'D', 'n', 'u', 'y', 'a', 'u', 'I', 'I',
    'I', 'I', 'n', 'i', 'i', 'i', 'a', 'D', 'D', 'D', 'a', 'm', 'p', 's', 'm',
    'R', 'R', 'R', 'R', 'a', 'l', 'l', 'l', 't', 'S', 'S', 'S', 'b', 'F', 'a',
    'e', 'H', 'C', 'C', 'G', 'D', 'l', 't', 't', 't', 'a', 'F', 'P', 'S', 'l',
    'r', 's', 'b', 'W', 'o', 'o', 'a', 'e', 'o', 'e', 'e', 'e', 'S', 'r', 'h',
    'c', 'e', 'a', 's', 'a', 'C', 'e', 'e', 'i', 'c', 'g', 'r', 'r', 'r', 'o',
    'e', 'a', 'a', 'B', 'm', 'U', 'n', 'h', 'f', 'f', 'n', 'i', 'F', 'P', 'D',
    'D', 'u', 'q', 's', 'l', 'u', 'e', 's', 'd', 'a', 'f', 'f', ' ', 'm', 'i',
    'a', 'e', 'e', 'r', 'u', 'e', 'e', 'r', 's', 'e', 'S', 'n', 'i', 'i', ' ',
    'I', 'l', 't', 's', 's', 'c', 'e', ' ', ' ', 's', 'I', 'r', 'a', 'n', 'c',
    'c', ' ', 'n', 't', 'h', 'i', 'i', 'e', 'n', ' ', ' ', 't', 'n', 'L', 'm',
    'e', 'i', 'i', ' ', 't', 'e', 'R', 'g', 'g', 'S', 'c', ' ', ' ', 'M', 'B',
    'o', 'p', 'l', 'e', 'e', ' ', 'e', 'r', 'a', 'n', 'n', 'e', 'y', ' ', ' ',
    'o', 'u', 'g', 'l', 's', 'n', 'n', ' ', 'r', 'C', 't', 'T', 'T', 'l', ' ',
    ' ', ' ', 'd', 'r', 'i', 'e', ' ', 't', 't', ' ', 'p', 'u', 'e', 'y', 'y',
    'e', ' ', ' ', ' ', 'e', 's', 'c', 'R', ' ', 'S', 's', ' ', 'F', 't', 's',
    'p', 'p', 'c', ' ', ' ', ' ', ' ', 't', ' ', 'a', ' ', 'i', ' ', ' ', 'a',
    'o', ' ', 'e', 'e', 't', ' ', ' ', ' ', ' ', ' ', ' ', 't', ' ', 'z', ' ',
    ' ', 'c', 'f', ' ', 'F', 'F', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e', ' ',
    'e', ' ', ' ', 't', 'f', ' ', 'o', 'o', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', 'o', ' ', ' ', 'r', 'r', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'r', ' ', ' ', 'T', 'R', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'x', 'x', ' ',
    ' ', ' ', ' ' };

  int32_T tmp_sizes[2];
  char_T b_tmp_data[21];
  int32_T pb_sizes;
  uint8_T pb_data[520];
  int32_T ps;
  int64_T i17;
  *psize = 0;
  for (i16 = 0; i16 < 1024; i16++) {
    pbytes[i16] = 0;
  }

  for (p = 0; p < 17; p++) {
    for (i16 = 0; i16 < 21; i16++) {
      propList[i16] = b_propList[p + 17 * i16];
    }

    b_deblank(propList, b_tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(obj, b_tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    for (i16 = 0; i16 < ps; i16++) {
      i17 = (int64_T)*psize + (int64_T)(1 + i16);
      if (i17 > 2147483647LL) {
        i17 = 2147483647LL;
      } else {
        if (i17 < -2147483648LL) {
          i17 = -2147483648LL;
        }
      }

      tmp_data[i16] = emlrtDynamicBoundsCheckFastR2012b((int32_T)i17, 1, 1024,
        &b_emlrtBCI, emlrtRootTLSGlobal);
    }

    emlrtSizeEqCheck1DFastR2012b(ps, pb_sizes, &j_emlrtECI, emlrtRootTLSGlobal);
    for (i16 = 0; i16 < ps; i16++) {
      pbytes[tmp_data[i16] - 1] = pb_data[i16];
    }

    i17 = (int64_T)*psize + (int64_T)ps;
    if (i17 > 2147483647LL) {
      i17 = 2147483647LL;
    } else {
      if (i17 < -2147483648LL) {
        i17 = -2147483648LL;
      }
    }

    *psize = (int32_T)i17;
  }
}

static void b_deblank(char_T x[21], char_T y_data[], int32_T y_sizes[2])
{
  int32_T ncols;
  boolean_T exitg1;
  static boolean_T bv3[128] = { false, false, false, false, false, false, false,
    false, false, true, true, true, true, true, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, true, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false,
    false, false, false, false, false, false, false, false, false, false, false
  };

  boolean_T b3;
  int32_T i18;
  ncols = 21;
  exitg1 = false;
  while (exitg1 == false && ncols > 0) {
    if (!bv3[(uint8_T)x[ncols - 1]]) {
      b3 = false;
    } else {
      b3 = true;
    }

    if (b3) {
      ncols--;
    } else {
      exitg1 = true;
    }
  }

  if (1 > ncols) {
    ncols = 0;
  }

  y_sizes[0] = 1;
  y_sizes[1] = ncols;
  for (i18 = 0; i18 < ncols; i18++) {
    y_data[i18] = x[i18];
  }
}

static void SDRTxZynqFMC23Base_packTunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj, int32_T *psize, uint8_T pbytes[1024])
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T i19;
  int32_T tmp_data[520];
  int32_T p;
  char_T propList[15];
  static char_T b_propList[30] = { 'C', 'A', 'e', 't', 'n', 't', 't', 'e', 'e',
    'n', 'r', 'u', 'F', 'a', 'r', 't', 'e', 'i', 'q', 'o', 'u', 'n', 'e', ' ',
    'n', ' ', 'c', ' ', 'y', ' ' };

  int32_T tmp_sizes[2];
  char_T b_tmp_data[15];
  int32_T pb_sizes;
  uint8_T pb_data[520];
  int32_T ps;
  int64_T i20;
  b_obj = obj;
  *psize = 0;
  for (i19 = 0; i19 < 1024; i19++) {
    pbytes[i19] = 0;
  }

  for (p = 0; p < 2; p++) {
    for (i19 = 0; i19 < 15; i19++) {
      propList[i19] = b_propList[p + (i19 << 1)];
    }

    deblank(propList, b_tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(b_obj, b_tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    for (i19 = 0; i19 < ps; i19++) {
      i20 = (int64_T)*psize + (int64_T)(1 + i19);
      if (i20 > 2147483647LL) {
        i20 = 2147483647LL;
      } else {
        if (i20 < -2147483648LL) {
          i20 = -2147483648LL;
        }
      }

      tmp_data[i19] = emlrtDynamicBoundsCheckFastR2012b((int32_T)i20, 1, 1024,
        &b_emlrtBCI, emlrtRootTLSGlobal);
    }

    emlrtSizeEqCheck1DFastR2012b(ps, pb_sizes, &j_emlrtECI, emlrtRootTLSGlobal);
    for (i19 = 0; i19 < ps; i19++) {
      pbytes[tmp_data[i19] - 1] = pb_data[i19];
    }

    i20 = (int64_T)*psize + (int64_T)ps;
    if (i20 > 2147483647LL) {
      i20 = 2147483647LL;
    } else {
      if (i20 < -2147483648LL) {
        i20 = -2147483648LL;
      }
    }

    *psize = (int32_T)i20;
  }
}

static void sdr_setupImpl(int32_T creationArgsSize, uint8_T creationArgs_data[],
  int32_T creationArgs_sizes, int32_T nonTunablePropsSize, uint8_T
  nonTunableProps_data[], int32_T nonTunableProps_sizes, int32_T
  tunablePropsSize, uint8_T tunableProps[1024], int32_T *driverHandle,
  SDRPluginStatusT *errStat, char_T errId[1024], char_T errStr[1024])
{
  int32_T i21;
  (void)creationArgs_sizes;
  (void)nonTunableProps_sizes;

  /*    Copyright 2014 The MathWorks, Inc. */
  /*  */
  /*  This function unifies handling of interp vs. codegen call as well as */
  /*  errStat / errStr assignment. */
  /*  */
  /*    Copyright 2011-2014 The MathWorks, Inc. */
  /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
  /*  FIXME: make variable */
  /*  FIXME: make variable */
  /*  function is being called in interpreted mode */
  /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
  /*  where device is unplugged in middle of stream. */
  /*  We really want to avoid this as it makes debug much more difficult. */
  /*  coder.ceval('mexLock'); */
  for (i21 = 0; i21 < 1024; i21++) {
    errId[i21] = '\x00';
    errStr[i21] = '\x00';
  }

  setupImpl_c(creationArgsSize, &creationArgs_data[0], nonTunablePropsSize,
              &nonTunableProps_data[0], tunablePropsSize, tunableProps,
              driverHandle, errStat, errId, errStr);
}

static void SDRTxZynqFMC23Base_setNontunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T p;
  char_T propList[21];
  int32_T pb_sizes;
  static char_T b_propList[357] = { 'E', 'N', 'B', 'B', 'N', 'F', 'F', 'F', 'F',
    'A', 'F', 'F', 'F', 'D', 'D', 'D', 'D', 'n', 'u', 'y', 'a', 'u', 'I', 'I',
    'I', 'I', 'n', 'i', 'i', 'i', 'a', 'D', 'D', 'D', 'a', 'm', 'p', 's', 'm',
    'R', 'R', 'R', 'R', 'a', 'l', 'l', 'l', 't', 'S', 'S', 'S', 'b', 'F', 'a',
    'e', 'H', 'C', 'C', 'G', 'D', 'l', 't', 't', 't', 'a', 'F', 'P', 'S', 'l',
    'r', 's', 'b', 'W', 'o', 'o', 'a', 'e', 'o', 'e', 'e', 'e', 'S', 'r', 'h',
    'c', 'e', 'a', 's', 'a', 'C', 'e', 'e', 'i', 'c', 'g', 'r', 'r', 'r', 'o',
    'e', 'a', 'a', 'B', 'm', 'U', 'n', 'h', 'f', 'f', 'n', 'i', 'F', 'P', 'D',
    'D', 'u', 'q', 's', 'l', 'u', 'e', 's', 'd', 'a', 'f', 'f', ' ', 'm', 'i',
    'a', 'e', 'e', 'r', 'u', 'e', 'e', 'r', 's', 'e', 'S', 'n', 'i', 'i', ' ',
    'I', 'l', 't', 's', 's', 'c', 'e', ' ', ' ', 's', 'I', 'r', 'a', 'n', 'c',
    'c', ' ', 'n', 't', 'h', 'i', 'i', 'e', 'n', ' ', ' ', 't', 'n', 'L', 'm',
    'e', 'i', 'i', ' ', 't', 'e', 'R', 'g', 'g', 'S', 'c', ' ', ' ', 'M', 'B',
    'o', 'p', 'l', 'e', 'e', ' ', 'e', 'r', 'a', 'n', 'n', 'e', 'y', ' ', ' ',
    'o', 'u', 'g', 'l', 's', 'n', 'n', ' ', 'r', 'C', 't', 'T', 'T', 'l', ' ',
    ' ', ' ', 'd', 'r', 'i', 'e', ' ', 't', 't', ' ', 'p', 'u', 'e', 'y', 'y',
    'e', ' ', ' ', ' ', 'e', 's', 'c', 'R', ' ', 'S', 's', ' ', 'F', 't', 's',
    'p', 'p', 'c', ' ', ' ', ' ', ' ', 't', ' ', 'a', ' ', 'i', ' ', ' ', 'a',
    'o', ' ', 'e', 'e', 't', ' ', ' ', ' ', ' ', ' ', ' ', 't', ' ', 'z', ' ',
    ' ', 'c', 'f', ' ', 'F', 'F', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e', ' ',
    'e', ' ', ' ', 't', 'f', ' ', 'o', 'o', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', 'o', ' ', ' ', 'r', 'r', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'r', ' ', ' ', 'T', 'R', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'x', 'x', ' ',
    ' ', ' ', ' ' };

  int32_T tmp_sizes[2];
  char_T tmp_data[21];
  uint8_T pb_data[520];
  int32_T ps;
  int32_T driverHandle;
  char_T errId[1024];
  char_T errStr[1024];
  SDRPluginStatusT errStat_i;
  const mxArray *y;
  static const int32_T iv40[2] = { 1, 1024 };

  const mxArray *m9;
  const mxArray *b_y;
  static const int32_T iv41[2] = { 1, 1024 };

  const mxArray *varargin_1;
  SDRSystemBase_setPropertyList(obj);
  b_obj = obj;
  for (p = 0; p < 17; p++) {
    for (pb_sizes = 0; pb_sizes < 21; pb_sizes++) {
      propList[pb_sizes] = b_propList[p + 17 * pb_sizes];
    }

    b_deblank(propList, tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(b_obj, tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    driverHandle = b_obj->pDriverHandle;

    /*    Copyright 2013-2014 The MathWorks, Inc. */
    /*  */
    /*  This function unifies handling of interp vs. codegen call as well as */
    /*  errStat / errStr assignment. */
    /*  */
    /*    Copyright 2011-2014 The MathWorks, Inc. */
    /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
    /*  FIXME: make variable */
    /*  FIXME: make variable */
    /*  function is being called in interpreted mode */
    /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
    /*  where device is unplugged in middle of stream. */
    /*  We really want to avoid this as it makes debug much more difficult. */
    /*  coder.ceval('mexLock'); */
    for (pb_sizes = 0; pb_sizes < 1024; pb_sizes++) {
      errId[pb_sizes] = '\x00';
      errStr[pb_sizes] = '\x00';
    }

    setConfiguration_c(driverHandle, ps, &pb_data[0], &errStat_i, errId, errStr);
    if (errStat_i == SDRDriverError) {
      y = NULL;
      m9 = emlrtCreateCharArray(2, iv40);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m9, errId);
      emlrtAssign(&y, m9);
      b_y = NULL;
      m9 = emlrtCreateCharArray(2, iv41);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m9, errStr);
      emlrtAssign(&b_y, m9);
      varargin_1 = b_message(y, b_y, &i_emlrtMCI);
      isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
      error(emlrtAlias(varargin_1), &j_emlrtMCI);
      emlrtDestroyArray(&varargin_1);
    }
  }
}

static void SDRSystemBase_setPropertyList(comm_internal_SDRTxZC706FMC23SL *obj)
{
  int32_T p;
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  char_T propList[15];
  int32_T pb_sizes;
  static char_T b_propList[75] = { 'O', 'S', 'D', 'N', 'S', 'u', 'a', 'a', 'u',
    'a', 't', 'm', 't', 'm', 'm', 'p', 'p', 'a', 'C', 'p', 'u', 'l', 'I', 'h',
    'l', 't', 'e', 's', 'a', 'e', 'D', 's', 'C', 'n', 'R', 'a', 'P', 'o', 'n',
    'a', 't', 'e', 'm', 'e', 't', 'a', 'r', 'p', 'l', 'e', 'T', 'F', 'l', 's',
    ' ', 'y', 'r', 'e', ' ', ' ', 'p', 'a', 'x', ' ', ' ', 'e', 'm', ' ', ' ',
    ' ', ' ', 'e', ' ', ' ', ' ' };

  int32_T tmp_sizes[2];
  char_T tmp_data[15];
  uint8_T pb_data[520];
  int32_T ps;
  int32_T driverHandle;
  char_T errId[1024];
  char_T errStr[1024];
  SDRPluginStatusT errStat_i;
  const mxArray *y;
  static const int32_T iv42[2] = { 1, 1024 };

  const mxArray *m10;
  const mxArray *b_y;
  static const int32_T iv43[2] = { 1, 1024 };

  const mxArray *varargin_1;
  for (p = 0; p < 5; p++) {
    b_obj = obj;
    for (pb_sizes = 0; pb_sizes < 15; pb_sizes++) {
      propList[pb_sizes] = b_propList[p + 5 * pb_sizes];
    }

    deblank(propList, tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(b_obj, tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    driverHandle = b_obj->pDriverHandle;

    /*    Copyright 2013-2014 The MathWorks, Inc. */
    /*  */
    /*  This function unifies handling of interp vs. codegen call as well as */
    /*  errStat / errStr assignment. */
    /*  */
    /*    Copyright 2011-2014 The MathWorks, Inc. */
    /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
    /*  FIXME: make variable */
    /*  FIXME: make variable */
    /*  function is being called in interpreted mode */
    /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
    /*  where device is unplugged in middle of stream. */
    /*  We really want to avoid this as it makes debug much more difficult. */
    /*  coder.ceval('mexLock'); */
    for (pb_sizes = 0; pb_sizes < 1024; pb_sizes++) {
      errId[pb_sizes] = '\x00';
      errStr[pb_sizes] = '\x00';
    }

    setConfiguration_c(driverHandle, ps, &pb_data[0], &errStat_i, errId, errStr);
    if (errStat_i == SDRDriverError) {
      y = NULL;
      m10 = emlrtCreateCharArray(2, iv42);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m10, errId);
      emlrtAssign(&y, m10);
      b_y = NULL;
      m10 = emlrtCreateCharArray(2, iv43);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m10, errStr);
      emlrtAssign(&b_y, m10);
      varargin_1 = b_message(y, b_y, &i_emlrtMCI);
      isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
      error(emlrtAlias(varargin_1), &j_emlrtMCI);
      emlrtDestroyArray(&varargin_1);
    }
  }
}

static void SDRTxZynqFMC23Base_setTunableGroupProps
  (comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T p;
  char_T propList[15];
  int32_T pb_sizes;
  static char_T b_propList[30] = { 'C', 'A', 'e', 't', 'n', 't', 't', 'e', 'e',
    'n', 'r', 'u', 'F', 'a', 'r', 't', 'e', 'i', 'q', 'o', 'u', 'n', 'e', ' ',
    'n', ' ', 'c', ' ', 'y', ' ' };

  int32_T tmp_sizes[2];
  char_T tmp_data[15];
  uint8_T pb_data[520];
  int32_T ps;
  int32_T driverHandle;
  char_T errId[1024];
  char_T errStr[1024];
  SDRPluginStatusT errStat_i;
  const mxArray *y;
  static const int32_T iv44[2] = { 1, 1024 };

  const mxArray *m11;
  const mxArray *b_y;
  static const int32_T iv45[2] = { 1, 1024 };

  const mxArray *varargin_1;
  b_obj = obj;
  for (p = 0; p < 2; p++) {
    for (pb_sizes = 0; pb_sizes < 15; pb_sizes++) {
      propList[pb_sizes] = b_propList[p + (pb_sizes << 1)];
    }

    deblank(propList, tmp_data, tmp_sizes);
    SDRTxZynqFMC23Base_packProperty(b_obj, tmp_data, tmp_sizes, &ps, pb_data,
      &pb_sizes);
    driverHandle = b_obj->pDriverHandle;

    /*    Copyright 2013-2014 The MathWorks, Inc. */
    /*  */
    /*  This function unifies handling of interp vs. codegen call as well as */
    /*  errStat / errStr assignment. */
    /*  */
    /*    Copyright 2011-2014 The MathWorks, Inc. */
    /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
    /*  FIXME: make variable */
    /*  FIXME: make variable */
    /*  function is being called in interpreted mode */
    /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
    /*  where device is unplugged in middle of stream. */
    /*  We really want to avoid this as it makes debug much more difficult. */
    /*  coder.ceval('mexLock'); */
    for (pb_sizes = 0; pb_sizes < 1024; pb_sizes++) {
      errId[pb_sizes] = '\x00';
      errStr[pb_sizes] = '\x00';
    }

    setConfiguration_c(driverHandle, ps, &pb_data[0], &errStat_i, errId, errStr);
    if (errStat_i == SDRDriverError) {
      y = NULL;
      m11 = emlrtCreateCharArray(2, iv44);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m11, errId);
      emlrtAssign(&y, m11);
      b_y = NULL;
      m11 = emlrtCreateCharArray(2, iv45);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m11, errStr);
      emlrtAssign(&b_y, m11);
      varargin_1 = b_message(y, b_y, &i_emlrtMCI);
      isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
      error(emlrtAlias(varargin_1), &j_emlrtMCI);
      emlrtDestroyArray(&varargin_1);
    }
  }
}

static boolean_T SDRTxZynqFMC23SL_stepImpl(comm_internal_SDRTxZC706FMC23SL *obj,
  creal_T d[8192])
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  real_T b_d[16384];
  int32_T i22;
  char_T errStr[1024];
  char_T errId[1024];
  SDRPluginStatusT errStat;
  uint8_T metaData[5120];
  int32_T metaDataSize;
  const mxArray *y;
  static const int32_T iv46[2] = { 1, 1024 };

  const mxArray *m12;
  const mxArray *b_y;
  static const int32_T iv47[2] = { 1, 1024 };

  const mxArray *varargin_1;
  real_T metaPtr;
  uint8_T bytes[4];
  int32_T intenc;
  int32_T valsize;
  real_T d2;
  int64_T i23;
  b_obj = obj;
  SDRSystemBase_updateTunable(b_obj);
  for (i22 = 0; i22 < 8192; i22++) {
    b_d[i22] = d[i22].re;
  }

  for (i22 = 0; i22 < 8192; i22++) {
    b_d[i22 + 8192] = d[i22].im;
  }

  sdr_txStepImplDouble(b_obj->pDriverHandle, 4096, b_d, &metaDataSize, metaData,
                       &errStat, errId, errStr);
  if (errStat == SDRDriverError) {
    y = NULL;
    m12 = emlrtCreateCharArray(2, iv46);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m12, errId);
    emlrtAssign(&y, m12);
    b_y = NULL;
    m12 = emlrtCreateCharArray(2, iv47);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m12, errStr);
    emlrtAssign(&b_y, m12);
    varargin_1 = b_message(y, b_y, &i_emlrtMCI);
    isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
    error(emlrtAlias(varargin_1), &j_emlrtMCI);
    emlrtDestroyArray(&varargin_1);
  }

  metaPtr = 1.0;
  b_obj->pLostSamples = false;
  while (metaPtr < (real_T)metaDataSize) {
    for (i22 = 0; i22 < 4; i22++) {
      bytes[i22] = metaData[emlrtDynamicBoundsCheckFastR2012b((int32_T)(metaPtr
        + (real_T)i22), 1, 5120, &e_emlrtBCI, emlrtRootTLSGlobal) - 1];
    }

    memcpy(&intenc, &bytes[0], (size_t)1 * sizeof(int32_T));
    for (i22 = 0; i22 < 4; i22++) {
      bytes[i22] = metaData[emlrtDynamicBoundsCheckFastR2012b((int32_T)(metaPtr
        + (4.0 + (real_T)i22)), 1, 5120, &e_emlrtBCI, emlrtRootTLSGlobal) - 1];
    }

    memcpy(&valsize, &bytes[0], (size_t)1 * sizeof(int32_T));
    metaPtr += 8.0;
    switch (intenc) {
     case 196864:
      emlrtDynamicBoundsCheckFastR2012b((int32_T)metaPtr, MIN_int32_T,
        MAX_int32_T, &d_emlrtBCI, emlrtRootTLSGlobal);
      d2 = muDoubleScalarRound(metaPtr + (real_T)valsize);
      if (d2 < 2.147483648E+9) {
        if (d2 >= -2.147483648E+9) {
          i22 = (int32_T)d2;
        } else {
          i22 = MIN_int32_T;
        }
      } else {
        i22 = MAX_int32_T;
      }

      i23 = (int64_T)i22 - 1LL;
      if (i23 > 2147483647LL) {
        i23 = 2147483647LL;
      } else {
        if (i23 < -2147483648LL) {
          i23 = -2147483648LL;
        }
      }

      i22 = (int32_T)i23;
      if ((int32_T)metaPtr > i22) {
        intenc = 1;
        i22 = 1;
      } else {
        intenc = emlrtDynamicBoundsCheckFastR2012b((int32_T)metaPtr, 1, 5120,
          &e_emlrtBCI, emlrtRootTLSGlobal);
        i22 = emlrtDynamicBoundsCheckFastR2012b(i22, 1, 5120, &e_emlrtBCI,
          emlrtRootTLSGlobal) + 1;
      }

      emlrtDynamicBoundsCheckFastR2012b(1, 1, i22 - intenc, &f_emlrtBCI,
        emlrtRootTLSGlobal);
      b_obj->pLostSamples = metaData[intenc - 1] != 0;
      break;
    }

    metaPtr += (real_T)valsize;
  }

  b_obj = obj;
  return b_obj->pLostSamples;
}

static void SDRSystemBase_updateTunable(comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T i24;
  uint8_T tpb[1024];
  int32_T tps;
  boolean_T sameSize;
  int32_T loop_ub;
  int32_T b_loop_ub;
  uint8_T tmp_data[1024];
  boolean_T tpb_data[1024];
  boolean_T sameBuffer;
  char_T errId[1024];
  char_T errStr[1024];
  SDRPluginStatusT errStat_i;
  const mxArray *y;
  static const int32_T iv48[2] = { 1, 1024 };

  const mxArray *m13;
  const mxArray *b_y;
  static const int32_T iv49[2] = { 1, 1024 };

  const mxArray *varargin_1;
  b_obj = obj;
  b_obj->pCenterFrequency = b_obj->CenterFrequency;
  for (i24 = 0; i24 < 2; i24++) {
    b_obj->pAttenuation[i24] = b_obj->Attenuation[i24];
  }

  SDRTxZynqFMC23Base_packTunableGroupProps(obj, &tps, tpb);
  sameSize = tps == obj->pLastTunablePackedSize;
  if (sameSize) {
    if (1 > tps) {
      loop_ub = 0;
    } else {
      loop_ub = emlrtDynamicBoundsCheckFastR2012b(tps, 1, 1024, &b_emlrtBCI,
        emlrtRootTLSGlobal);
    }

    if (1 > tps) {
      b_loop_ub = 0;
    } else {
      b_loop_ub = tps;
    }

    for (i24 = 0; i24 < b_loop_ub; i24++) {
      tmp_data[i24] = obj->pLastTunablePackedBuffer[i24];
    }

    emlrtSizeEqCheck1DFastR2012b(loop_ub, b_loop_ub, &j_emlrtECI,
      emlrtRootTLSGlobal);
    for (i24 = 0; i24 < loop_ub; i24++) {
      tpb_data[i24] = tpb[i24] == tmp_data[i24];
    }

    sameBuffer = all(tpb_data, loop_ub);
  } else {
    sameBuffer = false;
  }

  if (!sameSize || !sameBuffer) {
    loop_ub = obj->pDriverHandle;

    /*    Copyright 2013-2014 The MathWorks, Inc. */
    /*  */
    /*  This function unifies handling of interp vs. codegen call as well as */
    /*  errStat / errStr assignment. */
    /*  */
    /*    Copyright 2011-2014 The MathWorks, Inc. */
    /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
    /*  FIXME: make variable */
    /*  FIXME: make variable */
    /*  function is being called in interpreted mode */
    /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
    /*  where device is unplugged in middle of stream. */
    /*  We really want to avoid this as it makes debug much more difficult. */
    /*  coder.ceval('mexLock'); */
    for (i24 = 0; i24 < 1024; i24++) {
      errId[i24] = '\x00';
      errStr[i24] = '\x00';
    }

    processTunedPropertiesImpl_c(loop_ub, tps, tpb, &errStat_i, errId, errStr);
    if (errStat_i == SDRDriverError) {
      y = NULL;
      m13 = emlrtCreateCharArray(2, iv48);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m13, errId);
      emlrtAssign(&y, m13);
      b_y = NULL;
      m13 = emlrtCreateCharArray(2, iv49);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m13, errStr);
      emlrtAssign(&b_y, m13);
      varargin_1 = b_message(y, b_y, &i_emlrtMCI);
      isempty(emlrtAlias(varargin_1), &k_emlrtMCI);
      error(emlrtAlias(varargin_1), &j_emlrtMCI);
      emlrtDestroyArray(&varargin_1);
    }

    obj->pLastTunablePackedSize = tps;
    for (i24 = 0; i24 < 1024; i24++) {
      obj->pLastTunablePackedBuffer[i24] = tpb[i24];
    }
  }
}

static boolean_T all(boolean_T x_data[], int32_T x_sizes)
{
  boolean_T y;
  boolean_T p;
  boolean_T b_p;
  int32_T k;
  int32_T exitg2;
  int32_T b_k;
  const mxArray *b_y;
  static const int32_T iv50[2] = { 1, 41 };

  const mxArray *m14;
  char_T cv123[41];
  static char_T cv124[41] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 'l', 'l', '_', 'o', 'r', '_',
    'a', 'n', 'y', '_', 's', 'p', 'e', 'c', 'i', 'a', 'l', 'E', 'm', 'p', 't',
    'y' };

  const mxArray *c_y;
  static const int32_T iv51[2] = { 1, 51 };

  char_T cv125[51];
  static char_T cv126[51] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'e', 'm', 'l', '_', 'a', 'l', 'l', '_', 'o', 'r', '_',
    'a', 'n', 'y', '_', 'a', 'u', 't', 'o', 'D', 'i', 'm', 'I', 'n', 'c', 'o',
    'm', 'p', 'a', 't', 'i', 'b', 'i', 'l', 'i', 't', 'y' };

  boolean_T exitg1;
  p = false;
  b_p = false;
  k = 1;
  do {
    exitg2 = 0;
    if (k - 1 < 2) {
      if (k <= 1) {
        b_k = x_sizes;
      } else {
        b_k = 1;
      }

      if (b_k != 0) {
        exitg2 = 1;
      } else {
        k++;
      }
    } else {
      b_p = true;
      exitg2 = 1;
    }
  } while (exitg2 == 0);

  if (b_p) {
    p = true;
  }

  if (p) {
    b_y = NULL;
    m14 = emlrtCreateCharArray(2, iv50);
    for (k = 0; k < 41; k++) {
      cv123[k] = cv124[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 41, m14, cv123);
    emlrtAssign(&b_y, m14);
    error(message(b_y, &l_emlrtMCI), &l_emlrtMCI);
  }

  if (x_sizes == 1 || x_sizes != 1) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    c_y = NULL;
    m14 = emlrtCreateCharArray(2, iv51);
    for (k = 0; k < 51; k++) {
      cv125[k] = cv126[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m14, cv125);
    emlrtAssign(&c_y, m14);
    error(message(c_y, &l_emlrtMCI), &l_emlrtMCI);
  }

  y = true;
  k = 0;
  exitg1 = false;
  while (exitg1 == false && k + 1 <= x_sizes) {
    if ((int32_T)x_data[k] == 0) {
      y = false;
      exitg1 = true;
    } else {
      k++;
    }
  }

  return y;
}

static void sdr_txStepImplDouble(int32_T driverHandle, int32_T dataSize, real_T
  data[16384], int32_T *metaDataSize, uint8_T metaData[5120], SDRPluginStatusT
  *errStat, char_T errId[1024], char_T errStr[1024])
{
  int32_T i25;
  (void)dataSize;

  /*    Copyright 2014 The MathWorks, Inc. */
  /*  */
  /*  This function unifies handling of interp vs. codegen call as well as */
  /*  errStat / errStr assignment. */
  /*  */
  /*    Copyright 2011-2014 The MathWorks, Inc. */
  /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
  /*  FIXME: make variable */
  /*  FIXME: make variable */
  /*  function is being called in interpreted mode */
  /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
  /*  where device is unplugged in middle of stream. */
  /*  We really want to avoid this as it makes debug much more difficult. */
  /*  coder.ceval('mexLock'); */
  for (i25 = 0; i25 < 1024; i25++) {
    errId[i25] = '\x00';
    errStr[i25] = '\x00';
  }

  txStepImplVoid_c(driverHandle, 4096, data, metaDataSize, metaData, errStat,
                   errId, errStr);
}

static void SDRSystemBase_releaseImpl(comm_internal_SDRTxZC706FMC23SL *obj)
{
  comm_internal_SDRTxZC706FMC23SL *b_obj;
  int32_T driverHandle;
  char_T errId[1024];
  char_T errStr[1024];
  int32_T i26;
  SDRPluginStatusT errStat_i;
  const mxArray *y;
  static const int32_T iv52[2] = { 1, 1024 };

  const mxArray *m15;
  const mxArray *b_y;
  static const int32_T iv53[2] = { 1, 1024 };

  b_obj = obj;
  if (SDRSystemBase_isConnected(b_obj)) {
    driverHandle = b_obj->pDriverHandle;

    /*    Copyright 2013-2014 The MathWorks, Inc. */
    /*  */
    /*  This function unifies handling of interp vs. codegen call as well as */
    /*  errStat / errStr assignment. */
    /*  */
    /*    Copyright 2011-2014 The MathWorks, Inc. */
    /*  THESE SIZES MUST MATCH THOSE VALUES IN THE C-CODE! */
    /*  FIXME: make variable */
    /*  FIXME: make variable */
    /*  function is being called in interpreted mode */
    /*  FIXME: Test that removing this is okay.  Believe issue was on Windows */
    /*  where device is unplugged in middle of stream. */
    /*  We really want to avoid this as it makes debug much more difficult. */
    /*  coder.ceval('mexLock'); */
    for (i26 = 0; i26 < 1024; i26++) {
      errId[i26] = '\x00';
      errStr[i26] = '\x00';
    }

    releaseImpl_c(driverHandle, &errStat_i, errId, errStr);
    if (errStat_i == SDRDriverError) {
      y = NULL;
      m15 = emlrtCreateCharArray(2, iv52);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m15, errId);
      emlrtAssign(&y, m15);
      b_y = NULL;
      m15 = emlrtCreateCharArray(2, iv53);
      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 1024, m15, errStr);
      emlrtAssign(&b_y, m15);
      warning(b_message(y, b_y, &i_emlrtMCI), &i_emlrtMCI);
    }

    b_obj->pDriverHandle = 0;
  }
}

static boolean_T SDRSystemBase_isConnected(comm_internal_SDRTxZC706FMC23SL *obj)
{
  return obj->pDriverHandle > 0;
}

static void cast(b_sIvmHumfM4VG8K4LjAjoqqB x[8], sIvmHumfM4VG8K4LjAjoqqB y_data
                 [8], sIvmHumfM4VG8K4LjAjoqqB_size y_elems_sizes[8])
{
  int32_T j;
  int32_T b_j;
  for (j = 0; j < 8; j++) {
    y_elems_sizes[j].names[0] = 1;
    y_elems_sizes[j].names[1] = 1;
    y_data[j].names[0] = x[j].names;
    emlrtDimSizeEqCheckFastR2012b(1, 1, &k_emlrtECI, emlrtRootTLSGlobal);
    emlrtDimSizeGeqCheckFastR2012b(4, 3, &l_emlrtECI, emlrtRootTLSGlobal);
    y_elems_sizes[j].dims[0] = 1;
    y_elems_sizes[j].dims[1] = 3;
    for (b_j = 0; b_j < 3; b_j++) {
      y_data[j].dims[b_j] = x[j].dims[b_j];
    }

    y_data[j].dType = x[j].dType;
    y_data[j].dTypeSize = x[j].dTypeSize;
    y_elems_sizes[j].dTypeName[0] = 1;
    y_elems_sizes[j].dTypeName[1] = 1;
    y_data[j].dTypeName[0] = x[j].dTypeName;
    y_data[j].dTypeIndex = x[j].dTypeIndex;
    y_elems_sizes[j].dTypeChksum[0] = 1;
    y_elems_sizes[j].dTypeChksum[1] = 1;
    y_data[j].dTypeChksum[0] = x[j].dTypeChksum;
    y_data[j].complexity = x[j].complexity;
  }
}

static void cgxe_mdl_start(InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance)
{
  int32_T mti;
  uint32_T r;
  real_T varargin_5[2];
  boolean_T flag;
  comm_internal_SDRTxZC706FMC23SL *obj;
  const mxArray *y;
  static const int32_T iv54[2] = { 1, 51 };

  const mxArray *m16;
  char_T cv127[51];
  static char_T cv128[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv55[2] = { 1, 5 };

  char_T cv129[5];
  static char_T cv130[5] = { 's', 'e', 't', 'u', 'p' };

  real_T *CenterFrequency;
  real_T (*Attenuation)[2];
  Attenuation = (real_T (*)[2])(ssGetRunTimeParamInfo(moduleInstance->S, 1U))
    ->data;
  CenterFrequency = (real_T *)(ssGetRunTimeParamInfo(moduleInstance->S, 0U))
    ->data;
  moduleInstance->method = 7U;
  moduleInstance->method_not_empty = true;
  moduleInstance->state = 1144108930U;
  moduleInstance->state_not_empty = true;
  for (mti = 0; mti < 2; mti++) {
    moduleInstance->b_state[mti] = 362436069U + 158852560U * (uint32_T)mti;
  }

  moduleInstance->b_state_not_empty = true;
  for (mti = 0; mti < 625; mti++) {
    moduleInstance->c_state[mti] = 0U;
  }

  r = 5489U;
  moduleInstance->c_state[0] = 5489U;
  for (mti = 0; mti < 623; mti++) {
    r = (r ^ r >> 30U) * 1812433253U + (uint32_T)(1 + mti);
    moduleInstance->c_state[mti + 1] = r;
  }

  moduleInstance->c_state[624] = 624U;
  moduleInstance->c_state_not_empty = true;
  for (mti = 0; mti < 2; mti++) {
    varargin_5[mti] = (*Attenuation)[mti];
  }

  if (!moduleInstance->sysobj_not_empty) {
    SDRTxZC706FMC23SL_SDRTxZC706FMC23SL(moduleInstance, &moduleInstance->sysobj);
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_CenterFrequency(&moduleInstance->sysobj,
      *CenterFrequency);
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_Attenuation(&moduleInstance->sysobj, varargin_5);
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized) {
    y = NULL;
    m16 = emlrtCreateCharArray(2, iv54);
    for (mti = 0; mti < 51; mti++) {
      cv127[mti] = cv128[mti];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m16, cv127);
    emlrtAssign(&y, m16);
    b_y = NULL;
    m16 = emlrtCreateCharArray(2, iv55);
    for (mti = 0; mti < 5; mti++) {
      cv129[mti] = cv130[mti];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m16, cv129);
    emlrtAssign(&b_y, m16);
    error(b_message(y, b_y, &h_emlrtMCI), &h_emlrtMCI);
  }

  obj->isInitialized = true;
  SDRSystemBase_setupImpl(obj);
  obj->TunablePropsChanged = false;
}

static void cgxe_mdl_initialize(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance)
{
  real_T varargin_5[2];
  int32_T i27;
  boolean_T flag;
  comm_internal_SDRTxZC706FMC23SL *obj;
  const mxArray *y;
  static const int32_T iv56[2] = { 1, 45 };

  const mxArray *m17;
  char_T cv131[45];
  static char_T cv132[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv57[2] = { 1, 8 };

  char_T cv133[8];
  static char_T cv134[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  const mxArray *c_y;
  static const int32_T iv58[2] = { 1, 45 };

  const mxArray *d_y;
  static const int32_T iv59[2] = { 1, 5 };

  char_T cv135[5];
  static char_T cv136[5] = { 'r', 'e', 's', 'e', 't' };

  real_T *CenterFrequency;
  real_T (*Attenuation)[2];
  Attenuation = (real_T (*)[2])(ssGetRunTimeParamInfo(moduleInstance->S, 1U))
    ->data;
  CenterFrequency = (real_T *)(ssGetRunTimeParamInfo(moduleInstance->S, 0U))
    ->data;
  for (i27 = 0; i27 < 2; i27++) {
    varargin_5[i27] = (*Attenuation)[i27];
  }

  if (!moduleInstance->sysobj_not_empty) {
    SDRTxZC706FMC23SL_SDRTxZC706FMC23SL(moduleInstance, &moduleInstance->sysobj);
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_CenterFrequency(&moduleInstance->sysobj,
      *CenterFrequency);
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_Attenuation(&moduleInstance->sysobj, varargin_5);
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m17 = emlrtCreateCharArray(2, iv56);
    for (i27 = 0; i27 < 45; i27++) {
      cv131[i27] = cv132[i27];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m17, cv131);
    emlrtAssign(&y, m17);
    b_y = NULL;
    m17 = emlrtCreateCharArray(2, iv57);
    for (i27 = 0; i27 < 8; i27++) {
      cv133[i27] = cv134[i27];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m17, cv133);
    emlrtAssign(&b_y, m17);
    error(b_message(y, b_y, &h_emlrtMCI), &h_emlrtMCI);
  }

  flag = obj->isInitialized;
  if (flag && moduleInstance->sysobj.isReleased) {
    c_y = NULL;
    m17 = emlrtCreateCharArray(2, iv58);
    for (i27 = 0; i27 < 45; i27++) {
      cv131[i27] = cv132[i27];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m17, cv131);
    emlrtAssign(&c_y, m17);
    d_y = NULL;
    m17 = emlrtCreateCharArray(2, iv59);
    for (i27 = 0; i27 < 5; i27++) {
      cv135[i27] = cv136[i27];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m17, cv135);
    emlrtAssign(&d_y, m17);
    error(b_message(c_y, d_y, &h_emlrtMCI), &h_emlrtMCI);
  }
}

static void cgxe_mdl_outputs(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance)
{
  real_T varargin_5[2];
  int32_T k;
  boolean_T flag;
  real_T hoistedGlobal_CenterFrequency;
  boolean_T p;
  real_T hoistedGlobal_Attenuation[2];
  boolean_T exitg1;
  comm_internal_SDRTxZC706FMC23SL *obj;
  const mxArray *y;
  static const int32_T iv60[2] = { 1, 45 };

  const mxArray *m18;
  char_T cv137[45];
  static char_T cv138[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv61[2] = { 1, 4 };

  char_T cv139[4];
  static char_T cv140[4] = { 's', 't', 'e', 'p' };

  const mxArray *c_y;
  static const int32_T iv62[2] = { 1, 51 };

  char_T cv141[51];
  static char_T cv142[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  const mxArray *d_y;
  static const int32_T iv63[2] = { 1, 5 };

  char_T cv143[5];
  static char_T cv144[5] = { 's', 'e', 't', 'u', 'p' };

  real_T *CenterFrequency;
  boolean_T *b_y0;
  creal_T (*u0)[8192];
  real_T (*Attenuation)[2];
  Attenuation = (real_T (*)[2])(ssGetRunTimeParamInfo(moduleInstance->S, 1U))
    ->data;
  CenterFrequency = (real_T *)(ssGetRunTimeParamInfo(moduleInstance->S, 0U))
    ->data;
  b_y0 = (boolean_T *)ssGetOutputPortSignal(moduleInstance->S, 0U);
  u0 = (creal_T (*)[8192])ssGetInputPortSignal(moduleInstance->S, 0U);
  for (k = 0; k < 2; k++) {
    varargin_5[k] = (*Attenuation)[k];
  }

  for (k = 0; k < 8192; k++) {
    moduleInstance->varargin_11[k].re = (*u0)[k].re;
    moduleInstance->varargin_11[k].im = (*u0)[k].im;
  }

  if (!moduleInstance->sysobj_not_empty) {
    SDRTxZC706FMC23SL_SDRTxZC706FMC23SL(moduleInstance, &moduleInstance->sysobj);
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_CenterFrequency(&moduleInstance->sysobj,
      *CenterFrequency);
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_Attenuation(&moduleInstance->sysobj, varargin_5);
  }

  hoistedGlobal_CenterFrequency = moduleInstance->sysobj.CenterFrequency;
  flag = false;
  p = true;
  if (!(hoistedGlobal_CenterFrequency == *CenterFrequency)) {
    p = false;
  }

  if (p) {
    flag = true;
  }

  if (!flag) {
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_CenterFrequency(&moduleInstance->sysobj,
      *CenterFrequency);
  }

  for (k = 0; k < 2; k++) {
    hoistedGlobal_Attenuation[k] = moduleInstance->sysobj.Attenuation[k];
  }

  flag = false;
  p = true;
  k = 0;
  exitg1 = false;
  while (exitg1 == false && k < 2) {
    if (!(hoistedGlobal_Attenuation[k] == varargin_5[k])) {
      p = false;
      exitg1 = true;
    } else {
      k++;
    }
  }

  if (p) {
    flag = true;
  }

  if (!flag) {
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_Attenuation(&moduleInstance->sysobj, varargin_5);
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m18 = emlrtCreateCharArray(2, iv60);
    for (k = 0; k < 45; k++) {
      cv137[k] = cv138[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m18, cv137);
    emlrtAssign(&y, m18);
    b_y = NULL;
    m18 = emlrtCreateCharArray(2, iv61);
    for (k = 0; k < 4; k++) {
      cv139[k] = cv140[k];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 4, m18, cv139);
    emlrtAssign(&b_y, m18);
    error(b_message(y, b_y, &h_emlrtMCI), &h_emlrtMCI);
  }

  if (!obj->isInitialized) {
    if (obj->isInitialized) {
      c_y = NULL;
      m18 = emlrtCreateCharArray(2, iv62);
      for (k = 0; k < 51; k++) {
        cv141[k] = cv142[k];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m18, cv141);
      emlrtAssign(&c_y, m18);
      d_y = NULL;
      m18 = emlrtCreateCharArray(2, iv63);
      for (k = 0; k < 5; k++) {
        cv143[k] = cv144[k];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m18, cv143);
      emlrtAssign(&d_y, m18);
      error(b_message(c_y, d_y, &h_emlrtMCI), &h_emlrtMCI);
    }

    obj->isInitialized = true;
    SDRSystemBase_setupImpl(obj);
    obj->TunablePropsChanged = false;
  }

  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
  }

  *b_y0 = SDRTxZynqFMC23SL_stepImpl(obj, moduleInstance->varargin_11);
}

static void cgxe_mdl_update(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance)
{
  (void)moduleInstance;
}

static void cgxe_mdl_terminate(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance)
{
  real_T varargin_5[2];
  int32_T i28;
  boolean_T flag;
  comm_internal_SDRTxZC706FMC23SL *obj;
  const mxArray *y;
  static const int32_T iv64[2] = { 1, 45 };

  const mxArray *m19;
  char_T cv145[45];
  static char_T cv146[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv65[2] = { 1, 8 };

  char_T cv147[8];
  static char_T cv148[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  const mxArray *c_y;
  static const int32_T iv66[2] = { 1, 45 };

  const mxArray *d_y;
  static const int32_T iv67[2] = { 1, 7 };

  char_T cv149[7];
  static char_T cv150[7] = { 'r', 'e', 'l', 'e', 'a', 's', 'e' };

  real_T *CenterFrequency;
  real_T (*Attenuation)[2];
  Attenuation = (real_T (*)[2])(ssGetRunTimeParamInfo(moduleInstance->S, 1U))
    ->data;
  CenterFrequency = (real_T *)(ssGetRunTimeParamInfo(moduleInstance->S, 0U))
    ->data;
  for (i28 = 0; i28 < 2; i28++) {
    varargin_5[i28] = (*Attenuation)[i28];
  }

  if (!moduleInstance->sysobj_not_empty) {
    SDRTxZC706FMC23SL_SDRTxZC706FMC23SL(moduleInstance, &moduleInstance->sysobj);
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_CenterFrequency(&moduleInstance->sysobj,
      *CenterFrequency);
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }

    SDRTxZynqFMC23Base_set_Attenuation(&moduleInstance->sysobj, varargin_5);
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m19 = emlrtCreateCharArray(2, iv64);
    for (i28 = 0; i28 < 45; i28++) {
      cv145[i28] = cv146[i28];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m19, cv145);
    emlrtAssign(&y, m19);
    b_y = NULL;
    m19 = emlrtCreateCharArray(2, iv65);
    for (i28 = 0; i28 < 8; i28++) {
      cv147[i28] = cv148[i28];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m19, cv147);
    emlrtAssign(&b_y, m19);
    error(b_message(y, b_y, &h_emlrtMCI), &h_emlrtMCI);
  }

  flag = obj->isInitialized;
  if (flag) {
    obj = &moduleInstance->sysobj;
    if (moduleInstance->sysobj.isReleased) {
      c_y = NULL;
      m19 = emlrtCreateCharArray(2, iv66);
      for (i28 = 0; i28 < 45; i28++) {
        cv145[i28] = cv146[i28];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m19, cv145);
      emlrtAssign(&c_y, m19);
      d_y = NULL;
      m19 = emlrtCreateCharArray(2, iv67);
      for (i28 = 0; i28 < 7; i28++) {
        cv149[i28] = cv150[i28];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 7, m19, cv149);
      emlrtAssign(&d_y, m19);
      error(b_message(c_y, d_y, &h_emlrtMCI), &h_emlrtMCI);
    }

    if (obj->isInitialized) {
      obj->isReleased = true;
      SDRSystemBase_releaseImpl(obj);
    }
  }
}

static const mxArray *mw__internal__name__resolution__fcn(void)
{
  const mxArray *nameCaptureInfo;
  nameCaptureInfo = NULL;
  emlrtAssign(&nameCaptureInfo, emlrtCreateStructMatrix(216, 1, 0, NULL));
  info_helper(&nameCaptureInfo);
  b_info_helper(&nameCaptureInfo);
  c_info_helper(&nameCaptureInfo);
  d_info_helper(&nameCaptureInfo);
  emlrtNameCapturePostProcessR2013b(&nameCaptureInfo);
  return nameCaptureInfo;
}

static void info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 0);
  emlrtAddField(*info, emlrt_marshallOut("repmat"), "name", 0);
  emlrtAddField(*info, emlrt_marshallOut("struct"), "dominantType", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "resolved", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 1);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 1);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 2);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_valid_size_arg"), "name", 2);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "resolved", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(1368215430U), "fileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 3);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 3);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isintegral"),
                "context", 4);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 4);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context", 5);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 5);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 6);
  emlrtAddField(*info, emlrt_marshallOut("eml_is_integer_class"), "name", 6);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_is_integer_class.m"),
                "resolved", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851182U), "fileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 7);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 7);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context", 8);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 8);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 9);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 9);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context", 10);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 10);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 11);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexIntRelop"), "name",
                11);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m"),
                "resolved", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760722U), "fileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!apply_float_relop"),
                "context", 12);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 12);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!float_class_contains_indexIntClass"),
                "context", 13);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 13);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!is_signed_indexIntClass"),
                "context", 14);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 14);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 15);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 15);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 16);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 16);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 17);
  emlrtAddField(*info, emlrt_marshallOut("max"), "name", 17);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "resolved", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(1311287716U), "fileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "context", 18);
  emlrtAddField(*info, emlrt_marshallOut("eml_min_or_max"), "name", 18);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                "resolved", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 19);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 19);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                19);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                20);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 20);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 21);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_alloc"), "name", 21);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "resolved", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "context", 22);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalexpAlloc"), "name",
                22);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                "resolved", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 23);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 23);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 24);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 24);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                24);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 26);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 26);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 27);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 27);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context", 28);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 28);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                28);
  emlrtAddField(*info, b_emlrt_marshallOut(1398907998U), "fileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 29);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 29);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 30);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 30);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 31);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 31);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 32);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 32);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 33);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 33);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 34);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 34);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 35);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.System"), "name",
                35);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "resolved", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/coder/coder/+coder/ExternalDependency.m"),
                "context", 36);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 36);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 37);
  emlrtAddField(*info, emlrt_marshallOut("coder.ExternalDependency"), "name", 37);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 37);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/coder/coder/+coder/ExternalDependency.m"),
                "resolved", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(1378329646U), "fileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 37);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase_pr"
    "opdefs.p"), "context", 38);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 38);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 39);
  emlrtAddField(*info, emlrt_marshallOut("comm.internal.SDRSystemBase_propdefs"),
                "name", 39);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 39);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase_pr"
    "opdefs.p"), "resolved", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835244U), "fileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 39);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/+internal/CustomDialog.p"),
                "context", 40);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 40);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "matlab.system.mixin.internal.CustomDialog"), "name", 41);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/+internal/CustomDialog.p"),
                "resolved", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(1410839906U), "fileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 42);
  emlrtAddField(*info, emlrt_marshallOut("comm.internal.SDRSystemBase"), "name",
                42);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "resolved", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835244U), "fileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base_propdef"
    "s.p"), "context", 43);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 43);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 44);
  emlrtAddField(*info, emlrt_marshallOut(
    "comm.internal.SDRTxZynqFMC23Base_propdefs"), "name", 44);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 44);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base_propdef"
    "s.p"), "resolved", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835502U), "fileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 44);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/CustomIcon.p"),
                "context", 45);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 45);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 45);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 45);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 46);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.mixin.CustomIcon"),
                "name", 46);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 46);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/CustomIcon.p"),
                "resolved", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(1410839906U), "fileTimeLo", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 46);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23SL.p"),
                "context", 47);
  emlrtAddField(*info, emlrt_marshallOut("comm.internal.SDRTxZynqFMC23Base"),
                "name", 47);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 47);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "resolved", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835502U), "fileTimeLo", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 47);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+mixin/+coder/Propagates.p"),
                "context", 48);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 48);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 48);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 48);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23SL.p"),
                "context", 49);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.mixin.coder.Propagates"),
                "name", 49);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 49);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+mixin/+coder/Propagates.p"),
                "resolved", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 49);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrplug/sdrplugins/xzc706afmc3/manual/+comm/+interna"
    "l/SDRTxZC706FMC23SL.m"), "context", 50);
  emlrtAddField(*info, emlrt_marshallOut("comm.internal.SDRTxZynqFMC23SL"),
                "name", 50);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 50);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23SL.p"),
                "resolved", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835502U), "fileTimeLo", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 50);
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 51);
  emlrtAddField(*info, emlrt_marshallOut("comm.internal.SDRTxZC706FMC23SL"),
                "name", 51);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 51);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrplug/sdrplugins/xzc706afmc3/manual/+comm/+interna"
    "l/SDRTxZC706FMC23SL.m"), "resolved", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(1415387794U), "fileTimeLo", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 51);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 52);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 52);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 52);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 52);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 53);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 53);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 53);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 53);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 54);
  emlrtAddField(*info, emlrt_marshallOut("randi"), "name", 54);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 54);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/randi.m"), "resolved", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(1383909690U), "fileTimeLo", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 54);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/randi.m"), "context", 55);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_valid_size_arg"), "name",
                55);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 55);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "resolved", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(1368215430U), "fileTimeLo", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 55);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/randi.m"), "context", 56);
  emlrtAddField(*info, emlrt_marshallOut("rand"), "name", 56);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 56);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/rand.m"), "resolved", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(1383909690U), "fileTimeLo", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 56);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/rand.m"), "context", 57);
  emlrtAddField(*info, emlrt_marshallOut("eml_is_rand_extrinsic"), "name", 57);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 57);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_is_rand_extrinsic.m"),
                "resolved", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(1368215432U), "fileTimeLo", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 57);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/rand.m"), "context", 58);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand"), "name", 58);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 58);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand.m"), "resolved",
                58);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380220U), "fileTimeLo", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 58);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand.m"), "context",
                59);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_str2id"), "name", 59);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 59);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_str2id.m"),
                "resolved", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380222U), "fileTimeLo", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 59);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_str2id.m"),
                "context", 60);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 60);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 60);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 60);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand.m"), "context",
                61);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mcg16807_stateful"), "name",
                61);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 61);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mcg16807_stateful.m"),
                "resolved", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(1366194644U), "fileTimeLo", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 61);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mcg16807_stateful.m"),
                "context", 62);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mcg16807"), "name", 62);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 62);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mcg16807.m"),
                "resolved", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380220U), "fileTimeLo", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 62);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mcg16807_stateful.m"),
                "context", 63);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mcg16807"), "name", 63);
  emlrtAddField(*info, emlrt_marshallOut("uint32"), "dominantType", 63);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mcg16807.m"),
                "resolved", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380220U), "fileTimeLo", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 63);
}

static const mxArray *emlrt_marshallOut(const char * u)
{
  const mxArray *y;
  const mxArray *m20;
  y = NULL;
  m20 = emlrtCreateString(u);
  emlrtAssign(&y, m20);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const uint32_T u)
{
  const mxArray *y;
  const mxArray *m21;
  y = NULL;
  m21 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
  *(uint32_T *)mxGetData(m21) = u;
  emlrtAssign(&y, m21);
  return y;
}

static void b_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand.m"), "context",
                64);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_shr3cong_stateful"), "name",
                64);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 64);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_shr3cong_stateful.m"),
                "resolved", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(1366194644U), "fileTimeLo", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 64);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_shr3cong_stateful.m"),
                "context", 65);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_shr3cong"), "name", 65);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 65);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_shr3cong.m"),
                "resolved", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380220U), "fileTimeLo", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 65);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_shr3cong_stateful.m"),
                "context", 66);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_shr3cong"), "name", 66);
  emlrtAddField(*info, emlrt_marshallOut("uint32"), "dominantType", 66);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_shr3cong.m"),
                "resolved", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(1313380220U), "fileTimeLo", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 66);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand.m"), "context",
                67);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mt19937ar_stateful"), "name",
                67);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 67);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar_stateful.m"),
                "resolved", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(1366194644U), "fileTimeLo", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 67);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar_stateful.m"),
                "context", 68);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mt19937ar"), "name", 68);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 68);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m"),
                "resolved", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(1406845548U), "fileTimeLo", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 68);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar_stateful.m"),
                "context", 69);
  emlrtAddField(*info, emlrt_marshallOut("eml_rand_mt19937ar"), "name", 69);
  emlrtAddField(*info, emlrt_marshallOut("uint32"), "dominantType", 69);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m"),
                "resolved", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(1406845548U), "fileTimeLo", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 69);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m!genrandu"),
                "context", 70);
  emlrtAddField(*info, emlrt_marshallOut("eps"), "name", 70);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 70);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/eps.m"), "resolved", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 70);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/eps.m"), "context", 71);
  emlrtAddField(*info, emlrt_marshallOut("eml_eps"), "name", 71);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 71);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_eps.m"), "resolved", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 71);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_eps.m"), "context", 72);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 72);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 72);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 72);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m!is_valid_state"),
                "context", 73);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 73);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 73);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 73);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "context", 74);
  emlrtAddField(*info, emlrt_marshallOut("eml_isequal_core"), "name", 74);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 74);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m"),
                "resolved", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851186U), "fileTimeLo", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 74);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m!isequal_scalar"),
                "context", 75);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 75);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 75);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 75);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context", 76);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 76);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 76);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 76);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m!is_valid_state"),
                "context", 77);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 77);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 77);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 77);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m!is_valid_state"),
                "context", 78);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_plus"), "name", 78);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 78);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                "resolved", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 78);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                79);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexPlus"), "name", 79);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 79);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                "resolved", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 79);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/eml_rand_mt19937ar.m!genrandu"),
                "context", 80);
  emlrtAddField(*info, emlrt_marshallOut("eml_error"), "name", 80);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 80);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_error.m"), "resolved", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(1343862758U), "fileTimeLo", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 80);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/randfun/randi.m"), "context", 81);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 81);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 81);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 81);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 82);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 82);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 82);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 82);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 83);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_floor"), "name", 83);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 83);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                "resolved", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851126U), "fileTimeLo", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 83);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 84);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 84);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 84);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 84);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 85);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 85);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 85);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 85);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 86);
  emlrtAddField(*info, emlrt_marshallOut("repmat"), "name", 86);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 86);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "resolved", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 86);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 87);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.cell"), "name", 87);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 87);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/cell.p"),
                "resolved", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 87);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 88);
  emlrtAddField(*info, emlrt_marshallOut("validateattributes"), "name", 88);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.cell"), "dominantType",
                88);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "resolved", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750104U), "fileTimeLo", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 88);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 89);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 89);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 89);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 89);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 90);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 90);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 90);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 90);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!is_valid_size_input"),
                "context", 91);
  emlrtAddField(*info, emlrt_marshallOut("isrow"), "name", 91);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 91);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isrow.m"), "resolved", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542760U), "fileTimeLo", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 91);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!is_valid_size_input"),
                "context", 92);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 92);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 92);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 92);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!is_valid_size_input"),
                "context", 93);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 93);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 93);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 93);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!is_valid_size_input"),
                "context", 94);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 94);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 94);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 94);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!size_check"),
                "context", 95);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 95);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 95);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 95);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 96);
  emlrtAddField(*info, emlrt_marshallOut("eml_warning"), "name", 96);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 96);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_warning.m"), "resolved",
                96);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851202U), "fileTimeLo", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 96);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!notisnan"),
                "context", 97);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 97);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 97);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 97);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 98);
  emlrtAddField(*info, emlrt_marshallOut("isfinite"), "name", 98);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 98);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "resolved",
                98);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 98);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!all"),
                "context", 99);
  emlrtAddField(*info, emlrt_marshallOut("isfinite"), "name", 99);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 99);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "resolved",
                99);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 99);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                100);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 100);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 100);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 100);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                101);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 101);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 101);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 101);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context",
                102);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 102);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 102);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 102);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 103);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 103);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 103);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 103);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context", 104);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 104);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 104);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 104);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 105);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 105);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 105);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 105);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context", 106);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 106);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 106);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 106);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23SL.p"),
                "context", 107);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 107);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 107);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 107);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 108);
  emlrtAddField(*info, emlrt_marshallOut("length"), "name", 108);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 108);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(1303178606U), "fileTimeLo", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 108);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m!intlength"),
                "context", 109);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 109);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 109);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 109);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 110);
  emlrtAddField(*info, emlrt_marshallOut("round"), "name", 110);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 110);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/round.m"), "resolved", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 110);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/round.m"), "context", 111);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 111);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 111);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 111);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/round.m"), "context", 112);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_round"), "name", 112);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 112);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_round.m"),
                "resolved", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(1307683638U), "fileTimeLo", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 112);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 113);
  emlrtAddField(*info, emlrt_marshallOut("deblank"), "name", 113);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 113);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "resolved",
                113);
  emlrtAddField(*info, b_emlrt_marshallOut(1331337288U), "fileTimeLo", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 113);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "context",
                114);
  emlrtAddField(*info, emlrt_marshallOut("ismatrix"), "name", 114);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 114);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/ismatrix.m"), "resolved",
                114);
  emlrtAddField(*info, b_emlrt_marshallOut(1331337258U), "fileTimeLo", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 114);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m!allwspace"),
                "context", 115);
  emlrtAddField(*info, emlrt_marshallOut("isstrprop"), "name", 115);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 115);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m"), "resolved",
                115);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013094U), "fileTimeLo", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 115);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                "context", 116);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_supported_string"), "name",
                116);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 116);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                "resolved", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 116);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m!inrange"),
                "context", 117);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 117);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 117);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 117);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"), "context",
                118);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 118);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 118);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 118);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                "context", 119);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 119);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 119);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 119);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                "context", 120);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 120);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 120);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 120);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                "context", 121);
  emlrtAddField(*info, emlrt_marshallOut("colon"), "name", 121);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 121);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328388U), "fileTimeLo", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 121);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 122);
  emlrtAddField(*info, emlrt_marshallOut("colon"), "name", 122);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 122);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328388U), "fileTimeLo", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 122);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 123);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 123);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 123);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 123);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 124);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 124);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 124);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 124);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 125);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 125);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 125);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 125);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                "context", 126);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 126);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 126);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 126);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                "context", 127);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 127);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 127);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 127);
}

static void c_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 128);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 128);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 128);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 128);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 129);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 129);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 129);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 129);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 130);
  emlrtAddField(*info, emlrt_marshallOut("eml_isa_uint"), "name", 130);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 130);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                130);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 130);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "context",
                131);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isaUint"), "name", 131);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 131);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/isaUint.p"),
                "resolved", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 131);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 132);
  emlrtAddField(*info, emlrt_marshallOut("eml_unsigned_class"), "name", 132);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 132);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                "resolved", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 132);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                "context", 133);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.unsignedClass"), "name",
                133);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 133);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                "resolved", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 133);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                "context", 134);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 134);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 134);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 134);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 135);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 135);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 135);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 135);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 136);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 136);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 136);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 136);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 137);
  emlrtAddField(*info, emlrt_marshallOut("eml_isa_uint"), "name", 137);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 137);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                137);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 137);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 138);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_plus"), "name", 138);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 138);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                "resolved", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 138);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_signed_integer_colon"),
                "context", 139);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 139);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 139);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 139);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/isstrprop.m!apply_property_predicate"),
                "context", 140);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 140);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 140);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 140);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 141);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 141);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 141);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 141);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "context", 142);
  emlrtAddField(*info, emlrt_marshallOut("eml_strcmp"), "name", 142);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 142);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_strcmp.m"), "resolved",
                142);
  emlrtAddField(*info, b_emlrt_marshallOut(1386456354U), "fileTimeLo", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 142);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 143);
  emlrtAddField(*info, emlrt_marshallOut("error"), "name", 143);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 143);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/error.m"), "resolved", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762366U), "fileTimeLo", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 143);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "context", 144);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 144);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 144);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 144);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 145);
  emlrtAddField(*info, emlrt_marshallOut("sdrplugin.PUP"), "name", 145);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 145);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "resolved", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835236U), "fileTimeLo", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 145);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 146);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 146);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 146);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 146);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                147);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 147);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 147);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 147);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                148);
  emlrtAddField(*info, emlrt_marshallOut("deblank"), "name", 148);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 148);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/deblank.m"), "resolved",
                148);
  emlrtAddField(*info, b_emlrt_marshallOut(1331337288U), "fileTimeLo", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 148);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                "context", 149);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 149);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 149);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 149);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                "context", 150);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_nbits"), "name", 150);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 150);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "resolved",
                150);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 150);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_nbits.m"), "context",
                151);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 151);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 151);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 151);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                "context", 152);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_rdivide"), "name", 152);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 152);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "resolved", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 152);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "context", 153);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexDivide"), "name",
                153);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 153);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                "resolved", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 153);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                154);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_times"), "name", 154);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 154);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                "resolved", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 154);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                "context", 155);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexTimes"), "name",
                155);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 155);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                "resolved", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 155);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                156);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_rdivide"), "name", 156);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 156);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "resolved", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 156);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "context", 157);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexDivide"), "name",
                157);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 157);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                "resolved", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 157);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                158);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_times"), "name", 158);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 158);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                "resolved", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 158);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_times.m"),
                "context", 159);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexTimes"), "name",
                159);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 159);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                "resolved", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 159);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                160);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 160);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 160);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 160);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 161);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 161);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 161);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 161);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                162);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 162);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 162);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 162);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "context", 163);
  emlrtAddField(*info, emlrt_marshallOut("length"), "name", 163);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 163);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(1303178606U), "fileTimeLo", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 163);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "context", 164);
  emlrtAddField(*info, emlrt_marshallOut("sdrplugin.PUP"), "name", 164);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 164);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "resolved", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835236U), "fileTimeLo", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 164);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "context", 165);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 165);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 165);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 165);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 166);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 166);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 166);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 166);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 167);
  emlrtAddField(*info, emlrt_marshallOut("error"), "name", 167);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 167);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/error.m"), "resolved", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762366U), "fileTimeLo", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 167);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/xilinxzynqbasedradio/toolbox/shared/sdr/sdrz/sdrz/+comm/+internal/SDRTxZynqFMC23Base.p"),
                "context", 168);
  emlrtAddField(*info, emlrt_marshallOut("sdrplugin.PUP"), "name", 168);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 168);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "resolved", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835236U), "fileTimeLo", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 168);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 169);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 169);
  emlrtAddField(*info, emlrt_marshallOut("uint32"), "dominantType", 169);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 169);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                170);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 170);
  emlrtAddField(*info, emlrt_marshallOut("uint32"), "dominantType", 170);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 170);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 171);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 171);
  emlrtAddField(*info, emlrt_marshallOut("int16"), "dominantType", 171);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 171);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                172);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 172);
  emlrtAddField(*info, emlrt_marshallOut("int16"), "dominantType", 172);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 172);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 173);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 173);
  emlrtAddField(*info, emlrt_marshallOut("uint16"), "dominantType", 173);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 173);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                174);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 174);
  emlrtAddField(*info, emlrt_marshallOut("uint16"), "dominantType", 174);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 174);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_pack64Val"),
                "context", 175);
  emlrtAddField(*info, emlrt_marshallOut("sdrplugin.PUP"), "name", 175);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 175);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "resolved", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(1415835236U), "fileTimeLo", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 175);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_pack64Val"),
                "context", 176);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 176);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 176);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 176);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 177);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 177);
  emlrtAddField(*info, emlrt_marshallOut("uint64"), "dominantType", 177);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 177);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                178);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 178);
  emlrtAddField(*info, emlrt_marshallOut("uint64"), "dominantType", 178);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 178);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p!l_packVal"),
                "context", 179);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 179);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 179);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 179);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                180);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 180);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 180);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 180);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                "context", 181);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_nbits"), "name", 181);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 181);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_nbits.m"),
                "resolved", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(1307683642U), "fileTimeLo", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 181);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_nbits.m"),
                "context", 182);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 182);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 182);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 182);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m!bytes_per_element"),
                "context", 183);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_rdivide"), "name", 183);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 183);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "resolved", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 183);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_rdivide.m"),
                "context", 184);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexDivide"), "name",
                184);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 184);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexDivide.m"),
                "resolved", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 184);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 185);
  emlrtAddField(*info, emlrt_marshallOut("sdr_setupImpl"), "name", 185);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 185);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_setupImpl.m"),
                "resolved", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(1401925164U), "fileTimeLo", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 185);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_setupImpl.m"),
                "context", 186);
  emlrtAddField(*info, emlrt_marshallOut("sdr_mapiPrivate"), "name", 186);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 186);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "resolved", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 186);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "context", 187);
  emlrtAddField(*info, emlrt_marshallOut("SDRPluginStatusT"), "name", 187);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 187);
  emlrtAddField(*info, emlrt_marshallOut(
    "[N]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/SDRPluginStatusT.m"),
                "resolved", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390604U), "fileTimeLo", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 187);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "context", 188);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 188);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 188);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 188);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "context", 189);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 189);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 189);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 189);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 190);
  emlrtAddField(*info, emlrt_marshallOut("SDRPluginStatusT"), "name", 190);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 190);
  emlrtAddField(*info, emlrt_marshallOut(
    "[N]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/SDRPluginStatusT.m"),
                "resolved", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390604U), "fileTimeLo", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 190);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 191);
  emlrtAddField(*info, emlrt_marshallOut("error"), "name", 191);
  emlrtAddField(*info, emlrt_marshallOut("mxArray"), "dominantType", 191);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/error.m"), "resolved", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762366U), "fileTimeLo", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 191);
}

static void d_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 192);
  emlrtAddField(*info, emlrt_marshallOut("sdr_setConfiguration"), "name", 192);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 192);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_setConfiguration.m"),
                "resolved", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 192);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_setConfiguration.m"),
                "context", 193);
  emlrtAddField(*info, emlrt_marshallOut("sdr_mapiPrivate"), "name", 193);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 193);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "resolved", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 193);
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 194);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 194);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 194);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                194);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 194);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 195);
  emlrtAddField(*info, emlrt_marshallOut("all"), "name", 195);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 195);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "resolved", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 195);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 196);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 196);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 196);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 196);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 197);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 197);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 197);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 197);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 198);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.allOrAny"), "name", 198);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 198);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "resolved", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615558U), "fileTimeLo", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 198);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 199);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 199);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 199);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 199);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 200);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 200);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 200);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                200);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 200);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 201);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.constNonSingletonDim"),
                "name", 201);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 201);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/constNonSingletonDim.m"),
                "resolved", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 201);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 202);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.prodsize"), "name", 202);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 202);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/prodsize.m"),
                "resolved", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(1360314988U), "fileTimeLo", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 202);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 203);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexMinus"), "name",
                203);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 203);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexMinus.m"),
                "resolved", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 203);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 204);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexTimes"), "name",
                204);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 204);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexTimes.m"),
                "resolved", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 204);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 205);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexPlus"), "name",
                205);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 205);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                "resolved", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 205);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 206);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexPlus"), "name",
                206);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 206);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                "resolved", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 206);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 207);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 207);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 207);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 207);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 208);
  emlrtAddField(*info, emlrt_marshallOut("sdr_processTunedPropertiesImpl"),
                "name", 208);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 208);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_processTunedProperti"
    "esImpl.m"), "resolved", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 208);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_processTunedProperti"
    "esImpl.m"), "context", 209);
  emlrtAddField(*info, emlrt_marshallOut("sdr_mapiPrivate"), "name", 209);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 209);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "resolved", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 209);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 210);
  emlrtAddField(*info, emlrt_marshallOut("sdr_txStepImplDouble"), "name", 210);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 210);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_txStepImplDouble.m"),
                "resolved", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(1413232350U), "fileTimeLo", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 210);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_txStepImplDouble.m"),
                "context", 211);
  emlrtAddField(*info, emlrt_marshallOut("sdr_mapiPrivate"), "name", 211);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 211);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "resolved", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 211);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+sdrplugin/PUP.p"),
                "context", 212);
  emlrtAddField(*info, emlrt_marshallOut("typecast"), "name", 212);
  emlrtAddField(*info, emlrt_marshallOut("uint8"), "dominantType", 212);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"),
                "resolved", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(1407196896U), "fileTimeLo", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 212);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/typecast.m"), "context",
                213);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 213);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 213);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 213);
  emlrtAddField(*info, emlrt_marshallOut(
    "[C]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/+comm/+internal/SDRSystemBase.p"),
                "context", 214);
  emlrtAddField(*info, emlrt_marshallOut("sdr_releaseImpl"), "name", 214);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 214);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_releaseImpl.m"),
                "resolved", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 214);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_releaseImpl.m"),
                "context", 215);
  emlrtAddField(*info, emlrt_marshallOut("sdr_mapiPrivate"), "name", 215);
  emlrtAddField(*info, emlrt_marshallOut("int32"), "dominantType", 215);
  emlrtAddField(*info, emlrt_marshallOut(
    "[E]C:/MATLAB/SupportPackages/R2014b/sdrpluginbase/toolbox/shared/sdr/sdrplug/sdrpluginbase/host/sdrmapi/sdr_mapiPrivate.m"),
                "resolved", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(1415390606U), "fileTimeLo", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 215);
}

static const mxArray *mw__internal__autoInference__fcn(void)
{
  const mxArray *infoCache;
  char_T info_slVer[3];
  real_T info_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4];
  s7UBIGHSehQY1gCsIQWwr5C info_VerificationInfo_checksums[4];
  real_T info_RestoreInfo_cgxeChksum[4];
  int32_T info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[2];
  real_T info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[1];
  real_T info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex;
  char_T info_RestoreInfo_DispatcherInfo_sysObjChksum[22];
  sIvmHumfM4VG8K4LjAjoqqB_size
    info_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes[8];
  sIvmHumfM4VG8K4LjAjoqqB info_RestoreInfo_DispatcherInfo_persisVarDWork_data[8];
  real_T info_RestoreInfo_DispatcherInfo_objTypeSize;
  char_T info_RestoreInfo_DispatcherInfo_objTypeName[31];
  real_T info_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed;
  real_T info_RestoreInfo_DispatcherInfo_Ports_complexity;
  real_T info_RestoreInfo_DispatcherInfo_Ports_dType;
  int32_T info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[2];
  real_T info_RestoreInfo_DispatcherInfo_Ports_dims_data[4];
  real_T info_RestoreInfo_DispatcherInfo_Ports_dimModes;
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *c_y;
  const mxArray *d_y;
  const mxArray *e_y;
  const mxArray *m22;
  const mxArray *f_y;
  const mxArray *g_y;
  const mxArray *h_y;
  const mxArray *i_y;
  int32_T i29;
  const mxArray *j_y;
  static const int32_T iv68[2] = { 1, 31 };

  const mxArray *k_y;
  sIvmHumfM4VG8K4LjAjoqqB_size u_elems_sizes[8];
  sIvmHumfM4VG8K4LjAjoqqB u_data[8];
  const mxArray *l_y;
  const sIvmHumfM4VG8K4LjAjoqqB_size *tmp_elems_sizes;
  const sIvmHumfM4VG8K4LjAjoqqB *tmp_data;
  int32_T u_sizes[2];
  int32_T i30;
  int32_T i;
  char_T b_tmp_data[16];
  int32_T tmp_sizes;
  char_T b_u_data[16];
  const mxArray *m_y;
  const mxArray *n_y;
  const mxArray *o_y;
  int32_T b_u_sizes[2];
  char_T c_tmp_data[1];
  char_T c_u_data[1];
  const mxArray *p_y;
  const mxArray *q_y;
  int32_T c_u_sizes[2];
  char_T d_tmp_data[22];
  char_T d_u_data[22];
  const mxArray *r_y;
  const mxArray *s_y;
  const mxArray *t_y;
  static const int32_T iv69[2] = { 1, 22 };

  const mxArray *u_y;
  const mxArray *v_y;
  const mxArray *w_y;
  const mxArray *x_y;
  const mxArray *y_y;
  int32_T d_u_sizes[2];
  const mxArray *ab_y;
  real_T *pData;
  const mxArray *bb_y;
  static const int32_T iv70[2] = { 1, 4 };

  const mxArray *cb_y;
  s7UBIGHSehQY1gCsIQWwr5C u[4];
  const mxArray *db_y;
  const s7UBIGHSehQY1gCsIQWwr5C *r0;
  const mxArray *eb_y;
  static const int32_T iv71[2] = { 1, 4 };

  const mxArray *fb_y;
  const mxArray *gb_y;
  static const int32_T iv72[2] = { 1, 4 };

  const mxArray *hb_y;
  static const int32_T iv73[2] = { 1, 3 };

  infoCache = NULL;
  mw__internal__call__autoinference
    (&info_RestoreInfo_DispatcherInfo_Ports_dimModes,
     info_RestoreInfo_DispatcherInfo_Ports_dims_data,
     info_RestoreInfo_DispatcherInfo_Ports_dims_sizes,
     &info_RestoreInfo_DispatcherInfo_Ports_dType,
     &info_RestoreInfo_DispatcherInfo_Ports_complexity,
     &info_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed,
     info_RestoreInfo_DispatcherInfo_objTypeName,
     &info_RestoreInfo_DispatcherInfo_objTypeSize,
     info_RestoreInfo_DispatcherInfo_persisVarDWork_data,
     info_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes,
     info_RestoreInfo_DispatcherInfo_sysObjChksum,
     &info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex,
     info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data,
     info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes,
     info_RestoreInfo_cgxeChksum, info_VerificationInfo_checksums,
     info_VerificationInfo_codeGenOnlyInfo_codeGenChksum, info_slVer);
  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  b_y = NULL;
  emlrtAssign(&b_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  c_y = NULL;
  emlrtAssign(&c_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  d_y = NULL;
  emlrtAssign(&d_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  e_y = NULL;
  m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dimModes);
  emlrtAssign(&e_y, m22);
  emlrtAddField(d_y, e_y, "dimModes", 0);
  emlrtAddField(d_y, c_emlrt_marshallOut
                (info_RestoreInfo_DispatcherInfo_Ports_dims_data,
                 info_RestoreInfo_DispatcherInfo_Ports_dims_sizes), "dims", 0);
  f_y = NULL;
  m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dType);
  emlrtAssign(&f_y, m22);
  emlrtAddField(d_y, f_y, "dType", 0);
  g_y = NULL;
  m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_complexity);
  emlrtAssign(&g_y, m22);
  emlrtAddField(d_y, g_y, "complexity", 0);
  h_y = NULL;
  m22 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_Ports_outputBuiltInDTEqUsed);
  emlrtAssign(&h_y, m22);
  emlrtAddField(d_y, h_y, "outputBuiltInDTEqUsed", 0);
  emlrtAddField(c_y, d_y, "Ports", 0);
  i_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 - i29;
  }

  emlrtAssign(&i_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  emlrtAddField(i_y, NULL, "names", 0);
  emlrtAddField(i_y, NULL, "dims", 0);
  emlrtAddField(i_y, NULL, "dType", 0);
  emlrtAddField(i_y, NULL, "complexity", 0);
  emlrtAddField(c_y, i_y, "dWork", 0);
  j_y = NULL;
  m22 = emlrtCreateCharArray(2, iv68);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 31, m22,
    info_RestoreInfo_DispatcherInfo_objTypeName);
  emlrtAssign(&j_y, m22);
  emlrtAddField(c_y, j_y, "objTypeName", 0);
  k_y = NULL;
  m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_objTypeSize);
  emlrtAssign(&k_y, m22);
  emlrtAddField(c_y, k_y, "objTypeSize", 0);
  for (i29 = 0; i29 < 8; i29++) {
    u_elems_sizes[i29] =
      info_RestoreInfo_DispatcherInfo_persisVarDWork_elems_sizes[i29];
    u_data[i29] = info_RestoreInfo_DispatcherInfo_persisVarDWork_data[i29];
  }

  l_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 + 7 * i29;
  }

  emlrtAssign(&l_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  for (i29 = 0; i29 < 8; i29++) {
    tmp_elems_sizes = &u_elems_sizes[i29];
    tmp_data = &u_data[i29];
    u_sizes[0] = 1;
    u_sizes[1] = tmp_elems_sizes->names[1];
    i30 = tmp_elems_sizes->names[0];
    i = tmp_elems_sizes->names[1];
    tmp_sizes = i30 * i;
    i *= i30;
    for (i30 = 0; i30 < i; i30++) {
      b_tmp_data[i30] = tmp_data->names[i30];
    }

    for (i30 = 0; i30 < tmp_sizes; i30++) {
      b_u_data[i30] = b_tmp_data[i30];
    }

    m_y = NULL;
    m22 = emlrtCreateCharArray(2, u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m22, (char_T *)
      &b_u_data);
    emlrtAssign(&m_y, m22);
    emlrtAddField(l_y, m_y, "names", i29);
    emlrtAddField(l_y, c_emlrt_marshallOut(tmp_data->dims, tmp_elems_sizes->dims),
                  "dims", i29);
    info_RestoreInfo_DispatcherInfo_Ports_dimModes = tmp_data->dType;
    n_y = NULL;
    m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dimModes);
    emlrtAssign(&n_y, m22);
    emlrtAddField(l_y, n_y, "dType", i29);
    info_RestoreInfo_DispatcherInfo_Ports_dimModes = tmp_data->dTypeSize;
    o_y = NULL;
    m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dimModes);
    emlrtAssign(&o_y, m22);
    emlrtAddField(l_y, o_y, "dTypeSize", i29);
    b_u_sizes[0] = 1;
    b_u_sizes[1] = tmp_elems_sizes->dTypeName[1];
    i30 = tmp_elems_sizes->dTypeName[0];
    i = tmp_elems_sizes->dTypeName[1];
    tmp_sizes = i30 * i;
    i *= i30;
    for (i30 = 0; i30 < i; i30++) {
      c_tmp_data[i30] = tmp_data->dTypeName[i30];
    }

    for (i30 = 0; i30 < tmp_sizes; i30++) {
      c_u_data[i30] = c_tmp_data[i30];
    }

    p_y = NULL;
    m22 = emlrtCreateCharArray(2, b_u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, b_u_sizes[1], m22, (char_T *)
      &c_u_data);
    emlrtAssign(&p_y, m22);
    emlrtAddField(l_y, p_y, "dTypeName", i29);
    info_RestoreInfo_DispatcherInfo_Ports_dimModes = tmp_data->dTypeIndex;
    q_y = NULL;
    m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dimModes);
    emlrtAssign(&q_y, m22);
    emlrtAddField(l_y, q_y, "dTypeIndex", i29);
    c_u_sizes[0] = 1;
    c_u_sizes[1] = tmp_elems_sizes->dTypeChksum[1];
    i30 = tmp_elems_sizes->dTypeChksum[0];
    i = tmp_elems_sizes->dTypeChksum[1];
    tmp_sizes = i30 * i;
    i *= i30;
    for (i30 = 0; i30 < i; i30++) {
      d_tmp_data[i30] = tmp_data->dTypeChksum[i30];
    }

    for (i30 = 0; i30 < tmp_sizes; i30++) {
      d_u_data[i30] = d_tmp_data[i30];
    }

    r_y = NULL;
    m22 = emlrtCreateCharArray(2, c_u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, c_u_sizes[1], m22, (char_T *)
      &d_u_data);
    emlrtAssign(&r_y, m22);
    emlrtAddField(l_y, r_y, "dTypeChksum", i29);
    info_RestoreInfo_DispatcherInfo_Ports_dimModes = tmp_data->complexity;
    s_y = NULL;
    m22 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_Ports_dimModes);
    emlrtAssign(&s_y, m22);
    emlrtAddField(l_y, s_y, "complexity", i29);
  }

  emlrtAddField(c_y, l_y, "persisVarDWork", 0);
  t_y = NULL;
  m22 = emlrtCreateCharArray(2, iv69);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 22, m22,
    info_RestoreInfo_DispatcherInfo_sysObjChksum);
  emlrtAssign(&t_y, m22);
  emlrtAddField(c_y, t_y, "sysObjChksum", 0);
  u_y = NULL;
  emlrtAssign(&u_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  v_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 - i29;
  }

  emlrtAssign(&v_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  emlrtAddField(v_y, NULL, "Index", 0);
  emlrtAddField(v_y, NULL, "DataType", 0);
  emlrtAddField(v_y, NULL, "IsSigned", 0);
  emlrtAddField(v_y, NULL, "MantBits", 0);
  emlrtAddField(v_y, NULL, "FixExp", 0);
  emlrtAddField(v_y, NULL, "Slope", 0);
  emlrtAddField(v_y, NULL, "Bias", 0);
  emlrtAddField(u_y, v_y, "Out", 0);
  w_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 - i29;
  }

  emlrtAssign(&w_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  emlrtAddField(w_y, NULL, "Index", 0);
  emlrtAddField(w_y, NULL, "DataType", 0);
  emlrtAddField(w_y, NULL, "IsSigned", 0);
  emlrtAddField(w_y, NULL, "MantBits", 0);
  emlrtAddField(w_y, NULL, "FixExp", 0);
  emlrtAddField(w_y, NULL, "Slope", 0);
  emlrtAddField(w_y, NULL, "Bias", 0);
  emlrtAddField(u_y, w_y, "DW", 0);
  x_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 - i29;
  }

  emlrtAssign(&x_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  emlrtAddField(x_y, NULL, "Index", 0);
  emlrtAddField(x_y, NULL, "DataType", 0);
  emlrtAddField(x_y, NULL, "IsSigned", 0);
  emlrtAddField(x_y, NULL, "MantBits", 0);
  emlrtAddField(x_y, NULL, "FixExp", 0);
  emlrtAddField(x_y, NULL, "Slope", 0);
  emlrtAddField(x_y, NULL, "Bias", 0);
  emlrtAddField(u_y, x_y, "PersisDW", 0);
  emlrtAddField(c_y, u_y, "mapsInfo", 0);
  y_y = NULL;
  m22 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex);
  emlrtAssign(&y_y, m22);
  emlrtAddField(c_y, y_y, "objDWorkTypeNameIndex", 0);
  d_u_sizes[0] = 1;
  d_u_sizes[1] = 1;
  ab_y = NULL;
  m22 = emlrtCreateNumericArray(2, d_u_sizes, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m22);
  pData[0] = info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[0];
  emlrtAssign(&ab_y, m22);
  emlrtAddField(c_y, ab_y, "inputDFFlagsIndexField", 0);
  emlrtAddField(b_y, c_y, "DispatcherInfo", 0);
  bb_y = NULL;
  m22 = emlrtCreateNumericArray(2, iv70, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m22);
  for (i = 0; i < 4; i++) {
    pData[i] = info_RestoreInfo_cgxeChksum[i];
  }

  emlrtAssign(&bb_y, m22);
  emlrtAddField(b_y, bb_y, "cgxeChksum", 0);
  emlrtAddField(y, b_y, "RestoreInfo", 0);
  cb_y = NULL;
  emlrtAssign(&cb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  for (i29 = 0; i29 < 4; i29++) {
    u[i29] = info_VerificationInfo_checksums[i29];
  }

  db_y = NULL;
  for (i29 = 0; i29 < 2; i29++) {
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes[i29] = 1 + 3 * i29;
  }

  emlrtAssign(&db_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_Ports_dims_sizes, 0, NULL));
  for (i29 = 0; i29 < 4; i29++) {
    r0 = &u[i29];
    for (i30 = 0; i30 < 4; i30++) {
      info_RestoreInfo_DispatcherInfo_Ports_dims_data[i30] = r0->chksum[i30];
    }

    eb_y = NULL;
    m22 = emlrtCreateNumericArray(2, iv71, mxDOUBLE_CLASS, mxREAL);
    pData = (real_T *)mxGetPr(m22);
    for (i = 0; i < 4; i++) {
      pData[i] = info_RestoreInfo_DispatcherInfo_Ports_dims_data[i];
    }

    emlrtAssign(&eb_y, m22);
    emlrtAddField(db_y, eb_y, "chksum", i29);
  }

  emlrtAddField(cb_y, db_y, "checksums", 0);
  fb_y = NULL;
  emlrtAssign(&fb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  gb_y = NULL;
  m22 = emlrtCreateNumericArray(2, iv72, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m22);
  for (i = 0; i < 4; i++) {
    pData[i] = info_VerificationInfo_codeGenOnlyInfo_codeGenChksum[i];
  }

  emlrtAssign(&gb_y, m22);
  emlrtAddField(fb_y, gb_y, "codeGenChksum", 0);
  emlrtAddField(cb_y, fb_y, "codeGenOnlyInfo", 0);
  emlrtAddField(y, cb_y, "VerificationInfo", 0);
  hb_y = NULL;
  m22 = emlrtCreateCharArray(2, iv73);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 3, m22, info_slVer);
  emlrtAssign(&hb_y, m22);
  emlrtAddField(y, hb_y, "slVer", 0);
  emlrtAssign(&infoCache, y);
  return infoCache;
}

static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2])
{
  const mxArray *y;
  const mxArray *m23;
  real_T *pData;
  int32_T i31;
  int32_T i;
  y = NULL;
  m23 = emlrtCreateNumericArray(2, u_sizes, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m23);
  i31 = 0;
  for (i = 0; i < u_sizes[1]; i++) {
    pData[i31] = u_data[u_sizes[0] * i];
    i31++;
  }

  emlrtAssign(&y, m23);
  return y;
}

static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance)
{
  const mxArray *st;
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m24;
  const mxArray *c_y;
  const mxArray *d_y;
  const mxArray *e_y;
  static const int32_T iv74[1] = { 625 };

  uint32_T *pData;
  int32_T i;
  const mxArray *f_y;
  static const int32_T iv75[1] = { 2 };

  const mxArray *g_y;
  const mxArray *h_y;
  const mxArray *i_y;
  const mxArray *j_y;
  const mxArray *k_y;
  const mxArray *l_y;
  const mxArray *m_y;
  const mxArray *n_y;
  static const int32_T iv76[2] = { 1, 11 };

  char_T cv151[11];
  const mxArray *o_y;
  static const int32_T iv77[2] = { 1, 14 };

  char_T cv152[14];
  const mxArray *p_y;
  const mxArray *q_y;
  const mxArray *r_y;
  static const int32_T iv78[1] = { 1024 };

  uint8_T *b_pData;
  const mxArray *s_y;
  static const int32_T iv79[2] = { 1, 6 };

  char_T cv153[6];
  const mxArray *t_y;
  const mxArray *u_y;
  const mxArray *v_y;
  const mxArray *w_y;
  const mxArray *x_y;
  static const int32_T iv80[1] = { 2 };

  real_T *c_pData;
  const mxArray *y_y;
  const mxArray *ab_y;
  static const int32_T iv81[1] = { 2 };

  const mxArray *bb_y;
  static const int32_T iv82[2] = { 2, 128 };

  const mxArray *cb_y;
  static const int32_T iv83[1] = { 2 };

  const mxArray *db_y;
  static const int32_T iv84[1] = { 2 };

  const mxArray *eb_y;
  static const int32_T iv85[1] = { 2 };

  const mxArray *fb_y;
  static const int32_T iv86[2] = { 2, 6 };

  const mxArray *gb_y;
  static const int32_T iv87[2] = { 1, 13 };

  char_T cv154[13];
  const mxArray *hb_y;
  static const int32_T iv88[2] = { 1, 22 };

  char_T cv155[22];
  const mxArray *ib_y;
  const mxArray *jb_y;
  const mxArray *kb_y;
  static const int32_T iv89[1] = { 2 };

  const mxArray *lb_y;
  st = NULL;
  y = NULL;
  emlrtAssign(&y, emlrtCreateCellMatrix(10, 1));
  b_y = NULL;
  m24 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
  *(uint32_T *)mxGetData(m24) = moduleInstance->method;
  emlrtAssign(&b_y, m24);
  emlrtSetCell(y, 0, b_y);
  c_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->method_not_empty);
  emlrtAssign(&c_y, m24);
  emlrtSetCell(y, 1, c_y);
  d_y = NULL;
  m24 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
  *(uint32_T *)mxGetData(m24) = moduleInstance->state;
  emlrtAssign(&d_y, m24);
  emlrtSetCell(y, 2, d_y);
  e_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv74, mxUINT32_CLASS, mxREAL);
  pData = (uint32_T *)mxGetData(m24);
  for (i = 0; i < 625; i++) {
    pData[i] = moduleInstance->c_state[i];
  }

  emlrtAssign(&e_y, m24);
  emlrtSetCell(y, 3, e_y);
  f_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv75, mxUINT32_CLASS, mxREAL);
  pData = (uint32_T *)mxGetData(m24);
  for (i = 0; i < 2; i++) {
    pData[i] = moduleInstance->b_state[i];
  }

  emlrtAssign(&f_y, m24);
  emlrtSetCell(y, 4, f_y);
  g_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->state_not_empty);
  emlrtAssign(&g_y, m24);
  emlrtSetCell(y, 5, g_y);
  h_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->c_state_not_empty);
  emlrtAssign(&h_y, m24);
  emlrtSetCell(y, 6, h_y);
  i_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->b_state_not_empty);
  emlrtAssign(&i_y, m24);
  emlrtSetCell(y, 7, i_y);
  j_y = NULL;
  emlrtAssign(&j_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  k_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj.isInitialized);
  emlrtAssign(&k_y, m24);
  emlrtAddField(j_y, k_y, "isInitialized", 0);
  l_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj.isReleased);
  emlrtAssign(&l_y, m24);
  emlrtAddField(j_y, l_y, "isReleased", 0);
  m_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj.TunablePropsChanged);
  emlrtAssign(&m_y, m24);
  emlrtAddField(j_y, m_y, "TunablePropsChanged", 0);
  n_y = NULL;
  m24 = emlrtCreateCharArray(2, iv76);
  for (i = 0; i < 11; i++) {
    cv151[i] = moduleInstance->sysobj.RadioAddress[i];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 11, m24, cv151);
  emlrtAssign(&n_y, m24);
  emlrtAddField(j_y, n_y, "RadioAddress", 0);
  o_y = NULL;
  m24 = emlrtCreateCharArray(2, iv77);
  for (i = 0; i < 14; i++) {
    cv152[i] = moduleInstance->sysobj.prequester[i];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 14, m24, cv152);
  emlrtAssign(&o_y, m24);
  emlrtAddField(j_y, o_y, "prequester", 0);
  p_y = NULL;
  m24 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m24) = moduleInstance->sysobj.pDriverHandle;
  emlrtAssign(&p_y, m24);
  emlrtAddField(j_y, p_y, "pDriverHandle", 0);
  q_y = NULL;
  m24 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m24) = moduleInstance->sysobj.pLastTunablePackedSize;
  emlrtAssign(&q_y, m24);
  emlrtAddField(j_y, q_y, "pLastTunablePackedSize", 0);
  r_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv78, mxUINT8_CLASS, mxREAL);
  b_pData = (uint8_T *)mxGetData(m24);
  for (i = 0; i < 1024; i++) {
    b_pData[i] = moduleInstance->sysobj.pLastTunablePackedBuffer[i];
  }

  emlrtAssign(&r_y, m24);
  emlrtAddField(j_y, r_y, "pLastTunablePackedBuffer", 0);
  s_y = NULL;
  m24 = emlrtCreateCharArray(2, iv79);
  for (i = 0; i < 6; i++) {
    cv153[i] = moduleInstance->sysobj.OutputDataType[i];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 6, m24, cv153);
  emlrtAssign(&s_y, m24);
  emlrtAddField(j_y, s_y, "OutputDataType", 0);
  t_y = NULL;
  m24 = emlrtCreateDoubleScalar(moduleInstance->sysobj.SamplesPerFrame);
  emlrtAssign(&t_y, m24);
  emlrtAddField(j_y, t_y, "SamplesPerFrame", 0);
  u_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj.DataIsComplex);
  emlrtAssign(&u_y, m24);
  emlrtAddField(j_y, u_y, "DataIsComplex", 0);
  v_y = NULL;
  m24 = emlrtCreateDoubleScalar(moduleInstance->sysobj.NumChannels);
  emlrtAssign(&v_y, m24);
  emlrtAddField(j_y, v_y, "NumChannels", 0);
  w_y = NULL;
  m24 = emlrtCreateDoubleScalar(moduleInstance->sysobj.CenterFrequency);
  emlrtAssign(&w_y, m24);
  emlrtAddField(j_y, w_y, "CenterFrequency", 0);
  x_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv80, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.Attenuation[i];
  }

  emlrtAssign(&x_y, m24);
  emlrtAddField(j_y, x_y, "Attenuation", 0);
  y_y = NULL;
  m24 = emlrtCreateDoubleScalar(moduleInstance->sysobj.NumHWChannels);
  emlrtAssign(&y_y, m24);
  emlrtAddField(j_y, y_y, "NumHWChannels", 0);
  ab_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv81, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.FIRCoefficientSize[i];
  }

  emlrtAssign(&ab_y, m24);
  emlrtAddField(j_y, ab_y, "FIRCoefficientSize", 0);
  bb_y = NULL;
  m24 = emlrtCreateNumericArray(2, iv82, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 256; i++) {
    c_pData[i] = moduleInstance->sysobj.FIRCoefficients[i];
  }

  emlrtAssign(&bb_y, m24);
  emlrtAddField(j_y, bb_y, "FIRCoefficients", 0);
  cb_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv83, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.FIRGain[i];
  }

  emlrtAssign(&cb_y, m24);
  emlrtAddField(j_y, cb_y, "FIRGain", 0);
  db_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv84, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.FIRDecimInterpFactor[i];
  }

  emlrtAssign(&db_y, m24);
  emlrtAddField(j_y, db_y, "FIRDecimInterpFactor", 0);
  eb_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv85, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.AnalogFilterCutoff[i];
  }

  emlrtAssign(&eb_y, m24);
  emlrtAddField(j_y, eb_y, "AnalogFilterCutoff", 0);
  fb_y = NULL;
  m24 = emlrtCreateNumericArray(2, iv86, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 12; i++) {
    c_pData[i] = moduleInstance->sysobj.FilterPathRates[i];
  }

  emlrtAssign(&fb_y, m24);
  emlrtAddField(j_y, fb_y, "FilterPathRates", 0);
  gb_y = NULL;
  m24 = emlrtCreateCharArray(2, iv87);
  for (i = 0; i < 13; i++) {
    cv154[i] = moduleInstance->sysobj.FilterDesignTypeForTx[i];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 13, m24, cv154);
  emlrtAssign(&gb_y, m24);
  emlrtAddField(j_y, gb_y, "FilterDesignTypeForTx", 0);
  hb_y = NULL;
  m24 = emlrtCreateCharArray(2, iv88);
  for (i = 0; i < 22; i++) {
    cv155[i] = moduleInstance->sysobj.FilterDesignTypeForRx[i];
  }

  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 22, m24, cv155);
  emlrtAssign(&hb_y, m24);
  emlrtAddField(j_y, hb_y, "FilterDesignTypeForRx", 0);
  ib_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj.pLostSamples);
  emlrtAssign(&ib_y, m24);
  emlrtAddField(j_y, ib_y, "pLostSamples", 0);
  jb_y = NULL;
  m24 = emlrtCreateDoubleScalar(moduleInstance->sysobj.pCenterFrequency);
  emlrtAssign(&jb_y, m24);
  emlrtAddField(j_y, jb_y, "pCenterFrequency", 0);
  kb_y = NULL;
  m24 = emlrtCreateNumericArray(1, iv89, mxDOUBLE_CLASS, mxREAL);
  c_pData = (real_T *)mxGetPr(m24);
  for (i = 0; i < 2; i++) {
    c_pData[i] = moduleInstance->sysobj.pAttenuation[i];
  }

  emlrtAssign(&kb_y, m24);
  emlrtAddField(j_y, kb_y, "pAttenuation", 0);
  emlrtSetCell(y, 8, j_y);
  lb_y = NULL;
  m24 = emlrtCreateLogicalScalar(moduleInstance->sysobj_not_empty);
  emlrtAssign(&lb_y, m24);
  emlrtSetCell(y, 9, lb_y);
  emlrtAssign(&st, y);
  return st;
}

static uint32_T emlrt_marshallIn(const mxArray *b_method, const char_T
  *identifier)
{
  uint32_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(emlrtAlias(b_method), &thisId);
  emlrtDestroyArray(&b_method);
  return y;
}

static uint32_T b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  uint32_T y;
  y = x_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static boolean_T c_emlrt_marshallIn(const mxArray *b_method_not_empty, const
  char_T *identifier)
{
  boolean_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(emlrtAlias(b_method_not_empty), &thisId);
  emlrtDestroyArray(&b_method_not_empty);
  return y;
}

static boolean_T d_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  boolean_T y;
  y = y_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static uint32_T e_emlrt_marshallIn(const mxArray *d_state, const char_T
  *identifier)
{
  uint32_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = f_emlrt_marshallIn(emlrtAlias(d_state), &thisId);
  emlrtDestroyArray(&d_state);
  return y;
}

static uint32_T f_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  uint32_T y;
  y = x_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void g_emlrt_marshallIn(const mxArray *d_state, const char_T *identifier,
  uint32_T y[625])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  h_emlrt_marshallIn(emlrtAlias(d_state), &thisId, y);
  emlrtDestroyArray(&d_state);
}

static void h_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint32_T y[625])
{
  ab_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void i_emlrt_marshallIn(const mxArray *d_state, const char_T *identifier,
  uint32_T y[2])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  j_emlrt_marshallIn(emlrtAlias(d_state), &thisId, y);
  emlrtDestroyArray(&d_state);
}

static void j_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint32_T y[2])
{
  bb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const mxArray *b_sysobj, const char_T *identifier,
  comm_internal_SDRTxZC706FMC23SL *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  l_emlrt_marshallIn(emlrtAlias(b_sysobj), &thisId, y);
  emlrtDestroyArray(&b_sysobj);
}

static void l_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, comm_internal_SDRTxZC706FMC23SL *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[26] = { "isInitialized", "isReleased",
    "TunablePropsChanged", "RadioAddress", "prequester", "pDriverHandle",
    "pLastTunablePackedSize", "pLastTunablePackedBuffer", "OutputDataType",
    "SamplesPerFrame", "DataIsComplex", "NumChannels", "CenterFrequency",
    "Attenuation", "NumHWChannels", "FIRCoefficientSize", "FIRCoefficients",
    "FIRGain", "FIRDecimInterpFactor", "AnalogFilterCutoff", "FilterPathRates",
    "FilterDesignTypeForTx", "FilterDesignTypeForRx", "pLostSamples",
    "pCenterFrequency", "pAttenuation" };

  thisId.fParent = parentId;
  emlrtCheckStructR2012b(emlrtRootTLSGlobal, parentId, u, 26, fieldNames, 0U, 0);
  thisId.fIdentifier = "isInitialized";
  y->isInitialized = d_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "isInitialized")), &thisId);
  thisId.fIdentifier = "isReleased";
  y->isReleased = d_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "isReleased")), &thisId);
  thisId.fIdentifier = "TunablePropsChanged";
  y->TunablePropsChanged = d_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "TunablePropsChanged")), &thisId);
  thisId.fIdentifier = "RadioAddress";
  m_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "RadioAddress")), &thisId, y->RadioAddress);
  thisId.fIdentifier = "prequester";
  n_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "prequester")), &thisId, y->prequester);
  thisId.fIdentifier = "pDriverHandle";
  y->pDriverHandle = o_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pDriverHandle")), &thisId);
  thisId.fIdentifier = "pLastTunablePackedSize";
  y->pLastTunablePackedSize = o_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pLastTunablePackedSize")), &thisId);
  thisId.fIdentifier = "pLastTunablePackedBuffer";
  p_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "pLastTunablePackedBuffer")), &thisId, y->pLastTunablePackedBuffer);
  thisId.fIdentifier = "OutputDataType";
  q_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "OutputDataType")), &thisId, y->OutputDataType);
  thisId.fIdentifier = "SamplesPerFrame";
  y->SamplesPerFrame = r_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "SamplesPerFrame")), &thisId);
  thisId.fIdentifier = "DataIsComplex";
  y->DataIsComplex = d_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "DataIsComplex")), &thisId);
  thisId.fIdentifier = "NumChannels";
  y->NumChannels = r_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "NumChannels")), &thisId);
  thisId.fIdentifier = "CenterFrequency";
  y->CenterFrequency = r_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "CenterFrequency")), &thisId);
  thisId.fIdentifier = "Attenuation";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "Attenuation")), &thisId, y->Attenuation);
  thisId.fIdentifier = "NumHWChannels";
  y->NumHWChannels = r_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "NumHWChannels")), &thisId);
  thisId.fIdentifier = "FIRCoefficientSize";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FIRCoefficientSize")), &thisId, y->FIRCoefficientSize);
  thisId.fIdentifier = "FIRCoefficients";
  t_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FIRCoefficients")), &thisId, y->FIRCoefficients);
  thisId.fIdentifier = "FIRGain";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FIRGain")), &thisId, y->FIRGain);
  thisId.fIdentifier = "FIRDecimInterpFactor";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FIRDecimInterpFactor")), &thisId, y->FIRDecimInterpFactor);
  thisId.fIdentifier = "AnalogFilterCutoff";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "AnalogFilterCutoff")), &thisId, y->AnalogFilterCutoff);
  thisId.fIdentifier = "FilterPathRates";
  u_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FilterPathRates")), &thisId, y->FilterPathRates);
  thisId.fIdentifier = "FilterDesignTypeForTx";
  v_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FilterDesignTypeForTx")), &thisId, y->FilterDesignTypeForTx);
  thisId.fIdentifier = "FilterDesignTypeForRx";
  w_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "FilterDesignTypeForRx")), &thisId, y->FilterDesignTypeForRx);
  thisId.fIdentifier = "pLostSamples";
  y->pLostSamples = d_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pLostSamples")), &thisId);
  thisId.fIdentifier = "pCenterFrequency";
  y->pCenterFrequency = r_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pCenterFrequency")), &thisId);
  thisId.fIdentifier = "pAttenuation";
  s_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "pAttenuation")), &thisId, y->pAttenuation);
  emlrtDestroyArray(&u);
}

static void m_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[11])
{
  cb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void n_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[14])
{
  db_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static int32_T o_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  int32_T y;
  y = eb_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void p_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[1024])
{
  fb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void q_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[6])
{
  gb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static real_T r_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  real_T y;
  y = hb_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void s_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[2])
{
  ib_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void t_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[256])
{
  jb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void u_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, real_T y[12])
{
  kb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void v_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[13])
{
  lb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void w_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, char_T y[22])
{
  mb_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void mw__internal__setSimState__fcn(InstanceStruct_Hc1X60vKnc326iDURq7NcH
  *moduleInstance, const mxArray *st)
{
  const mxArray *u;
  u = emlrtAlias(st);
  moduleInstance->method = emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 0)), "method");
  moduleInstance->method_not_empty = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 1)), "method_not_empty");
  moduleInstance->state = e_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 2)), "state");
  g_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 3)), "state",
                     moduleInstance->c_state);
  i_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 4)), "state",
                     moduleInstance->b_state);
  moduleInstance->state_not_empty = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 5)), "state_not_empty");
  moduleInstance->c_state_not_empty = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 6)), "state_not_empty");
  moduleInstance->b_state_not_empty = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 7)), "state_not_empty");
  k_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 8)),
                     "sysobj", &moduleInstance->sysobj);
  moduleInstance->sysobj_not_empty = c_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 9)), "sysobj_not_empty");
  emlrtDestroyArray(&u);
  emlrtDestroyArray(&st);
}

static const mxArray *message(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  const mxArray *m25;
  pArray = b;
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m25, 1, &pArray,
    "message", true, location);
}

static void error(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray, "error", true,
                        location);
}

static void b_error(const mxArray *b, const mxArray *c, emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  pArrays[0] = b;
  pArrays[1] = c;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 2, pArrays, "error", true,
                        location);
}

static void warning(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray, "warning", true,
                        location);
}

static const mxArray *b_message(const mxArray *b, const mxArray *c, emlrtMCInfo *
  location)
{
  const mxArray *pArrays[2];
  const mxArray *m26;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m26, 2, pArrays,
    "message", true, location);
}

static void sdrplugin_internal_privsetupsession(const mxArray *b, emlrtMCInfo
  *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray,
                        "sdrplugin.internal.privsetupsession", true, location);
}

static void isempty(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray, "isempty", true,
                        location);
}

static uint32_T x_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId)
{
  uint32_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "uint32", false, 0U, 0);
  ret = *(uint32_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static boolean_T y_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  boolean_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "logical", false, 0U,
    0);
  ret = *mxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void ab_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint32_T ret[625])
{
  int32_T iv90[1];
  int32_T i32;
  iv90[0] = 625;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "uint32", false, 1U,
    iv90);
  for (i32 = 0; i32 < 625; i32++) {
    ret[i32] = (*(uint32_T (*)[625])mxGetData(src))[i32];
  }

  emlrtDestroyArray(&src);
}

static void bb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint32_T ret[2])
{
  int32_T iv91[1];
  int32_T i33;
  iv91[0] = 2;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "uint32", false, 1U,
    iv91);
  for (i33 = 0; i33 < 2; i33++) {
    ret[i33] = (*(uint32_T (*)[2])mxGetData(src))[i33];
  }

  emlrtDestroyArray(&src);
}

static void cb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[11])
{
  int32_T iv92[2];
  int32_T i34;
  for (i34 = 0; i34 < 2; i34++) {
    iv92[i34] = 1 + 10 * i34;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "char", false, 2U,
    iv92);
  emlrtImportCharArrayR2014b(emlrtRootTLSGlobal, src, ret, 11);
  emlrtDestroyArray(&src);
}

static void db_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[14])
{
  int32_T iv93[2];
  int32_T i35;
  for (i35 = 0; i35 < 2; i35++) {
    iv93[i35] = 1 + 13 * i35;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "char", false, 2U,
    iv93);
  emlrtImportCharArrayR2014b(emlrtRootTLSGlobal, src, ret, 14);
  emlrtDestroyArray(&src);
}

static int32_T eb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId)
{
  int32_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "int32", false, 0U, 0);
  ret = *(int32_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void fb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[1024])
{
  int32_T iv94[1];
  int32_T i36;
  iv94[0] = 1024;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "uint8", false, 1U,
    iv94);
  for (i36 = 0; i36 < 1024; i36++) {
    ret[i36] = (*(uint8_T (*)[1024])mxGetData(src))[i36];
  }

  emlrtDestroyArray(&src);
}

static void gb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[6])
{
  int32_T iv95[2];
  int32_T i37;
  for (i37 = 0; i37 < 2; i37++) {
    iv95[i37] = 1 + 5 * i37;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "char", false, 2U,
    iv95);
  emlrtImportCharArrayR2014b(emlrtRootTLSGlobal, src, ret, 6);
  emlrtDestroyArray(&src);
}

static real_T hb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  real_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "double", false, 0U, 0);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void ib_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[2])
{
  int32_T iv96[1];
  int32_T i38;
  iv96[0] = 2;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "double", false, 1U,
    iv96);
  for (i38 = 0; i38 < 2; i38++) {
    ret[i38] = (*(real_T (*)[2])mxGetData(src))[i38];
  }

  emlrtDestroyArray(&src);
}

static void jb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[256])
{
  int32_T iv97[2];
  int32_T i39;
  int32_T i40;
  for (i39 = 0; i39 < 2; i39++) {
    iv97[i39] = 2 + 126 * i39;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "double", false, 2U,
    iv97);
  for (i39 = 0; i39 < 128; i39++) {
    for (i40 = 0; i40 < 2; i40++) {
      ret[i40 + (i39 << 1)] = (*(real_T (*)[256])mxGetData(src))[i40 + (i39 << 1)];
    }
  }

  emlrtDestroyArray(&src);
}

static void kb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, real_T ret[12])
{
  int32_T iv98[2];
  int32_T i41;
  int32_T i42;
  for (i41 = 0; i41 < 2; i41++) {
    iv98[i41] = 2 + (i41 << 2);
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "double", false, 2U,
    iv98);
  for (i41 = 0; i41 < 6; i41++) {
    for (i42 = 0; i42 < 2; i42++) {
      ret[i42 + (i41 << 1)] = (*(real_T (*)[12])mxGetData(src))[i42 + (i41 << 1)];
    }
  }

  emlrtDestroyArray(&src);
}

static void lb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[13])
{
  int32_T iv99[2];
  int32_T i43;
  for (i43 = 0; i43 < 2; i43++) {
    iv99[i43] = 1 + 12 * i43;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "char", false, 2U,
    iv99);
  emlrtImportCharArrayR2014b(emlrtRootTLSGlobal, src, ret, 13);
  emlrtDestroyArray(&src);
}

static void mb_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, char_T ret[22])
{
  int32_T iv100[2];
  int32_T i44;
  for (i44 = 0; i44 < 2; i44++) {
    iv100[i44] = 1 + 21 * i44;
  }

  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "char", false, 2U,
    iv100);
  emlrtImportCharArrayR2014b(emlrtRootTLSGlobal, src, ret, 22);
  emlrtDestroyArray(&src);
}

static real_T eml_rand_mt19937ar(uint32_T d_state[625])
{
  real_T r;
  int32_T exitg1;
  uint32_T u[2];
  int32_T k;
  uint32_T mti;
  int32_T kk;
  uint32_T y;
  uint32_T b_y;
  uint32_T c_y;
  uint32_T d_y;
  boolean_T isvalid;
  boolean_T exitg2;

  /* ========================= COPYRIGHT NOTICE ============================ */
  /*  This is a uniform (0,1) pseudorandom number generator based on:        */
  /*                                                                         */
  /*  A C-program for MT19937, with initialization improved 2002/1/26.       */
  /*  Coded by Takuji Nishimura and Makoto Matsumoto.                        */
  /*                                                                         */
  /*  Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,      */
  /*  All rights reserved.                                                   */
  /*                                                                         */
  /*  Redistribution and use in source and binary forms, with or without     */
  /*  modification, are permitted provided that the following conditions     */
  /*  are met:                                                               */
  /*                                                                         */
  /*    1. Redistributions of source code must retain the above copyright    */
  /*       notice, this list of conditions and the following disclaimer.     */
  /*                                                                         */
  /*    2. Redistributions in binary form must reproduce the above copyright */
  /*       notice, this list of conditions and the following disclaimer      */
  /*       in the documentation and/or other materials provided with the     */
  /*       distribution.                                                     */
  /*                                                                         */
  /*    3. The names of its contributors may not be used to endorse or       */
  /*       promote products derived from this software without specific      */
  /*       prior written permission.                                         */
  /*                                                                         */
  /*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
  /*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
  /*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
  /*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT  */
  /*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
  /*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
  /*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
  /*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
  /*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
  /*  (INCLUDING  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE */
  /*  OF THIS  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  */
  /*                                                                         */
  /* =============================   END   ================================= */
  do {
    exitg1 = 0;
    for (k = 0; k < 2; k++) {
      mti = d_state[624] + 1U;
      if (mti >= 625U) {
        for (kk = 0; kk < 227; kk++) {
          y = d_state[kk] & 2147483648U | d_state[1 + kk] & 2147483647U;
          if ((int32_T)(y & 1U) == 0) {
            b_y = y >> 1U;
          } else {
            b_y = y >> 1U ^ 2567483615U;
          }

          d_state[kk] = d_state[397 + kk] ^ b_y;
        }

        for (kk = 0; kk < 396; kk++) {
          y = d_state[kk + 227] & 2147483648U | d_state[228 + kk] & 2147483647U;
          if ((int32_T)(y & 1U) == 0) {
            c_y = y >> 1U;
          } else {
            c_y = y >> 1U ^ 2567483615U;
          }

          d_state[kk + 227] = d_state[kk] ^ c_y;
        }

        y = d_state[623] & 2147483648U | d_state[0] & 2147483647U;
        if ((int32_T)(y & 1U) == 0) {
          d_y = y >> 1U;
        } else {
          d_y = y >> 1U ^ 2567483615U;
        }

        d_state[623] = d_state[396] ^ d_y;
        mti = 1U;
      }

      y = d_state[(int32_T)mti - 1];
      d_state[624] = mti;
      y ^= y >> 11U;
      y ^= y << 7U & 2636928640U;
      y ^= y << 15U & 4022730752U;
      y ^= y >> 18U;
      u[k] = y;
    }

    r = 1.1102230246251565E-16 * ((real_T)(u[0] >> 5U) * 6.7108864E+7 + (real_T)
                                  (u[1] >> 6U));
    if (r == 0.0) {
      if (d_state[624] >= 1U && d_state[624] < 625U) {
        isvalid = true;
      } else {
        isvalid = false;
      }

      if (isvalid) {
        isvalid = false;
        k = 0;
        exitg2 = false;
        while (exitg2 == false && k + 1 < 625) {
          if (d_state[k] == 0U) {
            k++;
          } else {
            isvalid = true;
            exitg2 = true;
          }
        }
      }

      if (!isvalid) {
        eml_error();
      }
    } else {
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return r;
}

/* CGXE Glue Code */
static void mdlOutputs_Hc1X60vKnc326iDURq7NcH(SimStruct *S, int_T tid)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_outputs(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlInitialize_Hc1X60vKnc326iDURq7NcH(SimStruct *S)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_initialize(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlUpdate_Hc1X60vKnc326iDURq7NcH(SimStruct *S, int_T tid)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_update(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static mxArray* getSimState_Hc1X60vKnc326iDURq7NcH(SimStruct *S)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  mxArray* mxSS;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mxSS = (mxArray *) mw__internal__getSimState__fcn(moduleInstance);
  CGXERT_LEAVE_CHECK();
  return mxSS;
}

static void setSimState_Hc1X60vKnc326iDURq7NcH(SimStruct *S, const mxArray *ss)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mw__internal__setSimState__fcn(moduleInstance, emlrtAlias(ss));
  CGXERT_LEAVE_CHECK();
}

static void mdlTerminate_Hc1X60vKnc326iDURq7NcH(SimStruct *S)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_terminate(moduleInstance);
  CGXERT_LEAVE_CHECK();
  free((void *)moduleInstance);
  ssSetUserData(S, NULL);
}

static void mdlStart_Hc1X60vKnc326iDURq7NcH(SimStruct *S)
{
  InstanceStruct_Hc1X60vKnc326iDURq7NcH *moduleInstance;
  moduleInstance = (InstanceStruct_Hc1X60vKnc326iDURq7NcH *)calloc(1, sizeof
    (InstanceStruct_Hc1X60vKnc326iDURq7NcH));
  moduleInstance->S = S;
  ssSetUserData(S, (void *)moduleInstance);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_start(moduleInstance);
  CGXERT_LEAVE_CHECK();

  {
    uint_T options = ssGetOptions(S);
    options |= SS_OPTION_RUNTIME_EXCEPTION_FREE_CODE;
    ssSetOptions(S, options);
  }

  ssSetmdlOutputs(S, mdlOutputs_Hc1X60vKnc326iDURq7NcH);
  ssSetmdlInitializeConditions(S, mdlInitialize_Hc1X60vKnc326iDURq7NcH);
  ssSetmdlUpdate(S, mdlUpdate_Hc1X60vKnc326iDURq7NcH);
  ssSetmdlTerminate(S, mdlTerminate_Hc1X60vKnc326iDURq7NcH);
}

static void mdlProcessParameters_Hc1X60vKnc326iDURq7NcH(SimStruct *S)
{
}

void method_dispatcher_Hc1X60vKnc326iDURq7NcH(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_Hc1X60vKnc326iDURq7NcH(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_Hc1X60vKnc326iDURq7NcH(S);
    break;

   case SS_CALL_MDL_GET_SIM_STATE:
    *((mxArray**) data) = getSimState_Hc1X60vKnc326iDURq7NcH(S);
    break;

   case SS_CALL_MDL_SET_SIM_STATE:
    setSimState_Hc1X60vKnc326iDURq7NcH(S, (const mxArray *) data);
    break;

   default:
    /* Unhandled method */
    /*
       sf_mex_error_message("Stateflow Internal Error:\n"
       "Error calling method dispatcher for module: Hc1X60vKnc326iDURq7NcH.\n"
       "Can't handle method %d.\n", method);
     */
    break;
  }
}

int autoInfer_dispatcher_Hc1X60vKnc326iDURq7NcH(mxArray* plhs[], const char
  * commandName)
{
  if (strcmp(commandName, "NameResolution") == 0) {
    plhs[0] = (mxArray*) mw__internal__name__resolution__fcn();
    return 1;
  }

  if (strcmp(commandName, "AutoInfer") == 0) {
    plhs[0] = (mxArray*) mw__internal__autoInference__fcn();
    return 1;
  }

  return 0;
}

mxArray *cgxe_Hc1X60vKnc326iDURq7NcH_BuildInfoUpdate(void)
{
  mxArray * mxBIArgs;
  mxArray * elem_1;
  mxArray * elem_2;
  mxArray * elem_3;
  mxArray * elem_4;
  mxArray * elem_5;
  mxArray * elem_6;
  mxArray * elem_7;
  mxArray * elem_8;
  mxArray * elem_9;
  mxArray * elem_10;
  mxBIArgs = mxCreateCellMatrix(1,3);
  elem_1 = mxCreateCellMatrix(1,6);
  elem_2 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,0,elem_2);
  elem_3 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,1,elem_3);
  elem_4 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,2,elem_4);
  elem_5 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,3,elem_5);
  elem_6 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,4,elem_6);
  elem_7 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,5,elem_7);
  mxSetCell(mxBIArgs,0,elem_1);
  elem_8 = mxCreateCellMatrix(1,1);
  elem_9 = mxCreateString("comm.internal.SDRSystemBase");
  mxSetCell(elem_8,0,elem_9);
  mxSetCell(mxBIArgs,1,elem_8);
  elem_10 = mxCreateCellMatrix(1,0);
  mxSetCell(mxBIArgs,2,elem_10);
  return mxBIArgs;
}
