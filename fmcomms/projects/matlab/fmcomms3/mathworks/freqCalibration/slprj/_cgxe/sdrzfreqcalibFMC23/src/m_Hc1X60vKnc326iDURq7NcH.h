#ifndef __Hc1X60vKnc326iDURq7NcH_h__
#define __Hc1X60vKnc326iDURq7NcH_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_slE07I4lDg6FknjQ3k8Q9CG
#define struct_slE07I4lDg6FknjQ3k8Q9CG

struct slE07I4lDg6FknjQ3k8Q9CG
{
  real_T Index;
  real_T DataType;
  real_T IsSigned;
  real_T MantBits;
  real_T FixExp;
  real_T Slope;
  real_T Bias;
};

#endif                                 /*struct_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef typedef_slE07I4lDg6FknjQ3k8Q9CG
#define typedef_slE07I4lDg6FknjQ3k8Q9CG

typedef struct slE07I4lDg6FknjQ3k8Q9CG slE07I4lDg6FknjQ3k8Q9CG;

#endif                                 /*typedef_slE07I4lDg6FknjQ3k8Q9CG*/

#include <stddef.h>
#include "sdrcapi.h"
#ifndef struct_stIdWsMGFAeT5aVn8Ny36oH
#define struct_stIdWsMGFAeT5aVn8Ny36oH

struct stIdWsMGFAeT5aVn8Ny36oH
{
  char_T names[16];
  real_T dims[4];
  real_T dType;
  real_T dTypeSize;
  char_T dTypeName[1];
  real_T dTypeIndex;
  char_T dTypeChksum[22];
  real_T complexity;
};

#endif                                 /*struct_stIdWsMGFAeT5aVn8Ny36oH*/

#ifndef typedef_sIvmHumfM4VG8K4LjAjoqqB
#define typedef_sIvmHumfM4VG8K4LjAjoqqB

typedef struct stIdWsMGFAeT5aVn8Ny36oH sIvmHumfM4VG8K4LjAjoqqB;

#endif                                 /*typedef_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef struct_stIdWsMGFAeT5aVn8Ny36oH_size
#define struct_stIdWsMGFAeT5aVn8Ny36oH_size

struct stIdWsMGFAeT5aVn8Ny36oH_size
{
  int32_T names[2];
  int32_T dims[2];
  int32_T dTypeName[2];
  int32_T dTypeChksum[2];
};

#endif                                 /*struct_stIdWsMGFAeT5aVn8Ny36oH_size*/

#ifndef typedef_sIvmHumfM4VG8K4LjAjoqqB_size
#define typedef_sIvmHumfM4VG8K4LjAjoqqB_size

typedef struct stIdWsMGFAeT5aVn8Ny36oH_size sIvmHumfM4VG8K4LjAjoqqB_size;

#endif                                 /*typedef_sIvmHumfM4VG8K4LjAjoqqB_size*/

#ifndef struct_s7UBIGHSehQY1gCsIQWwr5C
#define struct_s7UBIGHSehQY1gCsIQWwr5C

struct s7UBIGHSehQY1gCsIQWwr5C
{
  real_T chksum[4];
};

#endif                                 /*struct_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef typedef_s7UBIGHSehQY1gCsIQWwr5C
#define typedef_s7UBIGHSehQY1gCsIQWwr5C

typedef struct s7UBIGHSehQY1gCsIQWwr5C s7UBIGHSehQY1gCsIQWwr5C;

#endif                                 /*typedef_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef struct_sIvmHumfM4VG8K4LjAjoqqB
#define struct_sIvmHumfM4VG8K4LjAjoqqB

struct sIvmHumfM4VG8K4LjAjoqqB
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T dTypeSize;
  char_T dTypeName;
  real_T dTypeIndex;
  char_T dTypeChksum;
  real_T complexity;
};

#endif                                 /*struct_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef typedef_b_sIvmHumfM4VG8K4LjAjoqqB
#define typedef_b_sIvmHumfM4VG8K4LjAjoqqB

typedef struct sIvmHumfM4VG8K4LjAjoqqB b_sIvmHumfM4VG8K4LjAjoqqB;

#endif                                 /*typedef_b_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef struct_sfOd2wElE6un66xmZCZog7F
#define struct_sfOd2wElE6un66xmZCZog7F

struct sfOd2wElE6un66xmZCZog7F
{
  real_T dimModes;
  real_T dims[3];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sfOd2wElE6un66xmZCZog7F*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F
#define typedef_sfOd2wElE6un66xmZCZog7F

typedef struct sfOd2wElE6un66xmZCZog7F sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_sRzepbcAzWdfpiYnMYop13F
#define struct_sRzepbcAzWdfpiYnMYop13F

struct sRzepbcAzWdfpiYnMYop13F
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sRzepbcAzWdfpiYnMYop13F*/

#ifndef typedef_sRzepbcAzWdfpiYnMYop13F
#define typedef_sRzepbcAzWdfpiYnMYop13F

typedef struct sRzepbcAzWdfpiYnMYop13F sRzepbcAzWdfpiYnMYop13F;

#endif                                 /*typedef_sRzepbcAzWdfpiYnMYop13F*/

#ifndef struct_sZVQz5WVraeIWEljxFvLe8
#define struct_sZVQz5WVraeIWEljxFvLe8

struct sZVQz5WVraeIWEljxFvLe8
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sZVQz5WVraeIWEljxFvLe8*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8
#define typedef_sZVQz5WVraeIWEljxFvLe8

typedef struct sZVQz5WVraeIWEljxFvLe8 sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sWzLM2sxoJyiBvZh9w5OsiF
#define struct_sWzLM2sxoJyiBvZh9w5OsiF

struct sWzLM2sxoJyiBvZh9w5OsiF
{
  real_T codeGenChksum[4];
};

#endif                                 /*struct_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef typedef_sWzLM2sxoJyiBvZh9w5OsiF
#define typedef_sWzLM2sxoJyiBvZh9w5OsiF

typedef struct sWzLM2sxoJyiBvZh9w5OsiF sWzLM2sxoJyiBvZh9w5OsiF;

#endif                                 /*typedef_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef struct_s06CiRobUsuzDjCqnkPKzOH
#define struct_s06CiRobUsuzDjCqnkPKzOH

struct s06CiRobUsuzDjCqnkPKzOH
{
  boolean_T isInitialized;
  boolean_T isReleased;
  boolean_T TunablePropsChanged;
  char_T RadioAddress[11];
  char_T prequester[14];
  int32_T pDriverHandle;
  int32_T pLastTunablePackedSize;
  uint8_T pLastTunablePackedBuffer[1024];
  char_T OutputDataType[6];
  real_T SamplesPerFrame;
  boolean_T DataIsComplex;
  real_T NumChannels;
  real_T CenterFrequency;
  real_T Attenuation[2];
  real_T NumHWChannels;
  real_T FIRCoefficientSize[2];
  real_T FIRCoefficients[256];
  real_T FIRGain[2];
  real_T FIRDecimInterpFactor[2];
  real_T AnalogFilterCutoff[2];
  real_T FilterPathRates[12];
  char_T FilterDesignTypeForTx[13];
  char_T FilterDesignTypeForRx[22];
  boolean_T pLostSamples;
  real_T pCenterFrequency;
  real_T pAttenuation[2];
};

#endif                                 /*struct_s06CiRobUsuzDjCqnkPKzOH*/

#ifndef typedef_comm_internal_SDRTxZC706FMC23SL
#define typedef_comm_internal_SDRTxZC706FMC23SL

typedef struct s06CiRobUsuzDjCqnkPKzOH comm_internal_SDRTxZC706FMC23SL;

#endif                                 /*typedef_comm_internal_SDRTxZC706FMC23SL*/

#ifndef struct_sg1weJWW8oJqBR8eV7ljIZ
#define struct_sg1weJWW8oJqBR8eV7ljIZ

struct sg1weJWW8oJqBR8eV7ljIZ
{
  real_T TXSAMP;
  real_T TF;
  real_T T1;
  real_T T2;
  real_T DAC;
  real_T BBPLL;
  real_T Coefficient[128];
  real_T CoefficientSize;
  real_T Interp;
  real_T Gain;
  real_T RFBandwidth;
};

#endif                                 /*struct_sg1weJWW8oJqBR8eV7ljIZ*/

#ifndef typedef_sg1weJWW8oJqBR8eV7ljIZ
#define typedef_sg1weJWW8oJqBR8eV7ljIZ

typedef struct sg1weJWW8oJqBR8eV7ljIZ sg1weJWW8oJqBR8eV7ljIZ;

#endif                                 /*typedef_sg1weJWW8oJqBR8eV7ljIZ*/

#ifndef struct_sQHIqGdFAQG8LGsGwUkswcB
#define struct_sQHIqGdFAQG8LGsGwUkswcB

struct sQHIqGdFAQG8LGsGwUkswcB
{
  real_T RXSAMP;
  real_T RF;
  real_T R1;
  real_T R2;
  real_T ADC;
  real_T BBPLL;
  real_T Coefficient[128];
  real_T CoefficientSize;
  real_T Decimation;
  real_T Gain;
  real_T RFBandwidth;
};

#endif                                 /*struct_sQHIqGdFAQG8LGsGwUkswcB*/

#ifndef typedef_sQHIqGdFAQG8LGsGwUkswcB
#define typedef_sQHIqGdFAQG8LGsGwUkswcB

typedef struct sQHIqGdFAQG8LGsGwUkswcB sQHIqGdFAQG8LGsGwUkswcB;

#endif                                 /*typedef_sQHIqGdFAQG8LGsGwUkswcB*/

#ifndef struct_sQATVkQECR7Xg8EzvBP8oS
#define struct_sQATVkQECR7Xg8EzvBP8oS

struct sQATVkQECR7Xg8EzvBP8oS
{
  char_T names[5];
  real_T dims[4];
  real_T dType;
  real_T dTypeSize;
  real_T dTypeIndex;
  char_T dTypeChksum[22];
  real_T complexity;
};

#endif                                 /*struct_sQATVkQECR7Xg8EzvBP8oS*/

#ifndef typedef_sQATVkQECR7Xg8EzvBP8oS
#define typedef_sQATVkQECR7Xg8EzvBP8oS

typedef struct sQATVkQECR7Xg8EzvBP8oS sQATVkQECR7Xg8EzvBP8oS;

#endif                                 /*typedef_sQATVkQECR7Xg8EzvBP8oS*/

#ifndef struct_sT5vzDK5RgCB6kiB1HjrweC
#define struct_sT5vzDK5RgCB6kiB1HjrweC

struct sT5vzDK5RgCB6kiB1HjrweC
{
  char_T names[15];
  real_T dims[4];
  real_T dType;
  real_T dTypeSize;
  real_T dTypeIndex;
  char_T dTypeChksum[22];
  real_T complexity;
};

#endif                                 /*struct_sT5vzDK5RgCB6kiB1HjrweC*/

#ifndef typedef_sT5vzDK5RgCB6kiB1HjrweC
#define typedef_sT5vzDK5RgCB6kiB1HjrweC

typedef struct sT5vzDK5RgCB6kiB1HjrweC sT5vzDK5RgCB6kiB1HjrweC;

#endif                                 /*typedef_sT5vzDK5RgCB6kiB1HjrweC*/

#ifndef struct_sNtlDxKWhi3zqOKIlcsBsgG
#define struct_sNtlDxKWhi3zqOKIlcsBsgG

struct sNtlDxKWhi3zqOKIlcsBsgG
{
  char_T names[6];
  real_T dims[4];
  real_T dType;
  real_T dTypeSize;
  real_T dTypeIndex;
  char_T dTypeChksum[22];
  real_T complexity;
};

#endif                                 /*struct_sNtlDxKWhi3zqOKIlcsBsgG*/

#ifndef typedef_sNtlDxKWhi3zqOKIlcsBsgG
#define typedef_sNtlDxKWhi3zqOKIlcsBsgG

typedef struct sNtlDxKWhi3zqOKIlcsBsgG sNtlDxKWhi3zqOKIlcsBsgG;

#endif                                 /*typedef_sNtlDxKWhi3zqOKIlcsBsgG*/

#ifndef struct_svXqfNjsx9wZbVUjSqS1gnH
#define struct_svXqfNjsx9wZbVUjSqS1gnH

struct svXqfNjsx9wZbVUjSqS1gnH
{
  char_T names[16];
  real_T dims[4];
  real_T dType;
  real_T dTypeSize;
  real_T dTypeIndex;
  char_T dTypeChksum[22];
  real_T complexity;
};

#endif                                 /*struct_svXqfNjsx9wZbVUjSqS1gnH*/

#ifndef typedef_svXqfNjsx9wZbVUjSqS1gnH
#define typedef_svXqfNjsx9wZbVUjSqS1gnH

typedef struct svXqfNjsx9wZbVUjSqS1gnH svXqfNjsx9wZbVUjSqS1gnH;

#endif                                 /*typedef_svXqfNjsx9wZbVUjSqS1gnH*/

#ifndef typedef_InstanceStruct_Hc1X60vKnc326iDURq7NcH
#define typedef_InstanceStruct_Hc1X60vKnc326iDURq7NcH

typedef struct {
  SimStruct *S;
  comm_internal_SDRTxZC706FMC23SL sysobj;
  boolean_T sysobj_not_empty;
  uint32_T method;
  boolean_T method_not_empty;
  uint32_T state;
  boolean_T state_not_empty;
  uint32_T b_state[2];
  boolean_T b_state_not_empty;
  uint32_T c_state[625];
  boolean_T c_state_not_empty;
  creal_T varargin_11[8192];
} InstanceStruct_Hc1X60vKnc326iDURq7NcH;

#endif                                 /*typedef_InstanceStruct_Hc1X60vKnc326iDURq7NcH*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_Hc1X60vKnc326iDURq7NcH(SimStruct *S, int_T method,
  void* data);
extern int autoInfer_dispatcher_Hc1X60vKnc326iDURq7NcH(mxArray *lhs[], const
  char* commandName);

#endif
