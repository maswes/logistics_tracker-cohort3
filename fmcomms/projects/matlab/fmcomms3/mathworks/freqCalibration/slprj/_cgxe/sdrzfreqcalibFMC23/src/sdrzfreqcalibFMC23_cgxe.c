/* Include files */

#include "sdrzfreqcalibFMC23_cgxe.h"
#include "m_Hc1X60vKnc326iDURq7NcH.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_sdrzfreqcalibFMC23_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_sdrzfreqcalibFMC23_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_sdrzfreqcalibFMC23_method_dispatcher(SimStruct* S, int_T
  method, void* data)
{
  if (ssGetChecksum0(S) == 2131908541 &&
      ssGetChecksum1(S) == 3223019714 &&
      ssGetChecksum2(S) == 1952094914 &&
      ssGetChecksum3(S) == 3844393129) {
    method_dispatcher_Hc1X60vKnc326iDURq7NcH(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_sdrzfreqcalibFMC23_autoInfer_dispatcher(const mxArray* prhs, mxArray*
  lhs[], const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzfreqcalibFMC23:44") == 0 ) {
    return autoInfer_dispatcher_Hc1X60vKnc326iDURq7NcH(lhs, commandName);
  }

  return 0;
}
