/* Include files */

#include "sdrzfreqcalib_rxFMC23_cgxe.h"
#include "m_l7JJeF95Mq2jixcdSDpdW.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_sdrzfreqcalib_rxFMC23_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_sdrzfreqcalib_rxFMC23_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_sdrzfreqcalib_rxFMC23_method_dispatcher(SimStruct* S, int_T
  method, void* data)
{
  if (ssGetChecksum0(S) == 2705664043 &&
      ssGetChecksum1(S) == 4107920966 &&
      ssGetChecksum2(S) == 1819517240 &&
      ssGetChecksum3(S) == 726961814) {
    method_dispatcher_l7JJeF95Mq2jixcdSDpdW(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_sdrzfreqcalib_rxFMC23_autoInfer_dispatcher(const mxArray* prhs, mxArray*
  lhs[], const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzfreqcalib_rxFMC23:56") == 0 ) {
    return autoInfer_dispatcher_l7JJeF95Mq2jixcdSDpdW(lhs, commandName);
  }

  return 0;
}
