#define S_FUNCTION_LEVEL               2
#define S_FUNCTION_NAME                sdrzfreqcalib_rxFMC23_cgxe
#include "simstruc.h"
#include "sdrzfreqcalib_rxFMC23_cgxe.h"
#define MDL_START

static void mdlStart(SimStruct* S)
{
  unsigned int success;
  success = cgxe_sdrzfreqcalib_rxFMC23_method_dispatcher(S, SS_CALL_MDL_START,
    NULL);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }
}

#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S)
{
  /*sf_mex_error_direct_call(S, "mdlInitializeConditions", "sf_sfun");*/
  mexPrintf("ERROR: Calling model mdlInitializeConditions method directly.\n");
}

#define MDL_UPDATE

static void mdlUpdate(SimStruct *S, int_T tid)
{
  /*sf_mex_error_direct_call(S, "mdlUpdate", "sf_sfun");*/
  mexPrintf("ERROR: Calling model mdlUpdate method directly.\n");
}

static void mdlOutputs(SimStruct* S, int_T tid)
{
  /*sf_mex_error_direct_call(S, "mdlOutputs", "sf_sfun");*/
  mexPrintf("ERROR: Calling model mdlOUtputs method directly.\n");
}

static void mdlTerminate(SimStruct *S)
{
  /*sf_mex_error_direct_call(S, "mdlOutputs", "sf_sfun");*/
  mexPrintf("ERROR: Calling model mdlTerminate method directly.\n");
}

static void mdlInitializeSizes(SimStruct *S)
{
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
}

static mxArray* cgxe_get_supported_modules(void)
{
  mxArray* mxModules = mxCreateCellMatrix(1, 1);
  mxArray* mxChksum = NULL;
  uint32_T* checksumData = NULL;
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 2705664043;
  checksumData[1] = 4107920966;
  checksumData[2] = 1819517240;
  checksumData[3] = 726961814;
  mxSetCell(mxModules, 0, mxChksum);
  return mxModules;
}

static int cgxe_process_get_checksums(int nlhs, mxArray* plhs[], int nrhs, const
  mxArray* prhs[])
{
  const char* checksumFields[] = { "modules", "model", "makefile", "target",
    "overall" };

  mxArray* mxChecksum = mxCreateStructMatrix(1, 1, 5, checksumFields);
  mxSetField(mxChecksum, 0, "modules", cgxe_get_supported_modules());

  {
    mxArray* mxModelChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxModelChksum);
    checksumData[0] = 140980131;
    checksumData[1] = 2126022907;
    checksumData[2] = 4169780149;
    checksumData[3] = 3676834822;
    mxSetField(mxChecksum, 0, "model", mxModelChksum);
  }

  {
    mxArray* mxMakefileChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxMakefileChksum);
    checksumData[0] = 2048142134;
    checksumData[1] = 183991586;
    checksumData[2] = 2380359986;
    checksumData[3] = 215200257;
    mxSetField(mxChecksum, 0, "makefile", mxMakefileChksum);
  }

  {
    mxArray* mxTargetChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxTargetChksum);
    checksumData[0] = 3372951106;
    checksumData[1] = 918431882;
    checksumData[2] = 3347834876;
    checksumData[3] = 1981024879;
    mxSetField(mxChecksum, 0, "target", mxTargetChksum);
  }

  {
    mxArray* mxOverallChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxOverallChksum);
    checksumData[0] = 3253347055;
    checksumData[1] = 2090977253;
    checksumData[2] = 3844348472;
    checksumData[3] = 2031480312;
    mxSetField(mxChecksum, 0, "overall", mxOverallChksum);
  }

  plhs[0] = mxChecksum;
  return 1;
}

static int cgxe_mex_unlock_call(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  while (mexIsLocked()) {
    mexUnlock();
  }

  return 1;
}

static SimStruct* cgxe_unpack_simstruct(const mxArray *mxS)
{
  int *intPtr = (int*)malloc(sizeof(SimStruct*));
  int nEl = sizeof(SimStruct*)/sizeof(int);
  double *dblPtr = mxGetPr(mxS);
  int el;
  SimStruct *S;
  for (el=0; el < nEl; el++) {
    intPtr[el] = (int)(dblPtr[el]);
  }

  S = *((SimStruct**)intPtr);
  free(intPtr);
  return S;
}

static int cgxe_get_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_sdrzfreqcalib_rxFMC23_method_dispatcher(S,
    SS_CALL_MDL_GET_SIM_STATE, (void *) (plhs));
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_set_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_sdrzfreqcalib_rxFMC23_method_dispatcher(S,
    SS_CALL_MDL_SET_SIM_STATE, (void *) prhs[2]);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_get_BuildInfoUpdate(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "l7JJeF95Mq2jixcdSDpdW") == 0) {
    extern mxArray *cgxe_l7JJeF95Mq2jixcdSDpdW_BuildInfoUpdate(void);
    plhs[0] = cgxe_l7JJeF95Mq2jixcdSDpdW_BuildInfoUpdate();
    return 1;
  }

  return 0;
}

#define PROCESS_MEX_SFUNCTION_CMD_LINE_CALL

static int ProcessMexSfunctionCmdLineCall(int nlhs, mxArray* plhs[], int nrhs,
  const mxArray* prhs[])
{
  char commandName[64];
  if (nrhs < 1 || !mxIsChar(prhs[0]))
    return 0;
  mxGetString(prhs[0], commandName, sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName, "get_checksums") == 0) {
    return cgxe_process_get_checksums(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "mex_unlock") == 0) {
    return cgxe_mex_unlock_call(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_sim_state") == 0) {
    return cgxe_get_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if ((strcmp(commandName, "NameResolution") == 0)||
      (strcmp(commandName, "AutoInfer") == 0)) {
    if (nrhs < 2 || !mxIsChar(prhs[1]))
      return 0;
    return cgxe_sdrzfreqcalib_rxFMC23_autoInfer_dispatcher(prhs[1], plhs,
      commandName);
  }

  if (strcmp(commandName, "set_sim_state") == 0) {
    return cgxe_set_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_BuildInfoUpdate") == 0) {
    return cgxe_get_BuildInfoUpdate(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "mex_initialize") == 0) {
    cgxe_sdrzfreqcalib_rxFMC23_initializer();
    return 1;
  }

  if (strcmp(commandName, "mex_terminate") == 0) {
    cgxe_sdrzfreqcalib_rxFMC23_terminator();
    return 1;
  }

  return 0;
}

#include "simulink.c"
