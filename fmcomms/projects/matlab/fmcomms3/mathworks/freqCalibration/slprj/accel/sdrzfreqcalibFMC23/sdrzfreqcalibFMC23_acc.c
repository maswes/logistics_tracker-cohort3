#include "__cf_sdrzfreqcalibFMC23.h"
#include <math.h>
#include "sdrzfreqcalibFMC23_acc.h"
#include "sdrzfreqcalibFMC23_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
static void mdlOutputs ( SimStruct * S , int_T tid ) { real_T updateVal ;
int32_T j ; pxpuckcnqf * _rtB ; laq1ljlnuo * _rtP ; pywewm3dhq * _rtDW ;
_rtDW = ( ( pywewm3dhq * ) ssGetRootDWork ( S ) ) ; _rtP = ( ( laq1ljlnuo * )
ssGetDefaultParam ( S ) ) ; _rtB = ( ( pxpuckcnqf * ) _ssGetBlockIO ( S ) ) ;
updateVal = _rtP -> P_3 * 2.5566346464760685E-5 ; for ( j = 0 ; j < 4096 ; j
++ ) { _rtB -> bsaja4lu0q [ j ] . re = _rtP -> P_2 * muDoubleScalarCos (
_rtDW -> bzsfgwr1bk ) ; _rtB -> bsaja4lu0q [ j ] . im = _rtP -> P_2 *
muDoubleScalarSin ( _rtDW -> bzsfgwr1bk ) ; _rtDW -> bzsfgwr1bk += updateVal
; if ( _rtDW -> bzsfgwr1bk >= 6.2831853071795862 ) { _rtDW -> bzsfgwr1bk -=
6.2831853071795862 ; } else { if ( _rtDW -> bzsfgwr1bk < 0.0 ) { _rtDW ->
bzsfgwr1bk += 6.2831853071795862 ; } } } memcpy ( & _rtB -> bsaja4lu0q [ 4096
] , & _rtP -> P_5 [ 0 ] , sizeof ( creal_T ) << 12U ) ; ssCallAccelRunBlock (
S , 0 , 0 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock ( S , 1 , 4 ,
SS_CALL_MDL_OUTPUTS ) ; UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { UNUSED_PARAMETER ( tid
) ; } static void mdlInitializeSizes ( SimStruct * S ) { ssSetChecksumVal ( S
, 0 , 362780475U ) ; ssSetChecksumVal ( S , 1 , 1727337027U ) ;
ssSetChecksumVal ( S , 2 , 723757451U ) ; ssSetChecksumVal ( S , 3 ,
3213308978U ) ; { mxArray * slVerStructMat = NULL ; mxArray * slStrMat =
mxCreateString ( "simulink" ) ; char slVerChar [ 10 ] ; int status =
mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver" ) ; if ( status
== 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0 , "Version" ) ;
if ( slVerMat == NULL ) { status = 1 ; } else { status = mxGetString (
slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.4" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
pywewm3dhq ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( pxpuckcnqf ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } { int ssSizeofParams ;
ssGetSizeofParams ( S , & ssSizeofParams ) ; if ( ssSizeofParams != sizeof (
laq1ljlnuo ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetDefaultParam ( S , (
real_T * ) & jlyxrthgft ) ; } static void mdlInitializeSampleTimes (
SimStruct * S ) { } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
