#include "__cf_sdrzfreqcalibFMC23.h"
#ifndef RTW_HEADER_sdrzfreqcalibFMC23_acc_h_
#define RTW_HEADER_sdrzfreqcalibFMC23_acc_h_
#include <string.h>
#include <stddef.h>
#ifndef sdrzfreqcalibFMC23_acc_COMMON_INCLUDES_
#define sdrzfreqcalibFMC23_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "sdrzfreqcalibFMC23_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
typedef struct { creal_T bsaja4lu0q [ 8192 ] ; boolean_T devflwhhu1 ; char
pad_devflwhhu1 [ 7 ] ; } pxpuckcnqf ; typedef struct { real_T bzsfgwr1bk ;
struct { void * LoggedData ; } izn34nurr4 ; void * l00c3kjfr3 ; } pywewm3dhq
; struct laq1ljlnuo_ { real_T P_0 ; real_T P_1 [ 2 ] ; real_T P_2 ; real_T
P_3 ; real_T P_4 ; creal_T P_5 [ 4096 ] ; } ; extern laq1ljlnuo jlyxrthgft ;
#endif
