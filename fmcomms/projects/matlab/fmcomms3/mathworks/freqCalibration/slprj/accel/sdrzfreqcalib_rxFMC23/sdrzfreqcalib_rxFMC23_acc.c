#include "__cf_sdrzfreqcalib_rxFMC23.h"
#include <math.h>
#include "sdrzfreqcalib_rxFMC23_acc.h"
#include "sdrzfreqcalib_rxFMC23_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
static void mdlOutputs ( SimStruct * S , int_T tid ) { int32_T i ; int32_T
colIdx ; int32_T loop ; real_T nq2nr5hj35 ; real_T oa0d25axwy ; creal_T u ;
ktpaitr01z * _rtB ; egmpikrece * _rtP ; f4vhbxqnfr * _rtDW ; _rtDW = ( (
f4vhbxqnfr * ) ssGetRootDWork ( S ) ) ; _rtP = ( ( egmpikrece * )
ssGetDefaultParam ( S ) ) ; _rtB = ( ( ktpaitr01z * ) _ssGetBlockIO ( S ) ) ;
ssCallAccelRunBlock ( S , 0 , 0 , SS_CALL_MDL_OUTPUTS ) ; memcpy ( & _rtB ->
ajsprmkq4l [ 0 ] , & _rtB -> fzk54pvr0m [ 0 ] , sizeof ( creal_T ) << 12U ) ;
if ( _rtB -> atfmgxnlb2 > 0.0 ) { if ( ! _rtDW -> ggvvwqzltb ) {
ssCallAccelRunBlock ( S , 1 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ; _rtDW ->
ggvvwqzltb = true ; } ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_MDL_OUTPUTS )
; colIdx = 0 ; loop = 0 ; for ( i = 0 ; i < 4096 ; i ++ ) { _rtB ->
kf43khd2bc [ colIdx ] . re = _rtB -> ajsprmkq4l [ colIdx ] . re * eb1zamhryn
. p31pryshom [ loop ] ; _rtB -> kf43khd2bc [ colIdx ] . im = _rtB ->
ajsprmkq4l [ colIdx ] . im * eb1zamhryn . p31pryshom [ loop ] ; colIdx ++ ;
loop ++ ; } if ( ! _rtDW -> mgewj3wc3h ) { _rtDW -> mgewj3wc3h = true ; loop
= 0 ; for ( i = 0 ; i < 4096 ; i ++ ) { _rtB -> mal00juvrv [ loop ] =
eb1zamhryn . p31pryshom [ loop ] ; loop ++ ; } } LibOutputs_FFT ( & _rtDW ->
mgdzzzviuj [ 0U ] , & _rtB -> kf43khd2bc [ 0U ] , & _rtB -> pam43jhmpe [ 0U ]
, 4096 , 1 ) ; for ( i = 0 ; i < 4096 ; i ++ ) { u = _rtB -> pam43jhmpe [ i ]
; _rtB -> d55an0d3nc [ i ] = u . re * u . re + u . im * u . im ; } colIdx = 0
; for ( loop = 0 ; loop < 4096 ; loop ++ ) { oa0d25axwy = _rtB -> d55an0d3nc
[ colIdx ] * _rtP -> P_2 [ 0 ] ; colIdx ++ ; oa0d25axwy += _rtDW ->
oxcaimutu2 [ loop ] * _rtP -> P_2 [ 1 ] ; _rtB -> msttqldim3 [ loop ] =
oa0d25axwy ; } oa0d25axwy = 0.0 ; for ( loop = 0 ; loop < 4096 ; loop ++ ) {
oa0d25axwy += _rtB -> mal00juvrv [ loop ] * _rtB -> mal00juvrv [ loop ] ; }
oa0d25axwy = 1.0 / oa0d25axwy ; _rtB -> amroq0jufq = _rtB -> nbjbt5gi5x [ 0 ]
; nq2nr5hj35 = _rtB -> mse5q2qoqp [ 0 ] / _rtB -> amroq0jufq ; for ( i = 0 ;
i < 4096 ; i ++ ) { _rtB -> m1ispyrak3 [ i ] = _rtB -> msttqldim3 [ i ] *
oa0d25axwy * nq2nr5hj35 ; } ssCallAccelRunBlock ( S , 1 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> nmiz4jamvt = _rtB -> mvppapyzd3 - _rtP -> P_3
; srUpdateBC ( _rtDW -> p30uqd4cg2 ) ; } else { if ( _rtDW -> ggvvwqzltb ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_MDL_DISABLE ) ; ssCallAccelRunBlock
( S , 1 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW -> ggvvwqzltb = false ;
} } ssCallAccelRunBlock ( S , 3 , 3 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 3 , 4 , SS_CALL_MDL_OUTPUTS ) ; UNUSED_PARAMETER (
tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { ktpaitr01z * _rtB ;
f4vhbxqnfr * _rtDW ; _rtDW = ( ( f4vhbxqnfr * ) ssGetRootDWork ( S ) ) ; _rtB
= ( ( ktpaitr01z * ) _ssGetBlockIO ( S ) ) ; if ( _rtDW -> ggvvwqzltb ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_MDL_UPDATE ) ; memcpy ( & _rtDW ->
oxcaimutu2 [ 0 ] , & _rtB -> d55an0d3nc [ 0 ] , sizeof ( real_T ) << 12U ) ;
} UNUSED_PARAMETER ( tid ) ; } static void mdlInitializeSizes ( SimStruct * S
) { ssSetChecksumVal ( S , 0 , 112951018U ) ; ssSetChecksumVal ( S , 1 ,
4204419296U ) ; ssSetChecksumVal ( S , 2 , 3649446632U ) ; ssSetChecksumVal (
S , 3 , 1030036093U ) ; { mxArray * slVerStructMat = NULL ; mxArray *
slStrMat = mxCreateString ( "simulink" ) ; char slVerChar [ 10 ] ; int status
= mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver" ) ; if (
status == 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0 ,
"Version" ) ; if ( slVerMat == NULL ) { status = 1 ; } else { status =
mxGetString ( slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.4" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
f4vhbxqnfr ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( ktpaitr01z ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } { int ssSizeofParams ;
ssGetSizeofParams ( S , & ssSizeofParams ) ; if ( ssSizeofParams != sizeof (
egmpikrece ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetDefaultParam ( S , (
real_T * ) & pla1xyawqo ) ; rt_InitInfAndNaN ( sizeof ( real_T ) ) ; } static
void mdlInitializeSampleTimes ( SimStruct * S ) { { SimStruct * childS ;
SysOutputFcn * callSysFcns ; childS = ssGetSFunction ( S , 0 ) ; callSysFcns
= ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; } } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
