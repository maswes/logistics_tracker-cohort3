#include "__cf_sdrzfreqcalib_rxFMC23.h"
#ifndef RTW_HEADER_sdrzfreqcalib_rxFMC23_acc_h_
#define RTW_HEADER_sdrzfreqcalib_rxFMC23_acc_h_
#include <stddef.h>
#include <string.h>
#ifndef sdrzfreqcalib_rxFMC23_acc_COMMON_INCLUDES_
#define sdrzfreqcalib_rxFMC23_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "HostLib_FFT.h"
#endif
#include "sdrzfreqcalib_rxFMC23_acc_types.h"
#include "multiword_types.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
typedef struct { creal_T ajsprmkq4l [ 4096 ] ; creal_T fzk54pvr0m [ 8192 ] ;
creal_T kf43khd2bc [ 4096 ] ; creal_T pam43jhmpe [ 4096 ] ; real_T o351avpo4i
; real_T kbqd2vn3zd [ 2 ] ; real_T mal00juvrv [ 4096 ] ; real_T d55an0d3nc [
4096 ] ; real_T mse5q2qoqp [ 2 ] ; real_T nbjbt5gi5x [ 2 ] ; real_T
amroq0jufq ; real_T m1ispyrak3 [ 4096 ] ; real_T nmiz4jamvt ; real_T
mvppapyzd3 ; real_T atfmgxnlb2 ; real_T msttqldim3 [ 4096 ] ; boolean_T
alzaoppdlm ; char pad_alzaoppdlm [ 7 ] ; } ktpaitr01z ; typedef struct {
real_T oxcaimutu2 [ 4096 ] ; real_T mgdzzzviuj [ 137 ] ; struct { void *
LoggedData ; } oslcx2hijr ; void * dde0xgaxc5 ; int8_T p30uqd4cg2 ; boolean_T
mgewj3wc3h ; boolean_T ggvvwqzltb ; char pad_ggvvwqzltb [ 5 ] ; } f4vhbxqnfr
; typedef struct { real_T p31pryshom [ 4096 ] ; } jkeqipfx0p ; struct
egmpikrece_ { real_T P_0 ; real_T P_1 ; real_T P_2 [ 2 ] ; real_T P_3 ; } ;
extern egmpikrece pla1xyawqo ; extern const jkeqipfx0p eb1zamhryn ;
#endif
