// -------------------------------------------------------------
// 
// File Name: hdl_prj2_cplx\hdlsrc\commqpskrxhdl\MATLAB_Function3.v
// Created: 2015-05-27 12:23:08
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: MATLAB_Function3
// Source Path: commqpskrxhdl/HDLRx/Data Decoding/MATLAB Function3
// Hierarchy Level: 2
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module MATLAB_Function3
          (
           clk,
           reset,
           enb_1_2_0,
           enable,
           MF_Conj_re,
           MF_Conj_im,
           dvalid,
           Freeze_MF_Conj_re,
           Freeze_MF_Conj_im
          );


  input   clk;
  input   reset;
  input   enb_1_2_0;
  input   enable;
  input   signed [15:0] MF_Conj_re;  // sfix16_En10
  input   signed [15:0] MF_Conj_im;  // sfix16_En10
  output  dvalid;
  output  signed [15:0] Freeze_MF_Conj_re;  // sfix16_En10
  output  signed [15:0] Freeze_MF_Conj_im;  // sfix16_En10


  reg  dvalid_1;
  reg signed [15:0] Freeze_MF_Conj_re_1;  // sfix16_En10
  reg signed [15:0] Freeze_MF_Conj_im_1;  // sfix16_En10
  reg  state;
  reg [7:0] counter;  // uint8
  reg signed [15:0] MF_Out_Reg_re;  // sfix16_En10
  reg signed [15:0] MF_Out_Reg_im;  // sfix16_En10
  reg  state_next;
  reg [7:0] counter_next;  // uint8
  reg signed [15:0] MF_Out_Reg_next_re;  // sfix16_En10
  reg signed [15:0] MF_Out_Reg_next_im;  // sfix16_En10
  reg [7:0] counter_temp_1;  // uint8
  reg signed [8:0] sub_temp_1;  // sfix9


  always @(posedge clk)
    begin : MATLAB_Function3_1_process
      if (reset == 1'b1) begin
        state <= 1'b0;
        counter <= 8'd0;
        MF_Out_Reg_re <= 16'sb0000000000000000;
        MF_Out_Reg_im <= 16'sb0000000000000000;
      end
      else if (enb_1_2_0) begin
        state <= state_next;
        counter <= counter_next;
        MF_Out_Reg_re <= MF_Out_Reg_next_re;
        MF_Out_Reg_im <= MF_Out_Reg_next_im;
      end
    end

  always @(enable, MF_Conj_re, MF_Conj_im, state, counter, MF_Out_Reg_re, MF_Out_Reg_im) begin
    counter_temp_1 = counter;
    state_next = state;
    MF_Out_Reg_next_re = MF_Out_Reg_re;
    MF_Out_Reg_next_im = MF_Out_Reg_im;
    Freeze_MF_Conj_re_1 = MF_Out_Reg_re;
    Freeze_MF_Conj_im_1 = MF_Out_Reg_im;
    if (state == 32'sd0) begin
      dvalid_1 = 1'b0;
    end
    else begin
      //state==true
      dvalid_1 = 1'b1;
      sub_temp_1 = $signed({1'b0, counter}) - 1;
      if (sub_temp_1[8] == 1'b1) begin
        counter_temp_1 = 8'b00000000;
      end
      else begin
        counter_temp_1 = sub_temp_1[7:0];
      end
    end
    // state transition
    if ((state == 32'sd0) && (enable == 32'sd1)) begin
      state_next = 1'b1;
      counter_temp_1 = 8'd100;
      MF_Out_Reg_next_re = MF_Conj_re;
      MF_Out_Reg_next_im = MF_Conj_im;
    end
    else if ((state == 32'sd1) && (counter_temp_1 == 32'sd0)) begin
      if (enable == 32'sd1) begin
        // packages come back-to-back
        state_next = 1'b1;
        counter_temp_1 = 8'd100;
        MF_Out_Reg_next_re = MF_Conj_re;
        MF_Out_Reg_next_im = MF_Conj_im;
      end
      else begin
        state_next = 1'b0;
      end
    end
    counter_next = counter_temp_1;
  end



  assign dvalid = dvalid_1;

  assign Freeze_MF_Conj_re = Freeze_MF_Conj_re_1;

  assign Freeze_MF_Conj_im = Freeze_MF_Conj_im_1;

endmodule  // MATLAB_Function3

