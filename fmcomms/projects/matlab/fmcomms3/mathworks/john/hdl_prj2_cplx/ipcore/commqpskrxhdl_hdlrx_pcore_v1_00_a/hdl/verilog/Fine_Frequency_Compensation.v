// -------------------------------------------------------------
// 
// File Name: hdl_prj2_cplx\hdlsrc\commqpskrxhdl\Fine_Frequency_Compensation.v
// Created: 2015-05-27 12:23:09
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: Fine_Frequency_Compensation
// Source Path: commqpskrxhdl/HDLRx/Fine Frequency Compensation
// Hierarchy Level: 1
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module Fine_Frequency_Compensation
          (
           clk,
           reset,
           enb_1_2_0,
           enb,
           In_re,
           In_im,
           Out_re,
           Out_im
          );


  input   clk;
  input   reset;
  input   enb_1_2_0;
  input   enb;
  input   signed [15:0] In_re;  // sfix16_En14
  input   signed [15:0] In_im;  // sfix16_En14
  output  signed [15:0] Out_re;  // sfix16_En14
  output  signed [15:0] Out_im;  // sfix16_En14


  reg signed [15:0] Delay5_reg_re [0:8];  // sfix16_En14 [9]
  reg signed [15:0] Delay5_reg_im [0:8];  // sfix16_En14 [9]
  wire signed [15:0] Delay5_reg_next_re [0:8];  // sfix16_En14 [9]
  wire signed [15:0] Delay5_reg_next_im [0:8];  // sfix16_En14 [9]
  wire signed [15:0] Delay5_out1_re;  // sfix16_En14
  wire signed [15:0] Delay5_out1_im;  // sfix16_En14
  reg signed [15:0] Delay2_out1_re;  // sfix16_En14
  reg signed [15:0] Delay2_out1_im;  // sfix16_En14
  wire signed [15:0] e;  // sfix16_En14
  wire signed [21:0] Loop_Filter_out1;  // sfix22_En19
  wire signed [9:0] NCO_out1_re;  // sfix10_En8
  wire signed [9:0] NCO_out1_im;  // sfix10_En8
  wire signed [25:0] Product9_mul_temp;  // sfix26_En22
  wire signed [15:0] Product9_sub_cast;  // sfix16_En14
  wire signed [16:0] Product9_sub_cast_1;  // sfix17_En14
  wire signed [25:0] Product9_mul_temp_1;  // sfix26_En22
  wire signed [15:0] Product9_sub_cast_2;  // sfix16_En14
  wire signed [16:0] Product9_sub_cast_3;  // sfix17_En14
  wire signed [16:0] Product9_sub_temp;  // sfix17_En14
  wire signed [25:0] Product9_mul_temp_2;  // sfix26_En22
  wire signed [15:0] Product9_add_cast;  // sfix16_En14
  wire signed [16:0] Product9_add_cast_1;  // sfix17_En14
  wire signed [25:0] Product9_mul_temp_3;  // sfix26_En22
  wire signed [15:0] Product9_add_cast_2;  // sfix16_En14
  wire signed [16:0] Product9_add_cast_3;  // sfix17_En14
  wire signed [16:0] Product9_add_temp;  // sfix17_En14
  wire signed [15:0] Product9_out1_re;  // sfix16_En14
  wire signed [15:0] Product9_out1_im;  // sfix16_En14


  always @(posedge clk)
    begin : Delay5_process
      if (reset == 1'b1) begin
        Delay5_reg_re[0] <= 16'sb0000000000000000;
        Delay5_reg_im[0] <= 16'sb0000000000000000;
        Delay5_reg_re[1] <= 16'sb0000000000000000;
        Delay5_reg_im[1] <= 16'sb0000000000000000;
        Delay5_reg_re[2] <= 16'sb0000000000000000;
        Delay5_reg_im[2] <= 16'sb0000000000000000;
        Delay5_reg_re[3] <= 16'sb0000000000000000;
        Delay5_reg_im[3] <= 16'sb0000000000000000;
        Delay5_reg_re[4] <= 16'sb0000000000000000;
        Delay5_reg_im[4] <= 16'sb0000000000000000;
        Delay5_reg_re[5] <= 16'sb0000000000000000;
        Delay5_reg_im[5] <= 16'sb0000000000000000;
        Delay5_reg_re[6] <= 16'sb0000000000000000;
        Delay5_reg_im[6] <= 16'sb0000000000000000;
        Delay5_reg_re[7] <= 16'sb0000000000000000;
        Delay5_reg_im[7] <= 16'sb0000000000000000;
        Delay5_reg_re[8] <= 16'sb0000000000000000;
        Delay5_reg_im[8] <= 16'sb0000000000000000;
      end
      else if (enb_1_2_0) begin
        Delay5_reg_re[0] <= Delay5_reg_next_re[0];
        Delay5_reg_im[0] <= Delay5_reg_next_im[0];
        Delay5_reg_re[1] <= Delay5_reg_next_re[1];
        Delay5_reg_im[1] <= Delay5_reg_next_im[1];
        Delay5_reg_re[2] <= Delay5_reg_next_re[2];
        Delay5_reg_im[2] <= Delay5_reg_next_im[2];
        Delay5_reg_re[3] <= Delay5_reg_next_re[3];
        Delay5_reg_im[3] <= Delay5_reg_next_im[3];
        Delay5_reg_re[4] <= Delay5_reg_next_re[4];
        Delay5_reg_im[4] <= Delay5_reg_next_im[4];
        Delay5_reg_re[5] <= Delay5_reg_next_re[5];
        Delay5_reg_im[5] <= Delay5_reg_next_im[5];
        Delay5_reg_re[6] <= Delay5_reg_next_re[6];
        Delay5_reg_im[6] <= Delay5_reg_next_im[6];
        Delay5_reg_re[7] <= Delay5_reg_next_re[7];
        Delay5_reg_im[7] <= Delay5_reg_next_im[7];
        Delay5_reg_re[8] <= Delay5_reg_next_re[8];
        Delay5_reg_im[8] <= Delay5_reg_next_im[8];
      end
    end

  assign Delay5_out1_re = Delay5_reg_re[8];
  assign Delay5_out1_im = Delay5_reg_im[8];
  assign Delay5_reg_next_re[0] = In_re;
  assign Delay5_reg_next_im[0] = In_im;
  assign Delay5_reg_next_re[1] = Delay5_reg_re[0];
  assign Delay5_reg_next_im[1] = Delay5_reg_im[0];
  assign Delay5_reg_next_re[2] = Delay5_reg_re[1];
  assign Delay5_reg_next_im[2] = Delay5_reg_im[1];
  assign Delay5_reg_next_re[3] = Delay5_reg_re[2];
  assign Delay5_reg_next_im[3] = Delay5_reg_im[2];
  assign Delay5_reg_next_re[4] = Delay5_reg_re[3];
  assign Delay5_reg_next_im[4] = Delay5_reg_im[3];
  assign Delay5_reg_next_re[5] = Delay5_reg_re[4];
  assign Delay5_reg_next_im[5] = Delay5_reg_im[4];
  assign Delay5_reg_next_re[6] = Delay5_reg_re[5];
  assign Delay5_reg_next_im[6] = Delay5_reg_im[5];
  assign Delay5_reg_next_re[7] = Delay5_reg_re[6];
  assign Delay5_reg_next_im[7] = Delay5_reg_im[6];
  assign Delay5_reg_next_re[8] = Delay5_reg_re[7];
  assign Delay5_reg_next_im[8] = Delay5_reg_im[7];



  MATLAB_Function1   u_MATLAB_Function1   (.In_re(Delay2_out1_re),  // sfix16_En14
                                           .In_im(Delay2_out1_im),  // sfix16_En14
                                           .e(e)  // sfix16_En14
                                           );

  Loop_Filter   u_Loop_Filter   (.clk(clk),
                                 .reset(reset),
                                 .enb_1_2_0(enb_1_2_0),
                                 .e(e),  // sfix16_En14
                                 .v(Loop_Filter_out1)  // sfix22_En19
                                 );

  NCO   u_NCO   (.clk(clk),
                 .reset(reset),
                 .enb_1_2_0(enb_1_2_0),
                 .enb(enb),
                 .In1(Loop_Filter_out1),  // sfix22_En19
                 .Out1_re(NCO_out1_re),  // sfix10_En8
                 .Out1_im(NCO_out1_im)  // sfix10_En8
                 );

  assign Product9_mul_temp = Delay5_out1_re * NCO_out1_re;
  assign Product9_sub_cast = ((Product9_mul_temp[25] == 1'b0) && (Product9_mul_temp[24:23] != 2'b00) ? 16'sb0111111111111111 :
              ((Product9_mul_temp[25] == 1'b1) && (Product9_mul_temp[24:23] != 2'b11) ? 16'sb1000000000000000 :
              Product9_mul_temp[23:8] + $signed({1'b0, Product9_mul_temp[25] & (|Product9_mul_temp[7:0])})));
  assign Product9_sub_cast_1 = {Product9_sub_cast[15], Product9_sub_cast};
  assign Product9_mul_temp_1 = Delay5_out1_im * NCO_out1_im;
  assign Product9_sub_cast_2 = ((Product9_mul_temp_1[25] == 1'b0) && (Product9_mul_temp_1[24:23] != 2'b00) ? 16'sb0111111111111111 :
              ((Product9_mul_temp_1[25] == 1'b1) && (Product9_mul_temp_1[24:23] != 2'b11) ? 16'sb1000000000000000 :
              Product9_mul_temp_1[23:8] + $signed({1'b0, Product9_mul_temp_1[25] & (|Product9_mul_temp_1[7:0])})));
  assign Product9_sub_cast_3 = {Product9_sub_cast_2[15], Product9_sub_cast_2};
  assign Product9_sub_temp = Product9_sub_cast_1 - Product9_sub_cast_3;
  assign Product9_out1_re = ((Product9_sub_temp[16] == 1'b0) && (Product9_sub_temp[15] != 1'b0) ? 16'sb0111111111111111 :
              ((Product9_sub_temp[16] == 1'b1) && (Product9_sub_temp[15] != 1'b1) ? 16'sb1000000000000000 :
              $signed(Product9_sub_temp[15:0])));
  assign Product9_mul_temp_2 = Delay5_out1_im * NCO_out1_re;
  assign Product9_add_cast = ((Product9_mul_temp_2[25] == 1'b0) && (Product9_mul_temp_2[24:23] != 2'b00) ? 16'sb0111111111111111 :
              ((Product9_mul_temp_2[25] == 1'b1) && (Product9_mul_temp_2[24:23] != 2'b11) ? 16'sb1000000000000000 :
              Product9_mul_temp_2[23:8] + $signed({1'b0, Product9_mul_temp_2[25] & (|Product9_mul_temp_2[7:0])})));
  assign Product9_add_cast_1 = {Product9_add_cast[15], Product9_add_cast};
  assign Product9_mul_temp_3 = Delay5_out1_re * NCO_out1_im;
  assign Product9_add_cast_2 = ((Product9_mul_temp_3[25] == 1'b0) && (Product9_mul_temp_3[24:23] != 2'b00) ? 16'sb0111111111111111 :
              ((Product9_mul_temp_3[25] == 1'b1) && (Product9_mul_temp_3[24:23] != 2'b11) ? 16'sb1000000000000000 :
              Product9_mul_temp_3[23:8] + $signed({1'b0, Product9_mul_temp_3[25] & (|Product9_mul_temp_3[7:0])})));
  assign Product9_add_cast_3 = {Product9_add_cast_2[15], Product9_add_cast_2};
  assign Product9_add_temp = Product9_add_cast_1 + Product9_add_cast_3;
  assign Product9_out1_im = ((Product9_add_temp[16] == 1'b0) && (Product9_add_temp[15] != 1'b0) ? 16'sb0111111111111111 :
              ((Product9_add_temp[16] == 1'b1) && (Product9_add_temp[15] != 1'b1) ? 16'sb1000000000000000 :
              $signed(Product9_add_temp[15:0])));



  always @(posedge clk)
    begin : Delay2_process
      if (reset == 1'b1) begin
        Delay2_out1_re <= 16'sb0000000000000000;
        Delay2_out1_im <= 16'sb0000000000000000;
      end
      else if (enb_1_2_0) begin
        Delay2_out1_re <= Product9_out1_re;
        Delay2_out1_im <= Product9_out1_im;
      end
    end



  assign Out_re = Delay2_out1_re;

  assign Out_im = Delay2_out1_im;

endmodule  // Fine_Frequency_Compensation

