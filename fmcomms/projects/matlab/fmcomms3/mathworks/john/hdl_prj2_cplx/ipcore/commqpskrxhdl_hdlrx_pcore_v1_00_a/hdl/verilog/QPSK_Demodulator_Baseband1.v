// -------------------------------------------------------------
// 
// File Name: hdl_prj2_cplx\hdlsrc\commqpskrxhdl\QPSK_Demodulator_Baseband1.v
// Created: 2015-05-27 12:23:08
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: QPSK_Demodulator_Baseband1
// Source Path: commqpskrxhdl/HDLRx/Data Decoding/QPSK Demodulator Baseband1
// Hierarchy Level: 2
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module QPSK_Demodulator_Baseband1
          (
           in0_re,
           in0_im,
           out0_0,
           out0_1
          );


  input   signed [31:0] in0_re;  // sfix32_En20
  input   signed [31:0] in0_im;  // sfix32_En20
  output  out0_0;  // boolean
  output  out0_1;  // boolean


  wire inphase_lt_zero;
  wire inphase_eq_zero;
  wire quadrature_lt_zero;
  wire quadrature_eq_zero;
  wire [3:0] decisionLUTaddr;  // ufix4
  wire [1:0] DirectLookupTable_1 [0:15];  // ufix2 [16]
  wire [1:0] hardDecision;  // ufix2
  wire out0_part0;
  wire out0_part1;


  assign inphase_lt_zero = (in0_re < 32'sb00000000000000000000000000000000 ? 1'b1 :
              1'b0);



  assign inphase_eq_zero = (in0_re == 32'sb00000000000000000000000000000000 ? 1'b1 :
              1'b0);



  assign quadrature_lt_zero = (in0_im < 32'sb00000000000000000000000000000000 ? 1'b1 :
              1'b0);



  assign quadrature_eq_zero = (in0_im == 32'sb00000000000000000000000000000000 ? 1'b1 :
              1'b0);



  assign decisionLUTaddr = {inphase_lt_zero, inphase_eq_zero, quadrature_lt_zero, quadrature_eq_zero};



  assign DirectLookupTable_1[0] = 2'b00;
  assign DirectLookupTable_1[1] = 2'b00;
  assign DirectLookupTable_1[2] = 2'b10;
  assign DirectLookupTable_1[3] = 2'b00;
  assign DirectLookupTable_1[4] = 2'b01;
  assign DirectLookupTable_1[5] = 2'b00;
  assign DirectLookupTable_1[6] = 2'b10;
  assign DirectLookupTable_1[7] = 2'b00;
  assign DirectLookupTable_1[8] = 2'b01;
  assign DirectLookupTable_1[9] = 2'b11;
  assign DirectLookupTable_1[10] = 2'b11;
  assign DirectLookupTable_1[11] = 2'b00;
  assign DirectLookupTable_1[12] = 2'b00;
  assign DirectLookupTable_1[13] = 2'b00;
  assign DirectLookupTable_1[14] = 2'b00;
  assign DirectLookupTable_1[15] = 2'b00;
  assign hardDecision = DirectLookupTable_1[decisionLUTaddr];



  assign out0_part0 = hardDecision[1];



  assign out0_0 = out0_part0;

  assign out0_part1 = hardDecision[0];



  assign out0_1 = out0_part1;

endmodule  // QPSK_Demodulator_Baseband1

