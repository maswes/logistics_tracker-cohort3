// -------------------------------------------------------------
// 
// File Name: hdl_prj2_cplx\hdlsrc\commqpskrxhdl\YComponent_block1.v
// Created: 2015-05-27 12:23:04
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: YComponent_block1
// Source Path: commqpskrxhdl/HDLRx/Coarse Frequency Compensation/luise_fixpt/CORDIC/cordic_stage3/YComponent
// Hierarchy Level: 5
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module YComponent_block1
          (
           x,
           y,
           Sign,
           Next_Y
          );


  input   signed [31:0] x;  // sfix32_En26
  input   signed [31:0] y;  // sfix32_En26
  input   Sign;
  output  signed [31:0] Next_Y;  // sfix32_En26


  wire signed [31:0] Shift_Arith_out1;  // sfix32_En26
  wire signed [31:0] ZAccumulator2_out1;  // sfix32_En26


  assign Shift_Arith_out1 = x >>> 3;



  ZAccumulator2_block6   u_ZAccumulator2   (.x(Shift_Arith_out1),  // sfix32_En26
                                            .op(Sign),
                                            .y(y),  // sfix32_En26
                                            .Out1(ZAccumulator2_out1)  // sfix32_En26
                                            );

  assign Next_Y = ZAccumulator2_out1;

endmodule  // YComponent_block1

