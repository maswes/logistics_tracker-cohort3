// -------------------------------------------------------------
// 
// File Name: hdl_prj2_cplx\hdlsrc\commqpskrxhdl\ZAccumulator2_block7.v
// Created: 2015-05-27 12:23:04
// 
// Generated by MATLAB 8.3 and HDL Coder 3.4
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: ZAccumulator2_block7
// Source Path: commqpskrxhdl/HDLRx/Coarse Frequency Compensation/luise_fixpt/CORDIC/cordic_stage3/ZComponent/ZAccumulator2
// Hierarchy Level: 6
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module ZAccumulator2_block7
          (
           clk,
           reset,
           enb_1_2_0,
           op,
           x,
           y,
           Out1
          );


  input   clk;
  input   reset;
  input   enb_1_2_0;
  input   op;
  input   signed [15:0] x;  // sfix16_En13
  input   signed [15:0] y;  // sfix16_En13
  output  signed [15:0] Out1;  // sfix16_En13


  reg signed [15:0] delayMatch1_reg [0:5];  // sfix16 [6]
  wire signed [15:0] delayMatch1_reg_next [0:5];  // sfix16_En13 [6]
  wire signed [15:0] y_1;  // sfix16_En13
  wire signed [16:0] Sum2_sub_cast;  // sfix17_En13
  wire signed [16:0] Sum2_sub_cast_1;  // sfix17_En13
  wire signed [16:0] Sum2_sub_temp;  // sfix17_En13
  wire signed [15:0] Sum2_out1;  // sfix16_En13
  reg signed [15:0] delayMatch_reg [0:5];  // sfix16 [6]
  wire signed [15:0] delayMatch_reg_next [0:5];  // sfix16_En13 [6]
  wire signed [15:0] y_2;  // sfix16_En13
  wire signed [16:0] Sum1_add_cast;  // sfix17_En13
  wire signed [16:0] Sum1_add_cast_1;  // sfix17_En13
  wire signed [16:0] Sum1_add_temp;  // sfix17_En13
  wire signed [15:0] Sum1_out1;  // sfix16_En13
  wire signed [15:0] Switch_out1;  // sfix16_En13


  always @(posedge clk)
    begin : delayMatch1_process
      if (reset == 1'b1) begin
        delayMatch1_reg[0] <= 16'sb0000000000000000;
        delayMatch1_reg[1] <= 16'sb0000000000000000;
        delayMatch1_reg[2] <= 16'sb0000000000000000;
        delayMatch1_reg[3] <= 16'sb0000000000000000;
        delayMatch1_reg[4] <= 16'sb0000000000000000;
        delayMatch1_reg[5] <= 16'sb0000000000000000;
      end
      else if (enb_1_2_0) begin
        delayMatch1_reg[0] <= delayMatch1_reg_next[0];
        delayMatch1_reg[1] <= delayMatch1_reg_next[1];
        delayMatch1_reg[2] <= delayMatch1_reg_next[2];
        delayMatch1_reg[3] <= delayMatch1_reg_next[3];
        delayMatch1_reg[4] <= delayMatch1_reg_next[4];
        delayMatch1_reg[5] <= delayMatch1_reg_next[5];
      end
    end

  assign y_1 = delayMatch1_reg[5];
  assign delayMatch1_reg_next[0] = y;
  assign delayMatch1_reg_next[1] = delayMatch1_reg[0];
  assign delayMatch1_reg_next[2] = delayMatch1_reg[1];
  assign delayMatch1_reg_next[3] = delayMatch1_reg[2];
  assign delayMatch1_reg_next[4] = delayMatch1_reg[3];
  assign delayMatch1_reg_next[5] = delayMatch1_reg[4];



  assign Sum2_sub_cast = {x[15], x};
  assign Sum2_sub_cast_1 = {y_1[15], y_1};
  assign Sum2_sub_temp = Sum2_sub_cast - Sum2_sub_cast_1;
  assign Sum2_out1 = ((Sum2_sub_temp[16] == 1'b0) && (Sum2_sub_temp[15] != 1'b0) ? 16'sb0111111111111111 :
              ((Sum2_sub_temp[16] == 1'b1) && (Sum2_sub_temp[15] != 1'b1) ? 16'sb1000000000000000 :
              $signed(Sum2_sub_temp[15:0])));



  always @(posedge clk)
    begin : delayMatch_process
      if (reset == 1'b1) begin
        delayMatch_reg[0] <= 16'sb0000000000000000;
        delayMatch_reg[1] <= 16'sb0000000000000000;
        delayMatch_reg[2] <= 16'sb0000000000000000;
        delayMatch_reg[3] <= 16'sb0000000000000000;
        delayMatch_reg[4] <= 16'sb0000000000000000;
        delayMatch_reg[5] <= 16'sb0000000000000000;
      end
      else if (enb_1_2_0) begin
        delayMatch_reg[0] <= delayMatch_reg_next[0];
        delayMatch_reg[1] <= delayMatch_reg_next[1];
        delayMatch_reg[2] <= delayMatch_reg_next[2];
        delayMatch_reg[3] <= delayMatch_reg_next[3];
        delayMatch_reg[4] <= delayMatch_reg_next[4];
        delayMatch_reg[5] <= delayMatch_reg_next[5];
      end
    end

  assign y_2 = delayMatch_reg[5];
  assign delayMatch_reg_next[0] = y;
  assign delayMatch_reg_next[1] = delayMatch_reg[0];
  assign delayMatch_reg_next[2] = delayMatch_reg[1];
  assign delayMatch_reg_next[3] = delayMatch_reg[2];
  assign delayMatch_reg_next[4] = delayMatch_reg[3];
  assign delayMatch_reg_next[5] = delayMatch_reg[4];



  assign Sum1_add_cast = {x[15], x};
  assign Sum1_add_cast_1 = {y_2[15], y_2};
  assign Sum1_add_temp = Sum1_add_cast + Sum1_add_cast_1;
  assign Sum1_out1 = ((Sum1_add_temp[16] == 1'b0) && (Sum1_add_temp[15] != 1'b0) ? 16'sb0111111111111111 :
              ((Sum1_add_temp[16] == 1'b1) && (Sum1_add_temp[15] != 1'b1) ? 16'sb1000000000000000 :
              $signed(Sum1_add_temp[15:0])));



  assign Switch_out1 = (op == 1'b0 ? Sum2_out1 :
              Sum1_out1);



  assign Out1 = Switch_out1;

endmodule  // ZAccumulator2_block7

