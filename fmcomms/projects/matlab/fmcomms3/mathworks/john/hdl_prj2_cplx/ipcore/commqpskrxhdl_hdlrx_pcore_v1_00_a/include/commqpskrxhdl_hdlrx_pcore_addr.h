/*
 * File Name:         hdl_prj2_cplx\ipcore\commqpskrxhdl_hdlrx_pcore_v1_00_a\include\commqpskrxhdl_hdlrx_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-27 12:23:35
*/

#ifndef COMMQPSKRXHDL_HDLRX_PCORE_H_
#define COMMQPSKRXHDL_HDLRX_PCORE_H_

#define  IPCore_Reset_commqpskrxhdl_hdlrx_pcore     0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_commqpskrxhdl_hdlrx_pcore    0x4  //enabled (by default) when bit 0 is 0x1
#define  bit1_Data_commqpskrxhdl_hdlrx_pcore        0x100  //data register for port bit1
#define  bit2_Data_commqpskrxhdl_hdlrx_pcore        0x104  //data register for port bit2
#define  dValid_Data_commqpskrxhdl_hdlrx_pcore      0x108  //data register for port dValid
#define  dataIn_Re_Data_commqpskrxhdl_hdlrx_pcore   0x10C  //data register for port dataIn_Re
#define  dataIn_Im_Data_commqpskrxhdl_hdlrx_pcore   0x110  //data register for port dataIn_Im

#endif /* COMMQPSKRXHDL_HDLRX_PCORE_H_ */
