/* Include files */

#include <stddef.h>
#include "blas.h"
#include "sdrzQPSKRxFPGAFMC23_cgxe.h"
#include "m_TxLJ3WfyYC4Y5PZDNfGuUG.h"

/* Type Definitions */

/* Named Constants */
#define NumIterations                  (5.0)
#define ScaleOutput                    (true)

/* Variable Declarations */

/* Variable Definitions */
static const mxArray *eml_mx;
static const mxArray *b_eml_mx;
static const mxArray *c_eml_mx;
static const mxArray *d_eml_mx;
static const mxArray *e_eml_mx;
static const mxArray *f_eml_mx;
static emlrtMCInfo emlrtMCI = { 16, 13, "eml_warning",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_warning.m"
};

static emlrtMCInfo b_emlrtMCI = { 16, 5, "eml_warning",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\eml\\lib\\matlab\\eml\\eml_warning.m"
};

static emlrtMCInfo c_emlrtMCI = { 1, 1, "SystemCore",
  "C:\\Program Files\\MATLAB\\2014b\\toolbox\\shared\\system\\coder\\+matlab\\+system\\+coder\\SystemCore.p"
};

static emlrtMCInfo d_emlrtMCI = { -1, -1, "", "" };

/* Function Declarations */
static void mw__internal__call__autoinference(sfOd2wElE6un66xmZCZog7F
  infoCache_RestoreInfo_DispatcherInfo_Ports_data[2],
  sfOd2wElE6un66xmZCZog7F_size
  infoCache_RestoreInfo_DispatcherInfo_Ports_elems_sizes[2],
  sZVQz5WVraeIWEljxFvLe8 infoCache_RestoreInfo_DispatcherInfo_dWork_data[4],
  sZVQz5WVraeIWEljxFvLe8_size
  infoCache_RestoreInfo_DispatcherInfo_dWork_elems_sizes[4], char_T
  infoCache_RestoreInfo_DispatcherInfo_objTypeName[30], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize, char_T
  infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[22], real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias,
  slE07I4lDg6FknjQ3k8Q9CG infoCache_RestoreInfo_DispatcherInfo_mapsInfo_DW[3],
  real_T *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex, real_T
  infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[], int32_T
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[2], real_T
  infoCache_RestoreInfo_cgxeChksum[4], s7UBIGHSehQY1gCsIQWwr5C
  infoCache_VerificationInfo_checksums[4], real_T
  infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4], char_T
  infoCache_slVer[3]);
static void mw__internal__call__step(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance, cint16_T b_u0, boolean_T b_u1, int32_T *c_y0, boolean_T *c_y1);
static void cgxe_mdl_start(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance);
static void cgxe_mdl_initialize(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance);
static void cgxe_mdl_outputs(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance);
static void cgxe_mdl_update(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance);
static void cgxe_mdl_terminate(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance);
static const mxArray *mw__internal__name__resolution__fcn(void);
static void info_helper(const mxArray **info);
static const mxArray *emlrt_marshallOut(const char * u);
static const mxArray *b_emlrt_marshallOut(const uint32_T u);
static void b_info_helper(const mxArray **info);
static void c_info_helper(const mxArray **info);
static void d_info_helper(const mxArray **info);
static const mxArray *mw__internal__autoInference__fcn(void);
static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2]);
static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance);
static void emlrt_marshallIn(const mxArray *b_validPipeline, const char_T
  *identifier, boolean_T y[8]);
static void b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, boolean_T y[8]);
static void c_emlrt_marshallIn(const mxArray *b_xPipeline, const char_T
  *identifier, int32_T y[7]);
static void d_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7]);
static void e_emlrt_marshallIn(const mxArray *b_zPipeline, const char_T
  *identifier, int32_T y[7]);
static void f_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7]);
static void g_emlrt_marshallIn(const mxArray *b_sysobj, const char_T *identifier,
  dsp_HDLComplexToMagnitudeAngle *y);
static void h_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, dsp_HDLComplexToMagnitudeAngle *y);
static boolean_T i_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static void j_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7]);
static void k_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7]);
static void l_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[7]);
static void m_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[7]);
static int32_T n_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static int32_T o_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId);
static boolean_T p_emlrt_marshallIn(const mxArray *b_sysobj_not_empty, const
  char_T *identifier);
static void mw__internal__setSimState__fcn(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance, const mxArray *st);
static const mxArray *message(const mxArray *b, const mxArray *c, emlrtMCInfo
  *location);
static void error(const mxArray *b, emlrtMCInfo *location);
static const mxArray *fimath(char * b, char * c, char * d, char * e, char * f,
  char * g, char * h, char * i, char * j, char * k, char * l, real_T m, char * n,
  real_T o, char * p, real_T q, char * r, real_T s, char * t, real_T u, char * v,
  real_T w, char * y, real_T ab, char * bb, char * cb, char * db, real_T eb,
  char * fb, real_T gb, char * hb, real_T ib, char * jb, real_T kb, char * lb,
  real_T mb, char * nb, real_T ob, char * pb, real_T qb, char * rb, boolean_T sb,
  emlrtMCInfo *location);
static const mxArray *numerictype(char * b, real_T c, char * d, real_T e, char *
  f, real_T g, char * h, real_T i, char * j, real_T k, emlrtMCInfo *location);
static const mxArray *b_numerictype(char * b, boolean_T c, char * d, char * e,
  char * f, real_T g, char * h, real_T i, char * j, real_T k, char * l, real_T m,
  char * n, real_T o, emlrtMCInfo *location);
static void q_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, boolean_T ret[8]);
static void r_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7]);
static void s_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7]);
static boolean_T t_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static void u_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7]);
static void v_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7]);
static void w_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[7]);
static void x_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[7]);
static int32_T y_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId);
static int32_T ab_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId);

/* Function Definitions */
static void mw__internal__call__autoinference(sfOd2wElE6un66xmZCZog7F
  infoCache_RestoreInfo_DispatcherInfo_Ports_data[2],
  sfOd2wElE6un66xmZCZog7F_size
  infoCache_RestoreInfo_DispatcherInfo_Ports_elems_sizes[2],
  sZVQz5WVraeIWEljxFvLe8 infoCache_RestoreInfo_DispatcherInfo_dWork_data[4],
  sZVQz5WVraeIWEljxFvLe8_size
  infoCache_RestoreInfo_DispatcherInfo_dWork_elems_sizes[4], char_T
  infoCache_RestoreInfo_DispatcherInfo_objTypeName[30], real_T
  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize, char_T
  infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[22], real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope, real_T
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias,
  slE07I4lDg6FknjQ3k8Q9CG infoCache_RestoreInfo_DispatcherInfo_mapsInfo_DW[3],
  real_T *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex, real_T
  infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[], int32_T
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[2], real_T
  infoCache_RestoreInfo_cgxeChksum[4], s7UBIGHSehQY1gCsIQWwr5C
  infoCache_VerificationInfo_checksums[4], real_T
  infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4], char_T
  infoCache_slVer[3])
{
  sfOd2wElE6un66xmZCZog7F Ports_data[2];
  sfOd2wElE6un66xmZCZog7F_size Ports_elems_sizes[2];
  int32_T i0;
  static int8_T iv0[4] = { 1, 2, 1, 1 };

  sZVQz5WVraeIWEljxFvLe8_size dWork_elems_sizes[4];
  static char_T cv0[9] = { 'x', 'P', 'i', 'p', 'e', 'l', 'i', 'n', 'e' };

  sZVQz5WVraeIWEljxFvLe8 dWork_data[4];
  static int8_T iv1[4] = { 7, 2, 7, 1 };

  static char_T cv1[9] = { 'y', 'P', 'i', 'p', 'e', 'l', 'i', 'n', 'e' };

  static char_T cv2[9] = { 'z', 'P', 'i', 'p', 'e', 'l', 'i', 'n', 'e' };

  static char_T cv3[13] = { 'v', 'a', 'l', 'i', 'd', 'P', 'i', 'p', 'e', 'l',
    'i', 'n', 'e' };

  static int8_T iv2[4] = { 8, 2, 8, 1 };

  slE07I4lDg6FknjQ3k8Q9CG DW[3];
  slE07I4lDg6FknjQ3k8Q9CG t3_DW[3];
  sfOd2wElE6un66xmZCZog7F_size t2_Ports_elems_sizes[2];
  sfOd2wElE6un66xmZCZog7F t2_Ports_data[2];
  sZVQz5WVraeIWEljxFvLe8_size t2_dWork_elems_sizes[4];
  sZVQz5WVraeIWEljxFvLe8 t2_dWork_data[4];
  char_T t2_objTypeName[30];
  static char_T cv4[30] = { 'd', 's', 'p', '_', 'H', 'D', 'L', 'C', 'o', 'm',
    'p', 'l', 'e', 'x', 'T', 'o', 'M', 'a', 'g', 'n', 'i', 't', 'u', 'd', 'e',
    'A', 'n', 'g', 'l', 'e' };

  char_T t2_sysObjChksum[22];
  static char_T cv5[22] = { 'x', 'o', 'f', 'M', '2', '5', 'j', 'Q', 't', 'n',
    'L', '7', 't', 'L', 'g', 'h', 'D', 'r', 'O', '6', 'Z', 'G' };

  char_T t1_DispatcherInfo_objTypeName[30];
  char_T t1_DispatcherInfo_sysObjChksum[22];
  uint32_T t1_cgxeChksum[4];
  static uint32_T uv0[4] = { 3909935570U, 3517198310U, 466874085U, 219103150U };

  static uint32_T t7_chksum[4] = { 4070723412U, 3679677382U, 612573299U,
    945005729U };

  s7UBIGHSehQY1gCsIQWwr5C checksums[4];
  static int32_T t8_chksum[4] = { 411387096, 988166010, 1536253853, 1205939052 };

  static uint32_T t9_chksum[4] = { 3438071091U, 847298254U, 4007337887U,
    3413399679U };

  static uint32_T t10_chksum[4] = { 1029480361U, 2355502747U, 4015340215U,
    2392486915U };

  s7UBIGHSehQY1gCsIQWwr5C t0_checksums[4];
  uint32_T b_t10_chksum[4];
  static uint32_T t12_codeGenChksum[4] = { 907274058U, 3220786148U, 779473529U,
    2769831971U };

  static char_T cv6[3] = { '8', '.', '4' };

  Ports_data[0].dimModes = 0.0;
  Ports_elems_sizes[0].dims[0] = 1;
  Ports_elems_sizes[0].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    Ports_data[0].dims[i0] = (real_T)iv0[i0];
  }

  Ports_data[0].dType = 36.0;
  Ports_data[0].complexity = 0.0;
  Ports_data[0].outputBuiltInDTEqUsed = 0.0;
  Ports_data[1].dimModes = 0.0;
  Ports_elems_sizes[1].dims[0] = 1;
  Ports_elems_sizes[1].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    Ports_data[1].dims[i0] = (real_T)iv0[i0];
  }

  Ports_data[1].dType = 8.0;
  Ports_data[1].complexity = 0.0;
  Ports_data[1].outputBuiltInDTEqUsed = 0.0;
  dWork_elems_sizes[0].names[0] = 1;
  dWork_elems_sizes[0].names[1] = 9;
  for (i0 = 0; i0 < 9; i0++) {
    dWork_data[0].names[i0] = cv0[i0];
  }

  dWork_elems_sizes[0].dims[0] = 1;
  dWork_elems_sizes[0].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    dWork_data[0].dims[i0] = (real_T)iv1[i0];
  }

  dWork_data[0].dType = 37.0;
  dWork_data[0].complexity = 0.0;
  dWork_elems_sizes[1].names[0] = 1;
  dWork_elems_sizes[1].names[1] = 9;
  for (i0 = 0; i0 < 9; i0++) {
    dWork_data[1].names[i0] = cv1[i0];
  }

  dWork_elems_sizes[1].dims[0] = 1;
  dWork_elems_sizes[1].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    dWork_data[1].dims[i0] = (real_T)iv1[i0];
  }

  dWork_data[1].dType = 37.0;
  dWork_data[1].complexity = 0.0;
  dWork_elems_sizes[2].names[0] = 1;
  dWork_elems_sizes[2].names[1] = 9;
  for (i0 = 0; i0 < 9; i0++) {
    dWork_data[2].names[i0] = cv2[i0];
  }

  dWork_elems_sizes[2].dims[0] = 1;
  dWork_elems_sizes[2].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    dWork_data[2].dims[i0] = (real_T)iv1[i0];
  }

  dWork_data[2].dType = 36.0;
  dWork_data[2].complexity = 0.0;
  dWork_elems_sizes[3].names[0] = 1;
  dWork_elems_sizes[3].names[1] = 13;
  for (i0 = 0; i0 < 13; i0++) {
    dWork_data[3].names[i0] = cv3[i0];
  }

  dWork_elems_sizes[3].dims[0] = 1;
  dWork_elems_sizes[3].dims[1] = 4;
  for (i0 = 0; i0 < 4; i0++) {
    dWork_data[3].dims[i0] = (real_T)iv2[i0];
  }

  dWork_data[3].dType = 8.0;
  dWork_data[3].complexity = 0.0;
  DW[0].Index = 0.0;
  DW[0].DataType = 0.0;
  DW[0].IsSigned = 1.0;
  DW[0].MantBits = 17.0;
  DW[0].FixExp = -13.0;
  DW[0].Slope = 1.0;
  DW[0].Bias = 0.0;
  DW[1].Index = 1.0;
  DW[1].DataType = 0.0;
  DW[1].IsSigned = 1.0;
  DW[1].MantBits = 17.0;
  DW[1].FixExp = -13.0;
  DW[1].Slope = 1.0;
  DW[1].Bias = 0.0;
  DW[2].Index = 2.0;
  DW[2].DataType = 0.0;
  DW[2].IsSigned = 1.0;
  DW[2].MantBits = 19.0;
  DW[2].FixExp = -18.0;
  DW[2].Slope = 1.0;
  DW[2].Bias = 0.0;
  for (i0 = 0; i0 < 3; i0++) {
    t3_DW[i0] = DW[i0];
  }

  for (i0 = 0; i0 < 2; i0++) {
    t2_Ports_elems_sizes[i0] = Ports_elems_sizes[i0];
    t2_Ports_data[i0] = Ports_data[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t2_dWork_elems_sizes[i0] = dWork_elems_sizes[i0];
    t2_dWork_data[i0] = dWork_data[i0];
  }

  for (i0 = 0; i0 < 30; i0++) {
    t2_objTypeName[i0] = cv4[i0];
  }

  for (i0 = 0; i0 < 22; i0++) {
    t2_sysObjChksum[i0] = cv5[i0];
  }

  for (i0 = 0; i0 < 3; i0++) {
    DW[i0] = t3_DW[i0];
  }

  for (i0 = 0; i0 < 2; i0++) {
    Ports_elems_sizes[i0] = t2_Ports_elems_sizes[i0];
    Ports_data[i0] = t2_Ports_data[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    dWork_elems_sizes[i0] = t2_dWork_elems_sizes[i0];
    dWork_data[i0] = t2_dWork_data[i0];
  }

  for (i0 = 0; i0 < 30; i0++) {
    t1_DispatcherInfo_objTypeName[i0] = t2_objTypeName[i0];
  }

  for (i0 = 0; i0 < 22; i0++) {
    t1_DispatcherInfo_sysObjChksum[i0] = t2_sysObjChksum[i0];
  }

  for (i0 = 0; i0 < 3; i0++) {
    t3_DW[i0] = DW[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t1_cgxeChksum[i0] = uv0[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[0].chksum[i0] = (real_T)t7_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[1].chksum[i0] = (real_T)t8_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[2].chksum[i0] = (real_T)t9_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    checksums[3].chksum[i0] = (real_T)t10_chksum[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    t0_checksums[i0] = checksums[i0];
    b_t10_chksum[i0] = t12_codeGenChksum[i0];
  }

  for (i0 = 0; i0 < 2; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_Ports_elems_sizes[i0] =
      Ports_elems_sizes[i0];
    infoCache_RestoreInfo_DispatcherInfo_Ports_data[i0] = Ports_data[i0];
  }

  for (i0 = 0; i0 < 4; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_dWork_elems_sizes[i0] =
      dWork_elems_sizes[i0];
    infoCache_RestoreInfo_DispatcherInfo_dWork_data[i0] = dWork_data[i0];
  }

  for (i0 = 0; i0 < 30; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_objTypeName[i0] =
      t1_DispatcherInfo_objTypeName[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_objTypeSize = 128.0;
  for (i0 = 0; i0 < 22; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_sysObjChksum[i0] =
      t1_DispatcherInfo_sysObjChksum[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index = 0.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType = 0.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned = 1.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits = 19.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp = -18.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope = 1.0;
  *infoCache_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias = 0.0;
  for (i0 = 0; i0 < 3; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_mapsInfo_DW[i0] = t3_DW[i0];
  }

  *infoCache_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex = 3.0;
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[0] = 1;
  c_infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_si[1] = 2;
  for (i0 = 0; i0 < 2; i0++) {
    infoCache_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[i0] = 0.0;
  }

  for (i0 = 0; i0 < 4; i0++) {
    infoCache_RestoreInfo_cgxeChksum[i0] = (real_T)t1_cgxeChksum[i0];
    infoCache_VerificationInfo_checksums[i0] = t0_checksums[i0];
    infoCache_VerificationInfo_codeGenOnlyInfo_codeGenChksum[i0] = (real_T)
      b_t10_chksum[i0];
  }

  for (i0 = 0; i0 < 3; i0++) {
    infoCache_slVer[i0] = cv6[i0];
  }
}

static void mw__internal__call__step(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance, cint16_T b_u0, boolean_T b_u1, int32_T *c_y0, boolean_T *c_y1)
{
  boolean_T flag;
  dsp_HDLComplexToMagnitudeAngle *obj;
  const mxArray *y;
  static const int32_T iv3[2] = { 1, 45 };

  const mxArray *m0;
  char_T cv7[45];
  int32_T i1;
  static char_T cv8[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv4[2] = { 1, 4 };

  char_T cv9[4];
  static char_T cv10[4] = { 's', 't', 'e', 'p' };

  const mxArray *c_y;
  static const int32_T iv5[2] = { 1, 51 };

  char_T cv11[51];
  static char_T cv12[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  const mxArray *d_y;
  static const int32_T iv6[2] = { 1, 5 };

  char_T cv13[5];
  static char_T cv14[5] = { 's', 'e', 't', 'u', 'p' };

  boolean_T bv0[8];
  uint8_T b_obj[6];
  int32_T iv7[7];
  int32_T c;
  int32_T a0;
  int32_T ii;
  int32_T b_c;
  int32_T b_a0;
  int32_T c_c;
  int32_T c_a0;
  int32_T d_c;
  static uint16_T uv1[5] = { 38688U, 20442U, 10377U, 5208U, 2607U };

  int32_T d_a0;
  int32_T i2;
  int32_T e_a0;
  int32_T e_c;
  int32_T f_a0;
  int32_T f_c;
  int32_T g_a0;
  int32_T i3;
  uint8_T h_a0;
  int32_T g_c;
  int32_T h_c;
  int32_T i_c;
  if (!moduleInstance->sysobj_not_empty) {
    moduleInstance->sysobj.isInitialized = false;
    moduleInstance->sysobj.isReleased = false;
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m0 = emlrtCreateCharArray(2, iv3);
    for (i1 = 0; i1 < 45; i1++) {
      cv7[i1] = cv8[i1];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m0, cv7);
    emlrtAssign(&y, m0);
    b_y = NULL;
    m0 = emlrtCreateCharArray(2, iv4);
    for (i1 = 0; i1 < 4; i1++) {
      cv9[i1] = cv10[i1];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 4, m0, cv9);
    emlrtAssign(&b_y, m0);
    error(message(y, b_y, &c_emlrtMCI), &c_emlrtMCI);
  }

  if (!obj->isInitialized) {
    if (obj->isInitialized) {
      c_y = NULL;
      m0 = emlrtCreateCharArray(2, iv5);
      for (i1 = 0; i1 < 51; i1++) {
        cv11[i1] = cv12[i1];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m0, cv11);
      emlrtAssign(&c_y, m0);
      d_y = NULL;
      m0 = emlrtCreateCharArray(2, iv6);
      for (i1 = 0; i1 < 5; i1++) {
        cv13[i1] = cv14[i1];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m0, cv13);
      emlrtAssign(&d_y, m0);
      error(message(c_y, d_y, &c_emlrtMCI), &c_emlrtMCI);
    }

    obj->isInitialized = true;
    for (i1 = 0; i1 < 7; i1++) {
      obj->xPipeline[i1] = 0;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->yPipeline[i1] = 0;
    }

    obj->pXAbsolute = 0;
    obj->pYAbsolute = 0;
    for (i1 = 0; i1 < 7; i1++) {
      obj->zPipeline[i1] = 0;
    }

    obj->pQuadrantOut = 0;
    obj->pPipeout = 0;
    for (i1 = 0; i1 < 7; i1++) {
      obj->pXYReversed[i1] = 0;
    }

    for (i1 = 0; i1 < 8; i1++) {
      obj->validPipeline[i1] = false;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->pQuadrantIn[i1] = 0;
    }

    obj->TunablePropsChanged = false;
    for (i1 = 0; i1 < 7; i1++) {
      obj->xPipeline[i1] = 0;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->yPipeline[i1] = 0;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->zPipeline[i1] = 0;
    }

    for (i1 = 0; i1 < 8; i1++) {
      obj->validPipeline[i1] = false;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->pQuadrantIn[i1] = 0;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->pXYReversed[i1] = 0;
    }

    obj->pQuadrantOut = 0;
    obj->pPipeout = 0;
    obj->pXAbsolute = 0;
    obj->pYAbsolute = 0;
  }

  if (obj->TunablePropsChanged) {
    obj->TunablePropsChanged = false;
  }

  for (i1 = 0; i1 < 8; i1++) {
    bv0[i1] = obj->validPipeline[i1];
  }

  bv0[0] = b_u1;
  for (i1 = 0; i1 < 8; i1++) {
    obj->validPipeline[i1] = bv0[i1];
  }

  *c_y0 = obj->pQuadrantOut;
  *c_y1 = obj->validPipeline[7];
  for (i1 = 0; i1 < 6; i1++) {
    b_obj[i1] = obj->pQuadrantIn[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    obj->pQuadrantIn[1 + i1] = b_obj[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    b_obj[i1] = obj->pXYReversed[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    obj->pXYReversed[1 + i1] = b_obj[i1];
  }

  for (i1 = 0; i1 < 8; i1++) {
    bv0[i1] = obj->validPipeline[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    bv0[1 + i1] = obj->validPipeline[i1];
  }

  for (i1 = 0; i1 < 8; i1++) {
    obj->validPipeline[i1] = bv0[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    iv7[i1] = obj->xPipeline[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    iv7[1 + i1] = obj->xPipeline[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    obj->xPipeline[i1] = iv7[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    iv7[i1] = obj->yPipeline[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    iv7[1 + i1] = obj->yPipeline[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    obj->yPipeline[i1] = iv7[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    iv7[i1] = obj->zPipeline[i1];
  }

  for (i1 = 0; i1 < 6; i1++) {
    iv7[1 + i1] = obj->zPipeline[i1];
  }

  for (i1 = 0; i1 < 7; i1++) {
    obj->zPipeline[i1] = iv7[i1];
  }

  i1 = b_u0.re;
  if ((i1 & 131072) != 0) {
    obj->pXAbsolute = i1 | -131072;
  } else {
    obj->pXAbsolute = i1 & 131071;
  }

  c = obj->pXAbsolute;
  i1 = -c;
  if (c < 0) {
    if ((i1 & 131072) != 0) {
      obj->pXAbsolute = i1 | -131072;
    } else {
      obj->pXAbsolute = i1 & 131071;
    }
  } else {
    obj->pXAbsolute = c;
  }

  i1 = b_u0.im;
  if ((i1 & 131072) != 0) {
    obj->pYAbsolute = i1 | -131072;
  } else {
    obj->pYAbsolute = i1 & 131071;
  }

  c = obj->pYAbsolute;
  i1 = -c;
  if (c < 0) {
    if ((i1 & 131072) != 0) {
      obj->pYAbsolute = i1 | -131072;
    } else {
      obj->pYAbsolute = i1 & 131071;
    }
  } else {
    obj->pYAbsolute = c;
  }

  if (b_u0.re < 0 && b_u0.im < 0) {
    obj->pQuadrantIn[0] = 2;
  } else if (b_u0.re >= 0 && b_u0.im < 0) {
    obj->pQuadrantIn[0] = 1;
  } else if (b_u0.re < 0 && b_u0.im >= 0) {
    obj->pQuadrantIn[0] = 3;
  } else if (b_u0.re >= 0 && b_u0.im >= 0) {
    obj->pQuadrantIn[0] = 0;
  } else {
    obj->pQuadrantIn[0] = 0;
  }

  a0 = obj->pXAbsolute;
  c = obj->pYAbsolute;
  if (a0 > c) {
    obj->pXYReversed[0] = 0;
    for (i1 = 0; i1 < 7; i1++) {
      iv7[i1] = obj->xPipeline[i1];
    }

    i1 = obj->pXAbsolute;
    if ((i1 & 65536) != 0) {
      iv7[0] = i1 | -65536;
    } else {
      iv7[0] = i1 & 65535;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->xPipeline[i1] = iv7[i1];
    }

    for (i1 = 0; i1 < 7; i1++) {
      iv7[i1] = obj->yPipeline[i1];
    }

    i1 = obj->pYAbsolute;
    if ((i1 & 65536) != 0) {
      iv7[0] = i1 | -65536;
    } else {
      iv7[0] = i1 & 65535;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->yPipeline[i1] = iv7[i1];
    }
  } else {
    obj->pXYReversed[0] = 1;
    for (i1 = 0; i1 < 7; i1++) {
      iv7[i1] = obj->xPipeline[i1];
    }

    i1 = obj->pYAbsolute;
    if ((i1 & 65536) != 0) {
      iv7[0] = i1 | -65536;
    } else {
      iv7[0] = i1 & 65535;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->xPipeline[i1] = iv7[i1];
    }

    for (i1 = 0; i1 < 7; i1++) {
      iv7[i1] = obj->yPipeline[i1];
    }

    i1 = obj->pXAbsolute;
    if ((i1 & 65536) != 0) {
      iv7[0] = i1 | -65536;
    } else {
      iv7[0] = i1 & 65535;
    }

    for (i1 = 0; i1 < 7; i1++) {
      obj->yPipeline[i1] = iv7[i1];
    }
  }

  for (i1 = 0; i1 < 7; i1++) {
    iv7[i1] = obj->zPipeline[i1];
  }

  iv7[0] = 0;
  for (i1 = 0; i1 < 7; i1++) {
    obj->zPipeline[i1] = iv7[i1];
  }

  for (ii = 0; ii < 5; ii++) {
    c = obj->yPipeline[ii + 1];
    i1 = c >> (ii + 1);
    if ((i1 & 65536) != 0) {
      b_c = i1 | -65536;
    } else {
      b_c = i1 & 65535;
    }

    c = obj->xPipeline[ii + 1];
    i1 = c >> (ii + 1);
    if ((i1 & 65536) != 0) {
      c = i1 | -65536;
    } else {
      c = i1 & 65535;
    }

    a0 = obj->yPipeline[ii + 1];
    if (a0 < 0) {
      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->xPipeline[i1];
      }

      a0 = obj->xPipeline[ii + 1];
      if ((a0 & 131072) != 0) {
        b_a0 = a0 | -131072;
      } else {
        b_a0 = a0 & 131071;
      }

      if ((b_c & 131072) != 0) {
        c_c = b_c | -131072;
      } else {
        c_c = b_c & 131071;
      }

      i1 = b_a0 - c_c;
      if ((i1 & 131072) != 0) {
        i1 |= -131072;
      } else {
        i1 &= 131071;
      }

      if ((i1 & 65536) != 0) {
        iv7[ii + 1] = i1 | -65536;
      } else {
        iv7[ii + 1] = i1 & 65535;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->xPipeline[i1] = iv7[i1];
      }

      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->yPipeline[i1];
      }

      a0 = obj->yPipeline[ii + 1];
      if ((a0 & 131072) != 0) {
        c_a0 = a0 | -131072;
      } else {
        c_a0 = a0 & 131071;
      }

      if ((c & 131072) != 0) {
        d_c = c | -131072;
      } else {
        d_c = c & 131071;
      }

      i1 = c_a0 + d_c;
      if ((i1 & 131072) != 0) {
        i1 |= -131072;
      } else {
        i1 &= 131071;
      }

      if ((i1 & 65536) != 0) {
        iv7[ii + 1] = i1 | -65536;
      } else {
        iv7[ii + 1] = i1 & 65535;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->yPipeline[i1] = iv7[i1];
      }

      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->zPipeline[i1];
      }

      a0 = obj->zPipeline[ii + 1];
      i1 = uv1[ii];
      if ((a0 & 524288) != 0) {
        d_a0 = a0 | -524288;
      } else {
        d_a0 = a0 & 524287;
      }

      if ((i1 & 524288) != 0) {
        i2 = i1 | -524288;
      } else {
        i2 = i1;
      }

      i1 = d_a0 - i2;
      if ((i1 & 524288) != 0) {
        i1 |= -524288;
      } else {
        i1 &= 524287;
      }

      if ((i1 & 262144) != 0) {
        iv7[ii + 1] = i1 | -262144;
      } else {
        iv7[ii + 1] = i1 & 262143;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->zPipeline[i1] = iv7[i1];
      }
    } else {
      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->xPipeline[i1];
      }

      a0 = obj->xPipeline[ii + 1];
      if ((a0 & 131072) != 0) {
        e_a0 = a0 | -131072;
      } else {
        e_a0 = a0 & 131071;
      }

      if ((b_c & 131072) != 0) {
        e_c = b_c | -131072;
      } else {
        e_c = b_c & 131071;
      }

      i1 = e_a0 + e_c;
      if ((i1 & 131072) != 0) {
        i1 |= -131072;
      } else {
        i1 &= 131071;
      }

      if ((i1 & 65536) != 0) {
        iv7[ii + 1] = i1 | -65536;
      } else {
        iv7[ii + 1] = i1 & 65535;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->xPipeline[i1] = iv7[i1];
      }

      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->yPipeline[i1];
      }

      a0 = obj->yPipeline[ii + 1];
      if ((a0 & 131072) != 0) {
        f_a0 = a0 | -131072;
      } else {
        f_a0 = a0 & 131071;
      }

      if ((c & 131072) != 0) {
        f_c = c | -131072;
      } else {
        f_c = c & 131071;
      }

      i1 = f_a0 - f_c;
      if ((i1 & 131072) != 0) {
        i1 |= -131072;
      } else {
        i1 &= 131071;
      }

      if ((i1 & 65536) != 0) {
        iv7[ii + 1] = i1 | -65536;
      } else {
        iv7[ii + 1] = i1 & 65535;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->yPipeline[i1] = iv7[i1];
      }

      for (i1 = 0; i1 < 7; i1++) {
        iv7[i1] = obj->zPipeline[i1];
      }

      a0 = obj->zPipeline[ii + 1];
      i1 = uv1[ii];
      if ((a0 & 524288) != 0) {
        g_a0 = a0 | -524288;
      } else {
        g_a0 = a0 & 524287;
      }

      if ((i1 & 524288) != 0) {
        i3 = i1 | -524288;
      } else {
        i3 = i1;
      }

      i1 = g_a0 + i3;
      if ((i1 & 524288) != 0) {
        i1 |= -524288;
      } else {
        i1 &= 524287;
      }

      if ((i1 & 262144) != 0) {
        iv7[ii + 1] = i1 | -262144;
      } else {
        iv7[ii + 1] = i1 & 262143;
      }

      for (i1 = 0; i1 < 7; i1++) {
        obj->zPipeline[i1] = iv7[i1];
      }
    }
  }

  obj->pPipeout = obj->zPipeline[6];
  h_a0 = obj->pXYReversed[6];
  if (h_a0 == 1) {
    c = obj->pPipeout;
    if ((c & 524288) != 0) {
      g_c = c | -524288;
    } else {
      g_c = c & 524287;
    }

    i1 = 131072 - g_c;
    if ((i1 & 524288) != 0) {
      i1 |= -524288;
    } else {
      i1 &= 524287;
    }

    if ((i1 & 262144) != 0) {
      obj->pQuadrantOut = i1 | -262144;
    } else {
      obj->pQuadrantOut = i1 & 262143;
    }
  } else {
    obj->pQuadrantOut = obj->pPipeout;
  }

  h_a0 = obj->pQuadrantIn[6];
  if (h_a0 == 1) {
    c = obj->pQuadrantOut;
    i1 = -c;
    if ((i1 & 262144) != 0) {
      obj->pQuadrantOut = i1 | -262144;
    } else {
      obj->pQuadrantOut = i1 & 262143;
    }
  } else {
    h_a0 = obj->pQuadrantIn[6];
    if (h_a0 == 2) {
      c = obj->pQuadrantOut;
      if ((c & 524288) != 0) {
        h_c = c | -524288;
      } else {
        h_c = c & 524287;
      }

      i1 = -262144 + h_c;
      if ((i1 & 524288) != 0) {
        i1 |= -524288;
      } else {
        i1 &= 524287;
      }

      if ((i1 & 262144) != 0) {
        obj->pQuadrantOut = i1 | -262144;
      } else {
        obj->pQuadrantOut = i1 & 262143;
      }
    } else {
      h_a0 = obj->pQuadrantIn[6];
      if (h_a0 == 3) {
        c = obj->pQuadrantOut;
        if ((c & 524288) != 0) {
          i_c = c | -524288;
        } else {
          i_c = c & 524287;
        }

        i1 = 262143 - i_c;
        if ((i1 & 524288) != 0) {
          i1 |= -524288;
        } else {
          i1 &= 524287;
        }

        if ((i1 & 262144) != 0) {
          obj->pQuadrantOut = i1 | -262144;
        } else {
          obj->pQuadrantOut = i1 & 262143;
        }
      }
    }
  }

  if ((int32_T)obj->validPipeline[7] == 0) {
    for (i1 = 0; i1 < 7; i1++) {
      iv7[i1] = obj->zPipeline[i1];
    }

    iv7[6] = 0;
    for (i1 = 0; i1 < 7; i1++) {
      obj->zPipeline[i1] = iv7[i1];
    }

    obj->pQuadrantOut = 0;
  }
}

static void cgxe_mdl_start(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance)
{
  boolean_T flag;
  dsp_HDLComplexToMagnitudeAngle *obj;
  const mxArray *y;
  static const int32_T iv8[2] = { 1, 51 };

  const mxArray *m1;
  char_T cv15[51];
  int32_T i4;
  static char_T cv16[51] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'L', 'o', 'c', 'k', 'e', 'd', 'R', 'e', 'l', 'e',
    'a', 's', 'e', 'd', 'C', 'o', 'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv9[2] = { 1, 5 };

  char_T cv17[5];
  static char_T cv18[5] = { 's', 'e', 't', 'u', 'p' };

  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  boolean_T (*validPipeline)[8];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  emlrtAssignP(&f_eml_mx, numerictype("WordLength", 18.0, "FractionLength", 13.0,
    "BinaryPoint", 13.0, "Slope", 0.0001220703125, "FixedExponent", -13.0,
    &d_emlrtMCI));
  emlrtAssignP(&e_eml_mx, b_numerictype("SignednessBool", false, "Signedness",
    "Unsigned", "WordLength", 1.0, "FractionLength", 0.0, "BinaryPoint", 0.0,
    "Slope", 1.0, "FixedExponent", 0.0, &d_emlrtMCI));
  emlrtAssignP(&d_eml_mx, b_numerictype("SignednessBool", false, "Signedness",
    "Unsigned", "WordLength", 2.0, "FractionLength", 0.0, "BinaryPoint", 0.0,
    "Slope", 1.0, "FixedExponent", 0.0, &d_emlrtMCI));
  emlrtAssignP(&c_eml_mx, numerictype("WordLength", 19.0, "FractionLength", 18.0,
    "BinaryPoint", 18.0, "Slope", 3.814697265625E-6, "FixedExponent", -18.0,
    &d_emlrtMCI));
  emlrtAssignP(&b_eml_mx, numerictype("WordLength", 17.0, "FractionLength", 13.0,
    "BinaryPoint", 13.0, "Slope", 0.0001220703125, "FixedExponent", -13.0,
    &d_emlrtMCI));
  emlrtAssignP(&eml_mx, fimath("RoundMode", "nearest", "RoundingMethod",
    "Nearest", "OverflowMode", "saturate", "OverflowAction", "Saturate",
    "ProductMode", "FullPrecision", "ProductWordLength", 32.0,
    "MaxProductWordLength", 65535.0, "ProductFractionLength", 30.0,
    "ProductFixedExponent", -30.0, "ProductSlope", 9.3132257461547852E-10,
    "ProductSlopeAdjustmentFactor", 1.0, "ProductBias", 0.0, "SumMode",
    "FullPrecision", "SumWordLength", 32.0, "MaxSumWordLength", 65535.0,
    "SumFractionLength", 30.0, "SumFixedExponent", -30.0, "SumSlope",
    9.3132257461547852E-10, "SumSlopeAdjustmentFactor", 1.0, "SumBias", 0.0,
    "CastBeforeSum", true, &d_emlrtMCI));
  if (!moduleInstance->sysobj_not_empty) {
    moduleInstance->sysobj.isInitialized = false;
    moduleInstance->sysobj.isReleased = false;
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isInitialized) {
    y = NULL;
    m1 = emlrtCreateCharArray(2, iv8);
    for (i4 = 0; i4 < 51; i4++) {
      cv15[i4] = cv16[i4];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 51, m1, cv15);
    emlrtAssign(&y, m1);
    b_y = NULL;
    m1 = emlrtCreateCharArray(2, iv9);
    for (i4 = 0; i4 < 5; i4++) {
      cv17[i4] = cv18[i4];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m1, cv17);
    emlrtAssign(&b_y, m1);
    error(message(y, b_y, &c_emlrtMCI), &c_emlrtMCI);
  }

  obj->isInitialized = true;
  for (i4 = 0; i4 < 7; i4++) {
    obj->xPipeline[i4] = 0;
  }

  for (i4 = 0; i4 < 7; i4++) {
    obj->yPipeline[i4] = 0;
  }

  obj->pXAbsolute = 0;
  obj->pYAbsolute = 0;
  for (i4 = 0; i4 < 7; i4++) {
    obj->zPipeline[i4] = 0;
  }

  obj->pQuadrantOut = 0;
  obj->pPipeout = 0;
  for (i4 = 0; i4 < 7; i4++) {
    obj->pXYReversed[i4] = 0;
  }

  for (i4 = 0; i4 < 8; i4++) {
    obj->validPipeline[i4] = false;
  }

  for (i4 = 0; i4 < 7; i4++) {
    obj->pQuadrantIn[i4] = 0;
  }

  obj->TunablePropsChanged = false;
  for (i4 = 0; i4 < 7; i4++) {
    (*xPipeline)[i4] = moduleInstance->sysobj.xPipeline[i4];
  }

  for (i4 = 0; i4 < 7; i4++) {
    (*yPipeline)[i4] = moduleInstance->sysobj.yPipeline[i4];
  }

  for (i4 = 0; i4 < 7; i4++) {
    (*zPipeline)[i4] = moduleInstance->sysobj.zPipeline[i4];
  }

  for (i4 = 0; i4 < 8; i4++) {
    (*validPipeline)[i4] = moduleInstance->sysobj.validPipeline[i4];
  }
}

static void cgxe_mdl_initialize(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance)
{
  boolean_T flag;
  dsp_HDLComplexToMagnitudeAngle *obj;
  const mxArray *y;
  static const int32_T iv10[2] = { 1, 45 };

  const mxArray *m2;
  char_T cv19[45];
  int32_T i5;
  static char_T cv20[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv11[2] = { 1, 8 };

  char_T cv21[8];
  static char_T cv22[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  const mxArray *c_y;
  static const int32_T iv12[2] = { 1, 45 };

  const mxArray *d_y;
  static const int32_T iv13[2] = { 1, 5 };

  char_T cv23[5];
  static char_T cv24[5] = { 'r', 'e', 's', 'e', 't' };

  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  boolean_T (*validPipeline)[8];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  if (!moduleInstance->sysobj_not_empty) {
    moduleInstance->sysobj.isInitialized = false;
    moduleInstance->sysobj.isReleased = false;
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m2 = emlrtCreateCharArray(2, iv10);
    for (i5 = 0; i5 < 45; i5++) {
      cv19[i5] = cv20[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m2, cv19);
    emlrtAssign(&y, m2);
    b_y = NULL;
    m2 = emlrtCreateCharArray(2, iv11);
    for (i5 = 0; i5 < 8; i5++) {
      cv21[i5] = cv22[i5];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m2, cv21);
    emlrtAssign(&b_y, m2);
    error(message(y, b_y, &c_emlrtMCI), &c_emlrtMCI);
  }

  flag = obj->isInitialized;
  if (flag) {
    obj = &moduleInstance->sysobj;
    if (moduleInstance->sysobj.isReleased) {
      c_y = NULL;
      m2 = emlrtCreateCharArray(2, iv12);
      for (i5 = 0; i5 < 45; i5++) {
        cv19[i5] = cv20[i5];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m2, cv19);
      emlrtAssign(&c_y, m2);
      d_y = NULL;
      m2 = emlrtCreateCharArray(2, iv13);
      for (i5 = 0; i5 < 5; i5++) {
        cv23[i5] = cv24[i5];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 5, m2, cv23);
      emlrtAssign(&d_y, m2);
      error(message(c_y, d_y, &c_emlrtMCI), &c_emlrtMCI);
    }

    if (obj->isInitialized) {
      for (i5 = 0; i5 < 7; i5++) {
        obj->xPipeline[i5] = 0;
      }

      for (i5 = 0; i5 < 7; i5++) {
        obj->yPipeline[i5] = 0;
      }

      for (i5 = 0; i5 < 7; i5++) {
        obj->zPipeline[i5] = 0;
      }

      for (i5 = 0; i5 < 8; i5++) {
        obj->validPipeline[i5] = false;
      }

      for (i5 = 0; i5 < 7; i5++) {
        obj->pQuadrantIn[i5] = 0;
      }

      for (i5 = 0; i5 < 7; i5++) {
        obj->pXYReversed[i5] = 0;
      }

      obj->pQuadrantOut = 0;
      obj->pPipeout = 0;
      obj->pXAbsolute = 0;
      obj->pYAbsolute = 0;
    }
  }

  for (i5 = 0; i5 < 7; i5++) {
    (*xPipeline)[i5] = moduleInstance->sysobj.xPipeline[i5];
  }

  for (i5 = 0; i5 < 7; i5++) {
    (*yPipeline)[i5] = moduleInstance->sysobj.yPipeline[i5];
  }

  for (i5 = 0; i5 < 7; i5++) {
    (*zPipeline)[i5] = moduleInstance->sysobj.zPipeline[i5];
  }

  for (i5 = 0; i5 < 8; i5++) {
    (*validPipeline)[i5] = moduleInstance->sysobj.validPipeline[i5];
  }
}

static void cgxe_mdl_outputs(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance)
{
  int32_T i6;
  cint16_T *u0;
  boolean_T *u1;
  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  boolean_T (*validPipeline)[8];
  int32_T *b_y0;
  boolean_T *b_y1;
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  b_y1 = (boolean_T *)ssGetOutputPortSignal(moduleInstance->S, 1U);
  b_y0 = (int32_T *)ssGetOutputPortSignal(moduleInstance->S, 0U);
  u1 = (boolean_T *)ssGetInputPortSignal(moduleInstance->S, 1U);
  u0 = (cint16_T *)ssGetInputPortSignal(moduleInstance->S, 0U);
  for (i6 = 0; i6 < 7; i6++) {
    moduleInstance->sysobj.xPipeline[i6] = (*xPipeline)[i6];
    moduleInstance->sysobj.yPipeline[i6] = (*yPipeline)[i6];
    moduleInstance->sysobj.zPipeline[i6] = (*zPipeline)[i6];
  }

  for (i6 = 0; i6 < 8; i6++) {
    moduleInstance->sysobj.validPipeline[i6] = (*validPipeline)[i6];
  }

  mw__internal__call__step(moduleInstance, *u0, *u1, b_y0, b_y1);
  for (i6 = 0; i6 < 7; i6++) {
    (*xPipeline)[i6] = moduleInstance->sysobj.xPipeline[i6];
    (*yPipeline)[i6] = moduleInstance->sysobj.yPipeline[i6];
    (*zPipeline)[i6] = moduleInstance->sysobj.zPipeline[i6];
  }

  for (i6 = 0; i6 < 8; i6++) {
    (*validPipeline)[i6] = moduleInstance->sysobj.validPipeline[i6];
  }
}

static void cgxe_mdl_update(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance)
{
  int32_T i7;
  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  boolean_T (*validPipeline)[8];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  for (i7 = 0; i7 < 7; i7++) {
    (*xPipeline)[i7] = moduleInstance->sysobj.xPipeline[i7];
    (*yPipeline)[i7] = moduleInstance->sysobj.yPipeline[i7];
    (*zPipeline)[i7] = moduleInstance->sysobj.zPipeline[i7];
  }

  for (i7 = 0; i7 < 8; i7++) {
    (*validPipeline)[i7] = moduleInstance->sysobj.validPipeline[i7];
  }
}

static void cgxe_mdl_terminate(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance)
{
  boolean_T flag;
  dsp_HDLComplexToMagnitudeAngle *obj;
  const mxArray *y;
  static const int32_T iv14[2] = { 1, 45 };

  const mxArray *m3;
  char_T cv25[45];
  int32_T i8;
  static char_T cv26[45] = { 'M', 'A', 'T', 'L', 'A', 'B', ':', 's', 'y', 's',
    't', 'e', 'm', ':', 'm', 'e', 't', 'h', 'o', 'd', 'C', 'a', 'l', 'l', 'e',
    'd', 'W', 'h', 'e', 'n', 'R', 'e', 'l', 'e', 'a', 's', 'e', 'd', 'C', 'o',
    'd', 'e', 'g', 'e', 'n' };

  const mxArray *b_y;
  static const int32_T iv15[2] = { 1, 8 };

  char_T cv27[8];
  static char_T cv28[8] = { 'i', 's', 'L', 'o', 'c', 'k', 'e', 'd' };

  const mxArray *c_y;
  static const int32_T iv16[2] = { 1, 45 };

  const mxArray *d_y;
  static const int32_T iv17[2] = { 1, 7 };

  char_T cv29[7];
  static char_T cv30[7] = { 'r', 'e', 'l', 'e', 'a', 's', 'e' };

  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  boolean_T (*validPipeline)[8];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  emlrtDestroyArray(&f_eml_mx);
  emlrtDestroyArray(&e_eml_mx);
  emlrtDestroyArray(&d_eml_mx);
  emlrtDestroyArray(&c_eml_mx);
  emlrtDestroyArray(&b_eml_mx);
  emlrtDestroyArray(&eml_mx);
  if (!moduleInstance->sysobj_not_empty) {
    moduleInstance->sysobj.isInitialized = false;
    moduleInstance->sysobj.isReleased = false;
    moduleInstance->sysobj_not_empty = true;
    if (moduleInstance->sysobj.isInitialized &&
        !moduleInstance->sysobj.isReleased) {
      flag = true;
    } else {
      flag = false;
    }

    if (flag) {
      moduleInstance->sysobj.TunablePropsChanged = true;
    }
  }

  obj = &moduleInstance->sysobj;
  if (moduleInstance->sysobj.isReleased) {
    y = NULL;
    m3 = emlrtCreateCharArray(2, iv14);
    for (i8 = 0; i8 < 45; i8++) {
      cv25[i8] = cv26[i8];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m3, cv25);
    emlrtAssign(&y, m3);
    b_y = NULL;
    m3 = emlrtCreateCharArray(2, iv15);
    for (i8 = 0; i8 < 8; i8++) {
      cv27[i8] = cv28[i8];
    }

    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 8, m3, cv27);
    emlrtAssign(&b_y, m3);
    error(message(y, b_y, &c_emlrtMCI), &c_emlrtMCI);
  }

  flag = obj->isInitialized;
  if (flag) {
    obj = &moduleInstance->sysobj;
    if (moduleInstance->sysobj.isReleased) {
      c_y = NULL;
      m3 = emlrtCreateCharArray(2, iv16);
      for (i8 = 0; i8 < 45; i8++) {
        cv25[i8] = cv26[i8];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 45, m3, cv25);
      emlrtAssign(&c_y, m3);
      d_y = NULL;
      m3 = emlrtCreateCharArray(2, iv17);
      for (i8 = 0; i8 < 7; i8++) {
        cv29[i8] = cv30[i8];
      }

      emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 7, m3, cv29);
      emlrtAssign(&d_y, m3);
      error(message(c_y, d_y, &c_emlrtMCI), &c_emlrtMCI);
    }

    if (obj->isInitialized) {
      obj->isReleased = true;
    }
  }

  for (i8 = 0; i8 < 7; i8++) {
    (*xPipeline)[i8] = moduleInstance->sysobj.xPipeline[i8];
  }

  for (i8 = 0; i8 < 7; i8++) {
    (*yPipeline)[i8] = moduleInstance->sysobj.yPipeline[i8];
  }

  for (i8 = 0; i8 < 7; i8++) {
    (*zPipeline)[i8] = moduleInstance->sysobj.zPipeline[i8];
  }

  for (i8 = 0; i8 < 8; i8++) {
    (*validPipeline)[i8] = moduleInstance->sysobj.validPipeline[i8];
  }
}

static const mxArray *mw__internal__name__resolution__fcn(void)
{
  const mxArray *nameCaptureInfo;
  nameCaptureInfo = NULL;
  emlrtAssign(&nameCaptureInfo, emlrtCreateStructMatrix(243, 1, 0, NULL));
  info_helper(&nameCaptureInfo);
  b_info_helper(&nameCaptureInfo);
  c_info_helper(&nameCaptureInfo);
  d_info_helper(&nameCaptureInfo);
  emlrtNameCapturePostProcessR2013b(&nameCaptureInfo);
  return nameCaptureInfo;
}

static void info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 0);
  emlrtAddField(*info, emlrt_marshallOut("repmat"), "name", 0);
  emlrtAddField(*info, emlrt_marshallOut("struct"), "dominantType", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "resolved", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 0);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 0);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 1);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 1);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 1);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 1);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 2);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_valid_size_arg"), "name", 2);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "resolved", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(1368215430U), "fileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 2);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 2);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 3);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 3);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 3);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 3);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isintegral"),
                "context", 4);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 4);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 4);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 4);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context", 5);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 5);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 5);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 5);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 6);
  emlrtAddField(*info, emlrt_marshallOut("eml_is_integer_class"), "name", 6);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_is_integer_class.m"),
                "resolved", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851182U), "fileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 6);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 6);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 7);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 7);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 7);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 7);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "context", 8);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 8);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 8);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 8);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 9);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 9);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 9);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 9);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "context", 10);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 10);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 10);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 10);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m!isinbounds"),
                "context", 11);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexIntRelop"), "name",
                11);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m"),
                "resolved", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760722U), "fileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 11);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 11);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!apply_float_relop"),
                "context", 12);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 12);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 12);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 12);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!float_class_contains_indexIntClass"),
                "context", 13);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 13);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 13);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 13);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexIntRelop.m!is_signed_indexIntClass"),
                "context", 14);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 14);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 14);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 14);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 15);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 15);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 15);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 15);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_assert_valid_size_arg.m"),
                "context", 16);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 16);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 16);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 16);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 17);
  emlrtAddField(*info, emlrt_marshallOut("max"), "name", 17);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "resolved", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(1311287716U), "fileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 17);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 17);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/max.m"), "context", 18);
  emlrtAddField(*info, emlrt_marshallOut("eml_min_or_max"), "name", 18);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                "resolved", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 18);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 18);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 19);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 19);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                19);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 19);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 19);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                20);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 20);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 20);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 20);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 21);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_alloc"), "name", 21);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "resolved", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 21);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 21);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "context", 22);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalexpAlloc"), "name",
                22);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                "resolved", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 22);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 22);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 23);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 23);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 23);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 23);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 24);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 24);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                24);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 24);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 24);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 25);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 25);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 25);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "context", 26);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 26);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 26);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 26);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 27);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 27);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexInt"),
                "dominantType", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 27);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 27);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context", 28);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 28);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                28);
  emlrtAddField(*info, b_emlrt_marshallOut(1398907998U), "fileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 28);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 28);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 29);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 29);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 29);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 29);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m!eml_int_forloop_overflow_check_helper"),
                "context", 30);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 30);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 30);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 30);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 31);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 31);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 31);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 31);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 32);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 32);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 32);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 32);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 33);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 33);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 33);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 33);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "context", 34);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 34);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 34);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 34);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 35);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.System"), "name",
                35);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/System.p"),
                "resolved", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 35);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 35);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/CustomIcon.p"),
                "context", 36);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.matlabCodegenHandle"),
                "name", 36);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/matlabCodegenHandle.p"),
                "resolved", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 36);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 36);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 37);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.mixin.CustomIcon"),
                "name", 37);
  emlrtAddField(*info, emlrt_marshallOut("unknown"), "dominantType", 37);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/matlab/system/+matlab/+system/+mixin/CustomIcon.p"),
                "resolved", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(1410839906U), "fileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 37);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 37);
  emlrtAddField(*info, emlrt_marshallOut(""), "context", 38);
  emlrtAddField(*info, emlrt_marshallOut("dsp.HDLComplexToMagnitudeAngle"),
                "name", 38);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "resolved", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840832U), "fileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 38);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 38);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 39);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemProp"),
                "name", 39);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 39);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "resolved", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 39);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 39);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 40);
  emlrtAddField(*info, emlrt_marshallOut("matlab.system.coder.SystemCore"),
                "name", 40);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "resolved", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840022U), "fileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 40);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 40);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 41);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 41);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 41);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 41);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 42);
  emlrtAddField(*info, emlrt_marshallOut("repmat"), "name", 42);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/repmat.m"), "resolved", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 42);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 42);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 43);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.cell"), "name", 43);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/cell.p"),
                "resolved", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 43);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 43);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 44);
  emlrtAddField(*info, emlrt_marshallOut("validateattributes"), "name", 44);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.cell"), "dominantType",
                44);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "resolved", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750104U), "fileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 44);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 44);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 45);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 45);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 45);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 45);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 45);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 46);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 46);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 46);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 46);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 46);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!isintegral"),
                "context", 47);
  emlrtAddField(*info, emlrt_marshallOut("isfinite"), "name", 47);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 47);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "resolved",
                47);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 47);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 47);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context", 48);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 48);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 48);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 48);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 48);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context", 49);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 49);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 49);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 49);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 49);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isfinite.m"), "context", 50);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 50);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 50);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 50);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 50);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context", 51);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 51);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 51);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 51);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 51);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m!isintegral"),
                "context", 52);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 52);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 52);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 52);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 52);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 53);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 53);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 53);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 53);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 53);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "context", 54);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_floor"), "name", 54);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 54);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m"),
                "resolved", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851126U), "fileTimeLo", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 54);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 54);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "context", 55);
  emlrtAddField(*info, emlrt_marshallOut("eml_warning"), "name", 55);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 55);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_warning.m"), "resolved",
                55);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851202U), "fileTimeLo", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 55);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 55);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 56);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 56);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 56);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 56);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 56);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context", 57);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 57);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 57);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 57);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 57);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 58);
  emlrtAddField(*info, emlrt_marshallOut("isinf"), "name", 58);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 58);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "resolved", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742656U), "fileTimeLo", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 58);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 58);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isinf.m"), "context", 59);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 59);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 59);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 59);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 59);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 60);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.cell"), "name", 60);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 60);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXC]$matlabroot$/toolbox/coder/coder/+coder/+internal/cell.p"),
                "resolved", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 60);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 60);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemCore.p"),
                "context", 61);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 61);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 61);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 61);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 61);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 62);
  emlrtAddField(*info, emlrt_marshallOut("validateattributes"), "name", 62);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 62);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/lang/validateattributes.m"),
                "resolved", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750104U), "fileTimeLo", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 62);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 62);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 63);
  emlrtAddField(*info, emlrt_marshallOut("isfixed"), "name", 63);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 63);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfixed.m"),
                "resolved", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542778U), "fileTimeLo", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 63);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 63);
}

static const mxArray *emlrt_marshallOut(const char * u)
{
  const mxArray *y;
  const mxArray *m4;
  y = NULL;
  m4 = emlrtCreateString(u);
  emlrtAssign(&y, m4);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const uint32_T u)
{
  const mxArray *y;
  const mxArray *m5;
  y = NULL;
  m5 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
  *(uint32_T *)mxGetData(m5) = u;
  emlrtAssign(&y, m5);
  return y;
}

static void b_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfixed.m"),
                "context", 64);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 64);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                64);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 64);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 64);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 65);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 65);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 65);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/get.m"),
                "resolved", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(1386456354U), "fileTimeLo", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 65);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 65);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/get.m"),
                "context", 66);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 66);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 66);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_scalar_eg.m"),
                "resolved", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542772U), "fileTimeLo", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 66);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 66);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_scalar_eg.m"),
                "context", 67);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 67);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 67);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 67);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 67);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_scalar_eg.m"),
                "context", 68);
  emlrtAddField(*info, emlrt_marshallOut("fimath"), "name", 68);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 68);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/fimath.m"),
                "resolved", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 68);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 68);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/get.m"),
                "context", 69);
  emlrtAddField(*info, emlrt_marshallOut("eml_getfiprop_helper"), "name", 69);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 69);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXE]$matlabroot$/toolbox/fixedpoint/fixedpoint/eml_getfiprop_helper.m"),
                "resolved", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(1386456356U), "fileTimeLo", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 69);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 69);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 70);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 70);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 70);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                70);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 70);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 70);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "context", 71);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_supported_string"), "name",
                71);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 71);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                "resolved", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 71);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 71);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m!inrange"),
                "context", 72);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 72);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 72);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 72);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 72);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"), "context",
                73);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 73);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 73);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 73);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 73);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "context", 74);
  emlrtAddField(*info, emlrt_marshallOut("min"), "name", 74);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 74);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "resolved", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(1311287718U), "fileTimeLo", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 74);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 74);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datafun/min.m"), "context", 75);
  emlrtAddField(*info, emlrt_marshallOut("eml_min_or_max"), "name", 75);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 75);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m"),
                "resolved", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 75);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 75);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 76);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 76);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 76);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                76);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 76);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 76);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "context",
                77);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalarEg"), "name", 77);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 77);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalarEg.p"),
                "resolved", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 77);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 77);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_bin_extremum"),
                "context", 78);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_alloc"), "name", 78);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 78);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "resolved", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 78);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 78);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m"),
                "context", 79);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.scalexpAlloc"), "name",
                79);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 79);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/scalexpAlloc.p"),
                "resolved", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 79);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 79);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 80);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalar_eg"), "name", 80);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 80);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m"), "resolved",
                80);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 80);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 80);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_min_or_max.m!eml_scalar_bin_extremum"),
                "context", 81);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 81);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 81);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 81);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 81);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "context", 82);
  emlrtAddField(*info, emlrt_marshallOut("lower"), "name", 82);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 82);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/lower.m"), "resolved", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 82);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 82);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/lower.m"), "context", 83);
  emlrtAddField(*info, emlrt_marshallOut("eml_string_transform"), "name", 83);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 83);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_string_transform.m"),
                "resolved", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 83);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 83);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_string_transform.m"),
                "context", 84);
  emlrtAddField(*info, emlrt_marshallOut("eml_assert_supported_string"), "name",
                84);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 84);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                "resolved", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 84);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 84);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_assert_supported_string.m"),
                "context", 85);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 85);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 85);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 85);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 85);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_string_transform.m"),
                "context", 86);
  emlrtAddField(*info, emlrt_marshallOut("eml_charmax"), "name", 86);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 86);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_charmax.m"),
                "resolved", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 86);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 86);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_string_transform.m"),
                "context", 87);
  emlrtAddField(*info, emlrt_marshallOut("colon"), "name", 87);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 87);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328388U), "fileTimeLo", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 87);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 87);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 88);
  emlrtAddField(*info, emlrt_marshallOut("colon"), "name", 88);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 88);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "resolved", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328388U), "fileTimeLo", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 88);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 88);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 89);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 89);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 89);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 89);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 89);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 90);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 90);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 90);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 90);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 90);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m"), "context", 91);
  emlrtAddField(*info, emlrt_marshallOut("floor"), "name", 91);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 91);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/floor.m"), "resolved", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742654U), "fileTimeLo", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 91);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 91);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                "context", 92);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 92);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 92);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 92);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 92);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!checkrange"),
                "context", 93);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 93);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 93);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 93);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 93);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 94);
  emlrtAddField(*info, emlrt_marshallOut("intmin"), "name", 94);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 94);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmin.m"), "resolved", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 94);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 94);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 95);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 95);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 95);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 95);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 95);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_integer_colon_dispatcher"),
                "context", 96);
  emlrtAddField(*info, emlrt_marshallOut("eml_isa_uint"), "name", 96);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 96);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                96);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 96);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 96);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "context",
                97);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isaUint"), "name", 97);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 97);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/isaUint.p"),
                "resolved", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 97);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 97);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 98);
  emlrtAddField(*info, emlrt_marshallOut("eml_unsigned_class"), "name", 98);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 98);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                "resolved", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 98);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 98);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_unsigned_class.m"),
                "context", 99);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.unsignedClass"), "name",
                99);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 99);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                "resolved", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840170U), "fileTimeLo", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 99);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 99);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/unsignedClass.p"),
                "context", 100);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 100);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 100);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 100);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 100);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 101);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_class"), "name", 101);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 101);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_class.m"),
                "resolved", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(1323202978U), "fileTimeLo", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 101);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 101);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 102);
  emlrtAddField(*info, emlrt_marshallOut("intmax"), "name", 102);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 102);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/intmax.m"), "resolved", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(1362294282U), "fileTimeLo", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 102);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 102);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 103);
  emlrtAddField(*info, emlrt_marshallOut("eml_isa_uint"), "name", 103);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 103);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isa_uint.m"), "resolved",
                103);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013088U), "fileTimeLo", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 103);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 103);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!integer_colon_length_nonnegd"),
                "context", 104);
  emlrtAddField(*info, emlrt_marshallOut("eml_index_plus"), "name", 104);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 104);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"),
                "resolved", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614816U), "fileTimeLo", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 104);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 104);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_index_plus.m"), "context",
                105);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.indexPlus"), "name",
                105);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 105);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/indexPlus.m"),
                "resolved", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 105);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 105);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/colon.m!eml_signed_integer_colon"),
                "context", 106);
  emlrtAddField(*info, emlrt_marshallOut("eml_int_forloop_overflow_check"),
                "name", 106);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 106);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"),
                "resolved", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 106);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 106);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/eml_string_transform.m"),
                "context", 107);
  emlrtAddField(*info, emlrt_marshallOut("char"), "name", 107);
  emlrtAddField(*info, emlrt_marshallOut("int8"), "dominantType", 107);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/char.m"), "resolved", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(1319762368U), "fileTimeLo", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 107);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 107);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 108);
  emlrtAddField(*info, emlrt_marshallOut("fi_impl"), "name", 108);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 108);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m"), "resolved", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(1386456352U), "fileTimeLo", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 108);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 108);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m"), "context", 109);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 109);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 109);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                109);
  emlrtAddField(*info, b_emlrt_marshallOut(1398907998U), "fileTimeLo", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 109);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 109);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m!fi_helper"),
                "context", 110);
  emlrtAddField(*info, emlrt_marshallOut("length"), "name", 110);
  emlrtAddField(*info, emlrt_marshallOut("cell"), "dominantType", 110);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(1303178606U), "fileTimeLo", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 110);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 110);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m!fi_helper"),
                "context", 111);
  emlrtAddField(*info, emlrt_marshallOut("eml_fi_checkforconst"), "name", 111);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 111);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/eml_fi_checkforconst.m"),
                "resolved", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542752U), "fileTimeLo", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 111);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 111);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m!fi_helper"),
                "context", 112);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 112);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 112);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 112);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 112);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m!fi_helper"),
                "context", 113);
  emlrtAddField(*info, emlrt_marshallOut("eml_fi_constructor_helper"), "name",
                113);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 113);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXE]$matlabroot$/toolbox/fixedpoint/fixedpoint/eml_fi_constructor_helper.m"),
                "resolved", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013096U), "fileTimeLo", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 113);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 113);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fi_impl.m!fi_helper"),
                "context", 114);
  emlrtAddField(*info, emlrt_marshallOut("eml_fi_checkforerror"), "name", 114);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                114);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/eml_fi_checkforerror.m"),
                "resolved", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(1360314746U), "fileTimeLo", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 114);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 114);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 115);
  emlrtAddField(*info, emlrt_marshallOut("deal"), "name", 115);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 115);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/datatypes/deal.m"), "resolved",
                115);
  emlrtAddField(*info, b_emlrt_marshallOut(1299109166U), "fileTimeLo", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 115);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 115);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 116);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 116);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 116);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 116);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 116);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 117);
  emlrtAddField(*info, emlrt_marshallOut("issparse"), "name", 117);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 117);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/sparfun/issparse.m"), "resolved",
                117);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851230U), "fileTimeLo", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 117);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 117);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 118);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 118);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 118);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 118);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 118);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 119);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 119);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                119);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 119);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 119);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 120);
  emlrtAddField(*info, emlrt_marshallOut("eps"), "name", 120);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 120);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/eps.m"), "resolved", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 120);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 120);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/eps.m"), "context", 121);
  emlrtAddField(*info, emlrt_marshallOut("eml_eps"), "name", 121);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 121);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_eps.m"), "resolved", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 121);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 121);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_eps.m"), "context", 122);
  emlrtAddField(*info, emlrt_marshallOut("eml_float_model"), "name", 122);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 122);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_float_model.m"),
                "resolved", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(1326760396U), "fileTimeLo", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 122);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 122);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 123);
  emlrtAddField(*info, emlrt_marshallOut("dsp.HDLComplexToMagnitudeAngle"),
                "name", 123);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 123);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "resolved", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(1410840832U), "fileTimeLo", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 123);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 123);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/shared/system/coder/+matlab/+system/+coder/SystemProp.p"),
                "context", 124);
  emlrtAddField(*info, emlrt_marshallOut("issparse"), "name", 124);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 124);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/sparfun/issparse.m"), "resolved",
                124);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851230U), "fileTimeLo", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 124);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 124);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 125);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 125);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 125);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 125);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 125);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 126);
  emlrtAddField(*info, emlrt_marshallOut("abs"), "name", 126);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 126);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "resolved", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542752U), "fileTimeLo", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 126);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 126);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 127);
  emlrtAddField(*info, emlrt_marshallOut("isfloat"), "name", 127);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 127);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfloat.m"),
                "resolved", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542778U), "fileTimeLo", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 127);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 127);
}

static void c_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfloat.m"),
                "context", 128);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 128);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                128);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 128);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 128);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 129);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 129);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 129);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 129);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 129);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "context", 130);
  emlrtAddField(*info, emlrt_marshallOut("isfixed"), "name", 130);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 130);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfixed.m"),
                "resolved", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542778U), "fileTimeLo", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 130);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 130);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 131);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 131);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 131);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/get.m"),
                "resolved", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(1386456354U), "fileTimeLo", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 131);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 131);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 132);
  emlrtAddField(*info, emlrt_marshallOut("isslopebiasscaled"), "name", 132);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                132);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isslopebiasscaled.m"),
                "resolved", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542760U), "fileTimeLo", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 132);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 132);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isslopebiasscaled.m"),
                "context", 133);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 133);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                133);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                133);
  emlrtAddField(*info, b_emlrt_marshallOut(1398907998U), "fileTimeLo", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 133);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 133);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isslopebiasscaled.m"),
                "context", 134);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 134);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                134);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 134);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 134);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isslopebiasscaled.m"),
                "context", 135);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 135);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 135);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                135);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 135);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 135);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "context", 136);
  emlrtAddField(*info, emlrt_marshallOut("eml_isequal_core"), "name", 136);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 136);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m"),
                "resolved", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851186U), "fileTimeLo", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 136);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 136);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m!isequal_scalar"),
                "context", 137);
  emlrtAddField(*info, emlrt_marshallOut("isnan"), "name", 137);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 137);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742658U), "fileTimeLo", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 137);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 137);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 138);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 138);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 138);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 138);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 138);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 139);
  emlrtAddField(*info, emlrt_marshallOut("fimath"), "name", 139);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 139);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/fimath.m"),
                "resolved", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 139);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 139);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/@embedded/@fi/abs.m!localAbsIsDouble"),
                "context", 140);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 140);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                140);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 140);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 140);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/@embedded/@fi/abs.m!localAbsIsSingle"),
                "context", 141);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 141);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                141);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 141);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 141);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 142);
  emlrtAddField(*info, emlrt_marshallOut("isscaleddouble"), "name", 142);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                142);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/isscaleddouble.m"),
                "resolved", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542740U), "fileTimeLo", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 142);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 142);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/isscaleddouble.m"),
                "context", 143);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 143);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                143);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 143);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 143);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/abs.m"),
                "context", 144);
  emlrtAddField(*info, emlrt_marshallOut("lt"), "name", 144);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 144);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "resolved", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289824U), "fileTimeLo", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 144);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 144);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 145);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 145);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 145);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 145);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 145);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 146);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 146);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 146);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 146);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 146);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "context", 147);
  emlrtAddField(*info, emlrt_marshallOut("isnumerictype"), "name", 147);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                147);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isnumerictype.m"), "resolved",
                147);
  emlrtAddField(*info, b_emlrt_marshallOut(1398907998U), "fileTimeLo", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 147);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 147);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 148);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 148);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 148);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 148);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 148);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 149);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 149);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 149);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                149);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 149);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 149);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 150);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 150);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 150);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 150);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 150);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 151);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 151);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 151);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 151);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 151);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 152);
  emlrtAddField(*info, emlrt_marshallOut("eml_type_relop_const"), "name", 152);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 152);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_type_relop_const.m"),
                "resolved", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542774U), "fileTimeLo", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 152);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 152);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_type_relop_const.m"),
                "context", 153);
  emlrtAddField(*info, emlrt_marshallOut("all"), "name", 153);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 153);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "resolved", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(1372614814U), "fileTimeLo", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 153);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 153);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 154);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 154);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 154);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 154);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 154);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 155);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.isBuiltInNumeric"),
                "name", 155);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 155);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                "resolved", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(1395960656U), "fileTimeLo", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 155);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 155);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/all.m"), "context", 156);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.allOrAny"), "name", 156);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 156);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "resolved", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615558U), "fileTimeLo", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 156);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 156);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 157);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.assert"), "name", 157);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 157);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/assert.m"),
                "resolved", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(1389750174U), "fileTimeLo", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 157);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 157);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 158);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 158);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 158);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                158);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 158);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 158);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/allOrAny.m"),
                "context", 159);
  emlrtAddField(*info, emlrt_marshallOut("coder.internal.constNonSingletonDim"),
                "name", 159);
  emlrtAddField(*info, emlrt_marshallOut("logical"), "dominantType", 159);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/constNonSingletonDim.m"),
                "resolved", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(1372615560U), "fileTimeLo", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 159);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 159);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "context", 160);
  emlrtAddField(*info, emlrt_marshallOut("eml_make_same_complexity"), "name",
                160);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 160);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_make_same_complexity.m"),
                "resolved", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 160);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 160);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 161);
  emlrtAddField(*info, emlrt_marshallOut("lt"), "name", 161);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 161);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/lt.m"),
                "resolved", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289824U), "fileTimeLo", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 161);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 161);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 162);
  emlrtAddField(*info, emlrt_marshallOut("ge"), "name", 162);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 162);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "resolved", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 162);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 162);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 163);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 163);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 163);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 163);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 163);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 164);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 164);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 164);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 164);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 164);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 165);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 165);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 165);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 165);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 165);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 166);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 166);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 166);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                166);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 166);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 166);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 167);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 167);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 167);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 167);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 167);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 168);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 168);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 168);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 168);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 168);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 169);
  emlrtAddField(*info, emlrt_marshallOut("eml_type_relop_const"), "name", 169);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 169);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_type_relop_const.m"),
                "resolved", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542774U), "fileTimeLo", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 169);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 169);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/ge.m"),
                "context", 170);
  emlrtAddField(*info, emlrt_marshallOut("eml_make_same_complexity"), "name",
                170);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 170);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_make_same_complexity.m"),
                "resolved", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 170);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 170);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 171);
  emlrtAddField(*info, emlrt_marshallOut("gt"), "name", 171);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 171);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "resolved", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 171);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 171);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 172);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 172);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 172);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 172);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 172);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 173);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 173);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 173);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 173);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 173);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 174);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 174);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 174);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 174);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 174);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 175);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 175);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 175);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                175);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 175);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 175);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 176);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 176);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 176);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 176);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 176);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/gt.m"),
                "context", 177);
  emlrtAddField(*info, emlrt_marshallOut("eml_make_same_complexity"), "name",
                177);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 177);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_make_same_complexity.m"),
                "resolved", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 177);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 177);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 178);
  emlrtAddField(*info, emlrt_marshallOut("bitsra"), "name", 178);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 178);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/bitsra.m"),
                "resolved", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542766U), "fileTimeLo", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 178);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 178);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/bitsra.m"),
                "context", 179);
  emlrtAddField(*info, emlrt_marshallOut("eml_fi_bitshift"), "name", 179);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 179);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "resolved", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742660U), "fileTimeLo", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 179);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 179);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 180);
  emlrtAddField(*info, emlrt_marshallOut("eml_shift_checks"), "name", 180);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 180);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_shift_checks.m"),
                "resolved", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742660U), "fileTimeLo", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 180);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 180);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_shift_checks.m"),
                "context", 181);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 181);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 181);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 181);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 181);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_shift_checks.m"),
                "context", 182);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 182);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 182);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 182);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 182);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_shift_checks.m"),
                "context", 183);
  emlrtAddField(*info, emlrt_marshallOut("eml_isslopebiasscaled"), "name", 183);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 183);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_isslopebiasscaled.m"),
                "resolved", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 183);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 183);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_isslopebiasscaled.m"),
                "context", 184);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 184);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                184);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 184);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 184);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 185);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 185);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 185);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 185);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 185);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 186);
  emlrtAddField(*info, emlrt_marshallOut("isfixed"), "name", 186);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 186);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isfixed.m"),
                "resolved", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542778U), "fileTimeLo", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 186);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 186);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 187);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 187);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 187);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/numerictype.m"),
                "resolved", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328386U), "fileTimeLo", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 187);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 187);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 188);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 188);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 188);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 188);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 188);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 189);
  emlrtAddField(*info, emlrt_marshallOut("fimath"), "name", 189);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 189);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fimath.m"), "resolved", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(1381882698U), "fileTimeLo", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 189);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 189);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fimath.m"), "context", 190);
  emlrtAddField(*info, emlrt_marshallOut("eml_fimath_constructor_helper"),
                "name", 190);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 190);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXE]$matlabroot$/toolbox/fixedpoint/fixedpoint/eml_fimath_constructor_helper.m"),
                "resolved", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(1376013096U), "fileTimeLo", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 190);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 190);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 191);
  emlrtAddField(*info, emlrt_marshallOut("eml_get_int_shift_or_bit_index"),
                "name", 191);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 191);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/eml_get_int_shift_or_bit_index.m"),
                "resolved", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(1363742632U), "fileTimeLo", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 191);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 191);
}

static void d_info_helper(const mxArray **info)
{
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/eml_get_int_shift_or_bit_index.m"),
                "context", 192);
  emlrtAddField(*info, emlrt_marshallOut("fimath"), "name", 192);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 192);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/fimath.m"), "resolved", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(1381882698U), "fileTimeLo", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 192);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 192);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/eml_get_int_shift_or_bit_index.m"),
                "context", 193);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 193);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 193);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/numerictype.m"), "resolved",
                193);
  emlrtAddField(*info, b_emlrt_marshallOut(1348224318U), "fileTimeLo", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 193);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 193);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/numerictype.m"), "context",
                194);
  emlrtAddField(*info, emlrt_marshallOut("length"), "name", 194);
  emlrtAddField(*info, emlrt_marshallOut("cell"), "dominantType", 194);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(1303178606U), "fileTimeLo", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 194);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 194);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/@embedded/@fi/eml_fi_bitshift.m!localFixptBitshift"),
                "context", 195);
  emlrtAddField(*info, emlrt_marshallOut("eml_feval"), "name", 195);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 195);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_feval.m"),
                "resolved", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542770U), "fileTimeLo", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 195);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 195);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_feval.m"),
                "context", 196);
  emlrtAddField(*info, emlrt_marshallOut("eml_switch_helper"), "name", 196);
  emlrtAddField(*info, emlrt_marshallOut(""), "dominantType", 196);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_switch_helper.m"),
                "resolved", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(1393363258U), "fileTimeLo", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 196);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 196);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fi_bitshift.m"),
                "context", 197);
  emlrtAddField(*info, emlrt_marshallOut("fimath"), "name", 197);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 197);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/fimath.m"),
                "resolved", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328384U), "fileTimeLo", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 197);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 197);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 198);
  emlrtAddField(*info, emlrt_marshallOut("minus"), "name", 198);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 198);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "resolved", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542784U), "fileTimeLo", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 198);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 198);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 199);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 199);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 199);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 199);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 199);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 200);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 200);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 200);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 200);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 200);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 201);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 201);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 201);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 201);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 201);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 202);
  emlrtAddField(*info, emlrt_marshallOut("eml_checkfimathforbinaryops"), "name",
                202);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 202);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_checkfimathforbinaryops.m"),
                "resolved", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542768U), "fileTimeLo", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 202);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 202);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_checkfimathforbinaryops.m"),
                "context", 203);
  emlrtAddField(*info, emlrt_marshallOut("isFimathEqualForCodeGen"), "name", 203);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 203);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isFimathEqualForCodeGen.m"),
                "resolved", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(1374205472U), "fileTimeLo", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 203);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 203);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 204);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 204);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                204);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 204);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 204);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 205);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 205);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 205);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 205);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 205);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/minus.m"),
                "context", 206);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 206);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 206);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                206);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 206);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 206);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 207);
  emlrtAddField(*info, emlrt_marshallOut("plus"), "name", 207);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 207);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "resolved", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542784U), "fileTimeLo", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 207);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 207);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 208);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 208);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 208);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 208);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 208);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 209);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 209);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 209);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 209);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 209);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 210);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 210);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 210);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 210);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 210);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 211);
  emlrtAddField(*info, emlrt_marshallOut("eml_checkfimathforbinaryops"), "name",
                211);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 211);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_checkfimathforbinaryops.m"),
                "resolved", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542768U), "fileTimeLo", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 211);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 211);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 212);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 212);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                212);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 212);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 212);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 213);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 213);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 213);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 213);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 213);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/plus.m"),
                "context", 214);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 214);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 214);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                214);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 214);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 214);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 215);
  emlrtAddField(*info, emlrt_marshallOut("eq"), "name", 215);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 215);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "resolved", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(1397289822U), "fileTimeLo", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 215);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 215);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 216);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 216);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 216);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 216);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 216);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 216);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 216);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 216);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 217);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 217);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 217);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 217);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 217);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 217);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 217);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 217);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 218);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 218);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 218);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 218);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 218);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 218);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 218);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 218);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 219);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 219);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 219);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                219);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 219);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 219);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 219);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 219);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 220);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 220);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 220);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 220);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 220);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 220);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 220);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 220);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 221);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 221);
  emlrtAddField(*info, emlrt_marshallOut("double"), "dominantType", 221);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 221);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 221);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 221);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 221);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 221);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 222);
  emlrtAddField(*info, emlrt_marshallOut("eml_type_relop_const"), "name", 222);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 222);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_type_relop_const.m"),
                "resolved", 222);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542774U), "fileTimeLo", 222);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 222);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 222);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 222);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eq.m"),
                "context", 223);
  emlrtAddField(*info, emlrt_marshallOut("eml_make_same_complexity"), "name",
                223);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 223);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_make_same_complexity.m"),
                "resolved", 223);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 223);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 223);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 223);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 223);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 224);
  emlrtAddField(*info, emlrt_marshallOut("uminus"), "name", 224);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 224);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "resolved", 224);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542788U), "fileTimeLo", 224);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 224);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 224);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 224);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "context", 225);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 225);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 225);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 225);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 225);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 225);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 225);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 225);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "context", 226);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 226);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                226);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 226);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 226);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 226);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 226);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 226);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "context", 227);
  emlrtAddField(*info, emlrt_marshallOut("numerictype"), "name", 227);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                227);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/numerictype.m"), "resolved",
                227);
  emlrtAddField(*info, b_emlrt_marshallOut(1348224318U), "fileTimeLo", 227);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 227);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 227);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 227);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "context", 228);
  emlrtAddField(*info, emlrt_marshallOut("isequal"), "name", 228);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 228);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "resolved",
                228);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851158U), "fileTimeLo", 228);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 228);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 228);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 228);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isequal.m"), "context", 229);
  emlrtAddField(*info, emlrt_marshallOut("eml_isequal_core"), "name", 229);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 229);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_isequal_core.m"),
                "resolved", 229);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851186U), "fileTimeLo", 229);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 229);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 229);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 229);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/uminus.m"),
                "context", 230);
  emlrtAddField(*info, emlrt_marshallOut("isslopebiasscaled"), "name", 230);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                230);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isslopebiasscaled.m"),
                "resolved", 230);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542760U), "fileTimeLo", 230);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 230);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 230);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 230);
  emlrtAddField(*info, emlrt_marshallOut(
    "[IXC]$matlabroot$/toolbox/dsp/dsp/+dsp/HDLComplexToMagnitudeAngle.p"),
                "context", 231);
  emlrtAddField(*info, emlrt_marshallOut("mtimes"), "name", 231);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 231);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/mtimes.m"),
                "resolved", 231);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542784U), "fileTimeLo", 231);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 231);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 231);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 231);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/mtimes.m"),
                "context", 232);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 232);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 232);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 232);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 232);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 232);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 232);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 232);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/mtimes.m"),
                "context", 233);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 233);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                233);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 233);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 233);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 233);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 233);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 233);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/mtimes.m"),
                "context", 234);
  emlrtAddField(*info, emlrt_marshallOut("times"), "name", 234);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 234);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "resolved", 234);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542786U), "fileTimeLo", 234);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 234);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 234);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 234);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 235);
  emlrtAddField(*info, emlrt_marshallOut("eml_scalexp_compatible"), "name", 235);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 235);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                "resolved", 235);
  emlrtAddField(*info, b_emlrt_marshallOut(1286851196U), "fileTimeLo", 235);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 235);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 235);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 235);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 236);
  emlrtAddField(*info, emlrt_marshallOut("isfi"), "name", 236);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 236);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/isfi.m"), "resolved", 236);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542758U), "fileTimeLo", 236);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 236);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 236);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 236);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 237);
  emlrtAddField(*info, emlrt_marshallOut("isscaledtype"), "name", 237);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 237);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/isscaledtype.m"),
                "resolved", 237);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542780U), "fileTimeLo", 237);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 237);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 237);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 237);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 238);
  emlrtAddField(*info, emlrt_marshallOut("eml_checkfimathforbinaryops"), "name",
                238);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 238);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_checkfimathforbinaryops.m"),
                "resolved", 238);
  emlrtAddField(*info, b_emlrt_marshallOut(1346542768U), "fileTimeLo", 238);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 238);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 238);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 238);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 239);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 239);
  emlrtAddField(*info, emlrt_marshallOut("embedded.numerictype"), "dominantType",
                239);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@numerictype/get.m"),
                "resolved", 239);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 239);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 239);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 239);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 239);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 240);
  emlrtAddField(*info, emlrt_marshallOut("get"), "name", 240);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fimath"), "dominantType", 240);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fimath/get.m"),
                "resolved", 240);
  emlrtAddField(*info, b_emlrt_marshallOut(1378328382U), "fileTimeLo", 240);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 240);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 240);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 240);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 241);
  emlrtAddField(*info, emlrt_marshallOut("strcmpi"), "name", 241);
  emlrtAddField(*info, emlrt_marshallOut("char"), "dominantType", 241);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/strfun/strcmpi.m"), "resolved",
                241);
  emlrtAddField(*info, b_emlrt_marshallOut(1327451510U), "fileTimeLo", 241);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 241);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 241);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 241);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/times.m"),
                "context", 242);
  emlrtAddField(*info, emlrt_marshallOut("eml_fixpt_times"), "name", 242);
  emlrtAddField(*info, emlrt_marshallOut("embedded.fi"), "dominantType", 242);
  emlrtAddField(*info, emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/fixedpoint/@embedded/@fi/eml_fixpt_times.m"),
                "resolved", 242);
  emlrtAddField(*info, b_emlrt_marshallOut(1289552046U), "fileTimeLo", 242);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "fileTimeHi", 242);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeLo", 242);
  emlrtAddField(*info, b_emlrt_marshallOut(0U), "mFileTimeHi", 242);
}

static const mxArray *mw__internal__autoInference__fcn(void)
{
  const mxArray *infoCache;
  char_T info_slVer[3];
  real_T info_VerificationInfo_codeGenOnlyInfo_codeGenChksum[4];
  s7UBIGHSehQY1gCsIQWwr5C info_VerificationInfo_checksums[4];
  real_T info_RestoreInfo_cgxeChksum[4];
  int32_T info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[2];
  real_T info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[2];
  real_T info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex;
  slE07I4lDg6FknjQ3k8Q9CG info_RestoreInfo_DispatcherInfo_mapsInfo_DW[3];
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType;
  real_T info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index;
  char_T info_RestoreInfo_DispatcherInfo_sysObjChksum[22];
  real_T info_RestoreInfo_DispatcherInfo_objTypeSize;
  char_T info_RestoreInfo_DispatcherInfo_objTypeName[30];
  sZVQz5WVraeIWEljxFvLe8_size info_RestoreInfo_DispatcherInfo_dWork_elems_sizes
    [4];
  sZVQz5WVraeIWEljxFvLe8 info_RestoreInfo_DispatcherInfo_dWork_data[4];
  sfOd2wElE6un66xmZCZog7F_size
    info_RestoreInfo_DispatcherInfo_Ports_elems_sizes[2];
  sfOd2wElE6un66xmZCZog7F info_RestoreInfo_DispatcherInfo_Ports_data[2];
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *c_y;
  int32_T i9;
  sfOd2wElE6un66xmZCZog7F_size u_elems_sizes[2];
  sfOd2wElE6un66xmZCZog7F u_data[2];
  const mxArray *d_y;
  const sfOd2wElE6un66xmZCZog7F_size *tmp_elems_sizes;
  const sfOd2wElE6un66xmZCZog7F *tmp_data;
  real_T u;
  const mxArray *e_y;
  const mxArray *m6;
  const mxArray *f_y;
  const mxArray *g_y;
  const mxArray *h_y;
  sZVQz5WVraeIWEljxFvLe8_size b_u_elems_sizes[4];
  sZVQz5WVraeIWEljxFvLe8 b_u_data[4];
  const mxArray *i_y;
  const sZVQz5WVraeIWEljxFvLe8_size *b_tmp_elems_sizes;
  const sZVQz5WVraeIWEljxFvLe8 *b_tmp_data;
  int32_T u_sizes[2];
  int32_T i10;
  int32_T i;
  char_T c_tmp_data[13];
  int32_T tmp_sizes;
  char_T c_u_data[13];
  const mxArray *j_y;
  const mxArray *k_y;
  const mxArray *l_y;
  const mxArray *m_y;
  static const int32_T iv18[2] = { 1, 30 };

  const mxArray *n_y;
  const mxArray *o_y;
  const mxArray *p_y;
  static const int32_T iv19[2] = { 1, 22 };

  const mxArray *q_y;
  const mxArray *r_y;
  const mxArray *s_y;
  const mxArray *t_y;
  const mxArray *u_y;
  const mxArray *v_y;
  const mxArray *w_y;
  const mxArray *x_y;
  const mxArray *y_y;
  slE07I4lDg6FknjQ3k8Q9CG b_u[3];
  const mxArray *ab_y;
  const slE07I4lDg6FknjQ3k8Q9CG *r0;
  const mxArray *bb_y;
  const mxArray *cb_y;
  const mxArray *db_y;
  const mxArray *eb_y;
  const mxArray *fb_y;
  const mxArray *gb_y;
  const mxArray *hb_y;
  const mxArray *ib_y;
  const mxArray *jb_y;
  int32_T b_u_sizes[2];
  real_T d_u_data[2];
  const mxArray *kb_y;
  real_T *pData;
  const mxArray *lb_y;
  static const int32_T iv20[2] = { 1, 4 };

  const mxArray *mb_y;
  s7UBIGHSehQY1gCsIQWwr5C c_u[4];
  const mxArray *nb_y;
  const s7UBIGHSehQY1gCsIQWwr5C *r1;
  const mxArray *ob_y;
  static const int32_T iv21[2] = { 1, 4 };

  const mxArray *pb_y;
  const mxArray *qb_y;
  static const int32_T iv22[2] = { 1, 4 };

  const mxArray *rb_y;
  static const int32_T iv23[2] = { 1, 3 };

  infoCache = NULL;
  mw__internal__call__autoinference(info_RestoreInfo_DispatcherInfo_Ports_data,
    info_RestoreInfo_DispatcherInfo_Ports_elems_sizes,
    info_RestoreInfo_DispatcherInfo_dWork_data,
    info_RestoreInfo_DispatcherInfo_dWork_elems_sizes,
    info_RestoreInfo_DispatcherInfo_objTypeName,
    &info_RestoreInfo_DispatcherInfo_objTypeSize,
    info_RestoreInfo_DispatcherInfo_sysObjChksum,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope,
    &info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias,
    info_RestoreInfo_DispatcherInfo_mapsInfo_DW,
    &info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes,
    info_RestoreInfo_cgxeChksum, info_VerificationInfo_checksums,
    info_VerificationInfo_codeGenOnlyInfo_codeGenChksum, info_slVer);
  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  b_y = NULL;
  emlrtAssign(&b_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  c_y = NULL;
  emlrtAssign(&c_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  for (i9 = 0; i9 < 2; i9++) {
    u_elems_sizes[i9] = info_RestoreInfo_DispatcherInfo_Ports_elems_sizes[i9];
    u_data[i9] = info_RestoreInfo_DispatcherInfo_Ports_data[i9];
  }

  d_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 + i9;
  }

  emlrtAssign(&d_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  for (i9 = 0; i9 < 2; i9++) {
    tmp_elems_sizes = &u_elems_sizes[i9];
    tmp_data = &u_data[i9];
    u = tmp_data->dimModes;
    e_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&e_y, m6);
    emlrtAddField(d_y, e_y, "dimModes", i9);
    emlrtAddField(d_y, c_emlrt_marshallOut(tmp_data->dims, tmp_elems_sizes->dims),
                  "dims", i9);
    u = tmp_data->dType;
    f_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&f_y, m6);
    emlrtAddField(d_y, f_y, "dType", i9);
    u = tmp_data->complexity;
    g_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&g_y, m6);
    emlrtAddField(d_y, g_y, "complexity", i9);
    u = tmp_data->outputBuiltInDTEqUsed;
    h_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&h_y, m6);
    emlrtAddField(d_y, h_y, "outputBuiltInDTEqUsed", i9);
  }

  emlrtAddField(c_y, d_y, "Ports", 0);
  for (i9 = 0; i9 < 4; i9++) {
    b_u_elems_sizes[i9] = info_RestoreInfo_DispatcherInfo_dWork_elems_sizes[i9];
    b_u_data[i9] = info_RestoreInfo_DispatcherInfo_dWork_data[i9];
  }

  i_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 + 3 *
      i9;
  }

  emlrtAssign(&i_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  for (i9 = 0; i9 < 4; i9++) {
    b_tmp_elems_sizes = &b_u_elems_sizes[i9];
    b_tmp_data = &b_u_data[i9];
    u_sizes[0] = 1;
    u_sizes[1] = b_tmp_elems_sizes->names[1];
    i10 = b_tmp_elems_sizes->names[0];
    i = b_tmp_elems_sizes->names[1];
    tmp_sizes = i10 * i;
    i *= i10;
    for (i10 = 0; i10 < i; i10++) {
      c_tmp_data[i10] = b_tmp_data->names[i10];
    }

    for (i10 = 0; i10 < tmp_sizes; i10++) {
      c_u_data[i10] = c_tmp_data[i10];
    }

    j_y = NULL;
    m6 = emlrtCreateCharArray(2, u_sizes);
    emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, u_sizes[1], m6, (char_T *)
      &c_u_data);
    emlrtAssign(&j_y, m6);
    emlrtAddField(i_y, j_y, "names", i9);
    emlrtAddField(i_y, c_emlrt_marshallOut(b_tmp_data->dims,
      b_tmp_elems_sizes->dims), "dims", i9);
    u = b_tmp_data->dType;
    k_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&k_y, m6);
    emlrtAddField(i_y, k_y, "dType", i9);
    u = b_tmp_data->complexity;
    l_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&l_y, m6);
    emlrtAddField(i_y, l_y, "complexity", i9);
  }

  emlrtAddField(c_y, i_y, "dWork", 0);
  m_y = NULL;
  m6 = emlrtCreateCharArray(2, iv18);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 30, m6,
    info_RestoreInfo_DispatcherInfo_objTypeName);
  emlrtAssign(&m_y, m6);
  emlrtAddField(c_y, m_y, "objTypeName", 0);
  n_y = NULL;
  m6 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_objTypeSize);
  emlrtAssign(&n_y, m6);
  emlrtAddField(c_y, n_y, "objTypeSize", 0);
  o_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 - i9;
  }

  emlrtAssign(&o_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  emlrtAddField(o_y, NULL, "names", 0);
  emlrtAddField(o_y, NULL, "dims", 0);
  emlrtAddField(o_y, NULL, "dType", 0);
  emlrtAddField(o_y, NULL, "dTypeSize", 0);
  emlrtAddField(o_y, NULL, "dTypeName", 0);
  emlrtAddField(o_y, NULL, "dTypeIndex", 0);
  emlrtAddField(o_y, NULL, "dTypeChksum", 0);
  emlrtAddField(o_y, NULL, "complexity", 0);
  emlrtAddField(c_y, o_y, "persisVarDWork", 0);
  p_y = NULL;
  m6 = emlrtCreateCharArray(2, iv19);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 22, m6,
    info_RestoreInfo_DispatcherInfo_sysObjChksum);
  emlrtAssign(&p_y, m6);
  emlrtAddField(c_y, p_y, "sysObjChksum", 0);
  q_y = NULL;
  emlrtAssign(&q_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  r_y = NULL;
  emlrtAssign(&r_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  s_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Index);
  emlrtAssign(&s_y, m6);
  emlrtAddField(r_y, s_y, "Index", 0);
  t_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_DataType);
  emlrtAssign(&t_y, m6);
  emlrtAddField(r_y, t_y, "DataType", 0);
  u_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_IsSigned);
  emlrtAssign(&u_y, m6);
  emlrtAddField(r_y, u_y, "IsSigned", 0);
  v_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_MantBits);
  emlrtAssign(&v_y, m6);
  emlrtAddField(r_y, v_y, "MantBits", 0);
  w_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_FixExp);
  emlrtAssign(&w_y, m6);
  emlrtAddField(r_y, w_y, "FixExp", 0);
  x_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Slope);
  emlrtAssign(&x_y, m6);
  emlrtAddField(r_y, x_y, "Slope", 0);
  y_y = NULL;
  m6 = emlrtCreateDoubleScalar(info_RestoreInfo_DispatcherInfo_mapsInfo_Out_Bias);
  emlrtAssign(&y_y, m6);
  emlrtAddField(r_y, y_y, "Bias", 0);
  emlrtAddField(q_y, r_y, "Out", 0);
  for (i9 = 0; i9 < 3; i9++) {
    b_u[i9] = info_RestoreInfo_DispatcherInfo_mapsInfo_DW[i9];
  }

  ab_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 + (i9 <<
      1);
  }

  emlrtAssign(&ab_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  for (i9 = 0; i9 < 3; i9++) {
    r0 = &b_u[i9];
    u = r0->Index;
    bb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&bb_y, m6);
    emlrtAddField(ab_y, bb_y, "Index", i9);
    u = r0->DataType;
    cb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&cb_y, m6);
    emlrtAddField(ab_y, cb_y, "DataType", i9);
    u = r0->IsSigned;
    db_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&db_y, m6);
    emlrtAddField(ab_y, db_y, "IsSigned", i9);
    u = r0->MantBits;
    eb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&eb_y, m6);
    emlrtAddField(ab_y, eb_y, "MantBits", i9);
    u = r0->FixExp;
    fb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&fb_y, m6);
    emlrtAddField(ab_y, fb_y, "FixExp", i9);
    u = r0->Slope;
    gb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&gb_y, m6);
    emlrtAddField(ab_y, gb_y, "Slope", i9);
    u = r0->Bias;
    hb_y = NULL;
    m6 = emlrtCreateDoubleScalar(u);
    emlrtAssign(&hb_y, m6);
    emlrtAddField(ab_y, hb_y, "Bias", i9);
  }

  emlrtAddField(q_y, ab_y, "DW", 0);
  ib_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 - i9;
  }

  emlrtAssign(&ib_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  emlrtAddField(ib_y, NULL, "Index", 0);
  emlrtAddField(ib_y, NULL, "DataType", 0);
  emlrtAddField(ib_y, NULL, "IsSigned", 0);
  emlrtAddField(ib_y, NULL, "MantBits", 0);
  emlrtAddField(ib_y, NULL, "FixExp", 0);
  emlrtAddField(ib_y, NULL, "Slope", 0);
  emlrtAddField(ib_y, NULL, "Bias", 0);
  emlrtAddField(q_y, ib_y, "PersisDW", 0);
  emlrtAddField(c_y, q_y, "mapsInfo", 0);
  jb_y = NULL;
  m6 = emlrtCreateDoubleScalar
    (info_RestoreInfo_DispatcherInfo_objDWorkTypeNameIndex);
  emlrtAssign(&jb_y, m6);
  emlrtAddField(c_y, jb_y, "objDWorkTypeNameIndex", 0);
  b_u_sizes[0] = 1;
  b_u_sizes[1] = 2;
  for (i9 = 0; i9 < 2; i9++) {
    d_u_data[i9] =
      info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_data[i9];
  }

  kb_y = NULL;
  m6 = emlrtCreateNumericArray(2, b_u_sizes, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  i9 = 0;
  for (i = 0; i < 2; i++) {
    pData[i9] = d_u_data[b_u_sizes[0] * i];
    i9++;
  }

  emlrtAssign(&kb_y, m6);
  emlrtAddField(c_y, kb_y, "inputDFFlagsIndexField", 0);
  emlrtAddField(b_y, c_y, "DispatcherInfo", 0);
  lb_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv20, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = info_RestoreInfo_cgxeChksum[i];
  }

  emlrtAssign(&lb_y, m6);
  emlrtAddField(b_y, lb_y, "cgxeChksum", 0);
  emlrtAddField(y, b_y, "RestoreInfo", 0);
  mb_y = NULL;
  emlrtAssign(&mb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  for (i9 = 0; i9 < 4; i9++) {
    c_u[i9] = info_VerificationInfo_checksums[i9];
  }

  nb_y = NULL;
  for (i9 = 0; i9 < 2; i9++) {
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes[i9] = 1 + 3 *
      i9;
  }

  emlrtAssign(&nb_y, emlrtCreateStructArray(2,
    info_RestoreInfo_DispatcherInfo_inputDFFlagsIndexField_sizes, 0, NULL));
  for (i9 = 0; i9 < 4; i9++) {
    r1 = &c_u[i9];
    for (i10 = 0; i10 < 4; i10++) {
      info_RestoreInfo_cgxeChksum[i10] = r1->chksum[i10];
    }

    ob_y = NULL;
    m6 = emlrtCreateNumericArray(2, iv21, mxDOUBLE_CLASS, mxREAL);
    pData = (real_T *)mxGetPr(m6);
    for (i = 0; i < 4; i++) {
      pData[i] = info_RestoreInfo_cgxeChksum[i];
    }

    emlrtAssign(&ob_y, m6);
    emlrtAddField(nb_y, ob_y, "chksum", i9);
  }

  emlrtAddField(mb_y, nb_y, "checksums", 0);
  pb_y = NULL;
  emlrtAssign(&pb_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  qb_y = NULL;
  m6 = emlrtCreateNumericArray(2, iv22, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m6);
  for (i = 0; i < 4; i++) {
    pData[i] = info_VerificationInfo_codeGenOnlyInfo_codeGenChksum[i];
  }

  emlrtAssign(&qb_y, m6);
  emlrtAddField(pb_y, qb_y, "codeGenChksum", 0);
  emlrtAddField(mb_y, pb_y, "codeGenOnlyInfo", 0);
  emlrtAddField(y, mb_y, "VerificationInfo", 0);
  rb_y = NULL;
  m6 = emlrtCreateCharArray(2, iv23);
  emlrtInitCharArrayR2013a(emlrtRootTLSGlobal, 3, m6, info_slVer);
  emlrtAssign(&rb_y, m6);
  emlrtAddField(y, rb_y, "slVer", 0);
  emlrtAssign(&infoCache, y);
  return infoCache;
}

static const mxArray *c_emlrt_marshallOut(const real_T u_data[], const int32_T
  u_sizes[2])
{
  const mxArray *y;
  const mxArray *m7;
  real_T *pData;
  int32_T i11;
  int32_T i;
  y = NULL;
  m7 = emlrtCreateNumericArray(2, u_sizes, mxDOUBLE_CLASS, mxREAL);
  pData = (real_T *)mxGetPr(m7);
  i11 = 0;
  for (i = 0; i < u_sizes[1]; i++) {
    pData[i11] = u_data[u_sizes[0] * i];
    i11++;
  }

  emlrtAssign(&y, m7);
  return y;
}

static const mxArray *mw__internal__getSimState__fcn
  (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance)
{
  const mxArray *st;
  const mxArray *y;
  const mxArray *b_y;
  static const int32_T iv24[1] = { 8 };

  const mxArray *m8;
  const mxArray *c_y;
  void * hoistedGlobal;
  int32_T u[7];
  int32_T i;
  const mxArray *d_y;
  static const int32_T iv25[1] = { 7 };

  int32_T *pData;
  const mxArray *e_y;
  const mxArray *f_y;
  static const int32_T iv26[1] = { 7 };

  const mxArray *g_y;
  const mxArray *h_y;
  static const int32_T iv27[1] = { 7 };

  const mxArray *i_y;
  const mxArray *j_y;
  const mxArray *k_y;
  const mxArray *l_y;
  const mxArray *m_y;
  const mxArray *n_y;
  static const int32_T iv28[1] = { 7 };

  const mxArray *o_y;
  const mxArray *p_y;
  static const int32_T iv29[1] = { 7 };

  const mxArray *q_y;
  const mxArray *r_y;
  static const int32_T iv30[1] = { 7 };

  const mxArray *s_y;
  static const int32_T iv31[1] = { 8 };

  const mxArray *t_y;
  const mxArray *u_y;
  static const int32_T iv32[1] = { 7 };

  uint8_T *b_pData;
  const mxArray *v_y;
  const mxArray *w_y;
  static const int32_T iv33[1] = { 7 };

  const mxArray *x_y;
  const mxArray *y_y;
  const mxArray *ab_y;
  const mxArray *bb_y;
  const mxArray *cb_y;
  const mxArray *db_y;
  const mxArray *eb_y;
  const mxArray *fb_y;
  const mxArray *gb_y;
  boolean_T (*validPipeline)[8];
  int32_T (*zPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*xPipeline)[7];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  st = NULL;
  y = NULL;
  emlrtAssign(&y, emlrtCreateCellMatrix(6, 1));
  b_y = NULL;
  m8 = emlrtCreateLogicalArray(1, iv24);
  emlrtInitLogicalArray(8, m8, *validPipeline);
  emlrtAssign(&b_y, m8);
  emlrtSetCell(y, 0, b_y);
  c_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  for (i = 0; i < 7; i++) {
    u[i] = (*xPipeline)[i];
  }

  d_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv25, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = u[i];
  }

  emlrtAssign(&d_y, m8);
  emlrtAssign(&c_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, b_eml_mx,
    "simulinkarray", d_y, true, false));
  emlrtSetCell(y, 1, c_y);
  e_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  for (i = 0; i < 7; i++) {
    u[i] = (*yPipeline)[i];
  }

  f_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv26, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = u[i];
  }

  emlrtAssign(&f_y, m8);
  emlrtAssign(&e_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, b_eml_mx,
    "simulinkarray", f_y, true, false));
  emlrtSetCell(y, 2, e_y);
  g_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  for (i = 0; i < 7; i++) {
    u[i] = (*zPipeline)[i];
  }

  h_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv27, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = u[i];
  }

  emlrtAssign(&h_y, m8);
  emlrtAssign(&g_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, c_eml_mx,
    "simulinkarray", h_y, true, false));
  emlrtSetCell(y, 3, g_y);
  i_y = NULL;
  emlrtAssign(&i_y, emlrtCreateStructMatrix(1, 1, 0, NULL));
  j_y = NULL;
  m8 = emlrtCreateLogicalScalar(moduleInstance->sysobj.isInitialized);
  emlrtAssign(&j_y, m8);
  emlrtAddField(i_y, j_y, "isInitialized", 0);
  k_y = NULL;
  m8 = emlrtCreateLogicalScalar(moduleInstance->sysobj.isReleased);
  emlrtAssign(&k_y, m8);
  emlrtAddField(i_y, k_y, "isReleased", 0);
  l_y = NULL;
  m8 = emlrtCreateLogicalScalar(moduleInstance->sysobj.TunablePropsChanged);
  emlrtAssign(&l_y, m8);
  emlrtAddField(i_y, l_y, "TunablePropsChanged", 0);
  m_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  n_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv28, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = moduleInstance->sysobj.xPipeline[i];
  }

  emlrtAssign(&n_y, m8);
  emlrtAssign(&m_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, b_eml_mx,
    "simulinkarray", n_y, false, false));
  emlrtAddField(i_y, m_y, "xPipeline", 0);
  o_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  p_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv29, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = moduleInstance->sysobj.yPipeline[i];
  }

  emlrtAssign(&p_y, m8);
  emlrtAssign(&o_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, b_eml_mx,
    "simulinkarray", p_y, false, false));
  emlrtAddField(i_y, o_y, "yPipeline", 0);
  q_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  r_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv30, mxINT32_CLASS, mxREAL);
  pData = (int32_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    pData[i] = moduleInstance->sysobj.zPipeline[i];
  }

  emlrtAssign(&r_y, m8);
  emlrtAssign(&q_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, c_eml_mx,
    "simulinkarray", r_y, false, false));
  emlrtAddField(i_y, q_y, "zPipeline", 0);
  s_y = NULL;
  m8 = emlrtCreateLogicalArray(1, iv31);
  emlrtInitLogicalArray(8, m8, moduleInstance->sysobj.validPipeline);
  emlrtAssign(&s_y, m8);
  emlrtAddField(i_y, s_y, "validPipeline", 0);
  t_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  u_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv32, mxUINT8_CLASS, mxREAL);
  b_pData = (uint8_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    b_pData[i] = moduleInstance->sysobj.pQuadrantIn[i];
  }

  emlrtAssign(&u_y, m8);
  emlrtAssign(&t_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, d_eml_mx,
    "simulinkarray", u_y, false, false));
  emlrtAddField(i_y, t_y, "pQuadrantIn", 0);
  v_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  w_y = NULL;
  m8 = emlrtCreateNumericArray(1, iv33, mxUINT8_CLASS, mxREAL);
  b_pData = (uint8_T *)mxGetData(m8);
  for (i = 0; i < 7; i++) {
    b_pData[i] = moduleInstance->sysobj.pXYReversed[i];
  }

  emlrtAssign(&w_y, m8);
  emlrtAssign(&v_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, e_eml_mx,
    "simulinkarray", w_y, false, false));
  emlrtAddField(i_y, v_y, "pXYReversed", 0);
  x_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  y_y = NULL;
  m8 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m8) = moduleInstance->sysobj.pQuadrantOut;
  emlrtAssign(&y_y, m8);
  emlrtAssign(&x_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, c_eml_mx,
    "simulinkarray", y_y, false, false));
  emlrtAddField(i_y, x_y, "pQuadrantOut", 0);
  ab_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  bb_y = NULL;
  m8 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m8) = moduleInstance->sysobj.pPipeout;
  emlrtAssign(&bb_y, m8);
  emlrtAssign(&ab_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, c_eml_mx,
    "simulinkarray", bb_y, false, false));
  emlrtAddField(i_y, ab_y, "pPipeout", 0);
  cb_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  db_y = NULL;
  m8 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m8) = moduleInstance->sysobj.pXAbsolute;
  emlrtAssign(&db_y, m8);
  emlrtAssign(&cb_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, f_eml_mx,
    "simulinkarray", db_y, false, false));
  emlrtAddField(i_y, cb_y, "pXAbsolute", 0);
  eb_y = NULL;
  hoistedGlobal = emlrtRootTLSGlobal;
  fb_y = NULL;
  m8 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)mxGetData(m8) = moduleInstance->sysobj.pYAbsolute;
  emlrtAssign(&fb_y, m8);
  emlrtAssign(&eb_y, emlrtCreateFIR2013b(hoistedGlobal, eml_mx, f_eml_mx,
    "simulinkarray", fb_y, false, false));
  emlrtAddField(i_y, eb_y, "pYAbsolute", 0);
  emlrtSetCell(y, 4, i_y);
  gb_y = NULL;
  m8 = emlrtCreateLogicalScalar(moduleInstance->sysobj_not_empty);
  emlrtAssign(&gb_y, m8);
  emlrtSetCell(y, 5, gb_y);
  emlrtAssign(&st, y);
  return st;
}

static void emlrt_marshallIn(const mxArray *b_validPipeline, const char_T
  *identifier, boolean_T y[8])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  b_emlrt_marshallIn(emlrtAlias(b_validPipeline), &thisId, y);
  emlrtDestroyArray(&b_validPipeline);
}

static void b_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, boolean_T y[8])
{
  q_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void c_emlrt_marshallIn(const mxArray *b_xPipeline, const char_T
  *identifier, int32_T y[7])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  d_emlrt_marshallIn(emlrtAlias(b_xPipeline), &thisId, y);
  emlrtDestroyArray(&b_xPipeline);
}

static void d_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7])
{
  int32_T iv34[1];
  iv34[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv34, eml_mx,
                     b_eml_mx);
  r_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void e_emlrt_marshallIn(const mxArray *b_zPipeline, const char_T
  *identifier, int32_T y[7])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  f_emlrt_marshallIn(emlrtAlias(b_zPipeline), &thisId, y);
  emlrtDestroyArray(&b_zPipeline);
}

static void f_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7])
{
  int32_T iv35[1];
  iv35[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv35, eml_mx,
                     c_eml_mx);
  s_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void g_emlrt_marshallIn(const mxArray *b_sysobj, const char_T *identifier,
  dsp_HDLComplexToMagnitudeAngle *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  h_emlrt_marshallIn(emlrtAlias(b_sysobj), &thisId, y);
  emlrtDestroyArray(&b_sysobj);
}

static void h_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, dsp_HDLComplexToMagnitudeAngle *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[13] = { "isInitialized", "isReleased",
    "TunablePropsChanged", "xPipeline", "yPipeline", "zPipeline",
    "validPipeline", "pQuadrantIn", "pXYReversed", "pQuadrantOut", "pPipeout",
    "pXAbsolute", "pYAbsolute" };

  thisId.fParent = parentId;
  emlrtCheckStructR2012b(emlrtRootTLSGlobal, parentId, u, 13, fieldNames, 0U, 0);
  thisId.fIdentifier = "isInitialized";
  y->isInitialized = i_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "isInitialized")), &thisId);
  thisId.fIdentifier = "isReleased";
  y->isReleased = i_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "isReleased")), &thisId);
  thisId.fIdentifier = "TunablePropsChanged";
  y->TunablePropsChanged = i_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "TunablePropsChanged")), &thisId);
  thisId.fIdentifier = "xPipeline";
  j_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "xPipeline")), &thisId, y->xPipeline);
  thisId.fIdentifier = "yPipeline";
  j_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "yPipeline")), &thisId, y->yPipeline);
  thisId.fIdentifier = "zPipeline";
  k_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "zPipeline")), &thisId, y->zPipeline);
  thisId.fIdentifier = "validPipeline";
  b_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "validPipeline")), &thisId, y->validPipeline);
  thisId.fIdentifier = "pQuadrantIn";
  l_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "pQuadrantIn")), &thisId, y->pQuadrantIn);
  thisId.fIdentifier = "pXYReversed";
  m_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a(emlrtRootTLSGlobal, u, 0,
    "pXYReversed")), &thisId, y->pXYReversed);
  thisId.fIdentifier = "pQuadrantOut";
  y->pQuadrantOut = n_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pQuadrantOut")), &thisId);
  thisId.fIdentifier = "pPipeout";
  y->pPipeout = n_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pPipeout")), &thisId);
  thisId.fIdentifier = "pXAbsolute";
  y->pXAbsolute = o_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pXAbsolute")), &thisId);
  thisId.fIdentifier = "pYAbsolute";
  y->pYAbsolute = o_emlrt_marshallIn(emlrtAlias(emlrtGetFieldR2013a
    (emlrtRootTLSGlobal, u, 0, "pYAbsolute")), &thisId);
  emlrtDestroyArray(&u);
}

static boolean_T i_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  boolean_T y;
  y = t_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void j_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7])
{
  int32_T iv36[1];
  iv36[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv36, eml_mx,
                     b_eml_mx);
  u_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, int32_T y[7])
{
  int32_T iv37[1];
  iv37[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv37, eml_mx,
                     c_eml_mx);
  v_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void l_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[7])
{
  int32_T iv38[1];
  iv38[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv38, eml_mx,
                     d_eml_mx);
  w_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void m_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId, uint8_T y[7])
{
  int32_T iv39[1];
  iv39[0] = 7;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 1U, iv39, eml_mx,
                     e_eml_mx);
  x_emlrt_marshallIn(emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static int32_T n_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  int32_T y;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 0U, 0, eml_mx,
                     c_eml_mx);
  y = y_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static int32_T o_emlrt_marshallIn(const mxArray *u, const emlrtMsgIdentifier
  *parentId)
{
  int32_T y;
  emlrtCheckFiR2012b(emlrtRootTLSGlobal, parentId, u, false, 0U, 0, eml_mx,
                     f_eml_mx);
  y = ab_emlrt_marshallIn(emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static boolean_T p_emlrt_marshallIn(const mxArray *b_sysobj_not_empty, const
  char_T *identifier)
{
  boolean_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = i_emlrt_marshallIn(emlrtAlias(b_sysobj_not_empty), &thisId);
  emlrtDestroyArray(&b_sysobj_not_empty);
  return y;
}

static void mw__internal__setSimState__fcn(InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
  *moduleInstance, const mxArray *st)
{
  const mxArray *u;
  boolean_T (*validPipeline)[8];
  int32_T (*xPipeline)[7];
  int32_T (*yPipeline)[7];
  int32_T (*zPipeline)[7];
  validPipeline = (boolean_T (*)[8])ssGetDWork(moduleInstance->S, 3U);
  zPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 2U);
  yPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 1U);
  xPipeline = (int32_T (*)[7])ssGetDWork(moduleInstance->S, 0U);
  u = emlrtAlias(st);
  emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 0)),
                   "validPipeline", *validPipeline);
  c_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 1)),
                     "xPipeline", *xPipeline);
  c_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 2)),
                     "yPipeline", *yPipeline);
  e_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 3)),
                     "zPipeline", *zPipeline);
  g_emlrt_marshallIn(emlrtAlias(emlrtGetCell(emlrtRootTLSGlobal, u, 4)),
                     "sysobj", &moduleInstance->sysobj);
  moduleInstance->sysobj_not_empty = p_emlrt_marshallIn(emlrtAlias(emlrtGetCell
    (emlrtRootTLSGlobal, u, 5)), "sysobj_not_empty");
  emlrtDestroyArray(&u);
  emlrtDestroyArray(&st);
}

static const mxArray *message(const mxArray *b, const mxArray *c, emlrtMCInfo
  *location)
{
  const mxArray *pArrays[2];
  const mxArray *m9;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m9, 2, pArrays, "message",
    true, location);
}

static void error(const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 0, NULL, 1, &pArray, "error", true,
                        location);
}

static const mxArray *fimath(char * b, char * c, char * d, char * e, char * f,
  char * g, char * h, char * i, char * j, char * k, char * l, real_T m, char * n,
  real_T o, char * p, real_T q, char * r, real_T s, char * t, real_T u, char * v,
  real_T w, char * y, real_T ab, char * bb, char * cb, char * db, real_T eb,
  char * fb, real_T gb, char * hb, real_T ib, char * jb, real_T kb, char * lb,
  real_T mb, char * nb, real_T ob, char * pb, real_T qb, char * rb, boolean_T sb,
  emlrtMCInfo *location)
{
  const mxArray *pArrays[42];
  const mxArray *m10;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateString(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateString(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateString(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateString(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateString(k);
  pArrays[10] = emlrtCreateString(l);
  pArrays[11] = emlrtCreateDoubleScalar(m);
  pArrays[12] = emlrtCreateString(n);
  pArrays[13] = emlrtCreateDoubleScalar(o);
  pArrays[14] = emlrtCreateString(p);
  pArrays[15] = emlrtCreateDoubleScalar(q);
  pArrays[16] = emlrtCreateString(r);
  pArrays[17] = emlrtCreateDoubleScalar(s);
  pArrays[18] = emlrtCreateString(t);
  pArrays[19] = emlrtCreateDoubleScalar(u);
  pArrays[20] = emlrtCreateString(v);
  pArrays[21] = emlrtCreateDoubleScalar(w);
  pArrays[22] = emlrtCreateString(y);
  pArrays[23] = emlrtCreateDoubleScalar(ab);
  pArrays[24] = emlrtCreateString(bb);
  pArrays[25] = emlrtCreateString(cb);
  pArrays[26] = emlrtCreateString(db);
  pArrays[27] = emlrtCreateDoubleScalar(eb);
  pArrays[28] = emlrtCreateString(fb);
  pArrays[29] = emlrtCreateDoubleScalar(gb);
  pArrays[30] = emlrtCreateString(hb);
  pArrays[31] = emlrtCreateDoubleScalar(ib);
  pArrays[32] = emlrtCreateString(jb);
  pArrays[33] = emlrtCreateDoubleScalar(kb);
  pArrays[34] = emlrtCreateString(lb);
  pArrays[35] = emlrtCreateDoubleScalar(mb);
  pArrays[36] = emlrtCreateString(nb);
  pArrays[37] = emlrtCreateDoubleScalar(ob);
  pArrays[38] = emlrtCreateString(pb);
  pArrays[39] = emlrtCreateDoubleScalar(qb);
  pArrays[40] = emlrtCreateString(rb);
  pArrays[41] = emlrtCreateLogicalScalar(sb);
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m10, 42, pArrays,
    "fimath", true, location);
}

static const mxArray *numerictype(char * b, real_T c, char * d, real_T e, char *
  f, real_T g, char * h, real_T i, char * j, real_T k, emlrtMCInfo *location)
{
  const mxArray *pArrays[10];
  const mxArray *m11;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateDoubleScalar(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateDoubleScalar(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateDoubleScalar(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateDoubleScalar(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateDoubleScalar(k);
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m11, 10, pArrays,
    "numerictype", true, location);
}

static const mxArray *b_numerictype(char * b, boolean_T c, char * d, char * e,
  char * f, real_T g, char * h, real_T i, char * j, real_T k, char * l, real_T m,
  char * n, real_T o, emlrtMCInfo *location)
{
  const mxArray *pArrays[14];
  const mxArray *m12;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateLogicalScalar(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateString(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateDoubleScalar(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateDoubleScalar(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateDoubleScalar(k);
  pArrays[10] = emlrtCreateString(l);
  pArrays[11] = emlrtCreateDoubleScalar(m);
  pArrays[12] = emlrtCreateString(n);
  pArrays[13] = emlrtCreateDoubleScalar(o);
  return emlrtCallMATLABR2012b(emlrtRootTLSGlobal, 1, &m12, 14, pArrays,
    "numerictype", true, location);
}

static void q_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, boolean_T ret[8])
{
  int32_T iv40[1];
  int32_T i12;
  iv40[0] = 8;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "logical", false, 1U,
    iv40);
  for (i12 = 0; i12 < 8; i12++) {
    ret[i12] = (*(boolean_T (*)[8])mxGetLogicals(src))[i12];
  }

  emlrtDestroyArray(&src);
}

static void r_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7])
{
  const mxArray *mxInt;
  int32_T i13;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i13 = 0; i13 < 7; i13++) {
    ret[i13] = (*(int32_T (*)[7])mxGetData(mxInt))[i13];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static void s_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7])
{
  const mxArray *mxInt;
  int32_T i14;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i14 = 0; i14 < 7; i14++) {
    ret[i14] = (*(int32_T (*)[7])mxGetData(mxInt))[i14];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static boolean_T t_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  boolean_T ret;
  emlrtCheckBuiltInR2012b(emlrtRootTLSGlobal, msgId, src, "logical", false, 0U,
    0);
  ret = *mxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void u_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7])
{
  const mxArray *mxInt;
  int32_T i15;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i15 = 0; i15 < 7; i15++) {
    ret[i15] = (*(int32_T (*)[7])mxGetData(mxInt))[i15];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static void v_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, int32_T ret[7])
{
  const mxArray *mxInt;
  int32_T i16;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i16 = 0; i16 < 7; i16++) {
    ret[i16] = (*(int32_T (*)[7])mxGetData(mxInt))[i16];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static void w_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[7])
{
  const mxArray *mxInt;
  int32_T i17;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i17 = 0; i17 < 7; i17++) {
    ret[i17] = (*(uint8_T (*)[7])mxGetData(mxInt))[i17];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static void x_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId, uint8_T ret[7])
{
  const mxArray *mxInt;
  int32_T i18;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  for (i18 = 0; i18 < 7; i18++) {
    ret[i18] = (*(uint8_T (*)[7])mxGetData(mxInt))[i18];
  }

  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
}

static int32_T y_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier
  *msgId)
{
  int32_T ret;
  const mxArray *mxInt;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(int32_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

static int32_T ab_emlrt_marshallIn(const mxArray *src, const emlrtMsgIdentifier *
  msgId)
{
  int32_T ret;
  const mxArray *mxInt;
  (void)msgId;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(int32_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

/* CGXE Glue Code */
static void mdlOutputs_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S, int_T tid)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_outputs(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlInitialize_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_initialize(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static void mdlUpdate_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S, int_T tid)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_update(moduleInstance);
  CGXERT_LEAVE_CHECK();
}

static mxArray* getSimState_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  mxArray* mxSS;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mxSS = (mxArray *) mw__internal__getSimState__fcn(moduleInstance);
  CGXERT_LEAVE_CHECK();
  return mxSS;
}

static void setSimState_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S, const mxArray *ss)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  mw__internal__setSimState__fcn(moduleInstance, emlrtAlias(ss));
  CGXERT_LEAVE_CHECK();
}

static void mdlTerminate_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)ssGetUserData(S);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_terminate(moduleInstance);
  CGXERT_LEAVE_CHECK();
  free((void *)moduleInstance);
  ssSetUserData(S, NULL);
}

static void mdlStart_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S)
{
  InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *moduleInstance;
  moduleInstance = (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG *)calloc(1, sizeof
    (InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG));
  moduleInstance->S = S;
  ssSetUserData(S, (void *)moduleInstance);
  CGXERT_ENTER_CHECK();
  cgxe_mdl_start(moduleInstance);
  CGXERT_LEAVE_CHECK();

  {
    uint_T options = ssGetOptions(S);
    options |= SS_OPTION_RUNTIME_EXCEPTION_FREE_CODE;
    ssSetOptions(S, options);
  }

  ssSetmdlOutputs(S, mdlOutputs_TxLJ3WfyYC4Y5PZDNfGuUG);
  ssSetmdlInitializeConditions(S, mdlInitialize_TxLJ3WfyYC4Y5PZDNfGuUG);
  ssSetmdlUpdate(S, mdlUpdate_TxLJ3WfyYC4Y5PZDNfGuUG);
  ssSetmdlTerminate(S, mdlTerminate_TxLJ3WfyYC4Y5PZDNfGuUG);
}

static void mdlProcessParameters_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S)
{
}

void method_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_TxLJ3WfyYC4Y5PZDNfGuUG(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_TxLJ3WfyYC4Y5PZDNfGuUG(S);
    break;

   case SS_CALL_MDL_GET_SIM_STATE:
    *((mxArray**) data) = getSimState_TxLJ3WfyYC4Y5PZDNfGuUG(S);
    break;

   case SS_CALL_MDL_SET_SIM_STATE:
    setSimState_TxLJ3WfyYC4Y5PZDNfGuUG(S, (const mxArray *) data);
    break;

   default:
    /* Unhandled method */
    /*
       sf_mex_error_message("Stateflow Internal Error:\n"
       "Error calling method dispatcher for module: TxLJ3WfyYC4Y5PZDNfGuUG.\n"
       "Can't handle method %d.\n", method);
     */
    break;
  }
}

int autoInfer_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(mxArray* plhs[], const char
  * commandName)
{
  if (strcmp(commandName, "NameResolution") == 0) {
    plhs[0] = (mxArray*) mw__internal__name__resolution__fcn();
    return 1;
  }

  if (strcmp(commandName, "AutoInfer") == 0) {
    plhs[0] = (mxArray*) mw__internal__autoInference__fcn();
    return 1;
  }

  return 0;
}

mxArray *cgxe_TxLJ3WfyYC4Y5PZDNfGuUG_BuildInfoUpdate(void)
{
  mxArray * mxBIArgs;
  mxArray * elem_1;
  mxArray * elem_2;
  mxArray * elem_3;
  mxArray * elem_4;
  mxArray * elem_5;
  mxArray * elem_6;
  mxArray * elem_7;
  mxArray * elem_8;
  mxArray * elem_9;
  mxBIArgs = mxCreateCellMatrix(1,3);
  elem_1 = mxCreateCellMatrix(1,6);
  elem_2 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,0,elem_2);
  elem_3 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,1,elem_3);
  elem_4 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,2,elem_4);
  elem_5 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,3,elem_5);
  elem_6 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,4,elem_6);
  elem_7 = mxCreateCellMatrix(0,0);
  mxSetCell(elem_1,5,elem_7);
  mxSetCell(mxBIArgs,0,elem_1);
  elem_8 = mxCreateCellMatrix(1,0);
  mxSetCell(mxBIArgs,1,elem_8);
  elem_9 = mxCreateCellMatrix(1,0);
  mxSetCell(mxBIArgs,2,elem_9);
  return mxBIArgs;
}
