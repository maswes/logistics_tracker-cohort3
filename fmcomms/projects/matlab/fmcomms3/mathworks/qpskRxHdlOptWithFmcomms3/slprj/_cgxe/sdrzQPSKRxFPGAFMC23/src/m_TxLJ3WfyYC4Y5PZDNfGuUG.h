#ifndef __TxLJ3WfyYC4Y5PZDNfGuUG_h__
#define __TxLJ3WfyYC4Y5PZDNfGuUG_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_slE07I4lDg6FknjQ3k8Q9CG
#define struct_slE07I4lDg6FknjQ3k8Q9CG

struct slE07I4lDg6FknjQ3k8Q9CG
{
  real_T Index;
  real_T DataType;
  real_T IsSigned;
  real_T MantBits;
  real_T FixExp;
  real_T Slope;
  real_T Bias;
};

#endif                                 /*struct_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef typedef_slE07I4lDg6FknjQ3k8Q9CG
#define typedef_slE07I4lDg6FknjQ3k8Q9CG

typedef struct slE07I4lDg6FknjQ3k8Q9CG slE07I4lDg6FknjQ3k8Q9CG;

#endif                                 /*typedef_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef struct_sSowLTsTotFyJ7siSdApBMF
#define struct_sSowLTsTotFyJ7siSdApBMF

struct sSowLTsTotFyJ7siSdApBMF
{
  real_T WordLength;
  real_T FractionLength;
};

#endif                                 /*struct_sSowLTsTotFyJ7siSdApBMF*/

#ifndef typedef_sSowLTsTotFyJ7siSdApBMF
#define typedef_sSowLTsTotFyJ7siSdApBMF

typedef struct sSowLTsTotFyJ7siSdApBMF sSowLTsTotFyJ7siSdApBMF;

#endif                                 /*typedef_sSowLTsTotFyJ7siSdApBMF*/

#ifndef struct_s4rqpRwWyLUuZLLlhqkSqq
#define struct_s4rqpRwWyLUuZLLlhqkSqq

struct s4rqpRwWyLUuZLLlhqkSqq
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_s4rqpRwWyLUuZLLlhqkSqq*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F
#define typedef_sfOd2wElE6un66xmZCZog7F

typedef struct s4rqpRwWyLUuZLLlhqkSqq sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_s4rqpRwWyLUuZLLlhqkSqq_size
#define struct_s4rqpRwWyLUuZLLlhqkSqq_size

struct s4rqpRwWyLUuZLLlhqkSqq_size
{
  int32_T dims[2];
};

#endif                                 /*struct_s4rqpRwWyLUuZLLlhqkSqq_size*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F_size
#define typedef_sfOd2wElE6un66xmZCZog7F_size

typedef struct s4rqpRwWyLUuZLLlhqkSqq_size sfOd2wElE6un66xmZCZog7F_size;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F_size*/

#ifndef struct_sWoLls2DQ8h1WGLweWfwpPG
#define struct_sWoLls2DQ8h1WGLweWfwpPG

struct sWoLls2DQ8h1WGLweWfwpPG
{
  char_T names[13];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sWoLls2DQ8h1WGLweWfwpPG*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8
#define typedef_sZVQz5WVraeIWEljxFvLe8

typedef struct sWoLls2DQ8h1WGLweWfwpPG sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sWoLls2DQ8h1WGLweWfwpPG_size
#define struct_sWoLls2DQ8h1WGLweWfwpPG_size

struct sWoLls2DQ8h1WGLweWfwpPG_size
{
  int32_T names[2];
  int32_T dims[2];
};

#endif                                 /*struct_sWoLls2DQ8h1WGLweWfwpPG_size*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8_size
#define typedef_sZVQz5WVraeIWEljxFvLe8_size

typedef struct sWoLls2DQ8h1WGLweWfwpPG_size sZVQz5WVraeIWEljxFvLe8_size;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8_size*/

#ifndef struct_s7UBIGHSehQY1gCsIQWwr5C
#define struct_s7UBIGHSehQY1gCsIQWwr5C

struct s7UBIGHSehQY1gCsIQWwr5C
{
  real_T chksum[4];
};

#endif                                 /*struct_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef typedef_s7UBIGHSehQY1gCsIQWwr5C
#define typedef_s7UBIGHSehQY1gCsIQWwr5C

typedef struct s7UBIGHSehQY1gCsIQWwr5C s7UBIGHSehQY1gCsIQWwr5C;

#endif                                 /*typedef_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef struct_sfOd2wElE6un66xmZCZog7F
#define struct_sfOd2wElE6un66xmZCZog7F

struct sfOd2wElE6un66xmZCZog7F
{
  real_T dimModes;
  real_T dims[3];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sfOd2wElE6un66xmZCZog7F*/

#ifndef typedef_b_sfOd2wElE6un66xmZCZog7F
#define typedef_b_sfOd2wElE6un66xmZCZog7F

typedef struct sfOd2wElE6un66xmZCZog7F b_sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_b_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_sRzepbcAzWdfpiYnMYop13F
#define struct_sRzepbcAzWdfpiYnMYop13F

struct sRzepbcAzWdfpiYnMYop13F
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sRzepbcAzWdfpiYnMYop13F*/

#ifndef typedef_sRzepbcAzWdfpiYnMYop13F
#define typedef_sRzepbcAzWdfpiYnMYop13F

typedef struct sRzepbcAzWdfpiYnMYop13F sRzepbcAzWdfpiYnMYop13F;

#endif                                 /*typedef_sRzepbcAzWdfpiYnMYop13F*/

#ifndef struct_sZVQz5WVraeIWEljxFvLe8
#define struct_sZVQz5WVraeIWEljxFvLe8

struct sZVQz5WVraeIWEljxFvLe8
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sZVQz5WVraeIWEljxFvLe8*/

#ifndef typedef_b_sZVQz5WVraeIWEljxFvLe8
#define typedef_b_sZVQz5WVraeIWEljxFvLe8

typedef struct sZVQz5WVraeIWEljxFvLe8 b_sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_b_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sxU6zEVvq7tn2QHnyq6NPE
#define struct_sxU6zEVvq7tn2QHnyq6NPE

struct sxU6zEVvq7tn2QHnyq6NPE
{
  char_T names[9];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sxU6zEVvq7tn2QHnyq6NPE*/

#ifndef typedef_sxU6zEVvq7tn2QHnyq6NPE
#define typedef_sxU6zEVvq7tn2QHnyq6NPE

typedef struct sxU6zEVvq7tn2QHnyq6NPE sxU6zEVvq7tn2QHnyq6NPE;

#endif                                 /*typedef_sxU6zEVvq7tn2QHnyq6NPE*/

#ifndef struct_sc3VTylUkbr6pYiOsCVYrxB
#define struct_sc3VTylUkbr6pYiOsCVYrxB

struct sc3VTylUkbr6pYiOsCVYrxB
{
  char_T names[13];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sc3VTylUkbr6pYiOsCVYrxB*/

#ifndef typedef_sc3VTylUkbr6pYiOsCVYrxB
#define typedef_sc3VTylUkbr6pYiOsCVYrxB

typedef struct sc3VTylUkbr6pYiOsCVYrxB sc3VTylUkbr6pYiOsCVYrxB;

#endif                                 /*typedef_sc3VTylUkbr6pYiOsCVYrxB*/

#ifndef struct_sIvmHumfM4VG8K4LjAjoqqB
#define struct_sIvmHumfM4VG8K4LjAjoqqB

struct sIvmHumfM4VG8K4LjAjoqqB
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T dTypeSize;
  char_T dTypeName;
  real_T dTypeIndex;
  char_T dTypeChksum;
  real_T complexity;
};

#endif                                 /*struct_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef typedef_sIvmHumfM4VG8K4LjAjoqqB
#define typedef_sIvmHumfM4VG8K4LjAjoqqB

typedef struct sIvmHumfM4VG8K4LjAjoqqB sIvmHumfM4VG8K4LjAjoqqB;

#endif                                 /*typedef_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef struct_sWzLM2sxoJyiBvZh9w5OsiF
#define struct_sWzLM2sxoJyiBvZh9w5OsiF

struct sWzLM2sxoJyiBvZh9w5OsiF
{
  real_T codeGenChksum[4];
};

#endif                                 /*struct_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef typedef_sWzLM2sxoJyiBvZh9w5OsiF
#define typedef_sWzLM2sxoJyiBvZh9w5OsiF

typedef struct sWzLM2sxoJyiBvZh9w5OsiF sWzLM2sxoJyiBvZh9w5OsiF;

#endif                                 /*typedef_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef struct_stg5oKYMfv00T61iug9hTfC
#define struct_stg5oKYMfv00T61iug9hTfC

struct stg5oKYMfv00T61iug9hTfC
{
  boolean_T isInitialized;
  boolean_T isReleased;
  boolean_T TunablePropsChanged;
  int32_T xPipeline[7];
  int32_T yPipeline[7];
  int32_T zPipeline[7];
  boolean_T validPipeline[8];
  uint8_T pQuadrantIn[7];
  uint8_T pXYReversed[7];
  int32_T pQuadrantOut;
  int32_T pPipeout;
  int32_T pXAbsolute;
  int32_T pYAbsolute;
};

#endif                                 /*struct_stg5oKYMfv00T61iug9hTfC*/

#ifndef typedef_dsp_HDLComplexToMagnitudeAngle
#define typedef_dsp_HDLComplexToMagnitudeAngle

typedef struct stg5oKYMfv00T61iug9hTfC dsp_HDLComplexToMagnitudeAngle;

#endif                                 /*typedef_dsp_HDLComplexToMagnitudeAngle*/

#ifndef typedef_InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG
#define typedef_InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG

typedef struct {
  SimStruct *S;
  dsp_HDLComplexToMagnitudeAngle sysobj;
  boolean_T sysobj_not_empty;
} InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG;

#endif                                 /*typedef_InstanceStruct_TxLJ3WfyYC4Y5PZDNfGuUG*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(SimStruct *S, int_T method,
  void* data);
extern int autoInfer_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(mxArray *lhs[], const
  char* commandName);

#endif
