#ifndef __jYb1o5S6A7YFQ0qzurvoEH_h__
#define __jYb1o5S6A7YFQ0qzurvoEH_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_slE07I4lDg6FknjQ3k8Q9CG
#define struct_slE07I4lDg6FknjQ3k8Q9CG

struct slE07I4lDg6FknjQ3k8Q9CG
{
  real_T Index;
  real_T DataType;
  real_T IsSigned;
  real_T MantBits;
  real_T FixExp;
  real_T Slope;
  real_T Bias;
};

#endif                                 /*struct_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef typedef_slE07I4lDg6FknjQ3k8Q9CG
#define typedef_slE07I4lDg6FknjQ3k8Q9CG

typedef struct slE07I4lDg6FknjQ3k8Q9CG slE07I4lDg6FknjQ3k8Q9CG;

#endif                                 /*typedef_slE07I4lDg6FknjQ3k8Q9CG*/

#ifndef struct_s4rqpRwWyLUuZLLlhqkSqq
#define struct_s4rqpRwWyLUuZLLlhqkSqq

struct s4rqpRwWyLUuZLLlhqkSqq
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_s4rqpRwWyLUuZLLlhqkSqq*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F
#define typedef_sfOd2wElE6un66xmZCZog7F

typedef struct s4rqpRwWyLUuZLLlhqkSqq sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_s4rqpRwWyLUuZLLlhqkSqq_size
#define struct_s4rqpRwWyLUuZLLlhqkSqq_size

struct s4rqpRwWyLUuZLLlhqkSqq_size
{
  int32_T dims[2];
};

#endif                                 /*struct_s4rqpRwWyLUuZLLlhqkSqq_size*/

#ifndef typedef_sfOd2wElE6un66xmZCZog7F_size
#define typedef_sfOd2wElE6un66xmZCZog7F_size

typedef struct s4rqpRwWyLUuZLLlhqkSqq_size sfOd2wElE6un66xmZCZog7F_size;

#endif                                 /*typedef_sfOd2wElE6un66xmZCZog7F_size*/

#ifndef struct_sYL5FYf9EHVQKNInEqsmlLG
#define struct_sYL5FYf9EHVQKNInEqsmlLG

struct sYL5FYf9EHVQKNInEqsmlLG
{
  char_T names[8];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sYL5FYf9EHVQKNInEqsmlLG*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8
#define typedef_sZVQz5WVraeIWEljxFvLe8

typedef struct sYL5FYf9EHVQKNInEqsmlLG sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sYL5FYf9EHVQKNInEqsmlLG_size
#define struct_sYL5FYf9EHVQKNInEqsmlLG_size

struct sYL5FYf9EHVQKNInEqsmlLG_size
{
  int32_T names[2];
  int32_T dims[2];
};

#endif                                 /*struct_sYL5FYf9EHVQKNInEqsmlLG_size*/

#ifndef typedef_sZVQz5WVraeIWEljxFvLe8_size
#define typedef_sZVQz5WVraeIWEljxFvLe8_size

typedef struct sYL5FYf9EHVQKNInEqsmlLG_size sZVQz5WVraeIWEljxFvLe8_size;

#endif                                 /*typedef_sZVQz5WVraeIWEljxFvLe8_size*/

#ifndef struct_s7UBIGHSehQY1gCsIQWwr5C
#define struct_s7UBIGHSehQY1gCsIQWwr5C

struct s7UBIGHSehQY1gCsIQWwr5C
{
  real_T chksum[4];
};

#endif                                 /*struct_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef typedef_s7UBIGHSehQY1gCsIQWwr5C
#define typedef_s7UBIGHSehQY1gCsIQWwr5C

typedef struct s7UBIGHSehQY1gCsIQWwr5C s7UBIGHSehQY1gCsIQWwr5C;

#endif                                 /*typedef_s7UBIGHSehQY1gCsIQWwr5C*/

#ifndef struct_sfOd2wElE6un66xmZCZog7F
#define struct_sfOd2wElE6un66xmZCZog7F

struct sfOd2wElE6un66xmZCZog7F
{
  real_T dimModes;
  real_T dims[3];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sfOd2wElE6un66xmZCZog7F*/

#ifndef typedef_b_sfOd2wElE6un66xmZCZog7F
#define typedef_b_sfOd2wElE6un66xmZCZog7F

typedef struct sfOd2wElE6un66xmZCZog7F b_sfOd2wElE6un66xmZCZog7F;

#endif                                 /*typedef_b_sfOd2wElE6un66xmZCZog7F*/

#ifndef struct_sRzepbcAzWdfpiYnMYop13F
#define struct_sRzepbcAzWdfpiYnMYop13F

struct sRzepbcAzWdfpiYnMYop13F
{
  real_T dimModes;
  real_T dims[4];
  real_T dType;
  real_T complexity;
  real_T outputBuiltInDTEqUsed;
};

#endif                                 /*struct_sRzepbcAzWdfpiYnMYop13F*/

#ifndef typedef_sRzepbcAzWdfpiYnMYop13F
#define typedef_sRzepbcAzWdfpiYnMYop13F

typedef struct sRzepbcAzWdfpiYnMYop13F sRzepbcAzWdfpiYnMYop13F;

#endif                                 /*typedef_sRzepbcAzWdfpiYnMYop13F*/

#ifndef struct_sZVQz5WVraeIWEljxFvLe8
#define struct_sZVQz5WVraeIWEljxFvLe8

struct sZVQz5WVraeIWEljxFvLe8
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sZVQz5WVraeIWEljxFvLe8*/

#ifndef typedef_b_sZVQz5WVraeIWEljxFvLe8
#define typedef_b_sZVQz5WVraeIWEljxFvLe8

typedef struct sZVQz5WVraeIWEljxFvLe8 b_sZVQz5WVraeIWEljxFvLe8;

#endif                                 /*typedef_b_sZVQz5WVraeIWEljxFvLe8*/

#ifndef struct_sGPsr9GlFdIF3maLY46FcXF
#define struct_sGPsr9GlFdIF3maLY46FcXF

struct sGPsr9GlFdIF3maLY46FcXF
{
  char_T names[3];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sGPsr9GlFdIF3maLY46FcXF*/

#ifndef typedef_sGPsr9GlFdIF3maLY46FcXF
#define typedef_sGPsr9GlFdIF3maLY46FcXF

typedef struct sGPsr9GlFdIF3maLY46FcXF sGPsr9GlFdIF3maLY46FcXF;

#endif                                 /*typedef_sGPsr9GlFdIF3maLY46FcXF*/

#ifndef struct_skp0Kl4G5BGRodiUJKYv9AE
#define struct_skp0Kl4G5BGRodiUJKYv9AE

struct skp0Kl4G5BGRodiUJKYv9AE
{
  char_T names[8];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_skp0Kl4G5BGRodiUJKYv9AE*/

#ifndef typedef_skp0Kl4G5BGRodiUJKYv9AE
#define typedef_skp0Kl4G5BGRodiUJKYv9AE

typedef struct skp0Kl4G5BGRodiUJKYv9AE skp0Kl4G5BGRodiUJKYv9AE;

#endif                                 /*typedef_skp0Kl4G5BGRodiUJKYv9AE*/

#ifndef struct_sWfXrrLeGNvp4Lc5rCVBsbH
#define struct_sWfXrrLeGNvp4Lc5rCVBsbH

struct sWfXrrLeGNvp4Lc5rCVBsbH
{
  char_T names[7];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_sWfXrrLeGNvp4Lc5rCVBsbH*/

#ifndef typedef_sWfXrrLeGNvp4Lc5rCVBsbH
#define typedef_sWfXrrLeGNvp4Lc5rCVBsbH

typedef struct sWfXrrLeGNvp4Lc5rCVBsbH sWfXrrLeGNvp4Lc5rCVBsbH;

#endif                                 /*typedef_sWfXrrLeGNvp4Lc5rCVBsbH*/

#ifndef struct_s85jWlxI80XXhjSbwsUcEeF
#define struct_s85jWlxI80XXhjSbwsUcEeF

struct s85jWlxI80XXhjSbwsUcEeF
{
  char_T names[6];
  real_T dims[4];
  real_T dType;
  real_T complexity;
};

#endif                                 /*struct_s85jWlxI80XXhjSbwsUcEeF*/

#ifndef typedef_s85jWlxI80XXhjSbwsUcEeF
#define typedef_s85jWlxI80XXhjSbwsUcEeF

typedef struct s85jWlxI80XXhjSbwsUcEeF s85jWlxI80XXhjSbwsUcEeF;

#endif                                 /*typedef_s85jWlxI80XXhjSbwsUcEeF*/

#ifndef struct_sIvmHumfM4VG8K4LjAjoqqB
#define struct_sIvmHumfM4VG8K4LjAjoqqB

struct sIvmHumfM4VG8K4LjAjoqqB
{
  char_T names;
  real_T dims[3];
  real_T dType;
  real_T dTypeSize;
  char_T dTypeName;
  real_T dTypeIndex;
  char_T dTypeChksum;
  real_T complexity;
};

#endif                                 /*struct_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef typedef_sIvmHumfM4VG8K4LjAjoqqB
#define typedef_sIvmHumfM4VG8K4LjAjoqqB

typedef struct sIvmHumfM4VG8K4LjAjoqqB sIvmHumfM4VG8K4LjAjoqqB;

#endif                                 /*typedef_sIvmHumfM4VG8K4LjAjoqqB*/

#ifndef struct_sWzLM2sxoJyiBvZh9w5OsiF
#define struct_sWzLM2sxoJyiBvZh9w5OsiF

struct sWzLM2sxoJyiBvZh9w5OsiF
{
  real_T codeGenChksum[4];
};

#endif                                 /*struct_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef typedef_sWzLM2sxoJyiBvZh9w5OsiF
#define typedef_sWzLM2sxoJyiBvZh9w5OsiF

typedef struct sWzLM2sxoJyiBvZh9w5OsiF sWzLM2sxoJyiBvZh9w5OsiF;

#endif                                 /*typedef_sWzLM2sxoJyiBvZh9w5OsiF*/

#ifndef struct_sVE0w75YU6sT62X1vRHYrdB
#define struct_sVE0w75YU6sT62X1vRHYrdB

struct sVE0w75YU6sT62X1vRHYrdB
{
  boolean_T isInitialized;
  boolean_T isReleased;
  int32_T acc;
  int16_T phaseReg[6];
  boolean_T validReg[6];
  int16_T sineReg[6];
  int16_T cosReg[6];
  real_T pn_reg[19];
  int32_T phaseInc;
  int32_T phaseOff;
  int32_T tmpAcc;
  int32_T tmpAcc2;
  uint8_T dither;
  int16_T phaseQuant;
};

#endif                                 /*struct_sVE0w75YU6sT62X1vRHYrdB*/

#ifndef typedef_dsp_HDLNCO
#define typedef_dsp_HDLNCO

typedef struct sVE0w75YU6sT62X1vRHYrdB dsp_HDLNCO;

#endif                                 /*typedef_dsp_HDLNCO*/

#ifndef typedef_InstanceStruct_jYb1o5S6A7YFQ0qzurvoEH
#define typedef_InstanceStruct_jYb1o5S6A7YFQ0qzurvoEH

typedef struct {
  SimStruct *S;
  dsp_HDLNCO sysobj;
  boolean_T sysobj_not_empty;
} InstanceStruct_jYb1o5S6A7YFQ0qzurvoEH;

#endif                                 /*typedef_InstanceStruct_jYb1o5S6A7YFQ0qzurvoEH*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_jYb1o5S6A7YFQ0qzurvoEH(SimStruct *S, int_T method,
  void* data);
extern int autoInfer_dispatcher_jYb1o5S6A7YFQ0qzurvoEH(mxArray *lhs[], const
  char* commandName);

#endif
