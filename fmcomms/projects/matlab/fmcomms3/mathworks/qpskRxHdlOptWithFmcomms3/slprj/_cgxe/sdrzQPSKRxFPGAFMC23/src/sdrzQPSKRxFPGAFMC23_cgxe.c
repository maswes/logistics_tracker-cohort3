/* Include files */

#include "sdrzQPSKRxFPGAFMC23_cgxe.h"
#include "m_6G49rbutZhd1j25aViObvC.h"
#include "m_TxLJ3WfyYC4Y5PZDNfGuUG.h"
#include "m_jYb1o5S6A7YFQ0qzurvoEH.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_sdrzQPSKRxFPGAFMC23_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_sdrzQPSKRxFPGAFMC23_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_sdrzQPSKRxFPGAFMC23_method_dispatcher(SimStruct* S, int_T
  method, void* data)
{
  if (ssGetChecksum0(S) == 2868301158 &&
      ssGetChecksum1(S) == 3034545011 &&
      ssGetChecksum2(S) == 2329657714 &&
      ssGetChecksum3(S) == 485338011) {
    method_dispatcher_6G49rbutZhd1j25aViObvC(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3909935570 &&
      ssGetChecksum1(S) == 3517198310 &&
      ssGetChecksum2(S) == 466874085 &&
      ssGetChecksum3(S) == 219103150) {
    method_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 4162172738 &&
      ssGetChecksum1(S) == 1225037256 &&
      ssGetChecksum2(S) == 4270427844 &&
      ssGetChecksum3(S) == 2833247350) {
    method_dispatcher_jYb1o5S6A7YFQ0qzurvoEH(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_sdrzQPSKRxFPGAFMC23_autoInfer_dispatcher(const mxArray* prhs, mxArray*
  lhs[], const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzQPSKRxFPGAFMC23:10628") == 0 ) {
    return autoInfer_dispatcher_6G49rbutZhd1j25aViObvC(lhs, commandName);
  }

  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzQPSKRxFPGAFMC23:10612") == 0 ) {
    return autoInfer_dispatcher_TxLJ3WfyYC4Y5PZDNfGuUG(lhs, commandName);
  }

  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzQPSKRxFPGAFMC23:10435") == 0 ) {
    return autoInfer_dispatcher_jYb1o5S6A7YFQ0qzurvoEH(lhs, commandName);
  }

  if (strcmp(sid, "sdrzQPSKRxFPGAFMC23:10594") == 0 ) {
    return autoInfer_dispatcher_jYb1o5S6A7YFQ0qzurvoEH(lhs, commandName);
  }

  return 0;
}
