#include "__cf_sdrzQPSKRxFPGAFMC23.h"
#include <math.h>
#include "sdrzQPSKRxFPGAFMC23_acc.h"
#include "sdrzQPSKRxFPGAFMC23_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
static void mdlOutputs ( SimStruct * S , int_T tid ) { int32_T currentOffset
; int32_T outIdx ; int32_T kIdx ; int32_T iIdx ; int8_T reSign ; int8_T
imSign ; int32_T i ; int32_T k ; int16_T tmp ; int16_T tmp_p ; int64_T tmp_e
; int64_T tmp_i ; int64_T tmp_m ; int64_T tmp_g ; int64_T tmp_j ; int64_T
tmp_f ; real_T tmp_c ; real_T tmp_k ; apofffdfhu * _rtB ; p0d4ltqkb0 * _rtDW
; _rtDW = ( ( p0d4ltqkb0 * ) ssGetRootDWork ( S ) ) ; _rtB = ( ( apofffdfhu *
) _ssGetBlockIO ( S ) ) ; if ( ssIsSampleHit ( S , 3 , 0 ) ) {
ssCallAccelRunBlock ( S , 0 , 0 , SS_CALL_MDL_OUTPUTS ) ; memcpy ( & _rtB ->
obkxybx5mt [ 0 ] , & _rtB -> ktvy4zxbrp [ 0 ] , 4000U * sizeof ( creal_T ) )
; _rtB -> gslcw30p3u = _rtB -> c1t2it2tux ; if ( _rtB -> gslcw30p3u > 0.0 ) {
if ( ! _rtDW -> clfjv4sbg5 ) { ssCallAccelRunBlock ( S , 15 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 13 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 17 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 1 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 9 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 11 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; ssCallAccelRunBlock ( S , 12 , 0 ,
SS_CALL_RTW_GENERATED_ENABLE ) ; _rtDW -> clfjv4sbg5 = true ; } } else { if (
_rtDW -> clfjv4sbg5 ) { if ( _rtDW -> e40qniej0g ) { ssCallAccelRunBlock ( S
, 7 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW -> e40qniej0g = false ; }
ssCallAccelRunBlock ( S , 15 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 13 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 17 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 1 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; if (
_rtDW -> oyjntg5v1k ) { ssCallAccelRunBlock ( S , 2 , 0 ,
SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW -> oyjntg5v1k = false ; }
ssCallAccelRunBlock ( S , 9 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 11 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 12 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW ->
clfjv4sbg5 = false ; } } } if ( _rtDW -> clfjv4sbg5 ) { if ( ssIsSampleHit (
S , 1 , 0 ) ) { _rtB -> pr45gajxdt = _rtDW -> hcjfr00jlo [ 0 ] ; _rtB ->
obnfdr2i53 = _rtDW -> hdopk0vvuy ; _rtB -> ly2wus2tcm = _rtB -> obnfdr2i53 ;
if ( _rtB -> ly2wus2tcm ) { if ( ! _rtDW -> e40qniej0g ) {
ssCallAccelRunBlock ( S , 7 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ; _rtDW ->
e40qniej0g = true ; } _rtB -> kkfw1jwgh0 = 524288 ; _rtB -> kpj5dizbt4 =
_rtDW -> me2mx5snxx [ 0 ] ; _rtB -> f2oagwwe5w = _rtDW -> j3nqizf5sz [ 0 ] ;
_rtB -> ax0zsrbpeu = _rtDW -> hcwe4zz5v4 [ 0 ] ; _rtB -> aisymkzzms = _rtDW
-> glw3pp0wuv ; _rtB -> fzm4azyw5t = _rtDW -> fxwi43groy ; _rtB -> oxlr2stf2n
[ 0 ] = _rtDW -> oifeqzhrgp [ 0 ] ; _rtB -> oxlr2stf2n [ 1 ] = _rtDW ->
oifeqzhrgp [ 1 ] ; _rtB -> devnqy0tal = _rtDW -> hx4vxy3emn ; _rtB ->
nggubdrym0 = ( _rtB -> devnqy0tal >= _rtB -> kkfw1jwgh0 ) ;
ssCallAccelRunBlock ( S , 7 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> g2uf21tk3d
= _rtB -> pr45gajxdt . re ; _rtB -> ng104j1bnv = _rtB -> pr45gajxdt . im ;
outIdx = ( ( _rtB -> g2uf21tk3d & 65536 ) != 0 ? _rtB -> g2uf21tk3d | - 65536
: _rtB -> g2uf21tk3d & 65535 ) + ( ( _rtB -> ng104j1bnv & 65536 ) != 0 ? _rtB
-> ng104j1bnv | - 65536 : _rtB -> ng104j1bnv & 65535 ) ; _rtB -> owp5iuocjy =
( outIdx & 65536 ) != 0 ? outIdx | - 65536 : outIdx & 65535 ; outIdx = ( (
_rtB -> g2uf21tk3d & 65536 ) != 0 ? _rtB -> g2uf21tk3d | - 65536 : _rtB ->
g2uf21tk3d & 65535 ) - ( ( _rtB -> ng104j1bnv & 65536 ) != 0 ? _rtB ->
ng104j1bnv | - 65536 : _rtB -> ng104j1bnv & 65535 ) ; _rtB -> hlw24yjjep = (
outIdx & 65536 ) != 0 ? outIdx | - 65536 : outIdx & 65535 ; _rtB ->
pnfecimhgq = _rtDW -> mahnpzwfuh ; k = 1 ; tmp_f = ( int64_T ) _rtB ->
pnfecimhgq * 32768 ; tmp_f = ( tmp_f & 8589934592LL ) != 0LL ? tmp_f | -
8589934592LL : tmp_f & 8589934591LL ; tmp_f = ( ( tmp_f & 137438953472LL ) !=
0LL ? tmp_f | - 137438953472LL : tmp_f & 137438953471LL ) + _rtDW ->
m2uhskxrr3 [ 0 ] ; _rtB -> gtb0eqgj10 = ( tmp_f & 137438953472LL ) != 0LL ?
tmp_f | - 137438953472LL : tmp_f & 137438953471LL ; _rtB -> eif3ckhnnv = (
int16_T ) ( _rtB -> gtb0eqgj10 >> 15 ) ; _rtB -> fyen0ek4fb = _rtDW ->
ovpg4mr2uq ; tmp_f = ( int64_T ) _rtB -> fyen0ek4fb * ( - 32768 ) ; tmp_f = (
tmp_f & 8589934592LL ) != 0LL ? tmp_f | - 8589934592LL : tmp_f & 8589934591LL
; tmp_f = ( ( tmp_f & 137438953472LL ) != 0LL ? tmp_f | - 137438953472LL :
tmp_f & 137438953471LL ) + _rtDW -> evixspa5r4 [ 0 ] ; _rtB -> gtb0eqgj10 = (
tmp_f & 137438953472LL ) != 0LL ? tmp_f | - 137438953472LL : tmp_f &
137438953471LL ; _rtB -> n0ruzxn5bw = ( int16_T ) ( _rtB -> gtb0eqgj10 >> 15
) ; _rtB -> ng104j1bnv = _rtDW -> lwvw1oujt2 ; _rtB -> g2uf21tk3d = _rtDW ->
cjkwow3fio ; _rtB -> k0ssphgq1x . re = _rtB -> ng104j1bnv ; _rtB ->
k0ssphgq1x . im = _rtB -> g2uf21tk3d ; _rtB -> cr1trr3ihg . re = _rtB ->
k0ssphgq1x . re ; outIdx = - _rtB -> k0ssphgq1x . im ; if ( outIdx > 32767 )
{ outIdx = 32767 ; } _rtB -> cr1trr3ihg . im = ( int16_T ) outIdx ; _rtB ->
ng104j1bnv = _rtB -> k0ssphgq1x . re ; _rtB -> g2uf21tk3d = _rtB ->
k0ssphgq1x . im ; if ( _rtB -> ng104j1bnv < 0 ) { _rtB -> e3if514fnw = (
int16_T ) - _rtB -> ng104j1bnv ; } else { _rtB -> e3if514fnw = _rtB ->
ng104j1bnv ; } if ( _rtB -> g2uf21tk3d < 0 ) { _rtB -> ckwpdsnnuk = ( int16_T
) - _rtB -> g2uf21tk3d ; } else { _rtB -> ckwpdsnnuk = _rtB -> g2uf21tk3d ; }
_rtB -> ng104j1bnv = _rtDW -> kx3v4xvgfx ; _rtB -> g2uf21tk3d = _rtDW ->
j4btv1mlsw ; outIdx = _rtB -> ng104j1bnv << 5 ; outIdx = ( ( outIdx & 2097152
) != 0 ? outIdx | - 2097152 : outIdx & 2097151 ) + ( ( _rtB -> g2uf21tk3d &
2097152 ) != 0 ? _rtB -> g2uf21tk3d | - 2097152 : _rtB -> g2uf21tk3d &
2097151 ) ; _rtB -> cgifl5e3x5 = ( outIdx & 2097152 ) != 0 ? outIdx | -
2097152 : outIdx & 2097151 ; _rtB -> ng104j1bnv = _rtDW -> i1xhj1a4w3 ; _rtB
-> mrbxcp5zg4 = _rtDW -> bhcnk1pdyd ; _rtB -> g2uf21tk3d = _rtDW ->
hpaj1dyoaq ; _rtB -> psgbi0n5zu = _rtDW -> ho1z3ioaql ; _rtB -> dx02oxdbub =
( int16_T ) ( 26214 * _rtB -> psgbi0n5zu >> 11 ) ; if ( _rtB -> g2uf21tk3d <=
_rtB -> ng104j1bnv ) { _rtB -> bmwmlhuljm = _rtB -> g2uf21tk3d ; } else {
_rtB -> bmwmlhuljm = _rtB -> ng104j1bnv ; } if ( _rtB -> g2uf21tk3d >= _rtB
-> ng104j1bnv ) { _rtB -> pfgztrd5lk = _rtB -> g2uf21tk3d ; } else { _rtB ->
pfgztrd5lk = _rtB -> ng104j1bnv ; } tmp_f = ( int64_T ) ( _rtB -> aisymkzzms
. re * _rtB -> kpj5dizbt4 . re ) - _rtB -> aisymkzzms . im * _rtB ->
kpj5dizbt4 . im ; if ( tmp_f > 2147483647LL ) { tmp_f = 2147483647LL ; } else
{ if ( tmp_f < - 2147483648LL ) { tmp_f = - 2147483648LL ; } } tmp_j = (
int64_T ) ( _rtB -> aisymkzzms . re * _rtB -> kpj5dizbt4 . im ) + _rtB ->
aisymkzzms . im * _rtB -> kpj5dizbt4 . re ; if ( tmp_j > 2147483647LL ) {
tmp_j = 2147483647LL ; } else { if ( tmp_j < - 2147483648LL ) { tmp_j = -
2147483648LL ; } } _rtB -> ozv5wj1uic . re = ( int32_T ) tmp_f ; _rtB ->
ozv5wj1uic . im = ( int32_T ) tmp_j ; reSign = 0 ; imSign = 0 ; if ( _rtB ->
fzm4azyw5t . re > 0 ) { reSign = 1 ; } else { if ( _rtB -> fzm4azyw5t . re <
0 ) { reSign = - 1 ; } } if ( _rtB -> fzm4azyw5t . im > 0 ) { imSign = 1 ; }
else { if ( _rtB -> fzm4azyw5t . im < 0 ) { imSign = - 1 ; } } if ( reSign >
0 ) { if ( imSign >= 0 ) { k = 0 ; } else { k = 3 ; } } else if ( reSign < 0
) { if ( imSign <= 0 ) { k = 2 ; } } else if ( imSign < 0 ) { k = 3 ; } else
{ if ( ! ( imSign > 0 ) ) { k = 0 ; } } k ^= k >> 1 ; _rtB -> por40ilbdf [ 1
] = ( k % 2 != 0 ) ; k >>= 1 ; _rtB -> por40ilbdf [ 0 ] = ( k % 2 != 0 ) ;
srUpdateBC ( _rtDW -> cwps1jqc44 ) ; } else { if ( _rtDW -> e40qniej0g ) {
ssCallAccelRunBlock ( S , 7 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW ->
e40qniej0g = false ; } } _rtB -> iykaks3lja = _rtB -> oxlr2stf2n [ 0 ] ; _rtB
-> k2nymlmn0w = _rtB -> oxlr2stf2n [ 1 ] ; _rtB -> hzxylntxdo = ( _rtB ->
obnfdr2i53 && _rtB -> ax0zsrbpeu ) ; _rtB -> e5i2h0nzb1 = _rtB -> hzxylntxdo
; _rtB -> kuoz5wrkfz = ( int16_T ) ( _rtB -> e5i2h0nzb1 << 15 ) ; _rtB ->
e5i2h0nzb1 = ( int16_T ) ( _rtB -> k2nymlmn0w | _rtB -> kuoz5wrkfz ) ; _rtB
-> itg0myk2wa . re = _rtB -> iykaks3lja ; _rtB -> itg0myk2wa . im = _rtB ->
e5i2h0nzb1 ; iIdx = 1 ; kIdx = 8000 - _rtDW -> nhxj0sseye ; k = _rtDW ->
nhxj0sseye ; if ( kIdx <= 1 ) { i = 0 ; while ( i < kIdx ) { _rtDW ->
er5ywavmqi [ _rtDW -> nhxj0sseye ] = _rtB -> itg0myk2wa ; i = 1 ; } k = 0 ;
iIdx = 1 - kIdx ; } for ( i = 0 ; i < iIdx ; i ++ ) { _rtDW -> er5ywavmqi [ k
+ i ] = _rtB -> itg0myk2wa ; } _rtDW -> nhxj0sseye ++ ; if ( _rtDW ->
nhxj0sseye >= 8000 ) { _rtDW -> nhxj0sseye -= 8000 ; } _rtDW -> f03dllvydv ++
; if ( _rtDW -> f03dllvydv > 8000 ) { _rtDW -> ix310kqdnc = ( _rtDW ->
ix310kqdnc + _rtDW -> f03dllvydv ) - 8000 ; if ( _rtDW -> ix310kqdnc > 8000 )
{ _rtDW -> ix310kqdnc -= 8000 ; } _rtDW -> f03dllvydv = 8000 ; } } if (
ssIsSampleHit ( S , 4 , 0 ) ) { _rtDW -> f03dllvydv -= 4000 ; if ( _rtDW ->
f03dllvydv < 0 ) { _rtDW -> ix310kqdnc += _rtDW -> f03dllvydv ; if ( _rtDW ->
ix310kqdnc < 0 ) { _rtDW -> ix310kqdnc += 8000 ; } _rtDW -> f03dllvydv = 0 ;
} k = 0 ; currentOffset = _rtDW -> ix310kqdnc ; if ( _rtDW -> ix310kqdnc < 0
) { currentOffset = _rtDW -> ix310kqdnc + 8000 ; } kIdx = 8000 -
currentOffset ; iIdx = 4000 ; if ( kIdx <= 4000 ) { for ( i = 0 ; i < kIdx ;
i ++ ) { _rtB -> mbkey5nhdq [ i ] = _rtDW -> er5ywavmqi [ currentOffset + i ]
; } k = kIdx ; currentOffset = 0 ; iIdx = 4000 - kIdx ; } for ( i = 0 ; i <
iIdx ; i ++ ) { _rtB -> mbkey5nhdq [ k + i ] = _rtDW -> er5ywavmqi [
currentOffset + i ] ; } _rtDW -> ix310kqdnc = currentOffset + iIdx ; for ( i
= 0 ; i < 4000 ; i ++ ) { _rtB -> npm5nba0fs [ i ] = _rtB -> mbkey5nhdq [ i ]
. re ; _rtB -> dhlwcukbu1 [ i ] = _rtB -> mbkey5nhdq [ i ] . im ; }
ssCallAccelRunBlock ( S , 15 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 13 , 0 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 17 , 0 , SS_CALL_MDL_OUTPUTS ) ; for ( i = 0 ; i <
4000 ; i ++ ) { _rtB -> auzxfg0qc0 [ i ] = ( _rtB -> gf15hfa03w [ i ] != 0 )
; _rtB -> fit5tcfo3y [ i ] = ( _rtB -> k2own1bqf2 [ i ] != 0 ) ; _rtB ->
cchdcmxall [ i ] = ( _rtB -> izcqojkhqn [ i ] != 0 ) ; } ssCallAccelRunBlock
( S , 1 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> adr3ulpmeh = _rtB -> jcmz1ydeck
; if ( _rtB -> adr3ulpmeh > 0.0 ) { if ( ! _rtDW -> oyjntg5v1k ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ; _rtDW ->
oyjntg5v1k = true ; } } else { if ( _rtDW -> oyjntg5v1k ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW ->
oyjntg5v1k = false ; } } } if ( _rtDW -> oyjntg5v1k ) { if ( ssIsSampleHit (
S , 4 , 0 ) ) { memcpy ( & _rtDW -> khrfanygjs [ 0 ] , & _rtB -> arueid0soy [
0 ] , 8000U * sizeof ( boolean_T ) ) ; _rtDW -> grmfmipaiz = 0 ; } if (
ssIsSampleHit ( S , 2 , 0 ) ) { memcpy ( & _rtB -> lwvuz35deg [ 0 ] , & _rtDW
-> khrfanygjs [ _rtDW -> grmfmipaiz ] , 200U * sizeof ( boolean_T ) ) ; if (
_rtDW -> grmfmipaiz < 7800 ) { _rtDW -> grmfmipaiz += 200 ; } for ( kIdx = 0
; kIdx < 174 ; kIdx ++ ) { for ( k = ( ( ( _rtB -> lwvuz35deg [ kIdx + 26 ] -
( uint8_T ) ( ( ( uint8_T ) 1U ) * _rtDW -> fsypd3acuw [ 0 ] ) ) - ( uint8_T
) ( ( ( uint8_T ) 1U ) * _rtDW -> fsypd3acuw [ 1 ] ) ) - ( uint8_T ) ( ( (
uint8_T ) 0U ) * _rtDW -> fsypd3acuw [ 2 ] ) ) - ( uint8_T ) ( ( ( uint8_T )
1U ) * _rtDW -> fsypd3acuw [ 3 ] ) ; k < 0 ; k += 2 ) { } k %= 2 ; _rtB ->
c1q3jj5jrc [ kIdx ] = ( k != 0 ) ; _rtDW -> fsypd3acuw [ 3 ] = _rtDW ->
fsypd3acuw [ 2 ] ; _rtDW -> fsypd3acuw [ 2 ] = _rtDW -> fsypd3acuw [ 1 ] ;
_rtDW -> fsypd3acuw [ 1 ] = _rtDW -> fsypd3acuw [ 0 ] ; _rtDW -> fsypd3acuw [
0U ] = _rtB -> lwvuz35deg [ kIdx + 26 ] ; } ssCallAccelRunBlock ( S , 2 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; } srUpdateBC ( _rtDW -> kxizblrgd3 ) ; } if (
ssIsSampleHit ( S , 3 , 0 ) ) { for ( i = 0 ; i < 4000 ; i ++ ) { tmp_k =
muDoubleScalarFloor ( _rtB -> obkxybx5mt [ i ] . re * 32768.0 ) ; if (
muDoubleScalarIsNaN ( tmp_k ) || muDoubleScalarIsInf ( tmp_k ) ) { tmp_k =
0.0 ; } else { tmp_k = muDoubleScalarRem ( tmp_k , 65536.0 ) ; } tmp_c =
muDoubleScalarFloor ( _rtB -> obkxybx5mt [ i ] . im * 32768.0 ) ; if (
muDoubleScalarIsNaN ( tmp_c ) || muDoubleScalarIsInf ( tmp_c ) ) { tmp_c =
0.0 ; } else { tmp_c = muDoubleScalarRem ( tmp_c , 65536.0 ) ; } _rtB ->
h2prg42hre [ i ] . re = ( int16_T ) ( tmp_k < 0.0 ? ( int32_T ) ( int16_T ) -
( int16_T ) ( uint16_T ) - tmp_k : ( int32_T ) ( int16_T ) ( uint16_T ) tmp_k
) ; _rtB -> h2prg42hre [ i ] . im = ( int16_T ) ( tmp_c < 0.0 ? ( int32_T ) (
int16_T ) - ( int16_T ) ( uint16_T ) - tmp_c : ( int32_T ) ( int16_T ) (
uint16_T ) tmp_c ) ; } } _rtB -> bmj0fm255r = _rtDW -> ad2oe2x4ij ; if ( _rtB
-> bmj0fm255r > 409600 ) { _rtB -> bmj0fm255r = 409600 ; } else { if ( _rtB
-> bmj0fm255r < 0 ) { _rtB -> bmj0fm255r = 0 ; } } outIdx = _rtB ->
bmj0fm255r >> 6 ; if ( outIdx > 32767 ) { outIdx = 32767 ; } else { if (
outIdx < - 32768 ) { outIdx = - 32768 ; } } _rtB -> atpyggke1j = ( int16_T )
outIdx ; _rtB -> hpxpcxdtcp = _rtDW -> n3dnbcttno [ 0 ] ; _rtB -> atrcpenqfu
= _rtDW -> iucxrvfokz ; _rtB -> hd0v215ugx = _rtDW -> e0v0dl2owv ; _rtB ->
owd2auqy4e = _rtDW -> ps1gy5o5dm ; _rtB -> b44vh5ha13 = 102 ; _rtB ->
mlvsqidcjr = _rtB -> owd2auqy4e . re ; _rtB -> oatajrzybm = _rtB ->
owd2auqy4e . im ; if ( _rtB -> mlvsqidcjr < 0 ) { _rtB -> mixlfqdhax = (
int16_T ) - _rtB -> mlvsqidcjr ; } else { _rtB -> mixlfqdhax = _rtB ->
mlvsqidcjr ; } if ( _rtB -> oatajrzybm < 0 ) { _rtB -> n0zbgjwizw = ( int16_T
) - _rtB -> oatajrzybm ; } else { _rtB -> n0zbgjwizw = _rtB -> oatajrzybm ; }
_rtB -> oatajrzybm = _rtDW -> lkt5zsxrya ; _rtB -> mlvsqidcjr = _rtDW ->
eebc11a3iu ; outIdx = ( ( _rtB -> oatajrzybm & 65536 ) != 0 ? _rtB ->
oatajrzybm | - 65536 : _rtB -> oatajrzybm & 65535 ) + ( ( _rtB -> mlvsqidcjr
& 65536 ) != 0 ? _rtB -> mlvsqidcjr | - 65536 : _rtB -> mlvsqidcjr & 65535 )
; _rtB -> fdhjbzm4ct = ( outIdx & 65536 ) != 0 ? outIdx | - 65536 : outIdx &
65535 ; _rtB -> oatajrzybm = _rtDW -> l4mshu1tqq ; _rtB -> bqzcqbqfa1 = (
int16_T ) ( 26214 * _rtB -> oatajrzybm >> 16 ) ; _rtB -> oatajrzybm = _rtDW
-> m4n4w0ubg5 ; _rtB -> mlvsqidcjr = _rtDW -> c3vmxtlk1v ; if ( _rtB ->
oatajrzybm <= _rtB -> mlvsqidcjr ) { _rtB -> hajuj54fid = _rtB -> oatajrzybm
; } else { _rtB -> hajuj54fid = _rtB -> mlvsqidcjr ; } if ( _rtB ->
oatajrzybm >= _rtB -> mlvsqidcjr ) { _rtB -> ezn5q43x12 = _rtB -> oatajrzybm
; } else { _rtB -> ezn5q43x12 = _rtB -> mlvsqidcjr ; } _rtB -> hr5bw4hfhw =
_rtDW -> e0n51yk45m ; _rtB -> f3v1ijqvet = _rtDW -> bbt5qjvrgn ; outIdx =
_rtB -> b44vh5ha13 * _rtB -> atrcpenqfu ; _rtB -> oatajrzybm = ( int16_T ) (
( ( outIdx < 0 ? 1023 : 0 ) + outIdx ) >> 10 ) ; tmp_f = ( int64_T ) _rtB ->
atpyggke1j * _rtB -> f3v1ijqvet ; _rtB -> mlvsqidcjr = ( int16_T ) ( ( (
tmp_f < 0LL ? 511LL : 0LL ) + tmp_f ) >> 9 ) ; tmp_f = ( int64_T ) _rtB ->
bmj0fm255r * _rtB -> hpxpcxdtcp . re ; tmp_f = ( ( tmp_f < 0LL ? 2047LL : 0LL
) + tmp_f ) >> 11 ; if ( tmp_f > 32767LL ) { tmp_f = 32767LL ; } else { if (
tmp_f < - 32768LL ) { tmp_f = - 32768LL ; } } _rtB -> e5wvak3221 . re = (
int16_T ) tmp_f ; tmp_f = ( int64_T ) _rtB -> bmj0fm255r * _rtB -> hpxpcxdtcp
. im ; tmp_f = ( ( tmp_f < 0LL ? 2047LL : 0LL ) + tmp_f ) >> 11 ; if ( tmp_f
> 32767LL ) { tmp_f = 32767LL ; } else { if ( tmp_f < - 32768LL ) { tmp_f = -
32768LL ; } } _rtB -> e5wvak3221 . im = ( int16_T ) tmp_f ; _rtB ->
b44vh5ha13 = 512 ; outIdx = ( ( _rtB -> oatajrzybm & 8388608 ) != 0 ? _rtB ->
oatajrzybm | - 8388608 : _rtB -> oatajrzybm & 8388607 ) + ( ( _rtB ->
bmj0fm255r & 8388608 ) != 0 ? _rtB -> bmj0fm255r | - 8388608 : _rtB ->
bmj0fm255r & 8388607 ) ; outIdx = ( outIdx & 8388608 ) != 0 ? outIdx | -
8388608 : outIdx & 8388607 ; if ( outIdx > 4194303 ) { outIdx = 4194303 ; }
else { if ( outIdx < - 4194304 ) { outIdx = - 4194304 ; } } _rtB ->
gbatb3j4p3 = outIdx ; outIdx = ( ( _rtB -> b44vh5ha13 & 65536 ) != 0 ? _rtB
-> b44vh5ha13 | - 65536 : _rtB -> b44vh5ha13 & 65535 ) - ( ( _rtB ->
mlvsqidcjr & 65536 ) != 0 ? _rtB -> mlvsqidcjr | - 65536 : _rtB -> mlvsqidcjr
& 65535 ) ; _rtB -> f0m2xj1ngt = ( int16_T ) ( ( outIdx & 65536 ) != 0 ?
outIdx | - 65536 : outIdx & 65535 ) ; if ( ssIsSampleHit ( S , 1 , 0 ) ) {
_rtB -> k3dveqppjd = true ; _rtB -> nrs2t3bowr = kqdbxwmxxp . kol3sk4xzk ;
_rtB -> pqdb4tldvq = _rtDW -> nzei4visd3 [ 0 ] ; _rtB -> bysguabmws . re = (
int16_T ) ( _rtB -> pqdb4tldvq . re >> 8 ) ; _rtB -> bysguabmws . im = (
int16_T ) ( _rtB -> pqdb4tldvq . im >> 8 ) ; _rtB -> pqdb4tldvq = _rtDW ->
dwhbvc4zyv [ 0 ] ; outIdx = _rtB -> pqdb4tldvq . re >> 15 ; if ( outIdx >
32767 ) { outIdx = 32767 ; } else { if ( outIdx < - 32768 ) { outIdx = -
32768 ; } } currentOffset = _rtB -> pqdb4tldvq . im >> 15 ; if (
currentOffset > 32767 ) { currentOffset = 32767 ; } else { if ( currentOffset
< - 32768 ) { currentOffset = - 32768 ; } } _rtB -> bbbvyzsw3d . re = (
int16_T ) outIdx ; _rtB -> bbbvyzsw3d . im = ( int16_T ) currentOffset ; _rtB
-> pqdb4tldvq = _rtDW -> l05dqxcm3k [ 0 ] ; _rtB -> m5u2kus1zb = _rtDW ->
mfxh5amrmh ; _rtB -> kuoz5wrkfz = ( - 8192 ) ; _rtB -> lauf2rx1be = _rtDW ->
lkusdfylul ; _rtB -> kgyw32msh1 = _rtDW -> emogjyqg5j ; _rtB -> oefgwrftj0 =
_rtB -> kgyw32msh1 ; _rtB -> hzxylntxdo = ( _rtB -> oefgwrftj0 == iri5mili5t
( S ) -> ojw5xnzrzp ) ; _rtB -> jslxjvmliz = _rtB -> hzxylntxdo ; if ( _rtB
-> jslxjvmliz ) { _rtB -> e1iuhxt5fg . re = ( int16_T ) ( _rtB -> lauf2rx1be
. re >> 7 ) ; _rtB -> e1iuhxt5fg . im = ( int16_T ) ( _rtB -> lauf2rx1be . im
>> 7 ) ; srUpdateBC ( _rtDW -> bede2ximo0 ) ; } _rtB -> edigpp4t1l = _rtB ->
e1iuhxt5fg ; _rtB -> i00htlcezd = true ; ssCallAccelRunBlock ( S , 5 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> fjg5kw2vi4 = ( int16_T ) ( ( int64_T ) _rtB
-> kuoz5wrkfz * _rtB -> otlx0jpgkn >> 18 ) ; ssCallAccelRunBlock ( S , 4 , 0
, SS_CALL_MDL_OUTPUTS ) ; tmp_f = ( int64_T ) ( _rtB -> bbbvyzsw3d . re *
_rtB -> bbbvyzsw3d . re ) - _rtB -> bbbvyzsw3d . im * _rtB -> bbbvyzsw3d . im
; if ( tmp_f > 2147483647LL ) { tmp_f = 2147483647LL ; } else { if ( tmp_f <
- 2147483648LL ) { tmp_f = - 2147483648LL ; } } tmp_j = ( int64_T ) ( _rtB ->
bbbvyzsw3d . re * _rtB -> bbbvyzsw3d . im ) + _rtB -> bbbvyzsw3d . im * _rtB
-> bbbvyzsw3d . re ; if ( tmp_j > 2147483647LL ) { tmp_j = 2147483647LL ; }
else { if ( tmp_j < - 2147483648LL ) { tmp_j = - 2147483648LL ; } } _rtB ->
mirzucihdg . re = ( int32_T ) tmp_f ; _rtB -> mirzucihdg . im = ( int32_T )
tmp_j ; if ( _rtB -> cbzuifdwyb ) { _rtB -> nrs2t3bowr = _rtB -> cbjkaolvwh ;
} outIdx = _rtB -> m5u2kus1zb . re * _rtB -> nrs2t3bowr . re ; currentOffset
= _rtB -> m5u2kus1zb . im * _rtB -> nrs2t3bowr . im ; i = ( ( outIdx &
33554432 ) != 0 ? outIdx | - 33554432 : outIdx & 33554431 ) - ( (
currentOffset & 33554432 ) != 0 ? currentOffset | - 33554432 : currentOffset
& 33554431 ) ; if ( i > 33554431 ) { i = 33554431 ; } else { if ( i < -
33554432 ) { i = - 33554432 ; } } outIdx = _rtB -> m5u2kus1zb . re * _rtB ->
nrs2t3bowr . im ; currentOffset = _rtB -> m5u2kus1zb . im * _rtB ->
nrs2t3bowr . re ; kIdx = ( ( outIdx & 33554432 ) != 0 ? outIdx | - 33554432 :
outIdx & 33554431 ) + ( ( currentOffset & 33554432 ) != 0 ? currentOffset | -
33554432 : currentOffset & 33554431 ) ; if ( kIdx > 33554431 ) { kIdx =
33554431 ; } else { if ( kIdx < - 33554432 ) { kIdx = - 33554432 ; } } _rtB
-> fo2nolxzuc . re = i ; _rtB -> fo2nolxzuc . im = kIdx ; tmp_f = ( int64_T )
( _rtB -> m5u2kus1zb . re * _rtB -> m5u2kus1zb . re ) - _rtB -> m5u2kus1zb .
im * _rtB -> m5u2kus1zb . im ; if ( tmp_f > 2147483647LL ) { tmp_f =
2147483647LL ; } else { if ( tmp_f < - 2147483648LL ) { tmp_f = -
2147483648LL ; } } tmp_j = ( int64_T ) ( _rtB -> m5u2kus1zb . re * _rtB ->
m5u2kus1zb . im ) + _rtB -> m5u2kus1zb . im * _rtB -> m5u2kus1zb . re ; if (
tmp_j > 2147483647LL ) { tmp_j = 2147483647LL ; } else { if ( tmp_j < -
2147483648LL ) { tmp_j = - 2147483648LL ; } } _rtB -> ppb5okg5q5 . re = (
int32_T ) tmp_f ; _rtB -> ppb5okg5q5 . im = ( int32_T ) tmp_j ; _rtB ->
m5u2kus1zb = kqdbxwmxxp . kol3sk4xzk ; if ( _rtB -> hzxylntxdo == 0 ) { _rtB
-> a51liwe2ua = _rtB -> lauf2rx1be ; } else { tmp_f = ( int64_T ) _rtB ->
m5u2kus1zb . re << 6 ; tmp_j = ( int64_T ) _rtB -> m5u2kus1zb . im << 6 ;
_rtB -> a51liwe2ua . re = ( tmp_f & 4294967296LL ) != 0LL ? tmp_f | -
4294967296LL : tmp_f & 4294967295LL ; _rtB -> a51liwe2ua . im = ( tmp_j &
4294967296LL ) != 0LL ? tmp_j | - 4294967296LL : tmp_j & 4294967295LL ; }
_rtB -> ncqo5vvas3 = _rtDW -> ej45hqzclg [ 0 ] ; tmp_f = ( int64_T ) _rtB ->
ncqo5vvas3 . re << 1 ; tmp_j = ( int64_T ) _rtB -> ncqo5vvas3 . im << 1 ;
tmp_f = ( ( _rtB -> a51liwe2ua . re & 8589934592LL ) != 0LL ? _rtB ->
a51liwe2ua . re | - 8589934592LL : _rtB -> a51liwe2ua . re & 8589934591LL ) +
( ( tmp_f & 8589934592LL ) != 0LL ? tmp_f | - 8589934592LL : tmp_f &
8589934591LL ) ; tmp_f = ( tmp_f & 8589934592LL ) != 0LL ? tmp_f | -
8589934592LL : tmp_f & 8589934591LL ; tmp_j = ( ( _rtB -> a51liwe2ua . im &
8589934592LL ) != 0LL ? _rtB -> a51liwe2ua . im | - 8589934592LL : _rtB ->
a51liwe2ua . im & 8589934591LL ) + ( ( tmp_j & 8589934592LL ) != 0LL ? tmp_j
| - 8589934592LL : tmp_j & 8589934591LL ) ; tmp_j = ( tmp_j & 8589934592LL )
!= 0LL ? tmp_j | - 8589934592LL : tmp_j & 8589934591LL ; _rtB -> b10e0vrvkh .
re = ( tmp_f & 4294967296LL ) != 0LL ? tmp_f | - 4294967296LL : tmp_f &
4294967295LL ; _rtB -> b10e0vrvkh . im = ( tmp_j & 4294967296LL ) != 0LL ?
tmp_j | - 4294967296LL : tmp_j & 4294967295LL ; _rtB -> l1g4v2fo0a = _rtDW ->
miumd5j2rh ; _rtB -> jc1ky115dg = _rtDW -> a1ytrarwt0 ; _rtB -> ncqo5vvas3 =
_rtDW -> ge44h0c4z0 ; tmp_f = _rtB -> l1g4v2fo0a . re ; tmp_j = _rtB ->
l1g4v2fo0a . im ; tmp_g = _rtB -> jc1ky115dg . re ; tmp_m = _rtB ->
jc1ky115dg . im ; tmp_i = _rtB -> ncqo5vvas3 . re ; tmp_e = _rtB ->
ncqo5vvas3 . im ; tmp_f = ( ( tmp_f & 8589934592LL ) != 0LL ? tmp_f | -
8589934592LL : tmp_f & 8589934591LL ) + ( ( tmp_g & 8589934592LL ) != 0LL ?
tmp_g | - 8589934592LL : tmp_g & 8589934591LL ) ; tmp_f = ( ( tmp_f &
8589934592LL ) != 0LL ? tmp_f | - 8589934592LL : tmp_f & 8589934591LL ) + ( (
tmp_i & 8589934592LL ) != 0LL ? tmp_i | - 8589934592LL : tmp_i & 8589934591LL
) ; tmp_f = ( ( tmp_f & 8589934592LL ) != 0LL ? tmp_f | - 8589934592LL :
tmp_f & 8589934591LL ) >> 6 ; tmp_j = ( ( tmp_j & 8589934592LL ) != 0LL ?
tmp_j | - 8589934592LL : tmp_j & 8589934591LL ) + ( ( tmp_m & 8589934592LL )
!= 0LL ? tmp_m | - 8589934592LL : tmp_m & 8589934591LL ) ; tmp_j = ( ( tmp_j
& 8589934592LL ) != 0LL ? tmp_j | - 8589934592LL : tmp_j & 8589934591LL ) + (
( tmp_e & 8589934592LL ) != 0LL ? tmp_e | - 8589934592LL : tmp_e &
8589934591LL ) ; tmp_j = ( ( tmp_j & 8589934592LL ) != 0LL ? tmp_j | -
8589934592LL : tmp_j & 8589934591LL ) >> 6 ; _rtB -> kgkfzbiuc0 . re = (
tmp_f & 4294967296LL ) != 0LL ? tmp_f | - 4294967296LL : tmp_f & 4294967295LL
; _rtB -> kgkfzbiuc0 . im = ( tmp_j & 4294967296LL ) != 0LL ? tmp_j | -
4294967296LL : tmp_j & 4294967295LL ; _rtB -> a51liwe2ua = _rtDW ->
fxfohbwy34 ; tmp_f = _rtB -> a51liwe2ua . re >> 9 ; tmp_j = _rtB ->
a51liwe2ua . im >> 9 ; _rtB -> ieiheqzti2 . re = ( tmp_f & 4294967296LL ) !=
0LL ? tmp_f | - 4294967296LL : tmp_f & 4294967295LL ; _rtB -> ieiheqzti2 . im
= ( tmp_j & 4294967296LL ) != 0LL ? tmp_j | - 4294967296LL : tmp_j &
4294967295LL ; tmp_p = ( int16_T ) ( _rtB -> ieiheqzti2 . re >> 10 ) ; tmp =
( int16_T ) ( _rtB -> ieiheqzti2 . im >> 10 ) ; _rtB -> m5u2kus1zb . re = (
int16_T ) ( ( int16_T ) ( tmp_p & 1024 ) != 0 ? ( int32_T ) ( int16_T ) (
tmp_p | - 1024 ) : ( int32_T ) ( int16_T ) ( tmp_p & 1023 ) ) ; _rtB ->
m5u2kus1zb . im = ( int16_T ) ( ( int16_T ) ( tmp & 1024 ) != 0 ? ( int32_T )
( int16_T ) ( tmp | - 1024 ) : ( int32_T ) ( int16_T ) ( tmp & 1023 ) ) ;
outIdx = _rtB -> pqdb4tldvq . re >> 17 ; if ( outIdx > 8191 ) { outIdx = 8191
; } else { if ( outIdx < - 8192 ) { outIdx = - 8192 ; } } currentOffset =
_rtB -> pqdb4tldvq . im >> 17 ; if ( currentOffset > 8191 ) { currentOffset =
8191 ; } else { if ( currentOffset < - 8192 ) { currentOffset = - 8192 ; } }
_rtB -> buwdx50dwi . re = ( int16_T ) outIdx ; _rtB -> buwdx50dwi . im = (
int16_T ) currentOffset ; _rtB -> bbbvyzsw3d = _rtDW -> hxx3d0m5re ; _rtB ->
oefgwrftj0 = ( uint16_T ) ( ( int32_T ) ( ( uint32_T ) ( iri5mili5t ( S ) ->
dasricjf2b & 1023 ) + ( _rtB -> kgyw32msh1 & 1023 ) ) & 1023 ) ; tmp_p = (
int16_T ) _rtB -> oefgwrftj0 ; tmp = ( int16_T ) iri5mili5t ( S ) ->
luzvag423y ; tmp_p = ( int16_T ) ( ( ( int16_T ) ( tmp_p & 2048 ) != 0 ? (
int32_T ) ( int16_T ) ( tmp_p | - 2048 ) : ( int32_T ) ( int16_T ) ( tmp_p &
2047 ) ) - ( ( int16_T ) ( tmp & 2048 ) != 0 ? ( int32_T ) ( int16_T ) ( tmp
| - 2048 ) : ( int32_T ) ( int16_T ) ( tmp & 2047 ) ) ) ; _rtB -> kuoz5wrkfz
= ( int16_T ) ( ( int16_T ) ( tmp_p & 2048 ) != 0 ? ( int32_T ) ( int16_T ) (
tmp_p | - 2048 ) : ( int32_T ) ( int16_T ) ( tmp_p & 2047 ) ) ; if ( _rtB ->
oefgwrftj0 > ( ( uint16_T ) 511U ) ) { _rtB -> ghmizdgclj = ( uint16_T ) ( (
uint16_T ) _rtB -> kuoz5wrkfz & 511 ) ; } else { _rtB -> ghmizdgclj = (
uint16_T ) ( _rtB -> oefgwrftj0 & 511 ) ; } _rtB -> hzxylntxdo = ( _rtB ->
kgyw32msh1 == iri5mili5t ( S ) -> ch1npdvsii ) ; tmp_p = ( int16_T ) _rtB ->
kgyw32msh1 ; tmp = ( int16_T ) iri5mili5t ( S ) -> dasricjf2b ; tmp_p = (
int16_T ) ( ( ( int16_T ) ( tmp_p & 512 ) != 0 ? ( int32_T ) ( int16_T ) (
tmp_p | - 512 ) : ( int32_T ) ( int16_T ) ( tmp_p & 511 ) ) - ( ( int16_T ) (
tmp & 512 ) != 0 ? ( int32_T ) ( int16_T ) ( tmp | - 512 ) : ( int32_T ) (
int16_T ) ( tmp & 511 ) ) ) ; _rtB -> kuoz5wrkfz = ( int16_T ) ( ( int16_T )
( tmp_p & 512 ) != 0 ? ( int32_T ) ( int16_T ) ( tmp_p | - 512 ) : ( int32_T
) ( int16_T ) ( tmp_p & 511 ) ) ; tmp_p = ( int16_T ) iri5mili5t ( S ) ->
auo25ji4uq ; tmp_p = ( int16_T ) ( ( ( int16_T ) ( _rtB -> kuoz5wrkfz & 2048
) != 0 ? ( int32_T ) ( int16_T ) ( _rtB -> kuoz5wrkfz | - 2048 ) : ( int32_T
) ( int16_T ) ( _rtB -> kuoz5wrkfz & 2047 ) ) + ( ( int16_T ) ( tmp_p & 2048
) != 0 ? ( int32_T ) ( int16_T ) ( tmp_p | - 2048 ) : ( int32_T ) ( int16_T )
( tmp_p & 2047 ) ) ) ; _rtB -> e5i2h0nzb1 = ( int16_T ) ( ( int16_T ) ( tmp_p
& 2048 ) != 0 ? ( int32_T ) ( int16_T ) ( tmp_p | - 2048 ) : ( int32_T ) (
int16_T ) ( tmp_p & 2047 ) ) ; if ( _rtB -> kuoz5wrkfz >= 0 ) { _rtB ->
oefgwrftj0 = ( uint16_T ) ( ( uint16_T ) _rtB -> kuoz5wrkfz & 511 ) ; } else
{ _rtB -> oefgwrftj0 = ( uint16_T ) ( ( uint16_T ) _rtB -> e5i2h0nzb1 & 511 )
; } if ( ! iri5mili5t ( S ) -> oddfoi5s5z ) { _rtB -> oefgwrftj0 = _rtB ->
ghmizdgclj ; } if ( _rtB -> hzxylntxdo ) { _rtB -> ghmizdgclj = iri5mili5t (
S ) -> nxk1byo5xr ; } else { _rtB -> ghmizdgclj = _rtB -> oefgwrftj0 ; } if (
! iri5mili5t ( S ) -> b0shztvb23 ) { _rtB -> oefgwrftj0 = _rtB -> ghmizdgclj
; } if ( ! iri5mili5t ( S ) -> hrkj3i2gtk ) { _rtB -> oefgwrftj0 = _rtB ->
kgyw32msh1 ; } if ( iri5mili5t ( S ) -> gf54aa0ff4 ) { _rtB -> oefgwrftj0 =
iri5mili5t ( S ) -> fufu3agotw ; } if ( iri5mili5t ( S ) -> m1heelvk3v ) {
_rtB -> n4jdfanglr = iri5mili5t ( S ) -> gz0maqkjnv ; } else { _rtB ->
n4jdfanglr = _rtB -> oefgwrftj0 ; } _rtB -> d01lpmlce1 . re = _rtB ->
pqdb4tldvq . re ; if ( _rtB -> pqdb4tldvq . im <= MIN_int32_T ) { kIdx =
MAX_int32_T ; } else { kIdx = - _rtB -> pqdb4tldvq . im ; } _rtB ->
d01lpmlce1 . im = kIdx ; outIdx = _rtB -> m5u2kus1zb . re * _rtB ->
bbbvyzsw3d . re ; currentOffset = _rtB -> m5u2kus1zb . im * _rtB ->
bbbvyzsw3d . im ; i = ( ( outIdx & 16777216 ) != 0 ? outIdx | - 16777216 :
outIdx & 16777215 ) - ( ( currentOffset & 16777216 ) != 0 ? currentOffset | -
16777216 : currentOffset & 16777215 ) ; if ( i > 16777215 ) { i = 16777215 ;
} else { if ( i < - 16777216 ) { i = - 16777216 ; } } outIdx = _rtB ->
m5u2kus1zb . re * _rtB -> bbbvyzsw3d . im ; currentOffset = _rtB ->
m5u2kus1zb . im * _rtB -> bbbvyzsw3d . re ; kIdx = ( ( outIdx & 16777216 ) !=
0 ? outIdx | - 16777216 : outIdx & 16777215 ) + ( ( currentOffset & 16777216
) != 0 ? currentOffset | - 16777216 : currentOffset & 16777215 ) ; if ( kIdx
> 16777215 ) { kIdx = 16777215 ; } else { if ( kIdx < - 16777216 ) { kIdx = -
16777216 ; } } _rtB -> ete0gkwe0f . re = i ; _rtB -> ete0gkwe0f . im = kIdx ;
_rtB -> bakalnzyaw = _rtDW -> daxjg4l44p ; _rtB -> m5u2kus1zb = _rtDW ->
akfkgnwuwe [ 0 ] ; _rtB -> kuoz5wrkfz = _rtDW -> ah3gbqvttx ; _rtB ->
iculjrpa2d = 28594 * _rtB -> kuoz5wrkfz ; _rtB -> dw5k0n4dk5 = _rtB ->
iculjrpa2d >> 4 ; _rtB -> iculjrpa2d = _rtDW -> omegeptmak ; _rtB ->
pw2kuoh4ii = _rtDW -> pakeiyzsck ; _rtB -> bre3r0rst0 = 16892 * _rtB ->
kuoz5wrkfz ; tmp_f = ( int64_T ) _rtB -> pw2kuoh4ii << 11 ; tmp_j = _rtB ->
bre3r0rst0 ; tmp_f = ( ( tmp_f & 8796093022208LL ) != 0LL ? tmp_f | -
8796093022208LL : tmp_f & 8796093022207LL ) + ( ( tmp_j & 8796093022208LL )
!= 0LL ? tmp_j | - 8796093022208LL : tmp_j & 8796093022207LL ) ; _rtB ->
nmz4xr45v0 = ( int32_T ) ( ( ( tmp_f & 8796093022208LL ) != 0LL ? tmp_f | -
8796093022208LL : tmp_f & 8796093022207LL ) >> 11 ) ; tmp_f = _rtB ->
dw5k0n4dk5 ; tmp_j = _rtB -> nmz4xr45v0 ; tmp_f = ( ( tmp_f & 4294967296LL )
!= 0LL ? tmp_f | - 4294967296LL : tmp_f & 4294967295LL ) + ( ( tmp_j &
4294967296LL ) != 0LL ? tmp_j | - 4294967296LL : tmp_j & 4294967295LL ) ;
tmp_f = ( ( tmp_f & 4294967296LL ) != 0LL ? tmp_f | - 4294967296LL : tmp_f &
4294967295LL ) >> 10 ; if ( tmp_f > 2097151LL ) { tmp_f = 2097151LL ; } else
{ if ( tmp_f < - 2097152LL ) { tmp_f = - 2097152LL ; } } _rtB -> lif30c2zfa =
( int32_T ) tmp_f ; ssCallAccelRunBlock ( S , 9 , 0 , SS_CALL_MDL_OUTPUTS ) ;
_rtB -> kuoz5wrkfz = ( - 20861 ) ; _rtB -> awhzadqcrm = true ; _rtB ->
bbbvyzsw3d = kqdbxwmxxp . kol3sk4xzk ; _rtB -> nrs2t3bowr = _rtDW ->
gbi2dxkr31 ; _rtB -> dagwtnptfc = ( int16_T ) ( ( int64_T ) _rtB ->
iculjrpa2d * _rtB -> kuoz5wrkfz >> 19 ) ; ssCallAccelRunBlock ( S , 10 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; if ( _rtB -> mjhbdlc4h0 ) { _rtB -> kqygxacvla = _rtB
-> epjcz31024 ; } else { _rtB -> kqygxacvla = _rtB -> bbbvyzsw3d ; } outIdx =
_rtB -> m5u2kus1zb . re * _rtB -> nrs2t3bowr . re ; outIdx = ( ( outIdx < 0 ?
255 : 0 ) + outIdx ) >> 8 ; if ( outIdx > 32767 ) { outIdx = 32767 ; } else {
if ( outIdx < - 32768 ) { outIdx = - 32768 ; } } currentOffset = _rtB ->
m5u2kus1zb . im * _rtB -> nrs2t3bowr . im ; currentOffset = ( ( currentOffset
< 0 ? 255 : 0 ) + currentOffset ) >> 8 ; if ( currentOffset > 32767 ) {
currentOffset = 32767 ; } else { if ( currentOffset < - 32768 ) {
currentOffset = - 32768 ; } } outIdx -= currentOffset ; if ( outIdx > 32767 )
{ outIdx = 32767 ; } else { if ( outIdx < - 32768 ) { outIdx = - 32768 ; } }
currentOffset = _rtB -> m5u2kus1zb . re * _rtB -> nrs2t3bowr . im ;
currentOffset = ( ( currentOffset < 0 ? 255 : 0 ) + currentOffset ) >> 8 ; if
( currentOffset > 32767 ) { currentOffset = 32767 ; } else { if (
currentOffset < - 32768 ) { currentOffset = - 32768 ; } } kIdx = _rtB ->
m5u2kus1zb . im * _rtB -> nrs2t3bowr . re ; kIdx = ( ( kIdx < 0 ? 255 : 0 ) +
kIdx ) >> 8 ; if ( kIdx > 32767 ) { kIdx = 32767 ; } else { if ( kIdx < -
32768 ) { kIdx = - 32768 ; } } currentOffset += kIdx ; if ( currentOffset >
32767 ) { currentOffset = 32767 ; } else { if ( currentOffset < - 32768 ) {
currentOffset = - 32768 ; } } _rtB -> hl43yzi3bf . re = ( int16_T ) outIdx ;
_rtB -> hl43yzi3bf . im = ( int16_T ) currentOffset ; } outIdx = _rtDW ->
llzzb1nyuw ; i = _rtDW -> lampfbwg5i ; kIdx = _rtDW -> ktqdm4qoh2 ; _rtDW ->
nslsfzz2zu [ _rtDW -> ktqdm4qoh2 ] = _rtB -> hd0v215ugx ; for ( iIdx = 0 ;
iIdx < 21 ; iIdx ++ ) { _rtDW -> pdn2lc4yxb . re += ( int16_T ) ( _rtDW ->
nslsfzz2zu [ kIdx ] . re * kqdbxwmxxp . gvrussalxc [ i ] >> 15 ) ; _rtDW ->
pdn2lc4yxb . im += ( int16_T ) ( _rtDW -> nslsfzz2zu [ kIdx ] . im *
kqdbxwmxxp . gvrussalxc [ i ] >> 15 ) ; i ++ ; kIdx -= 2 ; if ( kIdx < 0 ) {
kIdx += 42 ; } } kIdx = _rtDW -> ktqdm4qoh2 + 1 ; if ( kIdx >= 42 ) { kIdx =
0 ; } currentOffset = _rtDW -> gjcoppf52o + 1 ; if ( currentOffset >= 2 ) {
_rtDW -> chexynbgdn = _rtDW -> pdn2lc4yxb ; _rtDW -> pdn2lc4yxb . re = 0 ;
_rtDW -> pdn2lc4yxb . im = 0 ; outIdx = _rtDW -> llzzb1nyuw + 1 ; if ( outIdx
>= 1 ) { outIdx = 0 ; } currentOffset = 0 ; i = 0 ; } _rtDW -> lampfbwg5i = i
; _rtDW -> gjcoppf52o = currentOffset ; _rtDW -> ktqdm4qoh2 = kIdx ; _rtDW ->
llzzb1nyuw = outIdx ; if ( ssIsSpecialSampleHit ( S , 1 , 0 , 0 ) ) { _rtB ->
n01bk0pygv = _rtDW -> chexynbgdn ; } if ( ssIsSampleHit ( S , 1 , 0 ) ) {
_rtB -> bwjlsfaasx = _rtDW -> liq2v41hed [ 0 ] ; _rtB -> lw3nl3crl1 = _rtDW
-> b4k2uxvbov ; _rtB -> bqiuv1fqqv = _rtDW -> e3i3twvaaj ; _rtB -> nf2uubuhjx
= _rtDW -> nvrxlyi3lt ; tmp_f = ( ( _rtB -> bqiuv1fqqv & 1099511627776LL ) !=
0LL ? _rtB -> bqiuv1fqqv | - 1099511627776LL : _rtB -> bqiuv1fqqv &
1099511627775LL ) + ( ( _rtB -> nf2uubuhjx & 1099511627776LL ) != 0LL ? _rtB
-> nf2uubuhjx | - 1099511627776LL : _rtB -> nf2uubuhjx & 1099511627775LL ) ;
_rtB -> ekjxufukx0 = ( tmp_f & 1099511627776LL ) != 0LL ? tmp_f | -
1099511627776LL : tmp_f & 1099511627775LL ; tmp_p = ( int16_T ) ( _rtB ->
ekjxufukx0 >> 23 ) ; _rtB -> md4f1xwmdg = ( int16_T ) ( ( int16_T ) ( tmp_p &
1024 ) != 0 ? ( int32_T ) ( int16_T ) ( tmp_p | - 1024 ) : ( int32_T ) (
int16_T ) ( tmp_p & 1023 ) ) ; ssCallAccelRunBlock ( S , 11 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> m5u2kus1zb = _rtDW -> i24inhgtzr ; _rtB ->
bbbvyzsw3d = _rtDW -> neiq5ebjcq ; _rtB -> kuoz5wrkfz = _rtDW -> jvbartsa2s ;
outIdx = _rtB -> bbbvyzsw3d . re * _rtB -> kuoz5wrkfz ; _rtB -> ncqo5vvas3 .
re = ( outIdx & 67108864 ) != 0 ? outIdx | - 67108864 : outIdx & 67108863 ;
outIdx = _rtB -> bbbvyzsw3d . im * _rtB -> kuoz5wrkfz ; _rtB -> ncqo5vvas3 .
im = ( outIdx & 67108864 ) != 0 ? outIdx | - 67108864 : outIdx & 67108863 ;
outIdx = _rtB -> m5u2kus1zb . re << 6 ; currentOffset = _rtB -> m5u2kus1zb .
im << 6 ; outIdx = ( ( outIdx & 134217728 ) != 0 ? outIdx | - 134217728 :
outIdx & 134217727 ) + ( ( _rtB -> ncqo5vvas3 . re & 134217728 ) != 0 ? _rtB
-> ncqo5vvas3 . re | - 134217728 : _rtB -> ncqo5vvas3 . re & 134217727 ) ;
_rtB -> pqdb4tldvq . re = ( outIdx & 134217728 ) != 0 ? outIdx | - 134217728
: outIdx & 134217727 ; outIdx = ( ( currentOffset & 134217728 ) != 0 ?
currentOffset | - 134217728 : currentOffset & 134217727 ) + ( ( _rtB ->
ncqo5vvas3 . im & 134217728 ) != 0 ? _rtB -> ncqo5vvas3 . im | - 134217728 :
_rtB -> ncqo5vvas3 . im & 134217727 ) ; _rtB -> pqdb4tldvq . im = ( outIdx &
134217728 ) != 0 ? outIdx | - 134217728 : outIdx & 134217727 ; _rtB ->
gucfxb3bk4 . re = ( int16_T ) ( _rtB -> pqdb4tldvq . re >> 10 ) ; _rtB ->
gucfxb3bk4 . im = ( int16_T ) ( _rtB -> pqdb4tldvq . im >> 10 ) ; _rtB ->
dnpotqqt0n = _rtDW -> a04aesxehx ; _rtB -> lacf4y5mww = _rtDW -> owfxt5smbq ;
_rtB -> m5u2kus1zb = _rtDW -> o0smrslkoa ; _rtB -> cuwnjuu52v = _rtDW ->
lfxuoewefr ; _rtB -> oltuvtvfbq = _rtDW -> grpn33twic ; _rtB -> fxydjlhsdg =
_rtDW -> izbn00uw3o ; _rtB -> bbbvyzsw3d = _rtDW -> bq40drks5y ; _rtB ->
cjm3wfce4l = _rtDW -> eucvkw0fmw ; _rtB -> nrs2t3bowr = _rtDW -> aonbw1ix0w ;
outIdx = _rtB -> m5u2kus1zb . re * _rtB -> cuwnjuu52v ; _rtB -> dssxwek3lp .
re = ( int16_T ) ( ( ( outIdx < 0 ? 2047 : 0 ) + outIdx ) >> 11 ) ; outIdx =
_rtB -> m5u2kus1zb . im * _rtB -> cuwnjuu52v ; _rtB -> dssxwek3lp . im = (
int16_T ) ( ( ( outIdx < 0 ? 2047 : 0 ) + outIdx ) >> 11 ) ; _rtB ->
cj4snyvswo . re = ( int16_T ) ( _rtB -> bwjlsfaasx . re >> 1 ) ; _rtB ->
cj4snyvswo . im = ( int16_T ) ( _rtB -> bwjlsfaasx . im >> 1 ) ; outIdx = ( (
_rtB -> cj4snyvswo . re & 65536 ) != 0 ? _rtB -> cj4snyvswo . re | - 65536 :
_rtB -> cj4snyvswo . re & 65535 ) - ( ( _rtB -> oltuvtvfbq . re & 65536 ) !=
0 ? _rtB -> oltuvtvfbq . re | - 65536 : _rtB -> oltuvtvfbq . re & 65535 ) ;
_rtB -> ncqo5vvas3 . re = ( outIdx & 65536 ) != 0 ? outIdx | - 65536 : outIdx
& 65535 ; outIdx = ( ( _rtB -> cj4snyvswo . im & 65536 ) != 0 ? _rtB ->
cj4snyvswo . im | - 65536 : _rtB -> cj4snyvswo . im & 65535 ) - ( ( _rtB ->
oltuvtvfbq . im & 65536 ) != 0 ? _rtB -> oltuvtvfbq . im | - 65536 : _rtB ->
oltuvtvfbq . im & 65535 ) ; _rtB -> ncqo5vvas3 . im = ( outIdx & 65536 ) != 0
? outIdx | - 65536 : outIdx & 65535 ; outIdx = ( ( _rtB -> nrs2t3bowr . re &
65536 ) != 0 ? _rtB -> nrs2t3bowr . re | - 65536 : _rtB -> nrs2t3bowr . re &
65535 ) + ( ( _rtB -> dssxwek3lp . re & 65536 ) != 0 ? _rtB -> dssxwek3lp .
re | - 65536 : _rtB -> dssxwek3lp . re & 65535 ) ; currentOffset = ( ( _rtB
-> nrs2t3bowr . im & 65536 ) != 0 ? _rtB -> nrs2t3bowr . im | - 65536 : _rtB
-> nrs2t3bowr . im & 65535 ) + ( ( _rtB -> dssxwek3lp . im & 65536 ) != 0 ?
_rtB -> dssxwek3lp . im | - 65536 : _rtB -> dssxwek3lp . im & 65535 ) ; _rtB
-> ig2xp2ocrs . re = ( int16_T ) ( ( outIdx & 65536 ) != 0 ? outIdx | - 65536
: outIdx & 65535 ) ; _rtB -> ig2xp2ocrs . im = ( int16_T ) ( ( currentOffset
& 65536 ) != 0 ? currentOffset | - 65536 : currentOffset & 65535 ) ; outIdx =
( ( _rtB -> oltuvtvfbq . re & 65536 ) != 0 ? _rtB -> oltuvtvfbq . re | -
65536 : _rtB -> oltuvtvfbq . re & 65535 ) + ( ( _rtB -> dnpotqqt0n . re &
65536 ) != 0 ? _rtB -> dnpotqqt0n . re | - 65536 : _rtB -> dnpotqqt0n . re &
65535 ) ; _rtB -> pqdb4tldvq . re = ( outIdx & 65536 ) != 0 ? outIdx | -
65536 : outIdx & 65535 ; outIdx = ( ( _rtB -> oltuvtvfbq . im & 65536 ) != 0
? _rtB -> oltuvtvfbq . im | - 65536 : _rtB -> oltuvtvfbq . im & 65535 ) + ( (
_rtB -> dnpotqqt0n . im & 65536 ) != 0 ? _rtB -> dnpotqqt0n . im | - 65536 :
_rtB -> dnpotqqt0n . im & 65535 ) ; _rtB -> pqdb4tldvq . im = ( outIdx &
65536 ) != 0 ? outIdx | - 65536 : outIdx & 65535 ; outIdx = ( ( _rtB ->
pqdb4tldvq . re & 131072 ) != 0 ? _rtB -> pqdb4tldvq . re | - 131072 : _rtB
-> pqdb4tldvq . re & 131071 ) - ( ( _rtB -> cj4snyvswo . re & 131072 ) != 0 ?
_rtB -> cj4snyvswo . re | - 131072 : _rtB -> cj4snyvswo . re & 131071 ) ;
_rtB -> k0faidogcp . re = ( outIdx & 131072 ) != 0 ? outIdx | - 131072 :
outIdx & 131071 ; outIdx = ( ( _rtB -> pqdb4tldvq . im & 131072 ) != 0 ? _rtB
-> pqdb4tldvq . im | - 131072 : _rtB -> pqdb4tldvq . im & 131071 ) - ( ( _rtB
-> cj4snyvswo . im & 131072 ) != 0 ? _rtB -> cj4snyvswo . im | - 131072 :
_rtB -> cj4snyvswo . im & 131071 ) ; _rtB -> k0faidogcp . im = ( outIdx &
131072 ) != 0 ? outIdx | - 131072 : outIdx & 131071 ; outIdx = ( ( _rtB ->
k0faidogcp . re & 262144 ) != 0 ? _rtB -> k0faidogcp . re | - 262144 : _rtB
-> k0faidogcp . re & 262143 ) - ( ( _rtB -> fxydjlhsdg . re & 262144 ) != 0 ?
_rtB -> fxydjlhsdg . re | - 262144 : _rtB -> fxydjlhsdg . re & 262143 ) ;
_rtB -> pqdb4tldvq . re = ( outIdx & 262144 ) != 0 ? outIdx | - 262144 :
outIdx & 262143 ; outIdx = ( ( _rtB -> k0faidogcp . im & 262144 ) != 0 ? _rtB
-> k0faidogcp . im | - 262144 : _rtB -> k0faidogcp . im & 262143 ) - ( ( _rtB
-> fxydjlhsdg . im & 262144 ) != 0 ? _rtB -> fxydjlhsdg . im | - 262144 :
_rtB -> fxydjlhsdg . im & 262143 ) ; _rtB -> pqdb4tldvq . im = ( outIdx &
262144 ) != 0 ? outIdx | - 262144 : outIdx & 262143 ; outIdx = ( ( _rtB ->
ncqo5vvas3 . re & 131072 ) != 0 ? _rtB -> ncqo5vvas3 . re | - 131072 : _rtB
-> ncqo5vvas3 . re & 131071 ) - ( ( _rtB -> fxydjlhsdg . re & 131072 ) != 0 ?
_rtB -> fxydjlhsdg . re | - 131072 : _rtB -> fxydjlhsdg . re & 131071 ) ;
_rtB -> k0faidogcp . re = ( outIdx & 131072 ) != 0 ? outIdx | - 131072 :
outIdx & 131071 ; outIdx = ( ( _rtB -> ncqo5vvas3 . im & 131072 ) != 0 ? _rtB
-> ncqo5vvas3 . im | - 131072 : _rtB -> ncqo5vvas3 . im & 131071 ) - ( ( _rtB
-> fxydjlhsdg . im & 131072 ) != 0 ? _rtB -> fxydjlhsdg . im | - 131072 :
_rtB -> fxydjlhsdg . im & 131071 ) ; _rtB -> k0faidogcp . im = ( outIdx &
131072 ) != 0 ? outIdx | - 131072 : outIdx & 131071 ; outIdx = ( ( _rtB ->
bbbvyzsw3d . re & 262144 ) != 0 ? _rtB -> bbbvyzsw3d . re | - 262144 : _rtB
-> bbbvyzsw3d . re & 262143 ) + ( ( _rtB -> k0faidogcp . re & 262144 ) != 0 ?
_rtB -> k0faidogcp . re | - 262144 : _rtB -> k0faidogcp . re & 262143 ) ;
currentOffset = ( ( _rtB -> bbbvyzsw3d . im & 262144 ) != 0 ? _rtB ->
bbbvyzsw3d . im | - 262144 : _rtB -> bbbvyzsw3d . im & 262143 ) + ( ( _rtB ->
k0faidogcp . im & 262144 ) != 0 ? _rtB -> k0faidogcp . im | - 262144 : _rtB
-> k0faidogcp . im & 262143 ) ; _rtB -> eekco0dozc . re = ( int16_T ) ( ( (
outIdx & 262144 ) != 0 ? outIdx | - 262144 : outIdx & 262143 ) >> 3 ) ; _rtB
-> eekco0dozc . im = ( int16_T ) ( ( ( currentOffset & 262144 ) != 0 ?
currentOffset | - 262144 : currentOffset & 262143 ) >> 3 ) ; outIdx = ( (
_rtB -> pqdb4tldvq . re & 524288 ) != 0 ? _rtB -> pqdb4tldvq . re | - 524288
: _rtB -> pqdb4tldvq . re & 524287 ) - ( ( _rtB -> bbbvyzsw3d . re & 524288 )
!= 0 ? _rtB -> bbbvyzsw3d . re | - 524288 : _rtB -> bbbvyzsw3d . re & 524287
) ; currentOffset = ( ( _rtB -> pqdb4tldvq . im & 524288 ) != 0 ? _rtB ->
pqdb4tldvq . im | - 524288 : _rtB -> pqdb4tldvq . im & 524287 ) - ( ( _rtB ->
bbbvyzsw3d . im & 524288 ) != 0 ? _rtB -> bbbvyzsw3d . im | - 524288 : _rtB
-> bbbvyzsw3d . im & 524287 ) ; _rtB -> mvy2t0qh33 . re = ( int16_T ) ( ( (
outIdx & 524288 ) != 0 ? outIdx | - 524288 : outIdx & 524287 ) >> 4 ) ; _rtB
-> mvy2t0qh33 . im = ( int16_T ) ( ( ( currentOffset & 524288 ) != 0 ?
currentOffset | - 524288 : currentOffset & 524287 ) >> 4 ) ; _rtB ->
kuoz5wrkfz = _rtDW -> bgj5snrzuc ; _rtB -> nf2uubuhjx = _rtDW -> oluwp2rosz ;
_rtB -> bre3r0rst0 = ( - 25249 ) * _rtB -> kuoz5wrkfz ; tmp_f = _rtB ->
nf2uubuhjx << 8 ; tmp_j = _rtB -> bre3r0rst0 ; tmp_f = ( ( tmp_f &
281474976710656LL ) != 0LL ? tmp_f | - 281474976710656LL : tmp_f &
281474976710655LL ) + ( ( tmp_j & 281474976710656LL ) != 0LL ? tmp_j | -
281474976710656LL : tmp_j & 281474976710655LL ) ; tmp_f = ( ( tmp_f &
281474976710656LL ) != 0LL ? tmp_f | - 281474976710656LL : tmp_f &
281474976710655LL ) >> 8 ; _rtB -> m5effulfyl = ( tmp_f & 549755813888LL ) !=
0LL ? tmp_f | - 549755813888LL : tmp_f & 549755813887LL ; tmp_f = ( - 24657 )
* _rtB -> kuoz5wrkfz ; _rtB -> fphe3z01fq = ( tmp_f & 549755813888LL ) != 0LL
? tmp_f | - 549755813888LL : tmp_f & 549755813887LL ; ssCallAccelRunBlock ( S
, 12 , 0 , SS_CALL_MDL_OUTPUTS ) ; } if ( ssIsSampleHit ( S , 3 , 0 ) ) {
memcpy ( & _rtDW -> aizrlzwrj2 [ 0 ] , & _rtB -> h2prg42hre [ 0 ] , 4000U *
sizeof ( cint16_T ) ) ; _rtDW -> gnt45vonqr = 0 ; } _rtB -> mw35yxwf0d =
_rtDW -> aizrlzwrj2 [ _rtDW -> gnt45vonqr ] ; if ( _rtDW -> gnt45vonqr < 3999
) { _rtDW -> gnt45vonqr ++ ; } if ( ssIsSpecialSampleHit ( S , 1 , 0 , 0 ) )
{ _rtB -> gx2hlv5rke = _rtB -> mw35yxwf0d ; } srUpdateBC ( _rtDW ->
nqnggmdwgl ) ; } UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { int32_T cfIdx ; int32_T
memIdx ; int32_T next ; int32_T j ; int64_T tmp ; apofffdfhu * _rtB ;
p0d4ltqkb0 * _rtDW ; _rtDW = ( ( p0d4ltqkb0 * ) ssGetRootDWork ( S ) ) ; _rtB
= ( ( apofffdfhu * ) _ssGetBlockIO ( S ) ) ; if ( _rtDW -> clfjv4sbg5 ) { if
( ssIsSampleHit ( S , 1 , 0 ) ) { _rtDW -> hcjfr00jlo [ 0U ] = _rtDW ->
hcjfr00jlo [ 1U ] ; _rtDW -> hcjfr00jlo [ 1 ] = _rtB -> gucfxb3bk4 ; _rtDW ->
hdopk0vvuy = _rtB -> cl5phq45un ; if ( _rtDW -> e40qniej0g ) { for ( cfIdx =
0 ; cfIdx < 19 ; cfIdx ++ ) { _rtDW -> me2mx5snxx [ ( uint32_T ) cfIdx ] =
_rtDW -> me2mx5snxx [ cfIdx + 1U ] ; } _rtDW -> me2mx5snxx [ 19 ] = _rtB ->
pr45gajxdt ; _rtDW -> j3nqizf5sz [ 0U ] = _rtDW -> j3nqizf5sz [ 1U ] ; _rtDW
-> j3nqizf5sz [ 1U ] = _rtDW -> j3nqizf5sz [ 2U ] ; _rtDW -> j3nqizf5sz [ 2U
] = _rtDW -> j3nqizf5sz [ 3U ] ; _rtDW -> j3nqizf5sz [ 3 ] = _rtB ->
cr1trr3ihg ; _rtDW -> hcwe4zz5v4 [ 0U ] = _rtDW -> hcwe4zz5v4 [ 1U ] ; _rtDW
-> hcwe4zz5v4 [ 1U ] = _rtDW -> hcwe4zz5v4 [ 2U ] ; _rtDW -> hcwe4zz5v4 [ 2 ]
= _rtB -> ckgu2elh4j ; _rtDW -> glw3pp0wuv = _rtB -> mivb3mzqb2 ; _rtDW ->
fxwi43groy = _rtB -> ozv5wj1uic ; _rtDW -> oifeqzhrgp [ 0 ] = _rtB ->
por40ilbdf [ 0 ] ; _rtDW -> oifeqzhrgp [ 1 ] = _rtB -> por40ilbdf [ 1 ] ;
_rtDW -> hx4vxy3emn = _rtB -> cgifl5e3x5 ; _rtDW -> mahnpzwfuh = _rtB ->
hlw24yjjep ; cfIdx = 1 ; memIdx = 0 ; next = 0 ; for ( j = 0 ; j < 11 ; j ++
) { tmp = ( int64_T ) _rtB -> pnfecimhgq * kqdbxwmxxp . hecqimcjis [ cfIdx ]
; tmp = ( tmp & 8589934592LL ) != 0LL ? tmp | - 8589934592LL : tmp &
8589934591LL ; tmp = ( ( tmp & 137438953472LL ) != 0LL ? tmp | -
137438953472LL : tmp & 137438953471LL ) + _rtDW -> m2uhskxrr3 [ next + 1 ] ;
_rtDW -> m2uhskxrr3 [ memIdx ] = ( tmp & 137438953472LL ) != 0LL ? tmp | -
137438953472LL : tmp & 137438953471LL ; cfIdx ++ ; next ++ ; memIdx ++ ; }
tmp = ( int64_T ) _rtB -> pnfecimhgq * kqdbxwmxxp . hecqimcjis [ cfIdx ] ;
tmp = ( tmp & 8589934592LL ) != 0LL ? tmp | - 8589934592LL : tmp &
8589934591LL ; _rtDW -> m2uhskxrr3 [ memIdx ] = ( tmp & 137438953472LL ) !=
0LL ? tmp | - 137438953472LL : tmp & 137438953471LL ; _rtDW -> ovpg4mr2uq =
_rtB -> owp5iuocjy ; cfIdx = 1 ; memIdx = 0 ; next = 0 ; for ( j = 0 ; j < 11
; j ++ ) { tmp = ( int64_T ) _rtB -> fyen0ek4fb * kqdbxwmxxp . pmf303vsdo [
cfIdx ] ; tmp = ( tmp & 8589934592LL ) != 0LL ? tmp | - 8589934592LL : tmp &
8589934591LL ; tmp = ( ( tmp & 137438953472LL ) != 0LL ? tmp | -
137438953472LL : tmp & 137438953471LL ) + _rtDW -> evixspa5r4 [ next + 1 ] ;
_rtDW -> evixspa5r4 [ memIdx ] = ( tmp & 137438953472LL ) != 0LL ? tmp | -
137438953472LL : tmp & 137438953471LL ; cfIdx ++ ; next ++ ; memIdx ++ ; }
tmp = ( int64_T ) _rtB -> fyen0ek4fb * kqdbxwmxxp . pmf303vsdo [ cfIdx ] ;
tmp = ( tmp & 8589934592LL ) != 0LL ? tmp | - 8589934592LL : tmp &
8589934591LL ; _rtDW -> evixspa5r4 [ memIdx ] = ( tmp & 137438953472LL ) !=
0LL ? tmp | - 137438953472LL : tmp & 137438953471LL ; _rtDW -> lwvw1oujt2 =
_rtB -> n0ruzxn5bw ; _rtDW -> cjkwow3fio = _rtB -> eif3ckhnnv ; _rtDW ->
kx3v4xvgfx = _rtB -> mrbxcp5zg4 ; _rtDW -> j4btv1mlsw = _rtB -> dx02oxdbub ;
_rtDW -> i1xhj1a4w3 = _rtB -> ckwpdsnnuk ; _rtDW -> bhcnk1pdyd = _rtB ->
pfgztrd5lk ; _rtDW -> hpaj1dyoaq = _rtB -> e3if514fnw ; _rtDW -> ho1z3ioaql =
_rtB -> bmwmlhuljm ; } } _rtDW -> ad2oe2x4ij = _rtB -> gbatb3j4p3 ; _rtDW ->
n3dnbcttno [ 0U ] = _rtDW -> n3dnbcttno [ 1U ] ; _rtDW -> n3dnbcttno [ 1U ] =
_rtDW -> n3dnbcttno [ 2U ] ; _rtDW -> n3dnbcttno [ 2U ] = _rtDW -> n3dnbcttno
[ 3U ] ; _rtDW -> n3dnbcttno [ 3U ] = _rtDW -> n3dnbcttno [ 4U ] ; _rtDW ->
n3dnbcttno [ 4 ] = _rtB -> owd2auqy4e ; _rtDW -> iucxrvfokz = _rtB ->
f0m2xj1ngt ; _rtDW -> e0v0dl2owv = _rtB -> e5wvak3221 ; _rtDW -> ps1gy5o5dm =
_rtB -> mw35yxwf0d ; _rtDW -> lkt5zsxrya = _rtB -> hr5bw4hfhw ; _rtDW ->
eebc11a3iu = _rtB -> bqzcqbqfa1 ; _rtDW -> l4mshu1tqq = _rtB -> hajuj54fid ;
_rtDW -> m4n4w0ubg5 = _rtB -> mixlfqdhax ; _rtDW -> c3vmxtlk1v = _rtB ->
n0zbgjwizw ; _rtDW -> e0n51yk45m = _rtB -> ezn5q43x12 ; _rtDW -> bbt5qjvrgn =
_rtB -> fdhjbzm4ct ; if ( ssIsSampleHit ( S , 1 , 0 ) ) { _rtDW -> nzei4visd3
[ 0U ] = _rtDW -> nzei4visd3 [ 1U ] ; _rtDW -> nzei4visd3 [ 1U ] = _rtDW ->
nzei4visd3 [ 2U ] ; _rtDW -> nzei4visd3 [ 2U ] = _rtDW -> nzei4visd3 [ 3U ] ;
_rtDW -> nzei4visd3 [ 3 ] = _rtB -> fo2nolxzuc ; _rtDW -> dwhbvc4zyv [ 0U ] =
_rtDW -> dwhbvc4zyv [ 1U ] ; _rtDW -> dwhbvc4zyv [ 1U ] = _rtDW -> dwhbvc4zyv
[ 2U ] ; _rtDW -> dwhbvc4zyv [ 2U ] = _rtDW -> dwhbvc4zyv [ 3U ] ; _rtDW ->
dwhbvc4zyv [ 3 ] = _rtB -> ppb5okg5q5 ; _rtDW -> l05dqxcm3k [ 0U ] = _rtDW ->
l05dqxcm3k [ 1U ] ; _rtDW -> l05dqxcm3k [ 1U ] = _rtDW -> l05dqxcm3k [ 2U ] ;
_rtDW -> l05dqxcm3k [ 2U ] = _rtDW -> l05dqxcm3k [ 3U ] ; _rtDW -> l05dqxcm3k
[ 3 ] = _rtB -> mirzucihdg ; _rtDW -> mfxh5amrmh = _rtB -> n01bk0pygv ; _rtDW
-> lkusdfylul = _rtB -> b10e0vrvkh ; _rtDW -> emogjyqg5j = _rtB -> n4jdfanglr
; _rtDW -> ej45hqzclg [ 0U ] = _rtDW -> ej45hqzclg [ 1U ] ; _rtDW ->
ej45hqzclg [ 1U ] = _rtDW -> ej45hqzclg [ 2U ] ; _rtDW -> ej45hqzclg [ 2U ] =
_rtDW -> ej45hqzclg [ 3U ] ; _rtDW -> ej45hqzclg [ 3 ] = _rtB -> ete0gkwe0f ;
_rtDW -> miumd5j2rh = _rtB -> d01lpmlce1 ; _rtDW -> a1ytrarwt0 = _rtB ->
l1g4v2fo0a ; _rtDW -> ge44h0c4z0 = _rtB -> jc1ky115dg ; _rtDW -> fxfohbwy34 =
_rtB -> kgkfzbiuc0 ; _rtDW -> hxx3d0m5re = _rtB -> buwdx50dwi ; _rtDW ->
daxjg4l44p = _rtB -> hl43yzi3bf ; for ( cfIdx = 0 ; cfIdx < 8 ; cfIdx ++ ) {
_rtDW -> akfkgnwuwe [ ( uint32_T ) cfIdx ] = _rtDW -> akfkgnwuwe [ cfIdx + 1U
] ; } _rtDW -> akfkgnwuwe [ 8 ] = _rtB -> bysguabmws ; _rtDW -> ah3gbqvttx =
_rtB -> cen4fynu0d ; _rtDW -> omegeptmak = _rtB -> lif30c2zfa ; _rtDW ->
pakeiyzsck = _rtB -> nmz4xr45v0 ; _rtDW -> gbi2dxkr31 = _rtB -> kqygxacvla ;
for ( cfIdx = 0 ; cfIdx < 5 ; cfIdx ++ ) { _rtDW -> liq2v41hed [ ( uint32_T )
cfIdx ] = _rtDW -> liq2v41hed [ cfIdx + 1U ] ; } _rtDW -> liq2v41hed [ 5 ] =
_rtB -> bakalnzyaw ; _rtDW -> b4k2uxvbov = _rtB -> o4n2c1srwa ; _rtDW ->
e3i3twvaaj = _rtB -> fphe3z01fq ; _rtDW -> nvrxlyi3lt = _rtB -> m5effulfyl ;
_rtDW -> i24inhgtzr = _rtB -> cjm3wfce4l ; _rtDW -> neiq5ebjcq = _rtB ->
ig2xp2ocrs ; _rtDW -> jvbartsa2s = _rtB -> cuwnjuu52v ; _rtDW -> a04aesxehx =
_rtB -> bwjlsfaasx ; _rtDW -> owfxt5smbq = _rtB -> dnpotqqt0n ; _rtDW ->
o0smrslkoa = _rtB -> eekco0dozc ; _rtDW -> lfxuoewefr = _rtB -> lw3nl3crl1 ;
_rtDW -> grpn33twic = _rtB -> cj4snyvswo ; _rtDW -> izbn00uw3o = _rtB ->
oltuvtvfbq ; _rtDW -> bq40drks5y = _rtB -> fxydjlhsdg ; _rtDW -> eucvkw0fmw =
_rtB -> lacf4y5mww ; _rtDW -> aonbw1ix0w = _rtB -> mvy2t0qh33 ; _rtDW ->
bgj5snrzuc = _rtB -> hrdfvkmtfc ; _rtDW -> oluwp2rosz = _rtB -> m5effulfyl ;
} } UNUSED_PARAMETER ( tid ) ; } static void mdlInitializeSizes ( SimStruct *
S ) { ssSetChecksumVal ( S , 0 , 2431871706U ) ; ssSetChecksumVal ( S , 1 ,
2441514492U ) ; ssSetChecksumVal ( S , 2 , 3207179130U ) ; ssSetChecksumVal (
S , 3 , 1029274224U ) ; { mxArray * slVerStructMat = NULL ; mxArray *
slStrMat = mxCreateString ( "simulink" ) ; char slVerChar [ 10 ] ; int status
= mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver" ) ; if (
status == 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0 ,
"Version" ) ; if ( slVerMat == NULL ) { status = 1 ; } else { status =
mxGetString ( slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.4" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
p0d4ltqkb0 ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( apofffdfhu ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } _ssSetConstBlockIO ( S , &
h4rqggz4rb ) ; rt_InitInfAndNaN ( sizeof ( real_T ) ) ; } static void
mdlInitializeSampleTimes ( SimStruct * S ) { { SimStruct * childS ;
SysOutputFcn * callSysFcns ; childS = ssGetSFunction ( S , 0 ) ; callSysFcns
= ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 1 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 2 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 3 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 4 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 5 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 6 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 7 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 8 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; } } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
