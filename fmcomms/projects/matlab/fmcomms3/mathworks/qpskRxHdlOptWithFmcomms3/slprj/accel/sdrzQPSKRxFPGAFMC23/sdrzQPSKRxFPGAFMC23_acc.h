#include "__cf_sdrzQPSKRxFPGAFMC23.h"
#ifndef RTW_HEADER_sdrzQPSKRxFPGAFMC23_acc_h_
#define RTW_HEADER_sdrzQPSKRxFPGAFMC23_acc_h_
#include <stddef.h>
#include <string.h>
#ifndef sdrzQPSKRxFPGAFMC23_acc_COMMON_INCLUDES_
#define sdrzQPSKRxFPGAFMC23_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "sdrzQPSKRxFPGAFMC23_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
typedef struct { creal_T obkxybx5mt [ 4000 ] ; creal_T ktvy4zxbrp [ 8000 ] ;
real_T gslcw30p3u ; real_T adr3ulpmeh ; real_T jcmz1ydeck ; real_T c1t2it2tux
; cint64_T lauf2rx1be ; cint64_T b10e0vrvkh ; cint64_T kgkfzbiuc0 ; cint64_T
ieiheqzti2 ; cint64_T a51liwe2ua ; int64_T m5effulfyl ; int64_T fphe3z01fq ;
int64_T bqiuv1fqqv ; int64_T nf2uubuhjx ; int64_T ekjxufukx0 ; int64_T
gtb0eqgj10 ; int32_T gbatb3j4p3 ; int32_T bmj0fm255r ; int32_T nmz4xr45v0 ;
int32_T dw5k0n4dk5 ; int32_T pw2kuoh4ii ; int32_T lif30c2zfa ; int32_T
otlx0jpgkn ; int32_T fdhjbzm4ct ; int32_T f3v1ijqvet ; cint32_T mirzucihdg ;
cint32_T l1g4v2fo0a ; cint32_T jc1ky115dg ; cint32_T d01lpmlce1 ; cint32_T
ete0gkwe0f ; cint32_T ncqo5vvas3 ; cint32_T fo2nolxzuc ; cint32_T pqdb4tldvq
; cint32_T ppb5okg5q5 ; cint32_T ozv5wj1uic ; cint32_T fzm4azyw5t ; int32_T
owp5iuocjy ; int32_T hlw24yjjep ; int32_T pnfecimhgq ; int32_T fyen0ek4fb ;
int32_T iculjrpa2d ; int32_T bre3r0rst0 ; int32_T cgifl5e3x5 ; int32_T
kkfw1jwgh0 ; int32_T devnqy0tal ; cint32_T k0faidogcp ; cint16_T itg0myk2wa ;
cint16_T mbkey5nhdq [ 4000 ] ; int16_T npm5nba0fs [ 4000 ] ; int16_T
dhlwcukbu1 [ 4000 ] ; int16_T fjg5kw2vi4 ; int16_T dagwtnptfc ; int16_T
iykaks3lja ; int16_T k2nymlmn0w ; int16_T e5i2h0nzb1 ; int16_T kuoz5wrkfz ;
cint16_T h2prg42hre [ 4000 ] ; cint16_T owd2auqy4e ; cint16_T mw35yxwf0d ;
cint16_T gx2hlv5rke ; cint16_T hpxpcxdtcp ; int16_T mixlfqdhax ; int16_T
n0zbgjwizw ; int16_T bqzcqbqfa1 ; int16_T hajuj54fid ; int16_T ezn5q43x12 ;
int16_T hr5bw4hfhw ; int16_T dx02oxdbub ; int16_T mlvsqidcjr ; int16_T
oatajrzybm ; int16_T atpyggke1j ; cint16_T pr45gajxdt ; cint16_T gucfxb3bk4 ;
cint16_T ig2xp2ocrs ; cint16_T mvy2t0qh33 ; cint16_T f2oagwwe5w ; cint16_T
cr1trr3ihg ; cint16_T mivb3mzqb2 ; cint16_T dssxwek3lp ; cint16_T kpj5dizbt4
; cint16_T aisymkzzms ; cint16_T k0ssphgq1x ; int16_T f0m2xj1ngt ; int16_T
hrdfvkmtfc ; int16_T eif3ckhnnv ; int16_T n0ruzxn5bw ; int16_T e3if514fnw ;
int16_T ckwpdsnnuk ; int16_T mrbxcp5zg4 ; int16_T bmwmlhuljm ; int16_T
pfgztrd5lk ; int16_T atrcpenqfu ; int16_T psgbi0n5zu ; int16_T b44vh5ha13 ;
int16_T g2uf21tk3d ; int16_T ng104j1bnv ; cint16_T e5wvak3221 ; cint16_T
bysguabmws ; cint16_T bakalnzyaw ; cint16_T hl43yzi3bf ; cint16_T n01bk0pygv
; cint16_T bwjlsfaasx ; cint16_T dnpotqqt0n ; cint16_T lacf4y5mww ; cint16_T
oltuvtvfbq ; cint16_T fxydjlhsdg ; cint16_T cjm3wfce4l ; cint16_T cj4snyvswo
; cint16_T hd0v215ugx ; cint16_T m5u2kus1zb ; int16_T cen4fynu0d ; cint16_T
kqygxacvla ; cint16_T epjcz31024 ; cint16_T cbjkaolvwh ; cint16_T nrs2t3bowr
; cint16_T edigpp4t1l ; cint16_T e1iuhxt5fg ; cint16_T bbbvyzsw3d ; int16_T
lw3nl3crl1 ; int16_T md4f1xwmdg ; int16_T cuwnjuu52v ; int16_T o4n2c1srwa ;
cint16_T buwdx50dwi ; uint16_T n4jdfanglr ; uint16_T kgyw32msh1 ; uint16_T
ghmizdgclj ; uint16_T oefgwrftj0 ; cint16_T eekco0dozc ; boolean_T obnfdr2i53
; boolean_T ly2wus2tcm ; boolean_T auzxfg0qc0 [ 4000 ] ; boolean_T fit5tcfo3y
[ 4000 ] ; boolean_T cchdcmxall [ 4000 ] ; boolean_T k3dveqppjd ; boolean_T
jslxjvmliz ; boolean_T i00htlcezd ; boolean_T awhzadqcrm ; boolean_T
cl5phq45un ; boolean_T mjhbdlc4h0 ; boolean_T ax0zsrbpeu ; boolean_T
oxlr2stf2n [ 2 ] ; boolean_T nggubdrym0 ; boolean_T por40ilbdf [ 2 ] ;
boolean_T ckgu2elh4j ; boolean_T hh32ahrw0k ; boolean_T cbzuifdwyb ;
boolean_T c1q3jj5jrc [ 174 ] ; boolean_T arueid0soy [ 8000 ] ; boolean_T
lwvuz35deg [ 200 ] ; boolean_T hzxylntxdo ; uint8_T izcqojkhqn [ 4000 ] ;
uint8_T gf15hfa03w [ 4000 ] ; uint8_T k2own1bqf2 [ 4000 ] ; char
pad_k2own1bqf2 [ 6 ] ; } apofffdfhu ; typedef struct { real_T d31q3auels [ 19
] ; real_T dttj1rnofi [ 19 ] ; cint64_T lkusdfylul ; cint64_T fxfohbwy34 ;
int64_T e3i3twvaaj ; int64_T nvrxlyi3lt ; int64_T oluwp2rosz ; int64_T
m2uhskxrr3 [ 12 ] ; int64_T evixspa5r4 [ 12 ] ; int64_T hazjfe45s5 ; cint64_T
fhvpdccb5a ; cint64_T jgsgkmrvj5 ; int64_T gbwmhxcrg2 ; int64_T cg5xxmi0be ;
int64_T botneqmuae ; void * fvsvt4pkfm ; void * io4hnch3y1 ; void *
csrnvazwyo ; void * orl0yythpm ; int32_T nhxj0sseye ; int32_T ix310kqdnc ;
int32_T f03dllvydv ; int32_T gjcoppf52o ; int32_T llzzb1nyuw ; int32_T
lampfbwg5i ; int32_T ktqdm4qoh2 ; int32_T gnt45vonqr ; int32_T grmfmipaiz ;
int32_T fsypd3acuw [ 4 ] ; int32_T ad2oe2x4ij ; int32_T pakeiyzsck ; int32_T
omegeptmak ; int32_T kqk5ezjgmp [ 7 ] ; int32_T mh2fxozgta [ 7 ] ; int32_T
inwfljdjat [ 7 ] ; int32_T ni3suzmoon ; int32_T jipw1ox3q5 ; int32_T
bbt5qjvrgn ; cint32_T l05dqxcm3k [ 4 ] ; cint32_T miumd5j2rh ; cint32_T
a1ytrarwt0 ; cint32_T ge44h0c4z0 ; cint32_T ej45hqzclg [ 4 ] ; cint32_T
nzei4visd3 [ 4 ] ; cint32_T dwhbvc4zyv [ 4 ] ; cint32_T fxwi43groy ; int32_T
mahnpzwfuh ; int32_T ovpg4mr2uq ; int32_T hx4vxy3emn ; int32_T punmptttcq ;
cint32_T pvtxaedqgj ; int32_T g5umflja2y ; int32_T bqbf1rnecp ; cint32_T
nkecj53igv ; int32_T m0thbnknua ; cint32_T pnwxt0oef1 ; cint32_T ksiqzohfuo ;
cint32_T mlci2tr5hu ; int32_T jhys4lfd5l ; cint32_T n01ypned2b ; cint16_T
er5ywavmqi [ 8000 ] ; cint16_T n3dnbcttno [ 5 ] ; cint16_T ps1gy5o5dm ;
cint16_T aizrlzwrj2 [ 4000 ] ; int16_T lkt5zsxrya ; int16_T eebc11a3iu ;
int16_T l4mshu1tqq ; int16_T m4n4w0ubg5 ; int16_T c3vmxtlk1v ; int16_T
e0n51yk45m ; int16_T j4btv1mlsw ; cint16_T hcjfr00jlo [ 2 ] ; cint16_T
neiq5ebjcq ; cint16_T aonbw1ix0w ; cint16_T me2mx5snxx [ 20 ] ; cint16_T
j3nqizf5sz [ 4 ] ; cint16_T glw3pp0wuv ; int16_T iucxrvfokz ; int16_T
bgj5snrzuc ; int16_T lwvw1oujt2 ; int16_T cjkwow3fio ; int16_T kx3v4xvgfx ;
int16_T i1xhj1a4w3 ; int16_T bhcnk1pdyd ; int16_T hpaj1dyoaq ; int16_T
ho1z3ioaql ; cint16_T e0v0dl2owv ; cint16_T mfxh5amrmh ; cint16_T daxjg4l44p
; cint16_T akfkgnwuwe [ 9 ] ; cint16_T liq2v41hed [ 6 ] ; cint16_T i24inhgtzr
; cint16_T a04aesxehx ; cint16_T owfxt5smbq ; cint16_T grpn33twic ; cint16_T
izbn00uw3o ; cint16_T bq40drks5y ; cint16_T eucvkw0fmw ; int16_T ah3gbqvttx ;
cint16_T gbi2dxkr31 ; int16_T gsdz5yj0wf [ 6 ] ; int16_T asxhi3j5mk [ 6 ] ;
int16_T oy55ucas25 [ 6 ] ; int16_T noeloyqybd [ 6 ] ; int16_T b4k2uxvbov ;
int16_T jvbartsa2s ; int16_T lfxuoewefr ; cint16_T hxx3d0m5re ; uint16_T
emogjyqg5j ; cint16_T o0smrslkoa ; int16_T og5wcnpu4f [ 6 ] ; int16_T
l0lcsta2kn [ 6 ] ; cint16_T chexynbgdn ; cint16_T pdn2lc4yxb ; cint16_T
nslsfzz2zu [ 42 ] ; int16_T ewfytebjjf ; uint16_T l4apnbhys2 ; int16_T
bykxe5f15h ; int16_T ezyp3bpb5s ; boolean_T hdopk0vvuy ; boolean_T be5gsnjaea
[ 6 ] ; boolean_T hcwe4zz5v4 [ 3 ] ; boolean_T oifeqzhrgp [ 2 ] ; boolean_T
hvncxvtpsg [ 8 ] ; boolean_T i5nhrdbhm3 [ 6 ] ; boolean_T khrfanygjs [ 8000 ]
; int8_T nqnggmdwgl ; int8_T cwps1jqc44 ; int8_T kxizblrgd3 ; int8_T
bede2ximo0 ; boolean_T clfjv4sbg5 ; boolean_T e40qniej0g ; boolean_T
oyjntg5v1k ; char pad_oyjntg5v1k ; } p0d4ltqkb0 ; typedef struct { const
uint16_T ojw5xnzrzp ; const uint16_T dasricjf2b ; const uint16_T fdz35t0c3r ;
const uint16_T fufu3agotw ; const uint16_T nxk1byo5xr ; const uint16_T
gz0maqkjnv ; const uint16_T ch1npdvsii ; const uint16_T luzvag423y ; const
uint16_T auo25ji4uq ; const boolean_T prdeddsm0q ; const boolean_T kzyqq2mrk3
; const boolean_T oddfoi5s5z ; const boolean_T b0shztvb23 ; const boolean_T
hrkj3i2gtk ; const boolean_T gf54aa0ff4 ; const boolean_T m1heelvk3v ; char
pad_m1heelvk3v [ 7 ] ; } dvqa44zkef ;
#define iri5mili5t(S) ((dvqa44zkef *) _ssGetConstBlockIO(S))
typedef struct { real_T mzcecwptdb ; real_T jkm22larr2 [ 2 ] ; real_T
pelugmrlqb ; real_T np44mk20in [ 2 ] ; real_T lchxmny1vd ; int64_T m3bptb2f5y
; int64_T gz022a53cr ; int64_T il3g3wvov5 ; int32_T a2hqbzyls2 [ 4 ] ;
int32_T mc1v35msjb ; int32_T hecqimcjis [ 13 ] ; int32_T pmf303vsdo [ 13 ] ;
int32_T m0c00vnrba ; int32_T ks3dgpjeqg ; int16_T of1djvu0s1 ; int16_T
pk0bgfelsv ; int16_T mi2j2gkf0m ; int16_T obihxvsb3s ; int16_T epxcprsual ;
int16_T gvrussalxc [ 42 ] ; int16_T abhr53zdil ; int16_T m3z5zt3u50 ;
cint16_T mz30e2sksn ; cint16_T kol3sk4xzk ; int16_T fvkc3aqwkw ; uint16_T
fahiwlzyuc ; uint16_T olov4zeda4 ; uint16_T oqzavtsbg3 ; uint16_T mgjlyhdzvr
; int16_T mhljruewxi ; int16_T p5534ophut ; int16_T bctxetad5j ; int16_T
fqpa0gwlgu ; int16_T amq3etcswq ; uint8_T cgwif415op [ 5 ] ; boolean_T
hofipeqih0 ; boolean_T jwdbcrhobw ; uint8_T ozrxn2shxe ; uint8_T m3txp41yj0 ;
uint8_T ekbn3ijjjf ; uint8_T i4j1q4b1uf ; uint8_T evykyoxvdd ; uint8_T
glcypylli3 ; uint8_T m4wvh22noe ; uint8_T dovcrlazr5 ; char pad_dovcrlazr5 [
7 ] ; } pqcuqenvfz ; extern const dvqa44zkef h4rqggz4rb ; extern const
pqcuqenvfz kqdbxwmxxp ;
#endif
