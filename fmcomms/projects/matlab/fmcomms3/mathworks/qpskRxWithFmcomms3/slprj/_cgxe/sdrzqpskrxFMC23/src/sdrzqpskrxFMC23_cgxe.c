/* Include files */

#include "sdrzqpskrxFMC23_cgxe.h"
#include "m_6G49rbutZhd1j25aViObvC.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_sdrzqpskrxFMC23_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_sdrzqpskrxFMC23_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_sdrzqpskrxFMC23_method_dispatcher(SimStruct* S, int_T method,
  void* data)
{
  if (ssGetChecksum0(S) == 2868301158 &&
      ssGetChecksum1(S) == 3034545011 &&
      ssGetChecksum2(S) == 2329657714 &&
      ssGetChecksum3(S) == 485338011) {
    method_dispatcher_6G49rbutZhd1j25aViObvC(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_sdrzqpskrxFMC23_autoInfer_dispatcher(const mxArray* prhs, mxArray* lhs[],
  const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzqpskrxFMC23:2163") == 0 ) {
    return autoInfer_dispatcher_6G49rbutZhd1j25aViObvC(lhs, commandName);
  }

  return 0;
}
