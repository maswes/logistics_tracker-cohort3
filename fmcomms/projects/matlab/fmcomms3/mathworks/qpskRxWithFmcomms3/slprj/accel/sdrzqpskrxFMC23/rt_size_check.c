#include "__cf_sdrzqpskrxFMC23.h"
#include "rt_size_check.h"
#include <stdio.h>
const char sizeErrorMsg [ ] =
 "Invalid runtime dimensions found. Run the model in normal mode simulation for specific information about the invalid input dimensions."
; const char genericErrorMsg [ ] =
 "A run-time error occurred. Run the model in normal mode simulation for specific information about the error."
; void rtErrorMsgID ( SimStruct * S , const char * msgID ) { ssSetErrorStatus
( S , genericErrorMsg ) ; } void rtSizeEqCheck1D ( SimStruct * S , int dims1
, int dims2 ) { if ( dims1 != dims2 ) { ssSetErrorStatus ( S , sizeErrorMsg )
; return ; } return ; } void rtSizeEqCheckND ( SimStruct * S , int * dims1 ,
int * dims2 , int nDims ) { int i ; bool hasError = false ; if ( nDims == 2 )
{ if ( ( dims1 [ 0 ] * dims1 [ 1 ] ) != ( dims2 [ 0 ] * dims2 [ 1 ] ) ) {
hasError = true ; } else { if ( ( dims1 [ 0 ] == dims2 [ 0 ] ) || ( dims1 [ 0
] == dims2 [ 1 ] ) ) { hasError = false ; } else { hasError = true ; } } }
else { for ( i = 0 ; i < nDims ; i ++ ) { if ( dims1 [ i ] != dims2 [ i ] ) {
hasError = true ; } } } if ( hasError ) { ssSetErrorStatus ( S , sizeErrorMsg
) ; } return ; }
