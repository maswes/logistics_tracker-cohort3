#include "__cf_sdrzqpskrxFMC23.h"
#include <math.h>
#include "sdrzqpskrxFMC23_acc.h"
#include "sdrzqpskrxFMC23_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
static void mdlOutputs ( SimStruct * S , int_T tid ) { int32_T outIdx ;
boolean_T curBuf ; int32_T inputIdx ; int32_T k ; int32_T iIdx ; int32_T
colIdx ; int32_T yIndx ; int32_T bIndx ; int32_T i ; creal_T u ; real_T
acc_re ; real_T acc_im ; nudldxhedl * _rtB ; cszlzfj2xv * _rtZCE ; oqnsh5busn
* _rtDW ; _rtDW = ( ( oqnsh5busn * ) ssGetRootDWork ( S ) ) ; _rtZCE = ( (
cszlzfj2xv * ) _ssGetPrevZCSigState ( S ) ) ; _rtB = ( ( nudldxhedl * )
_ssGetBlockIO ( S ) ) ; if ( ssIsSampleHit ( S , 1 , 0 ) ) {
ssCallAccelRunBlock ( S , 0 , 0 , SS_CALL_MDL_OUTPUTS ) ; memcpy ( & _rtB ->
ahww1v0n4g [ 0 ] , & _rtB -> bikpevkecr [ 0 ] , 4000U * sizeof ( creal_T ) )
; if ( _rtB -> c33d2e3yfp > 0.0 ) { if ( ! _rtDW -> n4ya3v4vfc ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 7 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 10 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 3 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 4 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 5 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 12 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ;
ssCallAccelRunBlock ( S , 11 , 0 , SS_CALL_RTW_GENERATED_ENABLE ) ; _rtDW ->
n4ya3v4vfc = true ; } } else { if ( _rtDW -> n4ya3v4vfc ) {
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 7 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 10 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 3 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 4 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 5 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 12 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ;
ssCallAccelRunBlock ( S , 11 , 0 , SS_CALL_RTW_GENERATED_DISABLE ) ; _rtDW ->
n4ya3v4vfc = false ; } } } if ( _rtDW -> n4ya3v4vfc ) { if ( ssIsSampleHit (
S , 1 , 0 ) ) { ssCallAccelRunBlock ( S , 1 , 0 , SS_CALL_MDL_OUTPUTS ) ; for
( i = 0 ; i < 4000 ; i ++ ) { _rtB -> ads1giluje [ i ] . re = 0.5 * _rtB ->
nzb31igwj1 [ i ] . re ; _rtB -> ads1giluje [ i ] . im = 0.5 * _rtB ->
nzb31igwj1 [ i ] . im ; } inputIdx = 0 ; curBuf = _rtDW -> pgzs3loahd ; yIndx
= _rtDW -> haswajj2w4 ; i = _rtDW -> mdlidvs5hy ; outIdx = _rtDW ->
f5gdbfik3w ; colIdx = _rtDW -> pxl1p3dxjh ; for ( iIdx = 0 ; iIdx < 4000 ;
iIdx ++ ) { bIndx = i ; _rtDW -> iun0seygxz [ i ] = _rtB -> ads1giluje [
inputIdx ] ; inputIdx ++ ; for ( k = 0 ; k < 21 ; k ++ ) { _rtDW ->
laivtoatrs . re += _rtDW -> iun0seygxz [ bIndx ] . re * e2v1ycplzh .
j1sydq5thw [ colIdx ] ; _rtDW -> laivtoatrs . im += _rtDW -> iun0seygxz [
bIndx ] . im * e2v1ycplzh . j1sydq5thw [ colIdx ] ; colIdx ++ ; bIndx -= 2 ;
if ( bIndx < 0 ) { bIndx += 42 ; } } i ++ ; if ( i >= 42 ) { i = 0 ; } yIndx
++ ; if ( yIndx >= 2 ) { colIdx = outIdx ; if ( curBuf ) { colIdx = outIdx +
2000 ; } _rtDW -> hcwmkho3rc [ colIdx ] = _rtDW -> laivtoatrs ; _rtDW ->
laivtoatrs . re = 0.0 ; _rtDW -> laivtoatrs . im = 0.0 ; outIdx ++ ; if (
outIdx >= 2000 ) { outIdx = 0 ; curBuf = ! curBuf ; } yIndx = 0 ; colIdx = 0
; } } _rtDW -> pxl1p3dxjh = colIdx ; _rtDW -> haswajj2w4 = yIndx ; _rtDW ->
mdlidvs5hy = i ; _rtDW -> f5gdbfik3w = outIdx ; _rtDW -> pgzs3loahd = curBuf
; if ( _rtDW -> i4jebm2wpn ) { colIdx = 2000 ; } else { colIdx = 0 ; } _rtDW
-> i4jebm2wpn = ! _rtDW -> i4jebm2wpn ; for ( outIdx = 0 ; outIdx < 2000 ;
outIdx ++ ) { _rtB -> nalxf3gfey [ outIdx ] = _rtDW -> hcwmkho3rc [ outIdx +
colIdx ] ; _rtB -> codri4hxse [ outIdx ] = muDoubleScalarHypot ( _rtB ->
nalxf3gfey [ outIdx ] . re , _rtB -> nalxf3gfey [ outIdx ] . im ) ; _rtB ->
hkcgb3znhn [ outIdx ] = muDoubleScalarAtan2 ( _rtB -> nalxf3gfey [ outIdx ] .
im , _rtB -> nalxf3gfey [ outIdx ] . re ) ; if ( ( _rtB -> codri4hxse [
outIdx ] < 0.0 ) && ( bpm2x50q0i ( S ) -> dwszgjcae4 > muDoubleScalarFloor (
bpm2x50q0i ( S ) -> dwszgjcae4 ) ) ) { _rtB -> codri4hxse [ outIdx ] = -
muDoubleScalarPower ( - _rtB -> codri4hxse [ outIdx ] , bpm2x50q0i ( S ) ->
dwszgjcae4 ) ; } else { _rtB -> codri4hxse [ outIdx ] = muDoubleScalarPower (
_rtB -> codri4hxse [ outIdx ] , bpm2x50q0i ( S ) -> dwszgjcae4 ) ; } _rtB ->
hkcgb3znhn [ outIdx ] *= bpm2x50q0i ( S ) -> dwszgjcae4 ; } for ( i = 0 ; i <
2000 ; i ++ ) { _rtB -> dn1oradyre [ i ] . re = _rtB -> codri4hxse [ i ] *
muDoubleScalarCos ( _rtB -> hkcgb3znhn [ i ] ) ; _rtB -> dn1oradyre [ i ] .
im = _rtB -> codri4hxse [ i ] * muDoubleScalarSin ( _rtB -> hkcgb3znhn [ i ]
) ; } colIdx = 0 ; yIndx = 0 ; for ( outIdx = 0 ; outIdx < 2000 ; outIdx ++ )
{ _rtB -> bjq3or5z1a [ colIdx ] . re = _rtB -> dn1oradyre [ colIdx ] . re *
e2v1ycplzh . nw5ghsiw3f [ yIndx ] ; _rtB -> bjq3or5z1a [ colIdx ] . im = _rtB
-> dn1oradyre [ colIdx ] . im * e2v1ycplzh . nw5ghsiw3f [ yIndx ] ; colIdx ++
; yIndx ++ ; } if ( ! _rtDW -> nxszviaj5l ) { _rtDW -> nxszviaj5l = true ;
yIndx = 0 ; for ( outIdx = 0 ; outIdx < 2000 ; outIdx ++ ) { _rtB ->
b5qrq2ek5t [ yIndx ] = e2v1ycplzh . nw5ghsiw3f [ yIndx ] ; yIndx ++ ; } }
colIdx = 0 ; yIndx = 0 ; for ( inputIdx = 0 ; inputIdx < 2000 ; inputIdx ++ )
{ _rtDW -> nqaktjija2 [ yIndx ] = _rtB -> bjq3or5z1a [ colIdx ] ; colIdx ++ ;
yIndx ++ ; } for ( inputIdx = 0 ; inputIdx < 48 ; inputIdx ++ ) { _rtDW ->
nqaktjija2 [ yIndx ] . re = 0.0 ; _rtDW -> nqaktjija2 [ yIndx ] . im = 0.0 ;
yIndx ++ ; } LibOutputs_FFT ( & _rtDW -> jokx1xb3bh [ 0U ] , & _rtDW ->
nqaktjija2 [ 0U ] , & _rtB -> eu1sertuq3 [ 0U ] , 2048 , 1 ) ; for ( i = 0 ;
i < 2048 ; i ++ ) { u = _rtB -> eu1sertuq3 [ i ] ; _rtB -> hgeqeqmhuo [ i ] =
u . re * u . re + u . im * u . im ; } yIndx = 0 ; for ( k = 0 ; k < 2048 ; k
++ ) { bIndx = k * 15 ; acc_re = _rtB -> hgeqeqmhuo [ yIndx ] * 0.0625 ;
yIndx ++ ; for ( inputIdx = _rtDW -> fiiczidjvp ; inputIdx < 15 ; inputIdx ++
) { acc_re += _rtDW -> ku15jnyvvv [ bIndx + inputIdx ] * 0.0625 ; } for (
inputIdx = 0 ; inputIdx < _rtDW -> fiiczidjvp ; inputIdx ++ ) { acc_re +=
_rtDW -> ku15jnyvvv [ bIndx + inputIdx ] * 0.0625 ; } _rtB -> kkqeijf4ln [ k
] = acc_re ; } acc_re = 0.0 ; for ( i = 0 ; i < 2000 ; i ++ ) { acc_re +=
_rtB -> b5qrq2ek5t [ i ] * _rtB -> b5qrq2ek5t [ i ] ; } _rtB -> jsixd0aqqh =
acc_re ; _rtB -> jsixd0aqqh = 1.0 / _rtB -> jsixd0aqqh ; _rtB -> pj4se3hmdj =
_rtB -> nwbiicofdf [ 0 ] ; _rtB -> brxltyz1w5 = _rtB -> mvguwk0w0p [ 0 ] /
_rtB -> pj4se3hmdj ; for ( i = 0 ; i < 2048 ; i ++ ) { _rtB -> ics0xgodfx [ i
] = _rtB -> kkqeijf4ln [ i ] * _rtB -> jsixd0aqqh * _rtB -> brxltyz1w5 ; }
ssCallAccelRunBlock ( S , 2 , 0 , SS_CALL_MDL_OUTPUTS ) ; for ( colIdx = 0 ;
colIdx < 2000 ; colIdx ++ ) { acc_re = 6.2831853071795862 * _rtDW ->
dokkb2txvq ; _rtB -> jnloimfcwm [ colIdx ] . re = _rtB -> nalxf3gfey [ colIdx
] . re * muDoubleScalarCos ( acc_re ) - _rtB -> nalxf3gfey [ colIdx ] . im *
muDoubleScalarSin ( acc_re ) ; _rtB -> jnloimfcwm [ colIdx ] . im = _rtB ->
nalxf3gfey [ colIdx ] . re * muDoubleScalarSin ( acc_re ) + _rtB ->
nalxf3gfey [ colIdx ] . im * muDoubleScalarCos ( acc_re ) ; _rtDW ->
dokkb2txvq += _rtB -> dr2gewpcvf * 8.1380208333333332E-6 ; _rtDW ->
evou2zmaxu [ colIdx ] = _rtB -> jnloimfcwm [ colIdx ] ; } _rtDW -> pwia2n14w5
= 0 ; } _rtB -> io3uq30u1f = _rtDW -> evou2zmaxu [ _rtDW -> pwia2n14w5 ] ; if
( _rtDW -> pwia2n14w5 < 1999 ) { _rtDW -> pwia2n14w5 ++ ; } _rtB ->
ak1wgay351 = _rtDW -> aypx04losp ; ssCallAccelRunBlock ( S , 7 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> ovunafnqpe = 0.054538589548168773 * _rtB ->
nlmf1vdngz ; _rtB -> gck55arbwj = 0.00025171656714539426 * _rtB -> nlmf1vdngz
; _rtB -> haf45tn0p2 = _rtB -> gck55arbwj + _rtDW -> chpsf1n451 [ 0 ] ; _rtDW
-> chpsf1n451 [ 0 ] = ( 0.0 * _rtB -> gck55arbwj + _rtDW -> chpsf1n451 [ 1 ]
) - ( - _rtB -> haf45tn0p2 ) ; _rtB -> ovunafnqpe += _rtB -> haf45tn0p2 ;
_rtB -> haf45tn0p2 = 0.0 * _rtB -> ovunafnqpe + _rtDW -> ohezz01r45 [ 0 ] ;
_rtDW -> ohezz01r45 [ 0 ] = ( _rtDW -> ohezz01r45 [ 1 ] + _rtB -> ovunafnqpe
) - ( - _rtB -> haf45tn0p2 ) ; _rtB -> haf45tn0p2 = - _rtB -> haf45tn0p2 ;
_rtB -> khplo514p2 . re = muDoubleScalarCos ( _rtB -> haf45tn0p2 ) ; _rtB ->
khplo514p2 . im = muDoubleScalarSin ( _rtB -> haf45tn0p2 ) ; _rtB ->
e0ingmmaud . re = _rtB -> io3uq30u1f . re * _rtB -> khplo514p2 . re - _rtB ->
io3uq30u1f . im * _rtB -> khplo514p2 . im ; _rtB -> e0ingmmaud . im = _rtB ->
io3uq30u1f . re * _rtB -> khplo514p2 . im + _rtB -> io3uq30u1f . im * _rtB ->
khplo514p2 . re ; _rtB -> dzdalvmkmp = _rtDW -> kgzg2jwn4l ;
ssCallAccelRunBlock ( S , 10 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> bkloxxpigt
= _rtDW -> docqlqayyg ; if ( _rtB -> bkloxxpigt > 0.0 ) { _rtB -> gbl2rxrmhf
= _rtDW -> agavq4p3vh ; _rtB -> gyuduycjnv = 99U ; _rtB -> pjgrzsyyuv = (
_rtB -> gbl2rxrmhf == _rtB -> gyuduycjnv ) ; _rtB -> gbl2rxrmhf = ( uint8_T )
( ( uint32_T ) _rtB -> gbl2rxrmhf + bpm2x50q0i ( S ) -> calysbj0k5 ) ; if (
_rtB -> gbl2rxrmhf > 99 ) { _rtB -> ooaq5mpmag = bpm2x50q0i ( S ) ->
h3xmoqgurk ; } else { _rtB -> ooaq5mpmag = _rtB -> gbl2rxrmhf ; } if ( _rtB
-> pjgrzsyyuv == 1 ) { for ( k = 0 ; k < 99 - _rtDW -> l3jzsuexxk ; k ++ ) {
_rtB -> lvjvsb4ofy [ k ] = _rtDW -> lxqe51ydaq [ _rtDW -> l3jzsuexxk + k ] ;
} colIdx = 99 - _rtDW -> l3jzsuexxk ; for ( k = 0 ; k < _rtDW -> l3jzsuexxk ;
k ++ ) { _rtB -> lvjvsb4ofy [ colIdx + k ] = _rtDW -> lxqe51ydaq [ k ] ; }
_rtB -> lvjvsb4ofy [ 99 ] = _rtB -> klggkyjzci ; } srUpdateBC ( _rtDW ->
i40s4lmpxi ) ; } _rtB -> pbbuasuds4 = ( _rtB -> pjgrzsyyuv && ( _rtB ->
bkloxxpigt != 0.0 ) ) ; if ( _rtB -> pbbuasuds4 && ( _rtZCE -> aejzsszyyp !=
POS_ZCSIG ) ) { _rtB -> o40xsaghf3 = _rtDW -> nzbllf0hxr ; memcpy ( & _rtB ->
ok3avg4e51 [ 0 ] , & _rtDW -> olorscjf4k [ 0 ] , 100U * sizeof ( creal_T ) )
; for ( outIdx = 0 ; outIdx < 100 ; outIdx ++ ) { _rtB -> ok3avg4e51 [ outIdx
+ 100 ] = _rtB -> lvjvsb4ofy [ outIdx ] ; _rtDW -> olorscjf4k [ outIdx ] =
_rtB -> lvjvsb4ofy [ outIdx ] ; } ssCallAccelRunBlock ( S , 3 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; _rtB -> lbeejvkum1 = _rtDW -> glnwzafuqj ; _rtB ->
lbeejvkum1 = - _rtB -> lbeejvkum1 ; _rtB -> dlpoznpwho . re =
muDoubleScalarCos ( _rtB -> lbeejvkum1 ) ; _rtB -> dlpoznpwho . im =
muDoubleScalarSin ( _rtB -> lbeejvkum1 ) ; for ( i = 0 ; i < 100 ; i ++ ) {
_rtB -> ifmxkklwqt [ i ] . re = _rtB -> jvfl2mdnh3 [ i ] . re * _rtB ->
dlpoznpwho . re - _rtB -> jvfl2mdnh3 [ i ] . im * _rtB -> dlpoznpwho . im ;
_rtB -> ifmxkklwqt [ i ] . im = _rtB -> jvfl2mdnh3 [ i ] . re * _rtB ->
dlpoznpwho . im + _rtB -> jvfl2mdnh3 [ i ] . im * _rtB -> dlpoznpwho . re ; }
for ( iIdx = 0 ; iIdx < 100 ; iIdx ++ ) { if ( _rtB -> ifmxkklwqt [ iIdx ] .
re > 0.0 ) { if ( _rtB -> ifmxkklwqt [ iIdx ] . im >= 0.0 ) { colIdx = 0 ; }
else { colIdx = 3 ; } } else if ( _rtB -> ifmxkklwqt [ iIdx ] . re < 0.0 ) {
if ( _rtB -> ifmxkklwqt [ iIdx ] . im <= 0.0 ) { colIdx = 2 ; } else { colIdx
= 1 ; } } else if ( _rtB -> ifmxkklwqt [ iIdx ] . im < 0.0 ) { colIdx = 3 ; }
else if ( _rtB -> ifmxkklwqt [ iIdx ] . im > 0.0 ) { colIdx = 1 ; } else {
colIdx = 0 ; } colIdx ^= colIdx >> 1 ; _rtB -> pygz2zy54e [ ( iIdx << 1 ) + 1
] = colIdx % 2 ; colIdx >>= 1 ; _rtB -> pygz2zy54e [ iIdx << 1 ] = colIdx % 2
; } for ( inputIdx = 0 ; inputIdx < 174 ; inputIdx ++ ) { colIdx = ( int32_T
) muDoubleScalarFloor ( _rtB -> pygz2zy54e [ inputIdx + 26 ] ) ; if ( ( _rtB
-> pygz2zy54e [ inputIdx + 26 ] != ( int32_T ) muDoubleScalarFloor ( _rtB ->
pygz2zy54e [ inputIdx + 26 ] ) ) || ( ( int32_T ) muDoubleScalarFloor ( _rtB
-> pygz2zy54e [ inputIdx + 26 ] ) < 0 ) || ( ( int32_T ) muDoubleScalarFloor
( _rtB -> pygz2zy54e [ inputIdx + 26 ] ) >= 2 ) ) { rtErrorMsgID ( S ,
"comm:block:invalidScramblerInputValues" ) ; } colIdx -= ( uint8_T ) _rtDW ->
fubnsmx14z [ 0 ] ; colIdx -= ( uint8_T ) _rtDW -> fubnsmx14z [ 1 ] ; for (
colIdx -= ( uint8_T ) _rtDW -> fubnsmx14z [ 3 ] ; colIdx < 0 ; colIdx += 2 )
{ } colIdx %= 2 ; _rtB -> hwdf35j1kw [ inputIdx ] = colIdx ; _rtDW ->
fubnsmx14z [ 3 ] = _rtDW -> fubnsmx14z [ 2 ] ; _rtDW -> fubnsmx14z [ 2 ] =
_rtDW -> fubnsmx14z [ 1 ] ; _rtDW -> fubnsmx14z [ 1 ] = _rtDW -> fubnsmx14z [
0 ] ; _rtDW -> fubnsmx14z [ 0U ] = ( int32_T ) muDoubleScalarFloor ( _rtB ->
pygz2zy54e [ inputIdx + 26 ] ) ; } ssCallAccelRunBlock ( S , 4 , 0 ,
SS_CALL_MDL_OUTPUTS ) ; colIdx = 0 ; for ( outIdx = 0 ; outIdx < 112 ; outIdx
++ ) { i = outIdx - 99 ; inputIdx = muIntScalarMax_sint32 ( i , 0 ) + 99 ;
bIndx = muIntScalarMin_sint32 ( outIdx , 12 ) ; acc_re = bpm2x50q0i ( S ) ->
ekqx1fjtnq [ inputIdx - 99 ] . re * _rtB -> lvjvsb4ofy [ inputIdx - outIdx ]
. re - bpm2x50q0i ( S ) -> ekqx1fjtnq [ inputIdx - 99 ] . im * - _rtB ->
lvjvsb4ofy [ inputIdx - outIdx ] . im ; acc_im = bpm2x50q0i ( S ) ->
ekqx1fjtnq [ inputIdx - 99 ] . re * - _rtB -> lvjvsb4ofy [ inputIdx - outIdx
] . im + bpm2x50q0i ( S ) -> ekqx1fjtnq [ inputIdx - 99 ] . im * _rtB ->
lvjvsb4ofy [ inputIdx - outIdx ] . re ; for ( inputIdx ++ ; inputIdx - 99 <=
bIndx ; inputIdx ++ ) { acc_re += bpm2x50q0i ( S ) -> ekqx1fjtnq [ inputIdx -
99 ] . re * _rtB -> lvjvsb4ofy [ inputIdx - outIdx ] . re - bpm2x50q0i ( S )
-> ekqx1fjtnq [ inputIdx - 99 ] . im * - _rtB -> lvjvsb4ofy [ inputIdx -
outIdx ] . im ; acc_im += bpm2x50q0i ( S ) -> ekqx1fjtnq [ inputIdx - 99 ] .
re * - _rtB -> lvjvsb4ofy [ inputIdx - outIdx ] . im + bpm2x50q0i ( S ) ->
ekqx1fjtnq [ inputIdx - 99 ] . im * _rtB -> lvjvsb4ofy [ inputIdx - outIdx ]
. re ; } _rtB -> hisc2cuf5w [ colIdx ] . re = acc_re ; _rtB -> hisc2cuf5w [
colIdx ] . im = acc_im ; colIdx ++ ; } for ( i = 0 ; i < 112 ; i ++ ) { _rtB
-> f0ctagntxc [ i ] = muDoubleScalarHypot ( _rtB -> hisc2cuf5w [ i ] . re ,
_rtB -> hisc2cuf5w [ i ] . im ) ; } colIdx = 1 ; _rtDW -> oxrc00i2lo = _rtB
-> f0ctagntxc [ 0 ] ; _rtB -> lbeejvkum1 = 1.0 ; for ( inputIdx = 0 ;
inputIdx < 111 ; inputIdx ++ ) { if ( _rtB -> f0ctagntxc [ colIdx ] > _rtDW
-> oxrc00i2lo ) { _rtDW -> oxrc00i2lo = _rtB -> f0ctagntxc [ colIdx ] ; _rtB
-> lbeejvkum1 = ( real_T ) inputIdx + 2.0 ; } colIdx ++ ; } _rtB ->
lbeejvkum1 = _rtB -> a5zkq5ztwi - _rtB -> lbeejvkum1 ; _rtB -> k5nvcxcsz1 =
_rtB -> a5zkq5ztwi - bpm2x50q0i ( S ) -> kq51p1ghq1 ; _rtB -> kun2hwxmxk =
muDoubleScalarMod ( _rtB -> lbeejvkum1 , _rtB -> k5nvcxcsz1 ) ; for ( i = 0 ;
i < 13 ; i ++ ) { _rtB -> mg53wagwfq [ i ] . re = bpm2x50q0i ( S ) ->
fiaclz4ezo [ i ] . re * _rtB -> jvfl2mdnh3 [ i ] . re - bpm2x50q0i ( S ) ->
fiaclz4ezo [ i ] . im * _rtB -> jvfl2mdnh3 [ i ] . im ; _rtB -> mg53wagwfq [
i ] . im = bpm2x50q0i ( S ) -> fiaclz4ezo [ i ] . re * _rtB -> jvfl2mdnh3 [ i
] . im + bpm2x50q0i ( S ) -> fiaclz4ezo [ i ] . im * _rtB -> jvfl2mdnh3 [ i ]
. re ; } for ( outIdx = 0 ; outIdx < 13 ; outIdx += 13 ) { for ( inputIdx =
outIdx ; inputIdx < outIdx + 1 ; inputIdx ++ ) { _rtDW -> lkd2d4gjgj = _rtB
-> mg53wagwfq [ inputIdx ] ; colIdx = 1 ; for ( yIndx = 11 ; yIndx >= 0 ;
yIndx += - 1 ) { _rtDW -> lkd2d4gjgj . re += _rtB -> mg53wagwfq [ inputIdx +
colIdx ] . re ; _rtDW -> lkd2d4gjgj . im += _rtB -> mg53wagwfq [ inputIdx +
colIdx ] . im ; colIdx ++ ; } _rtB -> dlpoznpwho . re = _rtDW -> lkd2d4gjgj .
re / 13.0 ; _rtB -> dlpoznpwho . im = _rtDW -> lkd2d4gjgj . im / 13.0 ; } }
_rtB -> inysglzztr = muDoubleScalarAtan2 ( _rtB -> dlpoznpwho . im , _rtB ->
dlpoznpwho . re ) ; ssCallAccelRunBlock ( S , 5 , 0 , SS_CALL_MDL_OUTPUTS ) ;
_rtDW -> nzbllf0hxr = _rtB -> kun2hwxmxk ; _rtDW -> glnwzafuqj = _rtB ->
k0vgiw3ilu ; _rtDW -> nygb5goqlu = 4 ; } _rtZCE -> aejzsszyyp = ( uint8_T ) (
_rtB -> pbbuasuds4 ? ( int32_T ) POS_ZCSIG : ( int32_T ) ZERO_ZCSIG ) ;
ssCallAccelRunBlock ( S , 12 , 0 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> haf45tn0p2
= - 1.1757602906949745E-5 * _rtB -> ahq4fkon4u ; _rtB -> gck55arbwj = _rtB ->
haf45tn0p2 + _rtDW -> kv4djvtzox [ 0 ] ; _rtDW -> kv4djvtzox [ 0 ] = ( 0.0 *
_rtB -> haf45tn0p2 + _rtDW -> kv4djvtzox [ 1 ] ) - ( - _rtB -> gck55arbwj ) ;
_rtB -> haf45tn0p2 = - 0.0029394007267374359 * _rtB -> ahq4fkon4u ; _rtB ->
mar44ysg0v = _rtB -> haf45tn0p2 + _rtB -> gck55arbwj ; ssCallAccelRunBlock (
S , 11 , 0 , SS_CALL_MDL_OUTPUTS ) ; srUpdateBC ( _rtDW -> h5bcihfbod ) ; }
UNUSED_PARAMETER ( tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { int32_T k ; nudldxhedl
* _rtB ; oqnsh5busn * _rtDW ; _rtDW = ( ( oqnsh5busn * ) ssGetRootDWork ( S )
) ; _rtB = ( ( nudldxhedl * ) _ssGetBlockIO ( S ) ) ; if ( _rtDW ->
n4ya3v4vfc ) { if ( ssIsSampleHit ( S , 1 , 0 ) ) { _rtDW -> fiiczidjvp -- ;
if ( _rtDW -> fiiczidjvp < 0 ) { _rtDW -> fiiczidjvp = 14 ; } for ( k = 0 ; k
< 2048 ; k ++ ) { _rtDW -> ku15jnyvvv [ _rtDW -> fiiczidjvp + k * 15 ] = _rtB
-> hgeqeqmhuo [ k ] ; } } _rtDW -> aypx04losp = _rtB -> e0ingmmaud ; _rtDW ->
kgzg2jwn4l = _rtB -> byczgcxgbm ; _rtDW -> docqlqayyg = _rtB -> ft51lilepa ;
if ( _rtB -> bkloxxpigt > 0.0 ) { _rtDW -> agavq4p3vh = _rtB -> ooaq5mpmag ;
_rtDW -> lxqe51ydaq [ _rtDW -> l3jzsuexxk ] = _rtB -> klggkyjzci ; _rtDW ->
l3jzsuexxk ++ ; while ( _rtDW -> l3jzsuexxk >= 99 ) { _rtDW -> l3jzsuexxk -=
99 ; } } } UNUSED_PARAMETER ( tid ) ; } static void mdlInitializeSizes (
SimStruct * S ) { ssSetChecksumVal ( S , 0 , 1041954985U ) ; ssSetChecksumVal
( S , 1 , 3614631508U ) ; ssSetChecksumVal ( S , 2 , 493262255U ) ;
ssSetChecksumVal ( S , 3 , 2447309135U ) ; { mxArray * slVerStructMat = NULL
; mxArray * slStrMat = mxCreateString ( "simulink" ) ; char slVerChar [ 10 ]
; int status = mexCallMATLAB ( 1 , & slVerStructMat , 1 , & slStrMat , "ver"
) ; if ( status == 0 ) { mxArray * slVerMat = mxGetField ( slVerStructMat , 0
, "Version" ) ; if ( slVerMat == NULL ) { status = 1 ; } else { status =
mxGetString ( slVerMat , slVerChar , 10 ) ; } } mxDestroyArray ( slStrMat ) ;
mxDestroyArray ( slVerStructMat ) ; if ( ( status == 1 ) || ( strcmp (
slVerChar , "8.4" ) != 0 ) ) { return ; } } ssSetOptions ( S ,
SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork ( S ) != sizeof (
oqnsh5busn ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( nudldxhedl ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } _ssSetConstBlockIO ( S , &
n5nqudfjwt ) ; rt_InitInfAndNaN ( sizeof ( real_T ) ) ; } static void
mdlInitializeSampleTimes ( SimStruct * S ) { { SimStruct * childS ;
SysOutputFcn * callSysFcns ; childS = ssGetSFunction ( S , 0 ) ; callSysFcns
= ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 1 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 2 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 3 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 4 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 5 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 6 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 7 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; } } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
