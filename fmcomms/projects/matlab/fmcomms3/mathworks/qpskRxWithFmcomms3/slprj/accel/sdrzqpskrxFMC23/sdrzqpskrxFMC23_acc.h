#include "__cf_sdrzqpskrxFMC23.h"
#ifndef RTW_HEADER_sdrzqpskrxFMC23_acc_h_
#define RTW_HEADER_sdrzqpskrxFMC23_acc_h_
#include <stddef.h>
#include <string.h>
#ifndef sdrzqpskrxFMC23_acc_COMMON_INCLUDES_
#define sdrzqpskrxFMC23_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "HostLib_FFT.h"
#endif
#include "sdrzqpskrxFMC23_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_size_check.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
typedef struct { creal_T ahww1v0n4g [ 4000 ] ; creal_T nalxf3gfey [ 2000 ] ;
creal_T dn1oradyre [ 2000 ] ; creal_T jnloimfcwm [ 2000 ] ; creal_T
ak1wgay351 ; creal_T e0ingmmaud ; creal_T klggkyjzci ; creal_T lvjvsb4ofy [
100 ] ; creal_T ok3avg4e51 [ 200 ] ; creal_T jvfl2mdnh3 [ 100 ] ; creal_T
nzb31igwj1 [ 4000 ] ; creal_T bikpevkecr [ 8000 ] ; creal_T ads1giluje [ 4000
] ; creal_T bjq3or5z1a [ 2000 ] ; creal_T eu1sertuq3 [ 2048 ] ; creal_T
io3uq30u1f ; creal_T khplo514p2 ; creal_T ifmxkklwqt [ 100 ] ; creal_T
hisc2cuf5w [ 112 ] ; creal_T mg53wagwfq [ 13 ] ; creal_T dlpoznpwho ; real_T
epwntxzsqn ; real_T bxihixhq5k [ 2 ] ; real_T b5qrq2ek5t [ 2000 ] ; real_T
hgeqeqmhuo [ 2048 ] ; real_T mvguwk0w0p [ 2 ] ; real_T nwbiicofdf [ 2 ] ;
real_T pj4se3hmdj ; real_T ics0xgodfx [ 2048 ] ; real_T dzdalvmkmp ; real_T
bkloxxpigt ; real_T mar44ysg0v ; real_T ahq4fkon4u ; real_T ft51lilepa ;
real_T byczgcxgbm ; real_T nlmf1vdngz ; real_T o40xsaghf3 ; real_T hwdf35j1kw
[ 174 ] ; real_T a5zkq5ztwi ; real_T kun2hwxmxk ; real_T inysglzztr ; real_T
k0vgiw3ilu ; real_T dr2gewpcvf ; real_T c33d2e3yfp ; real_T kkqeijf4ln [ 2048
] ; real_T brxltyz1w5 ; real_T pygz2zy54e [ 200 ] ; real_T f0ctagntxc [ 112 ]
; real_T k5nvcxcsz1 ; real_T codri4hxse [ 2000 ] ; real_T hkcgb3znhn [ 2000 ]
; real_T jsixd0aqqh ; real_T ovunafnqpe ; real_T gck55arbwj ; real_T
lbeejvkum1 ; real_T haf45tn0p2 ; uint8_T ooaq5mpmag ; uint8_T gyuduycjnv ;
uint8_T gbl2rxrmhf ; boolean_T pbbuasuds4 ; boolean_T pjgrzsyyuv ; char
pad_pjgrzsyyuv [ 3 ] ; } nudldxhedl ; typedef struct { creal_T hcwmkho3rc [
8000 ] ; creal_T laivtoatrs ; creal_T iun0seygxz [ 42 ] ; creal_T evou2zmaxu
[ 2000 ] ; creal_T aypx04losp ; creal_T lxqe51ydaq [ 99 ] ; creal_T
olorscjf4k [ 100 ] ; real_T ku15jnyvvv [ 30720 ] ; real_T dokkb2txvq ; real_T
chpsf1n451 [ 2 ] ; real_T ohezz01r45 [ 2 ] ; real_T kgzg2jwn4l ; real_T
docqlqayyg ; real_T kv4djvtzox [ 2 ] ; real_T nzbllf0hxr ; real_T glnwzafuqj
; real_T oxrc00i2lo ; real_T fpkgcmbqv4 ; creal_T nqaktjija2 [ 2048 ] ;
creal_T fdmx0c5t14 [ 13 ] ; creal_T lkd2d4gjgj ; real_T jokx1xb3bh [ 137 ] ;
void * czcysvtxpt ; void * pgs0gq1t0f ; int32_T haswajj2w4 ; int32_T
f5gdbfik3w ; int32_T pxl1p3dxjh ; int32_T mdlidvs5hy ; int32_T fiiczidjvp ;
int32_T pwia2n14w5 ; int32_T l3jzsuexxk ; int32_T fubnsmx14z [ 4 ] ; uint8_T
agavq4p3vh ; boolean_T pgzs3loahd ; boolean_T i4jebm2wpn ; int8_T h5bcihfbod
; int8_T nygb5goqlu ; int8_T i40s4lmpxi ; boolean_T nxszviaj5l ; boolean_T
n4ya3v4vfc ; char pad_n4ya3v4vfc [ 4 ] ; } oqnsh5busn ; typedef struct {
ZCSigState aejzsszyyp ; } cszlzfj2xv ; typedef struct { const creal_T
ekqx1fjtnq [ 13 ] ; const creal_T fiaclz4ezo [ 13 ] ; const real_T dwszgjcae4
; const real_T a3qldfyxm4 [ 13 ] ; const real_T pkghstvqqr [ 26 ] ; const
real_T kq51p1ghq1 ; const uint8_T calysbj0k5 ; const uint8_T h3xmoqgurk ;
const boolean_T pgd4c5zevn [ 13 ] ; char pad_pgd4c5zevn ; } fg5gflrjx1 ;
#define bpm2x50q0i(S) ((fg5gflrjx1 *) _ssGetConstBlockIO(S))
typedef struct { real_T bk111vg5ck ; real_T bkgolg1tzk [ 2 ] ; real_T
gx3hidcdil ; real_T biuqu1x3vf ; real_T h01rwfxf5s ; real_T gzrx50zh1a ;
real_T jrjgcje5zx ; real_T a15rlv12uv [ 8 ] ; real_T fr2ogmye4u ; real_T
j1sydq5thw [ 42 ] ; real_T htqj012fhi ; real_T nw5ghsiw3f [ 2000 ] ; real_T
bttingum52 [ 16 ] ; real_T a5vwmbf1bn ; real_T feqf2pyhcu ; real_T gv3yn4i2xg
[ 2 ] ; real_T ggbasqgmwl [ 2 ] ; real_T fdys3t1zkh [ 2 ] ; real_T jq5sbvnutl
; real_T omhahut034 ; creal_T eojyu31gqt ; int32_T dd2dvsjivb [ 4 ] ;
uint32_T a1gjgcwtgc ; uint8_T dgmr2amzjq [ 5 ] ; uint8_T kffqotnhvg ; uint8_T
bmeoriusj2 ; uint8_T augkohrpnk ; boolean_T fvcuzx24oc [ 13 ] ; char
pad_fvcuzx24oc [ 7 ] ; } e1pkwbr5pe ; extern const fg5gflrjx1 n5nqudfjwt ;
extern const e1pkwbr5pe e2v1ycplzh ;
#endif
