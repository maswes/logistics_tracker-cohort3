#include "__cf_sdrzqpskrxFMC23.h"
#ifndef RTW_HEADER_sdrzqpskrxFMC23_acc_types_h_
#define RTW_HEADER_sdrzqpskrxFMC23_acc_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef _DEFINED_TYPEDEF_FOR_struct_HK8f1MKh4H6RmcHm8ycpYE_
#define _DEFINED_TYPEDEF_FOR_struct_HK8f1MKh4H6RmcHm8ycpYE_
typedef struct { real_T RadioCenterFrequency ; real_T RadioFrontEndSampleRate
; real_T RadioFrontEndSamplePeriod ; real_T M ; real_T Upsampling ; real_T
Downsampling ; real_T Fs ; real_T Ts ; real_T FrameSize ; real_T BarkerLength
; real_T DataLength ; real_T MsgLength ; real_T RxBufferedFrames ; real_T
RCFiltSpan ; real_T RadioFrameSize ; real_T FineFreqPEDGain ; real_T
FineFreqCompensateGain ; real_T TimingRecTEDGain ; real_T
TimingRecCompensateGain ; } struct_HK8f1MKh4H6RmcHm8ycpYE ;
#endif
#endif
