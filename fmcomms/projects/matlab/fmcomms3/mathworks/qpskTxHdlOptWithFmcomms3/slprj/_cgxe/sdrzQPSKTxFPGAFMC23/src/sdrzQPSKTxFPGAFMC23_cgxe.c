/* Include files */

#include "sdrzQPSKTxFPGAFMC23_cgxe.h"
#include "m_WHFSDynsCloTPoQdGrWZLE.h"

static unsigned int cgxeModelInitialized = 0;
emlrtContext emlrtContextGlobal = { true, true, EMLRT_VERSION_INFO, NULL, "",
  NULL, false, { 0, 0, 0, 0 }, NULL };

void *emlrtRootTLSGlobal = NULL;
char cgxeRtErrBuf[4096];

/* CGXE Glue Code */
void cgxe_sdrzQPSKTxFPGAFMC23_initializer(void)
{
  if (cgxeModelInitialized == 0) {
    cgxeModelInitialized = 1;
    emlrtRootTLSGlobal = NULL;
    emlrtCreateSimulinkRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1,
      false, 0);
  }
}

void cgxe_sdrzQPSKTxFPGAFMC23_terminator(void)
{
  if (cgxeModelInitialized != 0) {
    cgxeModelInitialized = 0;
    emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
    emlrtRootTLSGlobal = NULL;
  }
}

unsigned int cgxe_sdrzQPSKTxFPGAFMC23_method_dispatcher(SimStruct* S, int_T
  method, void* data)
{
  if (ssGetChecksum0(S) == 3990513312 &&
      ssGetChecksum1(S) == 2673992604 &&
      ssGetChecksum2(S) == 346806757 &&
      ssGetChecksum3(S) == 90503639) {
    method_dispatcher_WHFSDynsCloTPoQdGrWZLE(S, method, data);
    return 1;
  }

  return 0;
}

int cgxe_sdrzQPSKTxFPGAFMC23_autoInfer_dispatcher(const mxArray* prhs, mxArray*
  lhs[], const char* commandName)
{
  char sid[64];
  mxGetString(prhs,sid, sizeof(sid)/sizeof(char));
  sid[(sizeof(sid)/sizeof(char)-1)] = '\0';
  if (strcmp(sid, "sdrzQPSKTxFPGAFMC23:6683") == 0 ) {
    return autoInfer_dispatcher_WHFSDynsCloTPoQdGrWZLE(lhs, commandName);
  }

  return 0;
}
