set_property SRC_FILE_INFO {cfile:c:/capstone/projects/pl/cap04/cap04.srcs/sources_1/bd/system/ip/system_sys_ps7_0/system_sys_ps7_0.xdc rfile:../../../cap04.srcs/sources_1/bd/system/ip/system_sys_ps7_0/system_sys_ps7_0.xdc id:1 order:EARLY scoped_inst:i_system_wrapper/system_i/sys_ps7/inst} [current_design]
set_property SRC_FILE_INFO {cfile:c:/capstone/projects/pl/cap04/cap04.srcs/sources_1/bd/system/ip/system_sys_audio_clkgen_0/system_sys_audio_clkgen_0.xdc rfile:../../../cap04.srcs/sources_1/bd/system/ip/system_sys_audio_clkgen_0/system_sys_audio_clkgen_0.xdc id:2 order:EARLY scoped_inst:i_system_wrapper/system_i/sys_audio_clkgen/inst} [current_design]
set_property SRC_FILE_INFO {cfile:c:/capstone/projects/pl/cap04/cap04.srcs/sources_1/bd/system/ip/system_wfifo_mem_0/system_wfifo_mem_0/system_wfifo_mem_0_clocks.xdc rfile:../../../cap04.srcs/sources_1/bd/system/ip/system_wfifo_mem_0/system_wfifo_mem_0/system_wfifo_mem_0_clocks.xdc id:3 order:LATE scoped_inst:i_system_wrapper/system_i/sys_wfifo/wfifo_mem/U0} [current_design]
set_property src_info {type:SCOPED_XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_1 0.15
set_property src_info {type:SCOPED_XDC file:1 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_0 0.3
set_property src_info {type:SCOPED_XDC file:2 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter [get_clocks -of_objects [get_ports clk_in1]] 0.05
set_property src_info {type:SCOPED_XDC file:3 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_max_delay -from [get_cells inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/rd_pntr_gc_reg[*]] -to [get_cells inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/gsync_stage[*].wr_stg_inst/Q_reg_reg[*]] -datapath_only [get_property -min PERIOD [get_clocks -of_objects [get_pins i_system_wrapper/system_i/sys_wfifo/wfifo_mem/U0/rd_clk]]]
set_property src_info {type:SCOPED_XDC file:3 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_max_delay -from [get_cells inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/wr_pntr_gc_reg[*]] -to [get_cells inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gcx.clkx/gsync_stage[*].rd_stg_inst/Q_reg_reg[*]] -datapath_only [get_property -min PERIOD [get_clocks -of_objects [get_pins i_system_wrapper/system_i/sys_wfifo/wfifo_mem/U0/wr_clk]]]
