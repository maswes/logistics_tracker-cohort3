//Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2014.2 (win64) Build 932637 Wed Jun 11 13:33:10 MDT 2014
//Date        : Sun Mar 08 13:09:22 2015
//Host        : brian-P75 running 64-bit major release  (build 9200)
//Command     : generate_target system_wrapper.bd
//Design      : system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    GPIO_I,
    GPIO_O,
    GPIO_T,
    ad9122_dma_irq,
    ad9643_dma_irq,
    adc_clk,
    adc_clk_in_n,
    adc_clk_in_p,
    adc_data_0,
    adc_data_1,
    adc_data_in_n,
    adc_data_in_p,
    adc_dma_sync,
    adc_dma_wdata,
    adc_dma_wr,
    adc_enable_0,
    adc_enable_1,
    adc_or_in_n,
    adc_or_in_p,
    adc_valid_0,
    adc_valid_1,
    dac_clk,
    dac_clk_in_n,
    dac_clk_in_p,
    dac_clk_out_n,
    dac_clk_out_p,
    dac_data_out_n,
    dac_data_out_p,
    dac_ddata_0,
    dac_ddata_1,
    dac_dma_rd,
    dac_dma_rdata,
    dac_enable_0,
    dac_enable_1,
    dac_frame_out_n,
    dac_frame_out_p,
    dac_valid_0,
    dac_valid_1,
    hdmi_data,
    hdmi_data_e,
    hdmi_hsync,
    hdmi_out_clk,
    hdmi_vsync,
    i2s_bclk,
    i2s_lrclk,
    i2s_mclk,
    i2s_sdata_in,
    i2s_sdata_out,
    iic_fmc_intr,
    iic_fmc_scl_io,
    iic_fmc_sda_io,
    iic_mux_scl_I,
    iic_mux_scl_O,
    iic_mux_scl_T,
    iic_mux_sda_I,
    iic_mux_sda_O,
    iic_mux_sda_T,
    otg_vbusoc,
    ps_intr_0,
    ps_intr_1,
    ps_intr_10,
    ps_intr_11,
    ps_intr_12,
    ps_intr_13,
    ps_intr_2,
    ps_intr_3,
    ps_intr_4,
    ps_intr_5,
    ps_intr_6,
    ps_intr_7,
    ps_intr_8,
    ps_intr_9,
    ref_clk,
    spdif);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input [31:0]GPIO_I;
  output [31:0]GPIO_O;
  output [31:0]GPIO_T;
  output ad9122_dma_irq;
  output ad9643_dma_irq;
  output adc_clk;
  input adc_clk_in_n;
  input adc_clk_in_p;
  output [15:0]adc_data_0;
  output [15:0]adc_data_1;
  input [13:0]adc_data_in_n;
  input [13:0]adc_data_in_p;
  input adc_dma_sync;
  input [31:0]adc_dma_wdata;
  input adc_dma_wr;
  output adc_enable_0;
  output adc_enable_1;
  input adc_or_in_n;
  input adc_or_in_p;
  output adc_valid_0;
  output adc_valid_1;
  output dac_clk;
  input dac_clk_in_n;
  input dac_clk_in_p;
  output dac_clk_out_n;
  output dac_clk_out_p;
  output [15:0]dac_data_out_n;
  output [15:0]dac_data_out_p;
  input [63:0]dac_ddata_0;
  input [63:0]dac_ddata_1;
  input dac_dma_rd;
  output [63:0]dac_dma_rdata;
  output dac_enable_0;
  output dac_enable_1;
  output dac_frame_out_n;
  output dac_frame_out_p;
  output dac_valid_0;
  output dac_valid_1;
  output [15:0]hdmi_data;
  output hdmi_data_e;
  output hdmi_hsync;
  output hdmi_out_clk;
  output hdmi_vsync;
  output [0:0]i2s_bclk;
  output [0:0]i2s_lrclk;
  output i2s_mclk;
  input i2s_sdata_in;
  output [0:0]i2s_sdata_out;
  output iic_fmc_intr;
  inout iic_fmc_scl_io;
  inout iic_fmc_sda_io;
  input [1:0]iic_mux_scl_I;
  output [1:0]iic_mux_scl_O;
  output iic_mux_scl_T;
  input [1:0]iic_mux_sda_I;
  output [1:0]iic_mux_sda_O;
  output iic_mux_sda_T;
  input otg_vbusoc;
  input ps_intr_0;
  input ps_intr_1;
  input ps_intr_10;
  input ps_intr_11;
  input ps_intr_12;
  input ps_intr_13;
  input ps_intr_2;
  input ps_intr_3;
  input ps_intr_4;
  input ps_intr_5;
  input ps_intr_6;
  input ps_intr_7;
  input ps_intr_8;
  input ps_intr_9;
  output ref_clk;
  output spdif;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire [31:0]GPIO_I;
  wire [31:0]GPIO_O;
  wire [31:0]GPIO_T;
  wire ad9122_dma_irq;
  wire ad9643_dma_irq;
  wire adc_clk;
  wire adc_clk_in_n;
  wire adc_clk_in_p;
  wire [15:0]adc_data_0;
  wire [15:0]adc_data_1;
  wire [13:0]adc_data_in_n;
  wire [13:0]adc_data_in_p;
  wire adc_dma_sync;
  wire [31:0]adc_dma_wdata;
  wire adc_dma_wr;
  wire adc_enable_0;
  wire adc_enable_1;
  wire adc_or_in_n;
  wire adc_or_in_p;
  wire adc_valid_0;
  wire adc_valid_1;
  wire dac_clk;
  wire dac_clk_in_n;
  wire dac_clk_in_p;
  wire dac_clk_out_n;
  wire dac_clk_out_p;
  wire [15:0]dac_data_out_n;
  wire [15:0]dac_data_out_p;
  wire [63:0]dac_ddata_0;
  wire [63:0]dac_ddata_1;
  wire dac_dma_rd;
  wire [63:0]dac_dma_rdata;
  wire dac_enable_0;
  wire dac_enable_1;
  wire dac_frame_out_n;
  wire dac_frame_out_p;
  wire dac_valid_0;
  wire dac_valid_1;
  wire [15:0]hdmi_data;
  wire hdmi_data_e;
  wire hdmi_hsync;
  wire hdmi_out_clk;
  wire hdmi_vsync;
  wire [0:0]i2s_bclk;
  wire [0:0]i2s_lrclk;
  wire i2s_mclk;
  wire i2s_sdata_in;
  wire [0:0]i2s_sdata_out;
  wire iic_fmc_intr;
  wire iic_fmc_scl_i;
  wire iic_fmc_scl_io;
  wire iic_fmc_scl_o;
  wire iic_fmc_scl_t;
  wire iic_fmc_sda_i;
  wire iic_fmc_sda_io;
  wire iic_fmc_sda_o;
  wire iic_fmc_sda_t;
  wire [1:0]iic_mux_scl_I;
  wire [1:0]iic_mux_scl_O;
  wire iic_mux_scl_T;
  wire [1:0]iic_mux_sda_I;
  wire [1:0]iic_mux_sda_O;
  wire iic_mux_sda_T;
  wire otg_vbusoc;
  wire ps_intr_0;
  wire ps_intr_1;
  wire ps_intr_10;
  wire ps_intr_11;
  wire ps_intr_12;
  wire ps_intr_13;
  wire ps_intr_2;
  wire ps_intr_3;
  wire ps_intr_4;
  wire ps_intr_5;
  wire ps_intr_6;
  wire ps_intr_7;
  wire ps_intr_8;
  wire ps_intr_9;
  wire ref_clk;
  wire spdif;

IOBUF iic_fmc_scl_iobuf
       (.I(iic_fmc_scl_o),
        .IO(iic_fmc_scl_io),
        .O(iic_fmc_scl_i),
        .T(iic_fmc_scl_t));
IOBUF iic_fmc_sda_iobuf
       (.I(iic_fmc_sda_o),
        .IO(iic_fmc_sda_io),
        .O(iic_fmc_sda_i),
        .T(iic_fmc_sda_t));
system system_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .GPIO_I(GPIO_I),
        .GPIO_O(GPIO_O),
        .GPIO_T(GPIO_T),
        .IIC_FMC_scl_i(iic_fmc_scl_i),
        .IIC_FMC_scl_o(iic_fmc_scl_o),
        .IIC_FMC_scl_t(iic_fmc_scl_t),
        .IIC_FMC_sda_i(iic_fmc_sda_i),
        .IIC_FMC_sda_o(iic_fmc_sda_o),
        .IIC_FMC_sda_t(iic_fmc_sda_t),
        .ad9122_dma_irq(ad9122_dma_irq),
        .ad9643_dma_irq(ad9643_dma_irq),
        .adc_clk(adc_clk),
        .adc_clk_in_n(adc_clk_in_n),
        .adc_clk_in_p(adc_clk_in_p),
        .adc_data_0(adc_data_0),
        .adc_data_1(adc_data_1),
        .adc_data_in_n(adc_data_in_n),
        .adc_data_in_p(adc_data_in_p),
        .adc_dma_sync(adc_dma_sync),
        .adc_dma_wdata(adc_dma_wdata),
        .adc_dma_wr(adc_dma_wr),
        .adc_enable_0(adc_enable_0),
        .adc_enable_1(adc_enable_1),
        .adc_or_in_n(adc_or_in_n),
        .adc_or_in_p(adc_or_in_p),
        .adc_valid_0(adc_valid_0),
        .adc_valid_1(adc_valid_1),
        .dac_clk(dac_clk),
        .dac_clk_in_n(dac_clk_in_n),
        .dac_clk_in_p(dac_clk_in_p),
        .dac_clk_out_n(dac_clk_out_n),
        .dac_clk_out_p(dac_clk_out_p),
        .dac_data_out_n(dac_data_out_n),
        .dac_data_out_p(dac_data_out_p),
        .dac_ddata_0(dac_ddata_0),
        .dac_ddata_1(dac_ddata_1),
        .dac_dma_rd(dac_dma_rd),
        .dac_dma_rdata(dac_dma_rdata),
        .dac_enable_0(dac_enable_0),
        .dac_enable_1(dac_enable_1),
        .dac_frame_out_n(dac_frame_out_n),
        .dac_frame_out_p(dac_frame_out_p),
        .dac_valid_0(dac_valid_0),
        .dac_valid_1(dac_valid_1),
        .hdmi_data(hdmi_data),
        .hdmi_data_e(hdmi_data_e),
        .hdmi_hsync(hdmi_hsync),
        .hdmi_out_clk(hdmi_out_clk),
        .hdmi_vsync(hdmi_vsync),
        .i2s_bclk(i2s_bclk),
        .i2s_lrclk(i2s_lrclk),
        .i2s_mclk(i2s_mclk),
        .i2s_sdata_in(i2s_sdata_in),
        .i2s_sdata_out(i2s_sdata_out),
        .iic_fmc_intr(iic_fmc_intr),
        .iic_mux_scl_I(iic_mux_scl_I),
        .iic_mux_scl_O(iic_mux_scl_O),
        .iic_mux_scl_T(iic_mux_scl_T),
        .iic_mux_sda_I(iic_mux_sda_I),
        .iic_mux_sda_O(iic_mux_sda_O),
        .iic_mux_sda_T(iic_mux_sda_T),
        .otg_vbusoc(otg_vbusoc),
        .ps_intr_0(ps_intr_0),
        .ps_intr_1(ps_intr_1),
        .ps_intr_10(ps_intr_10),
        .ps_intr_11(ps_intr_11),
        .ps_intr_12(ps_intr_12),
        .ps_intr_13(ps_intr_13),
        .ps_intr_2(ps_intr_2),
        .ps_intr_3(ps_intr_3),
        .ps_intr_4(ps_intr_4),
        .ps_intr_5(ps_intr_5),
        .ps_intr_6(ps_intr_6),
        .ps_intr_7(ps_intr_7),
        .ps_intr_8(ps_intr_8),
        .ps_intr_9(ps_intr_9),
        .ref_clk(ref_clk),
        .spdif(spdif));
endmodule
