
l
Command: %s
1870*	planAhead2:
&open_checkpoint axi_i2s_adi_routed.dcp2default:defaultZ12-2866h px
c
-Analyzing %s Unisim elements for replacement
17*netlist2
952default:defaultZ29-17h px
g
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px
u
Netlist was created with %s %s291*project2
Vivado2default:default2
2014.22default:defaultZ1-479h px
�
Loading clock regions from %s
13*device2_
KC:/Xilinx/Vivado/2014.2/data\parts/xilinx/zynq/zynq/xc7z020/ClockRegion.xml2default:defaultZ21-13h px
�
Loading clock buffers from %s
11*device2`
LC:/Xilinx/Vivado/2014.2/data\parts/xilinx/zynq/zynq/xc7z020/ClockBuffers.xml2default:defaultZ21-11h px
�
&Loading clock placement rules from %s
318*place2W
CC:/Xilinx/Vivado/2014.2/data/parts/xilinx/zynq/ClockPlacerRules.xml2default:defaultZ30-318h px
�
)Loading package pin functions from %s...
17*device2S
?C:/Xilinx/Vivado/2014.2/data\parts/xilinx/zynq/PinFunctions.xml2default:defaultZ21-17h px
�
Loading package from %s
16*device2b
NC:/Xilinx/Vivado/2014.2/data\parts/xilinx/zynq/zynq/xc7z020/clg484/Package.xml2default:defaultZ21-16h px
�
Loading io standards from %s
15*device2T
@C:/Xilinx/Vivado/2014.2/data\./parts/xilinx/zynq/IOStandards.xml2default:defaultZ21-15h px
<
Reading XDEF placement.
206*designutilsZ20-206h px
:
Reading XDEF routing.
207*designutilsZ20-207h px
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2$
Read XDEF File: 2default:default2
00:00:002default:default2 
00:00:00.1152default:default2
453.1762default:default2
1.6722default:defaultZ17-268h px
9
Restoring placement.
754*designutilsZ20-754h px
�
ORestored %s out of %s XDEF sites from archive | CPU: %s secs | Memory: %s MB |
403*designutils2
2872default:default2
2872default:default2
0.0000002default:default2
0.0000002default:defaultZ20-403h px
r
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px
�
!Unisim Transformation Summary:
%s111*project2�
�  A total of 10 instances were transformed.
  RAM32M => RAM32M (RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMS32, RAMS32): 10 instances
2default:defaultZ1-111h px
_
$Checkpoint was created with build %s293*project2
9326372default:defaultZ1-484h px
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2%
open_checkpoint: 2default:default2
00:00:052default:default2
00:00:052default:default2
453.1762default:default2
282.1452default:defaultZ17-268h px
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2"
Implementation2default:default2
xc7z0202default:defaultZ17-347h px
�
0Got license for feature '%s' and/or device '%s'
310*common2"
Implementation2default:default2
xc7z0202default:defaultZ17-349h px
u
,Running DRC as a precondition to command %s
1349*	planAhead2#
write_bitstream2default:defaultZ12-1349h px
M
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px
�

Rule violation (%s) %s - %s
20*drc2
NSTD-12default:default2,
Unspecified I/O Standard2default:default2�	
�	162 out of 162 logical ports use I/O standard (IOSTANDARD) value 'DEFAULT', instead of a user assigned specific value. This may cause I/O contention or incompatibility with the board power or connectivity affecting performance, signal integrity or in extreme cases cause damage to the device or the components to which it is connected. To correct this violation, specify all I/O standards. This design will fail to generate a bitstream unless all logical ports have a user specified I/O standard value defined. To allow bitstream creation with unspecified I/O standard values (not recommended), use this command: set_property SEVERITY {Warning} [get_drc_checks NSTD-1].  NOTE: When using the Vivado Runs infrastructure (e.g. launch_runs Tcl command), add this command to a .tcl file and add that file as a pre-hook for write_bitstream step for the implementation run. Problem ports: BCLK_O[0:0], LRCLK_O[0:0], SDATA_O[0:0], SDATA_I[0:0], S_AXIS_TDATA[31], S_AXIS_TDATA[30], S_AXIS_TDATA[29], S_AXIS_TDATA[28], S_AXIS_TDATA[27], S_AXIS_TDATA[26], S_AXIS_TDATA[25], S_AXIS_TDATA[24], S_AXIS_TDATA[23], S_AXIS_TDATA[22], S_AXIS_TDATA[21] (the first 15 of 93 listed).2default:defaultZ23-20h px
�

Rule violation (%s) %s - %s
20*drc2
UCIO-12default:default2.
Unconstrained Logical Port2default:default2�
�162 out of 162 logical ports have no user assigned specific location constraint (LOC). This may cause I/O contention or incompatibility with the board power or connectivity affecting performance, signal integrity or in extreme cases cause damage to the device or the components to which it is connected. To correct this violation, specify all pin locations. This design will fail to generate a bitstream unless all logical ports have a user specified site LOC constraint defined.  To allow bitstream creation with unspecified pin locations (not recommended), use this command: set_property SEVERITY {Warning} [get_drc_checks UCIO-1].  NOTE: When using the Vivado Runs infrastructure (e.g. launch_runs Tcl command), add this command to a .tcl file and add that file as a pre-hook for write_bitstream step for the implementation run.  Problem ports: BCLK_O[0:0], LRCLK_O[0:0], SDATA_O[0:0], SDATA_I[0:0], S_AXIS_TDATA[31], S_AXIS_TDATA[30], S_AXIS_TDATA[29], S_AXIS_TDATA[28], S_AXIS_TDATA[27], S_AXIS_TDATA[26], S_AXIS_TDATA[25], S_AXIS_TDATA[24], S_AXIS_TDATA[23], S_AXIS_TDATA[22], S_AXIS_TDATA[21] (the first 15 of 93 listed).2default:defaultZ23-20h px
c
DRC finished with %s
1905*	planAhead2(
2 Errors, 7 Warnings2default:defaultZ12-3199h px
f
BPlease refer to the DRC report (report_drc) for more information.
1906*	planAheadZ12-3200h px
O
+Error(s) found during DRC. Bitgen not run.
1345*	planAheadZ12-1345h px
W
Releasing license: %s
83*common2"
Implementation2default:defaultZ17-83h px
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2%
write_bitstream: 2default:default2
00:00:112default:default2
00:00:122default:default2
560.0392default:default2
106.8632default:defaultZ17-268h px
}
Exiting %s at %s...
206*common2
Vivado2default:default2,
Fri May 29 16:11:55 20152default:defaultZ17-206h px