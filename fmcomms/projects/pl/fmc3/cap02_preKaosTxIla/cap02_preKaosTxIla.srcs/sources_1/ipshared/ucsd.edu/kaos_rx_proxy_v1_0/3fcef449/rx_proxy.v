`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:  UCSD WES
// Engineer: Brian Kildea
// 
// Create Date: 06/07/2015 05:18:52 PM
// Design Name: 
// Module Name: rx_proxy
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module is a stand-in for an actual baseband QPSK demodulator. The
//              demodulator will accept as input one set of signals from downstream 
//              (i.e., toward RF) blocks and output another set of signals to upstream 
//              (i.e., toward the computer terminal) blocks.  The purpose of this proxy 
//              is simply to pass through signals from the adjacent input block to the 
//              adjacent output block in such a way that these adjacent blocks do not 
//              have to be altered when the actual QPSK demodulator block(s) is dropped in. 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rx_proxy (

    input         clk,
    input [15:0]  rx_i_data_in,
    input [15:0]  rx_q_data_in,
    input         rx_data_in_valid,
    
    output [31:0] rx_data_out,
    output        rx_data_out_valid
  );

//  reg [15:0] rx_i_data_in_reg      = 16'b0;
//  reg [15:0] rx_q_data_in_reg      = 16'b0;
  reg [31:0] rx_data_out_reg       = 32'b0;
  reg        rx_data_out_valid_reg =  1'b0;

  assign rx_data_out        = rx_data_out_reg;
  assign rx_data_out_valid  = rx_data_out_valid_reg;

  always @(posedge clk) begin

    // When the KAOS RX 9361 block wants to send the QPSK demodulator data
    // it will assert the 'rx_data_in_valid' signal. When this signal is
    // asserted we drive data onto the output data bus and also assert 
    // the 'rx_data_out_valid' output signal.
    if (rx_data_in_valid == 1'b1) begin
      rx_data_out_reg       <= {rx_i_data_in, rx_q_data_in}; 
      rx_data_out_valid_reg <= 1'b1;
    end

    // Only strobe 'rx_data_in_valid'. Therefore, 
    // if it's value is 1, clear it.
    if (rx_data_out_valid_reg == 1'b1) begin
      rx_data_out_valid_reg <= 1'b0;
    end
  end
endmodule
