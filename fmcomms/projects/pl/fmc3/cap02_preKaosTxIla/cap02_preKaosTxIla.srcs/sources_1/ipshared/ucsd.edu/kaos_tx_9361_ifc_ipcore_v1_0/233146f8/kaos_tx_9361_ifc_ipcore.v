// -------------------------------------------------------------
// 
// File Name: kaos_tx_9361_ifc_hdl_prj\hdlsrc\kaos_tx_9361_interface\kaos_tx_9361_ifc_ipcore.v
// Created: 2015-05-28 01:39:46
// 
// -------------------------------------------------------------
// 
// Module: kaos_tx_9361_ifc_ipcore
// Source Path: kaos_tx_9361_ifc_ipcore
// Hierarchy Level: 0
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module kaos_tx_9361_ifc_ipcore
          (
           input         clk,
           input         dac_enable_i0,
           input         dac_enable_i1,
           input         dac_enable_q0,
           input         dac_enable_q1,
           input         dac_valid_i0,
           input         dac_valid_i1,
           input         dac_valid_q0,
           input         dac_valid_q1,
           input  [31:0] kaos_tx_data,
           input         kaos_data_valid,
           output wire [15:0] dac_data_i0,
           output wire [15:0] dac_data_i1,
           output wire [15:0] dac_data_q0,
           output wire [15:0] dac_data_q1
          );

  reg        kaos_data_valid_reg =  1'b0;
  reg [31:0] kaos_tx_data_reg    = 32'b0;
  reg [15:0] dac_data_i0_reg     = 16'b0;
  reg [15:0] dac_data_i1_reg     = 16'b0;
  reg [15:0] dac_data_q0_reg     = 16'b0;
  reg [15:0] dac_data_q1_reg     = 16'b0;
  
  wire dut_enable;  // ufix1
  wire ce_out_sig;  // ufix1
  
//  wire [15:0] dac_data_i0;
//  wire [15:0] dac_data_i1;
//  wire [15:0] dac_data_q0;
//  wire [15:0] dac_data_q1;

  assign dac_data_i0 = dac_data_i0_reg;
  assign dac_data_i1 = dac_data_i1_reg;
  assign dac_data_q0 = dac_data_q0_reg;
  assign dac_data_q1 = dac_data_q1_reg;

  always @(posedge clk)
    begin

      // When there is data on the input, indicated by the
      // assertion of 'kaos_data_valid', read it into 
      // registers and store it.
      if (kaos_data_valid == 1'b1) begin
        kaos_tx_data_reg    <= kaos_tx_data;
      end
          
      // When the AD9361 DAC wants data it will assert the
      // 'dac_enable_XX' signals. When each of these is
      // asserted we drive the appropriate output bus from 
      // 'kaos_tx_data' register.
      if (dac_enable_i0 == 1'b1) begin
        dac_data_i0_reg     <= kaos_tx_data[31:16]; 
      end
          
      if (dac_enable_i1 == 1'b1) begin
        dac_data_i1_reg     <= kaos_tx_data[31:16]; 
      end
          
      if (dac_enable_q0 == 1'b1) begin
        dac_data_q0_reg     <= kaos_tx_data[15:0]; 
      end
      
      if (dac_enable_q1 == 1'b1) begin
        dac_data_q1_reg     <= kaos_tx_data[15:0]; 
      end
    end

endmodule  // kaos_tx_9361_ifc_ipcore

