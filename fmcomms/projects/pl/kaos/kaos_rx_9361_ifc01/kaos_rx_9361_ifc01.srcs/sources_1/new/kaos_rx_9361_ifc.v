`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD WES
// Engineer: Brian Kildea
// 
// Create Date: 06/07/2015 02:58:46 PM
// Design Name: 
// Module Name: kaos_rx_9361_ifc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module serves as a gateway between the FMCOMMS2 design blocks 
//              and the QPSK receiver implementation. This module is intended to 
//              facilitate the transition between FMCOMMS2 blocks and the receiver
//              which is intended to be a set of IP blocks generated using the
//              MathWorks Simulink HDL Coder utility.
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module kaos_rx_9361_ifc
  (
    input         clk,
    input         adc_enable_ch0,
    input         adc_enable_ch1,
    input         adc_enable_ch2,
    input         adc_enable_ch3,
    input         adc_valid_ch0,
    input         adc_valid_ch1,
    input         adc_valid_ch2,
    input         adc_valid_ch3,
    input  [15:0] adc_data_ch0,
    input  [15:0] adc_data_ch1,
    input  [15:0] adc_data_ch2,
    input  [15:0] adc_data_ch3,
    
    output [15:0] kaos_rx_i_data,
    output [15:0] kaos_rx_q_data,
    output        kaos_data_valid,
    output [15:0] kaos_rx_ch0_data,
    output [15:0] kaos_rx_ch1_data,
    output [15:0] kaos_rx_ch2_data,
    output [15:0] kaos_rx_ch3_data
  );

  reg [15:0] adc_data_ch0_reg     = 16'b0;
  reg [15:0] adc_data_ch1_reg     = 16'b0;
  reg [15:0] adc_data_ch2_reg     = 16'b0;
  reg [15:0] adc_data_ch3_reg     = 16'b0;

  reg [15:0] kaos_rx_i_data_reg   = 16'b0;
  reg [15:0] kaos_rx_q_data_reg   = 16'b0;
  reg        kaos_data_valid_reg  =  1'b0;
  
//  assign kaos_rx_i_data  = kaos_rx_i_data_reg;
//  assign kaos_rx_q_data  = kaos_rx_q_data_reg;
  assign kaos_data_valid = kaos_data_valid_reg;
  
  // These four output signals are for test purposes.
  // We intend to delete them, but for now make them available
  // scoping purposes.
  assign kaos_rx_ch0_data = adc_data_ch0_reg;
  assign kaos_rx_ch1_data = adc_data_ch1_reg;
  assign kaos_rx_ch2_data = adc_data_ch2_reg;
  assign kaos_rx_ch3_data = adc_data_ch3_reg;

  // We expect that the four received channels 
  // represent duplicated pairs of both the I 
  // and Q channels.  Presently, we do not know
  // which two channels of channels 0-3 are
  // the I channels and which two are the Q
  // channels.  We initially guess ch0 is an I
  // channel and ch 1 is a Q channel.  Therefore,
  // for the moment we arbitrarily assign these
  // channels to the I and Q outputs, respectively.   
  assign kaos_rx_i_data   = adc_data_ch0_reg;
  assign kaos_rx_q_data   = adc_data_ch1_reg;

  always @(posedge clk) begin
  
    // When the AD9361 ADC wants to send the receiver data it
    // will assert the 'adc_enable_chX' signals. When each of
    // these is asserted we drive data into the appropriate 
    // register from the corresponding input bus from the AD9361.
    if (adc_enable_ch0 == 1'b1) begin
      adc_data_ch0_reg     <= adc_data_ch0; 
    end
  
    if (adc_enable_ch1 == 1'b1) begin
      adc_data_ch1_reg     <= adc_data_ch1; 
    end
  
    if (adc_enable_ch2 == 1'b1) begin
      adc_data_ch2_reg     <= adc_data_ch2; 
    end
  
    if (adc_enable_ch3 == 1'b1) begin
      adc_data_ch3_reg     <= adc_data_ch3; 
    end
    
    if (adc_enable_ch0 == 1'b1 && adc_enable_ch1 == 1'b1) begin
      kaos_data_valid_reg <= 1'b1;
    end
    
    // Only strobe 'kaos_data_valid'. Therefore, 
    // if it's value is 1, clear it.
    if (kaos_data_valid == 1'b1) begin
      kaos_data_valid_reg <= 1'b0;
    end
  end

endmodule
