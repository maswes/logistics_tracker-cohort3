`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:  UCSD
// Engineer: Brian Kildea
// 
// Create Date: 06/07/2015 05:52:42 PM
// Design Name: 
// Module Name: kaos_rx_dma_ifc01
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module kaos_rx_dma_ifc01(

    input         clk,
    input  [31:0] rx_data_in,
    input         rx_data_in_valid,
    
    output [63:0] rx_data_out,
    output        rx_data_out_valid
  );

  reg [63:0] rx_data_out_reg       = 63'b0;
  reg        rx_data_out_valid_reg =  1'b0;

  assign rx_data_out        = rx_data_out_reg;
  assign rx_data_out_valid  = rx_data_out_valid_reg;

  always @(posedge clk) begin

    // When the KAOS QPSK Demodulator block wants to send data it
    // will assert this module's 'rx_data_in_valid' signal. When
    // this signal is asserted we drive data onto the output data
    // bus and also assert the 'rx_data_out_valid' output signal.
    if (rx_data_in_valid == 1'b1) begin
      rx_data_out_reg       <= rx_data_in; 
      rx_data_out_valid_reg <= 1'b1;
    end

    // Only strobe 'rx_data_in_valid'. Therefore, 
    // if it's value is 1, clear it.
    if (rx_data_out_valid_reg == 1'b1) begin
      rx_data_out_valid_reg <= 1'b0;
    end
  end
endmodule
