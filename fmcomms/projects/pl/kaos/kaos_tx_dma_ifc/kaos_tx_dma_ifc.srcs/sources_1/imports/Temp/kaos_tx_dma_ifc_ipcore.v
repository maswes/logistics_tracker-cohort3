// -------------------------------------------------------------
// 
// File Name: kaos_tx_dma_ifc_hdl_prj\hdlsrc\kaos_tx_dma_interface\kaos_tx_dma_ifc_ipcore.v
// Created: 2015-05-28 01:33:45
// 
// Generated by MATLAB 8.4 and HDL Coder 3.5
// 
// 
// -- -------------------------------------------------------------
// -- Rate and Clocking Details
// -- -------------------------------------------------------------
// Model base rate: -1
// Target subsystem base rate: -1
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: kaos_tx_dma_ifc_ipcore
// Source Path: kaos_tx_dma_ifc_ipcore
// Hierarchy Level: 0
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module kaos_tx_dma_ifc_ipcore
          (
           input   [63:0] dma_data,
           input          fifo_valid,
           input          clk,
           output         kaos_tx_data_valid,
           output  [1:0]  kaos_tx_bit_pair,
           output         dma_rd
          );


//  input   [63:0] dma_data;  // ufix64
//  input   fifo_valid;  // ufix1
//  input   clk;  // ufix1
//  output  kaos_tx_data_valid;  // ufix1
//  output  [1:0] kaos_tx_bit_pair;  // ufix2
//  output  dma_rd;  // ufix1

  reg  [63:0] dma_data_reg    = 64'h0;
  reg         fifo_valid_reg  =  1'b0;
  reg         dma_rd_reg;             // ufix1
  reg  [1:0]  kaos_tx_bit_pair_reg;   // ufix2

  wire        dut_enable;             // ufix1
  wire        ce_out_sig;             // ufix1
  wire        kaos_tx_data_valid_sig; // ufix1
 

//  kaos_tx_dma_ifc_ipcore_dut   u_kaos_tx_dma_ifc_ipcore_dut_inst   (.clk(clk),                                    // ufix1
//                                                                    .dut_enable(dut_enable),                      // ufix1
//                                                                    .dma_data(dma_data),                          // sfix64
//                                                                    .fifo_valid(fifo_valid),                      // ufix1
//                                                                    .ce_out(ce_out_sig),                          // ufix1
//                                                                    .kaos_tx_data_valid(kaos_tx_data_valid_sig),  // ufix1
//                                                                    .kaos_tx_bit_pair(kaos_tx_bit_pair_sig),      // ufix2
//                                                                    .dma_rd(dma_rd_sig)                           // ufix1
//                                                                    );

  assign kaos_tx_data_valid = kaos_tx_data_valid_sig;

  assign kaos_tx_bit_pair = kaos_tx_bit_pair_reg;

  assign dma_rd = dma_rd_reg;

  always @(posedge clk)
    begin
    
      // Drop the 'dma_rd' output whenever the DAC DMA
      // has raised our 'fifo_valid' input
      if (fifo_valid == 1'b1) begin
        dma_rd_reg <= 1'b0;
      end

      // When fifo_valid is first raised by the DAC DMA
      // grab the data from the DAC DMA and set
      // a flag to indicate that we've read it.
      if (fifo_valid && fifo_valid_reg == 1'b0) begin
        dma_data_reg         <= dma_data; // Get the data
        kaos_tx_bit_pair_reg <= {dma_data[1:0]};
        fifo_valid_reg       <= 1'b1;     // Set a flag
      end
    end

//  always @(negedge clk)
//    begin
    
//      // Clear the flag which indicates that the DAC DMA
//      // had data to read, as we'll have read it. Also,
//      // raise the dma_rd output to alert the DAC DMA to
//      // the fact that we're ready to accept more data.
//      if (fifo_valid_reg == 1'b1) begin
//        fifo_valid_reg <= 1'b0;
//        dma_rd_reg     <= 1'b1;
//      end
//    end

endmodule  // kaos_tx_dma_ifc_ipcore

