REM
REM Vivado(TM)
REM htr.txt: a Vivado-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for Vivado to track run status.
REM Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
REM

vivado -log kaos_tx_dma_ifc_ipcore.vds -m64 -mode batch -messageDb vivado.pb -source kaos_tx_dma_ifc_ipcore.tcl
