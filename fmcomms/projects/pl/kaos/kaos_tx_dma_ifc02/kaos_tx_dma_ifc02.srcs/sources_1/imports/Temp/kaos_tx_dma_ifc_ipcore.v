//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD WES
// Engineer: Brian Kildea
// 
// Create Date: 06/07/2015 02:58:46 PM
// Design Name: 
// Module Name: kaos_tx_dma_ifc_ipcore
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module serves as a gateway between the FMCOMMS2 design blocks 
//              and the QPSK transmitter implementation. This module is intended to 
//              facilitate the transition between the FMCOMMS2 block responsible for
//              interfacing with the processing system via DMA and the QPSK transmitter.
//              The latter is intended to be a set of IP blocks generated using the
//              MathWorks Simulink HDL Coder utility.
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns / 1 ns

module kaos_tx_dma_ifc_ipcore
          (
           input          clk,
           input   [63:0] dma_data,
           input          fifo_valid,
           output         kaos_tx_data_valid,
           output  [31:0] kaos_tx_hi_data,
           output  [31:0] kaos_tx_lo_data,
           output         dma_rd,
           output  wire   fifo_valid_s         // Simply exposes the fifo_valid_reg value to ILA
          );


//  reg  [63:0] dma_data_reg    = 64'h0;
  reg         fifo_valid_reg          =  1'b0;
  reg         dma_rd_reg              =  1'b0;
  reg  [31:0] kaos_tx_hi_data_reg     = 32'b0;
  reg  [31:0] kaos_tx_lo_data_reg     = 32'b0;
  reg         kaos_tx_data_valid_reg  =  1'b0;
  reg         processing_reg          =  1'b0;
  
  wire        dut_enable;
  wire        ce_out_sig;
 
  assign kaos_tx_data_valid = kaos_tx_data_valid_reg;

  assign kaos_tx_hi_data    = kaos_tx_hi_data_reg;
  assign kaos_tx_lo_data    = kaos_tx_lo_data_reg;

  assign dma_rd             = dma_rd_reg;

  assign fifo_valid_s       = fifo_valid_reg;

  always @(posedge clk)
    begin
    
      fifo_valid_reg   <= fifo_valid;     // We expose this to the fifo_valid_s output
      
      // Drop the 'dma_rd' output whenever the DAC DMA
      // has raised our 'fifo_valid' input
      if (dma_rd_reg == 1'b1) begin
        dma_rd_reg             <= 1'b0;
//        kaos_tx_data_reg       <= dma_data[31:0];
//        fifo_valid_reg         <= 1'b1;           // Set a flag
      end else begin
        dma_rd_reg             <= 1'b1;
      end
      
      if (fifo_valid == 1'b1) begin
//        dma_data_reg           <= dma_data;       // Get the data
        processing_reg         <= 1'b1;
      end
      
      if (processing_reg == 1'b1) begin
        kaos_tx_hi_data_reg    <= dma_data[63:32];
        kaos_tx_hi_data_reg    <= dma_data[31:0];
        kaos_tx_data_valid_reg <= 1'b1;             // Output data is valid; Raise output flag
        dma_rd_reg             <= 1'b1;             // Raise signal to DAC DMA to pull more TX data from DAC DMA
        processing_reg         <= 1'b0;
      end
      
      if (kaos_tx_data_valid_reg == 1'b1) begin
        kaos_tx_data_valid_reg <= 1'b0;             // We just want to strobe the data valid signal; It was high, so lower it.
      end
    end

//  always @(negedge clk)
//    begin
    
//      // Clear the flag which indicates that the DAC DMA
//      // had data to read, as we'll have read it. Also,
//      // raise the dma_rd output to alert the DAC DMA to
//      // the fact that we're ready to accept more data.
//      if (fifo_valid_reg == 1'b1) begin
//        fifo_valid_reg <= 1'b0;
////        dma_rd_reg     <= 1'b1;
//      end
//    end

endmodule  // kaos_tx_dma_ifc_ipcore

