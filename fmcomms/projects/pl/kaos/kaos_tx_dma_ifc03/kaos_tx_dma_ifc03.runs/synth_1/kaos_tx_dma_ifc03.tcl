# 
# Synthesis run script generated by Vivado
# 

  set_param gui.test TreeTableDev
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
set_msg_config -id {HDL-1065} -limit 10000
create_project -in_memory -part xc7z020clg484-1
set_property target_language Verilog [current_project]
set_property board_part em.avnet.com:zed:part0:1.0 [current_project]
set_param project.compositeFile.enableAutoGeneration 0
set_property default_lib xil_defaultlib [current_project]
set_property ip_repo_paths {
  C:/capstone/projects/pl/kaos/kaos_tx_dma_ifc03/kaos_tx_dma_ifc03.srcs/sources_1/new
  c:/capstone/projects/pl/kaos/kaos_tx_dma_ifc03/kaos_tx_dma_ifc03.srcs/sources_1/new
} [current_fileset]
read_verilog -library xil_defaultlib C:/capstone/projects/pl/kaos/kaos_tx_dma_ifc03/kaos_tx_dma_ifc03.srcs/sources_1/new/kaos_tx_dma_ifc03.v
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir C:/capstone/projects/pl/kaos/kaos_tx_dma_ifc03/kaos_tx_dma_ifc03.cache/wt [current_project]
set_property parent.project_dir C:/capstone/projects/pl/kaos/kaos_tx_dma_ifc03 [current_project]
catch { write_hwdef -file kaos_tx_dma_ifc03.hwdef }
synth_design -top kaos_tx_dma_ifc03 -part xc7z020clg484-1
write_checkpoint kaos_tx_dma_ifc03.dcp
report_utilization -file kaos_tx_dma_ifc03_utilization_synth.rpt -pb kaos_tx_dma_ifc03_utilization_synth.pb
