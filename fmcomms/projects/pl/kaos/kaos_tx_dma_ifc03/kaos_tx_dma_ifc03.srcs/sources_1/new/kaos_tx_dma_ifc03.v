//////////////////////////////////////////////////////////////////////////////////
// Company:  UCSD WES
// Engineer: Brian Kildea
// 
// Create Date: 06/07/2015 02:58:46 PM
// Design Name: 
// Module Name: kaos_tx_dma_ifc03
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module serves as a gateway between the FMCOMMS2 design blocks 
//              and the QPSK transmitter implementation. This module is intended to 
//              facilitate the transition between the FMCOMMS2 block responsible for
//              interfacing with the processing system via DMA and the QPSK transmitter.
//              The latter is intended to be a set of IP blocks generated using the
//              MathWorks Simulink HDL Coder utility.
//
//              kaos_tx_dma_ifc03 differs from kaos_tx_dma_ifc02 in the way data is sent
//              from this block to the QPSK transmitter.  Previously, we read data a 64-bit
//              word as fast as we could, threw away half since the processing system only
//              sends 32 bits at a time and then immediately pushed the remaining 32 bits 
//              to the next block (i.e., the QPSK transmitter).  Now, we still retrieve
//              64 bits from the DMA and keep only the 32 bits containing actual data. 
//              However, now we wait for the QPSK TX block to assert a 'data_enable' signal.
//              Each time it does so, we output one bit (MSB first).  If no data is available
//              we assert a 'data_underflow' signal. 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module kaos_tx_dma_ifc03 
  (

    input          clk,
    input   [63:0] dma_data,                // Data received from the DAC DMA to be transmitted.
    input          dma_fifo_valid,          
    input          xmtr_data_enable,        // Asserted by the QPSK Transmitter when it wants this block to send it a data bit

    output  wire   xmtr_data_valid,
    output  wire   xmtr_data,               // Data bit sent to QPSK TX
    output  wire   dma_rd,                  // Signal the DAC DMA to send another 64-bit word from the processing system
    output  wire   dma_fifo_valid_s         // Simply exposes the dma_fifo_valid_reg value to ILA
  );

  // Signals relating to comms with the DMA are prepended with 'dma_'.
  // Signals relating to the QPSK TX are prepended with 'xmtr_'.
  // Internal registers are prepended with 'kaos_'.
  reg         dma_fifo_valid_reg      =  1'b0;
  reg         dma_rd_reg              =  1'b0;
  reg  [31:0] kaos_tx_data_reg        = 32'b0;    // Where we store data when received from the DAC DMA.
  reg  [4:0]  kaos_index_reg          =  2'h1F;
  reg         xmtr_data_reg           =  1'b0;
  reg         xmtr_data_valid_reg     =  1'b0;
  
  assign xmtr_data          = xmtr_data_reg;
  assign xmtr_data_valid    = xmtr_data_valid_reg;
  assign dma_rd             = dma_rd_reg;
  assign dma_fifo_valid_s   = dma_fifo_valid_reg;

  always @(posedge xmtr_data_enable) begin
  
    xmtr_data_reg        = kaos_tx_data_reg[kaos_index_reg];
    kaos_index_reg       = kaos_index_reg + 1;
    xmtr_data_valid_reg  = 1'b1;                   // Output data is valid; Raise output flag
    
    if (kaos_index_reg == 0) begin
      dma_rd_reg     <= 1'b1;                      // Asserting high directs the AD9361 DAC DMA to send data
      kaos_index_reg <= 2'h1F;
    end else begin
      dma_rd_reg     <= 1'b0;                      // We just want to strobe the data read signal; It was high, so lower it.
    end
    
  end
  
  always @(negedge xmtr_data_enable) begin
      
    if (xmtr_data_valid_reg == 1'b1) begin
      xmtr_data_valid_reg <= 1'b0;                 // We just want to strobe the data valid signal; It was high, so lower it.
    end
  end
  
  always @(posedge clk) begin
    
    dma_fifo_valid_reg    = dma_fifo_valid;        // We expose this to the dma_fifo_valid_s output
      
    if (dma_fifo_valid_reg == 1'b1) begin
      kaos_tx_data_reg    <= dma_data[63:32];      // Get the data from the DMA. The high 32-bits contain the data.
    end
  end

endmodule
