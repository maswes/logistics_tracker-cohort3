# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ipgui::add_page $IPINST -name "Page 0" -layout vertical]
	set Component_Name [ipgui::add_param $IPINST -parent $Page0 -name Component_Name]
	set IQSEL [ipgui::add_param $IPINST -parent $Page0 -name IQSEL]
}

proc update_PARAM_VALUE.IQSEL { PARAM_VALUE.IQSEL } {
	# Procedure called to update IQSEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IQSEL { PARAM_VALUE.IQSEL } {
	# Procedure called to validate IQSEL
	return true
}


proc update_MODELPARAM_VALUE.IQSEL { MODELPARAM_VALUE.IQSEL PARAM_VALUE.IQSEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IQSEL}] ${MODELPARAM_VALUE.IQSEL}
}

