# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
	set Page0 [ipgui::add_page $IPINST -name "Page 0" -layout vertical]
	set Component_Name [ipgui::add_param $IPINST -parent $Page0 -name Component_Name]
	set PCORE_ADDR_WIDTH [ipgui::add_param $IPINST -parent $Page0 -name PCORE_ADDR_WIDTH]
}

proc update_PARAM_VALUE.PCORE_ADDR_WIDTH { PARAM_VALUE.PCORE_ADDR_WIDTH } {
	# Procedure called to update PCORE_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PCORE_ADDR_WIDTH { PARAM_VALUE.PCORE_ADDR_WIDTH } {
	# Procedure called to validate PCORE_ADDR_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.PCORE_ADDR_WIDTH { MODELPARAM_VALUE.PCORE_ADDR_WIDTH PARAM_VALUE.PCORE_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PCORE_ADDR_WIDTH}] ${MODELPARAM_VALUE.PCORE_ADDR_WIDTH}
}

