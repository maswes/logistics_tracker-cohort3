# README #

Provides the RFID front-end implementation for the Logistics Tracker.

### What is this repository for? ###

* Maintains the source code for the RFID front-end implementation running in Raspian Linux on the R-Pi B+

### How do I get setup ###

This implementation uses the LinkSprite Cottonwood RFID reader via TTL serial and incorporates an LCD and several LEDs to provide status indication.  Commenting out the LCD and LED code will not affect the RFID implementation.   In order to get setup, pull the src tree and place it in your home directory.   Ensure you have python 2.7 or later installed in Raspian.   Look at the import header to see what other library support you need and download them.  Everything is being actively supported and shouldn't be hard to find.
  

### Contribution guidelines ###

As a contributor, you're responsible for quality.  Please ensure that everything stays working so the next guys finds it better than you left it.

### Who do I talk to? ###

After the 2015 academic year, UCSD professor, Ryan Kastner is the POC.