clc;clear all;
n = 3;      % path loss exponent: 2 for free space; short range indoor 2.5-3 per 18000-6C
samples = 10000;
sigma = 2;  % 95% of the population 
Fc = 866e6; % default Cottonwood center freq where range testing occurred
lambda = 3e8/Fc;
Dc = .5;    % duty cycle.  For ASK, use 0.5 per 18000-6C
e = .5;     %efficiency associated with the tag switching:  .5 - .8 per 18000-6C
Ptx = 20;   % Cottonwood RFID TX rating in dBm
Gtx = 8;    % TX antenna gain for LinkSprite antenna
Grx = 0;    % RX antenna gain unknown, assume 1. Xerafy Micro X II (read distance up to 10 m)
Stag = [-9 -10 -11 -12 -13 -14 -15 -16 -17 -18 -19 -20]; % Per 18000-6C, known tag sensitivity range
Srdr = -86; % Per AS3992 datasheet
%generate pool of fade coefficients
D = normrnd(0, sigma,[1 samples])'; % fade coefficients in dB
%%
%calculate path loss using upper bound for path loss exponent 
Rmax_tag = (((lambda/(4*pi))^2)*(10.^(D/10))*(10.^((Ptx+Gtx+Grx-Stag)/10))).^(1/n);
%average out the fading
Ravg_tag1 = mean(Rmax_tag,1);

%%
%calculate path loss using lower bound for path loss exponent 
n = 2.5;
Rmax_tag = (((lambda/(4*pi))^2)*(10.^(D/10))*(10.^((Ptx+Gtx+Grx-Stag)/10))).^(1/n);
%average out the fading
Ravg_tag2 = mean(Rmax_tag,1);

%%
%calculate distance for FCC limit of 36 dBm EIRP
Ptx = 28;
%calculate path loss using lower bound for path loss exponent 
Rmax_tag = (((lambda/(4*pi))^2)*(10.^(D/10))*(10.^((Ptx+Gtx+Grx-Stag)/10))).^(1/n);
%average out the fading
Ravg_tag3 = mean(Rmax_tag,1);

%%
%plot it
plot(Stag, Ravg_tag3, Stag, Ravg_tag1, Stag, Ravg_tag2);
xlabel('Tag Sensitivity (dBm)');
ylabel('Read Range (m)');
ylim([0 25]);
title('Passive Tag Read Range vs. Tag Sensitivity');
legend('FCC Max Power 36 dBm Reader, Low Path Loss (n=2.5)', ...
    'High Path Loss (n=3) 28 Dbm Reader', ...
    'Low Path Loss (n=2.5) 28 dBm Reader');

%%
%calculate Rmax_reader given Srdr to demonstrate that tag read distance is
%the limiting factor for passive, non-battery-assisted power (bap)
Rmax_rdr = (((lambda/(4*pi))^4)*(10.^(2*D/10))*4*Dc*e*(10^((Ptx-Srdr)/10)) ...
    *(10^(2*Gtx/10))*(10^(2*Grx/10))).^(1/(2*n));
%average out the fading
Ravg_rdr = mean(Rmax_rdr,1)
% convert to line for plotting
Ravg_rdr_line = Ravg_rdr*ones(length(Stag));

figure;
plot(Stag, Ravg_tag3, Stag, Ravg_rdr_line);
xlabel('Tag Sensitivity (dBm)');
ylabel('Read Range (m)');
ylim([0 30]);
title('Passive Tag Read Range vs. Sensitivity');
legend('FCC Max Power 36 dBm Reader','-86 dBm Reader Sensitivity');

