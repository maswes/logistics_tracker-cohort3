# Authored by Ron Smudz for use with the Cottonwood Long-Range RFID Reader via TTL,
# R-Pi B+ with camera, HD44780 LCD and 3 LEDs (RGB) for state.  LCD is initialized
# in __init__ using GPIO rs=17, e=18, 4-pin logic 4-7 to 5,6,13 and 19. After
# detection, calls are made to register the RFID tags with a test server via TCP socket
# call on relayPort at relayAddress.
# 6/9/15 

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from datetime import datetime
import time
import threading
import serial
import sys
import binascii
import RPi.GPIO as GPIO
import socket
from ftplib import FTP
import os.path

def enum(**enums):
        return type('Enum', (), enums)

#globals
#BCM GPIO for LEDs
BLUE_LED = 16
RED_LED = 20
GREEN_LED = 21
baseDir = "/home/pi/project"
relayAddress = "192.168.1.116"
relayPort = 10000
NACK = False
bail = False
error = False
redLedState = False
blueLedState = True
greenLedState = False
nominalBlinkRate = .05
errorBlinkRate = 1
reportSuccessBlinkRate = 5
rfidQueryRate = .2
resetTimeInS = 60
startTime = time.time()
ErrorMessage = enum(ERR_UNK = 'Unknown error', ERR_CONNECT = 'System down', ERR_REPORT = 'Reporting Error', ERR_TIMEOUT = 'Timeout Error') 
errorMessage = ErrorMessage.ERR_UNK
imageRoot = "/home/pi/project/snapshots/"

#create the serial stream to reader. reader is fixed to 9600 best I can tell
ser = serial.Serial("/dev/ttyAMA0",9600,timeout=0)
#create LCD
lcd = Adafruit_CharLCD()

#define the byte where to find the first tag's EPC length
tag1LengthField = 7
#store the EPCs of all tags found in an inventory round
epcList = []
reportedList = []
#set to tag tuning freq of 890 MHz.  Adjust accordingly.
cmd0 = bytearray.fromhex("41080890940dd801")
#power command
cmd1 = bytearray.fromhex("1803ff")
#tag inventory command
cmd2 = bytearray.fromhex("430301")
#snapshot command
cmd3 = "raspistill -h 120 -w 160 -q 10 -o snapshots/" 

class SendSnapshotThread (threading.Thread):
        def __init__(self, threadID, name, counter, newTagIds):
                threading.Thread.__init__(self)
                self.threadID = threadID
                self.name = name
                self.counter = counter
                self.newTagIds = newTagIds
        def run(self):
                sendSnapshot(self.newTagIds + ".jpg")
                
class TakeSnapshotThread (threading.Thread):
        def __init__(self, threadID, name, counter, newTagIds):
                threading.Thread.__init__(self)
                self.threadID = threadID
                self.name = name
                self.counter = counter
                self.newTagIds = newTagIds
        def run(self):
                global NACK
                takeSnapshot(self.newTagIds)
                sendLock.acquire()
        
                if NACK == True:
                        NACK = False
                        #send snapshot
                        sendSnapshotThread = SendSnapshotThread(1, "SendSnapshotThread", 4, self.newTagIds)
                        sendSnapshotThread.start()

                sendLock.release()        
 
class ReportSuccessIndicatorThread (threading.Thread):
        def __init__(self, threadID, name, counter):
                threading.Thread.__init__(self)
                self.threadID = threadID
                self.name = name
                self.counter = counter
        def run(self):
                time.sleep(reportSuccessBlinkRate)
                toggleLed(GREEN_LED,6,1)
                GPIO.output(GREEN_LED, False)
                
class ReportThread (threading.Thread):
        def __init__(self, threadID, name, counter):
                threading.Thread.__init__(self)
                self.threadID = threadID
                self.name = name
                self.counter = counter
        def run(self):
                print("Starting " + self.name)
                
                while True:
                        time.sleep(.1)
                        
                        if bail == True:
                                break
                        elif error == True:
                                handleError()
                        else:
                                deconflictEPCList()


def reportEPCList(newTagIds):
        global error, errorMessage, NACK

        print("reporting new tag(s)")

        #take snapshot
        sendLock.acquire()
        takeSnapshotThread = TakeSnapshotThread(1, "TakeSnapshotThread", 3, newTagIds)
        takeSnapshotThread.start()

        #reset any previous successful reporting status light
        GPIO.output(GREEN_LED, False)

        try:
                makeSocket()
                sock.sendall(newTagIds)
                data = sock.recv(1024)
                
                if not data:
                        print("no data")
                        error = True
                else:        
                        print("reply: " + data)

                        if 'ACK' in data:
                                GPIO.output(GREEN_LED, True)
                                #create reporting success thread
                                reportSuccessIndicatorThread = ReportSuccessIndicatorThread(1, "ReportSuccessIndicatorThread", 2)
                                reportSuccessIndicatorThread.start()
                        else:
                                NACK = True
                                print("High value item(s) detected")
                                
        except socket.error:
                error = True
                errorMessage = ErrorMessage.ERR_REPORT
        except socket.timeout:
                error = True
                errorMessage = ErrorMessage.ERR_TIMEOUT
        finally:
                sendLock.release()
                sock.close()
                
def handleError():
        print(errorMessage)
        #red LED blink at 1 Hz
        global redLedState
        redLedState = not redLedState
        GPIO.output(BLUE_LED, False)
        GPIO.output(GREEN_LED, False)
        GPIO.output(RED_LED, redLedState)
        
        #close and remake the socket until error clears
        if (errorMessage == ErrorMessage.ERR_CONNECT or errorMessage == ErrorMessage.ERR_REPORT):

                if 'sock' in globals(): 
                        sock.close()

                makeSocket()

        time.sleep(errorBlinkRate)
 
def deconflictEPCList():
        global reportedList, startTime
        
        threadLock.acquire()
        #copy any shared variable
        copy = epcList[:]
        #needs this sleep to perform the copy
        time.sleep(.01)
        threadLock.release()

        epcListSet = set(copy)
        reportedSet = set(reportedList)
        
        #union set after reporting
        unionSet = reportedSet.union(epcListSet)
        
        #new items in field to report
        diff = abs(len(unionSet) - len(reportedSet)) 
        if diff > 0:
                toggleLed(RED_LED, diff, 20)
##                print("unionSet: " + str(len(unionSet)))
##                print(", ".join(str(e) for e in unionSet))
##                print("reportedSet: " + str(len(reportedSet)))
##                print(", ".join(str(e) for e in reportedSet))
                #find newly appeared
                arrivedSet = epcListSet.difference(reportedSet)
                print("arrivedSet: " + str(len(arrivedSet)))
                newTags = ",".join(str(e) for e in arrivedSet)
                print(newTags)
                reportedList = list(unionSet)
                GPIO.output(RED_LED, False)
                reportEPCList(newTags)
                #extend elapsed time when new tags detected
                startTime = time.time()
                
        elapsed_time = time.time() - startTime
        if elapsed_time > resetTimeInS:
                reportedList[:] = []
                deleteSnapshots()
                startTime = time.time()
  
def takeSnapshot(fileName):
        #for lossy links, consider sending .bmp vice .jpg
        p = Popen(cmd3 + fileName + ".jpg", shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

def deleteSnapshots():
        p = Popen("rm -r " + baseDir + "/snapshots/*.jpg", shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

def sendSnapshot(filename):
        fullpath = imageRoot + filename
        file =open(fullpath, "rb")
        blocksize = os.path.getsize(fullpath)
        ftp = FTP(relayAddress)
        
        try:
                ftp.login("wes_test", "")
                ftp.storbinary("STOR " + filename, file, blocksize)
                ftp.quit()
                GPIO.output(GREEN_LED, True)
                #create reporting success thread
                reportSuccessIndicatorThread = ReportSuccessIndicatorThread(1, "ReportSuccessIndicatorThread", 2)
                reportSuccessIndicatorThread.start()
        except:
                print("failed to transfer image file " + filename)
                error = ErrorMessage.ERR_REPORT
        finally:
                file.close()

def toggleLed(led, count, multiplier):
        global blueLedState, greenLedState, redLedState

        if not error == True:
                while count >= 0:
                        if led == BLUE_LED:
                                blueLedState = not blueLedState
                                GPIO.output(BLUE_LED, blueLedState)
                        elif led == GREEN_LED:
                                greenLedState = not greenLedState
                                GPIO.output(GREEN_LED, greenLedState)
                        else:
                                redLedState = not redLedState
                                GPIO.output(RED_LED, redLedState)
                        count = count - 1
                        time.sleep(nominalBlinkRate*multiplier)

              
def getTagCount(response):
        #hex 44 starts the reply for the hex 43 command above
        #if not found, set detected to 0 and try again in the 
        #next round.  Can happen when there is not enough time
        #between the serial write and subsequent read.
        if(binascii.hexlify(response).startswith('44')):
                detected = response[2]
        else:
                detected = 0
        return detected

def makeSocket():
        global sock, error, errorMessage

        try:
                sock = socket.create_connection((relayAddress, relayPort),10.0)
                GPIO.output(RED_LED, False)
                error = False
        except socket.error:
                error = True
                errorMessage = ErrorMessage.ERR_CONNECT
        except socket.timeout:
                error = True
                errorMessage = ErrorMessage.ERR_TIMEOUT
 
def setup():
        global reportSuccessIndicatorThread, snapshotThread

        #init IO for LED status
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(16, GPIO.OUT)
        GPIO.setup(20, GPIO.OUT)
        GPIO.setup(21, GPIO.OUT)
        GPIO.output(BLUE_LED, True)
        GPIO.output(RED_LED, False)
        GPIO.output(GREEN_LED, False)

        #turn on reader at tag-tuned freq, tx to power ff (20 dBm max)
        ser.write(cmd0)
        time.sleep(.5)
        ser.write(cmd1)
        time.sleep(.5)
        ser.flushInput()

        #init the LCD for 16 columns by 2 rows.  Pin designations
        #are handled in the init() called during instantiation
        lcd.begin(16,2)

        #open a TCP socket for reporting
        makeSocket()
                
def run():
        global blueLedState
        #ready state = solid blue
        ser.write(cmd2)
        lcd.clear()
        lcd.message(datetime.now().strftime('%b %d %H:%M:%S\n'))
        #allow adequate time for the response to fill the buffer
        time.sleep(rfidQueryRate)
        value = bytearray(ser.read(256))

        if not error:
                count = getTagCount(value)
                sOut = "Tags detected: " + str(count)
                #print(sOut)
                lcd.message(sOut)
                offset = tag1LengthField

                i = count
                threadLock.acquire()

                while  i > 0:
                        toggleLed(BLUE_LED, 5, 1)
                        epcLen = value[offset]
                        tagStart = offset + 1
                        tagEnd = tagStart + epcLen
                        epcList.append(binascii.hexlify(value[tagStart:tagEnd]))
                        offset = tagEnd + 7
                        i -= 1

                threadLock.release()
                GPIO.output(BLUE_LED, True)
        else:
                lcd.message(errorMessage)

        #for tag in epcList:
        #        print("tag EPC: " + tag)

        #allow time for the list to be read
        time.sleep(2.7)
        #clear the list
        epcList[:] = []
        
if __name__ == '__main__':
        setup()

        threadLock = threading.Lock()
        sendLock = threading.Lock()
        
        #comment out reportEPCThread instantiation and start if you don't
        #want to send data back to the server
        reportEPCThread = ReportThread(1, "ReportEPCThread", 1)
        reportEPCThread.start();
        
        try:
                while True:
                        run()

        except KeyboardInterrupt:
                ser.close()
                GPIO.cleanup()
                bail = True
                print("bailing...")
                
