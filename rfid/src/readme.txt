The source files in this directory contain what you need to detect, photograph, process and report RFID tags detected by the reader.  The snapshots directory is where images
get stored after the camera is triggered by a detection.  Since I can't provide an entire R-Pi image to the repo, use the imports in main.py to pull in the library support 
you need.

main.py - runs on the R-Pi to process the RFID tags discovered by the RFID reader
test_server.py - test server used to receive and process tag data sent by main.py
Adafruit_CharLCD.py - HD44780 LCD driver support
Adafruit_CharLCD.pyc - HD44780 LCD driver support