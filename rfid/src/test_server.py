import socket
import sys

#hack to get local address
#sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#sock.connect(("gmail.com",80))
#address = sock.getsockname()[0]
#sock.close()

address = "192.168.4.1"

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
port = 10000
server_address = (address, port)
print("starting up on " + address + " port " + str(port))
sock.bind(server_address)
# Listen for incoming connections
sock.listen(1)

while True:
	# Wait for a connection
	print("waiting for a connection")
	connection, client_address = sock.accept()

	try:     
		data = connection.recv(1024)
##		connection.sendall(data)

		if data:
			#high value item
			if '000000001' in str(data):
				print("High value detected: " + str(data))
				connection.sendall(data)
			else:
				print("New inventory registered: " + str(data))
				connection.sendall(bytes('ACK', 'UTF-8'))
		else:
			print("no data")

	finally:
		connection.close()
		

	
