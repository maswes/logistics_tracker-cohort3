This is a virtual WiFi driver. It has two parts, the kernel driver and the
user driver.

The kernel driver is called mac80211_serial, and the user driver is called
user_driver.

The kernel driver is based on mac80211_hwsim. It compiles as a loadable
kernel module. When the kernel module is loaded, it creates a wlan interface
that can be used by the operating system as a WiFi device. However, this
wlan interface doesn't not transmit or receive from hardware. Instead,
it opens a netlink socket that can be used by a user application. When
the operating system wants to tranmit an 802.11 (WiFi) packet, 
the packet will be written by this kernel driver to the netlink socket. 
A user application can listen on the socket for the 802.11 packet. When
the user application receives the 802.11 packet, it can transmit it via
a mechanism of its choosing. For example, when the command "iw wlan0 scan"
is issued on the command line, the operating system will send an 802.11
packet to the wlan0 interface. The packet will go to this kernel driver
which will then transmit the 802.11 packet over the netlink socket. The
802.11 packet will be received by the user application listening on the socket
and the user application can then transmit the packet using any mechanism.
The user application can send the packet to hardware to transmit the packet
over the air. The user application will also listen for 802.11 packets
When it receives an 802.11 packet, it will send the packet to the netlink
socket. The packet will be received by this kernel driver and propagated
up the networking stack.

In this program, a user driver is provided. This user driver demonstrates
the communication with the kernel driver over the netlink socket. This user
driver listens on the netlink socket for packets from the kernel driver.
When the user driver receives an 802.11  packet from the kernel driver, 
it broadcasts the packet as a UDP packet to IP address 255.255.255.255. 
The user driver also listens for broadcast UDP packets with destination address
255.255.255.255. When it receives a UDP packet to 255.255.255.255, it sends
the payload over the netlink socket to the kernel driver. In this way,
broadcast UDP packets are used to simulate a WiFi network. As far as the
operating system is concerned, the wlan0 interface presented by the 
kernel driver is a WiFi driver, and all applications used with WiFi drivers
work. For example, hostapd can be used to host an access point, wpa_supplicant
can be used to connect to an access point, iw can be used to scan for access
points, and dns_masq can be used to host a DNS server.

To compile the kernel driver, the source code for the linux distribution
for the device must be used. The files in 
/kernel_driver/drivers/net/wireless are copied to the drivers/net/wireless
folder of the kernel source code. Notice that in the Kconfig and Makefile
entries are added for mac80211_serial. The .config file is copied to the 
root of the kernel source code. The kernel is the compiled as normal. After
the kernel is compiled, the kernel driver (mac80211_serial) is compiled
with this command: "make drivers/net/wireless/mac80211_serial.ko".

uImage for the kernel and the mac80211_serial.ko module is then copied to the
device. "insmod mac80211_serial.ko" is run to install the kernel module.

To compile the user driver, the user driver is copied to the device.
The netlink library is installed by running "sudo apt-get install libnl-3-dev".
Then the user driver is compiled by running "source compile.sh" to run the
compile script. To run the user driver, run 
"user_driver <ethernet interface> <ip address>" where <ethernet interface> is
the name of the ethernet interface that the UDP packets should be broadcast on
and <ip address> is the IP address of that interface. For example, 
"user_driver eth0 10.0.2.15".

For more details, see the wiki.
