#include <netlink/netlink.h>
#include <netlink/genl/genl.h>
#include <netlink/genl/ctrl.h>
#include <net/if.h>
#include <pthread.h>
#include <arpa/inet.h>

//copy this from iw
#include "nl80211.h"
#include "user_driver.h"

#define HELLO_PORT 12345
#define HELLO_GROUP "225.255.255.255"

static int expectedId;
static int tx_sock;
static nl_sock* sk;
static uint8_t mac_addr[6];
static in_addr_t ip_addr;
static char eth_interface[64];

static int nlCallback(struct nl_msg* msg, void* arg)
{
	int status;
	sockaddr_in addr = {0};
    addr.sin_addr.s_addr=htonl(-1); /* send message to 255.255.255.255 */
    addr.sin_port = htons(HELLO_PORT); /* port number */
    addr.sin_family = PF_INET;

    struct nlmsghdr* ret_hdr = nlmsg_hdr(msg);
    struct nlattr *tb_msg[HWSIM_ATTR_MAX + 1];

    printf("============\n");
    printf("NL msg recevied: len: %d, type: %d, flags: %d, seq: %d, pid: %d\n",
    		ret_hdr->nlmsg_len, ret_hdr->nlmsg_type, ret_hdr->nlmsg_flags,
    		ret_hdr->nlmsg_seq, ret_hdr->nlmsg_pid);
    if (ret_hdr->nlmsg_type != expectedId)
    {
        // what is this??
    	printf("Unexpected msg type: %d instead of %d\n",
    			ret_hdr->nlmsg_type, expectedId);
        return 0;
    }

    struct genlmsghdr *gnlh = (struct genlmsghdr*) nlmsg_data(ret_hdr);

    nla_parse(tb_msg, HWSIM_ATTR_MAX, genlmsg_attrdata(gnlh, 0),
              genlmsg_attrlen(gnlh, 0), NULL);

    printf("gnlh->cmd: %d\n", gnlh->cmd);
    printf("gnlh->version: %d\n", gnlh->version);

    if (tb_msg[HWSIM_ATTR_ADDR_TRANSMITTER])
    {
    	uint8_t *transmitter = (uint8_t*) nla_data(tb_msg[HWSIM_ATTR_ADDR_TRANSMITTER]);
    	printf("transmitter len: %d, addr: %02x %02x %02x %02x %02x %02x\n",
    			nla_len(tb_msg[HWSIM_ATTR_ADDR_TRANSMITTER]),
    			transmitter[0], transmitter[1], transmitter[2],
    			transmitter[3], transmitter[4], transmitter[5]);
    }

    if (tb_msg[HWSIM_ATTR_FRAME])
    {
    	uint8_t *frame = (uint8_t*) nla_data(tb_msg[HWSIM_ATTR_FRAME]);
    	int i;
    	int len = nla_len(tb_msg[HWSIM_ATTR_FRAME]);
    	printf("Transmitting: ");
    	for (i = 0; i < len; i++)
    	{
    		printf("%02x ", frame[i]);
    	}
    	printf("\n");

    	if (gnlh->cmd == HWSIM_CMD_FRAME)
    	{
    		status = sendto(tx_sock, frame, len, 0, (struct sockaddr *)&addr, sizeof(addr));
    		printf("TX sendto Status = %d\n", status);

    		// Notify the interface that the frame has been transmitted
			// Create the netlink message
			nl_msg* tx_info_msg = nlmsg_alloc();

			// setup the message
			genlmsg_put(tx_info_msg, NL_AUTO_PORT, NL_AUTO_SEQ,
					expectedId, 0, 0, HWSIM_CMD_TX_INFO_FRAME, 0);

			//add message attributes
			if (nla_put(tx_info_msg, HWSIM_ATTR_ADDR_TRANSMITTER,
					sizeof(mac_addr), mac_addr) < 0)
				printf("put transmitter failed\n");
			if (nla_put_u32(tx_info_msg, HWSIM_ATTR_FLAGS,
					nla_get_u32(tb_msg[HWSIM_ATTR_FLAGS]) | HWSIM_TX_STAT_ACK) < 0)
				printf("put flags failed\n");
			if (nla_put_u64(tx_info_msg, HWSIM_ATTR_COOKIE,
					nla_get_u64(tb_msg[HWSIM_ATTR_COOKIE])) < 0)
				printf("put cookie failed\n");
			int tx_info_len = nla_len(tb_msg[HWSIM_ATTR_TX_INFO]);
			hwsim_tx_rate* tx_rate_info = (hwsim_tx_rate*) nla_data(tb_msg[HWSIM_ATTR_TX_INFO]);
			printf("tx_info_len: %d\n", tx_info_len);
			printf("sizeof(hwsim_tx_rate): %d\n", sizeof(hwsim_tx_rate));
			tx_rate_info[0].count = 1;
			tx_rate_info[1].idx = -1;
			if (nla_put(tx_info_msg, HWSIM_ATTR_TX_INFO, tx_info_len,
					tx_rate_info) < 0)
				printf("put tx_info failed\n");
			if (nla_put_u32(tx_info_msg, HWSIM_ATTR_SIGNAL, 0) < 0)
				printf("put signal failed\n");

			//send the messge (this frees it)
			int ret = nl_send_auto(sk, tx_info_msg);
			printf("Send confirmation to interface: %d\n", ret);
    	}
    }

    /*
	HWSIM_ATTR_ADDR_RECEIVER,
	HWSIM_ATTR_ADDR_TRANSMITTER,
	HWSIM_ATTR_FRAME,
	HWSIM_ATTR_FLAGS,
	HWSIM_ATTR_RX_RATE,
	HWSIM_ATTR_SIGNAL,
	HWSIM_ATTR_TX_INFO,
	HWSIM_ATTR_COOKIE,
	HWSIM_ATTR_CHANNELS,
	HWSIM_ATTR_RADIO_ID,
	HWSIM_ATTR_REG_HINT_ALPHA2,
	HWSIM_ATTR_REG_CUSTOM_REG,
	HWSIM_ATTR_REG_STRICT_REG,
	HWSIM_ATTR_SUPPORT_P2P_DEVICE,
	HWSIM_ATTR_USE_CHANCTX,
	*/
    /*
    if (tb_msg[NL80211_ATTR_IFTYPE]) {
        int type = nla_get_u32(tb_msg[NL80211_ATTR_IFTYPE]);

        printf("Type: %d", type);
    }*/
    return 0;
}

void* listen_to_udp(void *arg)
{
	struct sockaddr_in addr;
	int sock, status;
	sock = socket (AF_INET, SOCK_DGRAM, 0);
	printf("sock: %d\n", sock);

    int yes = 1;
    status = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int) );
    status = setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, eth_interface, 
        strlen(eth_interface));
    printf("Setsockopt Status = %d\n", status);

    /* set up destination address */
    memset(&addr,0,sizeof(addr));
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port=htons(HELLO_PORT);
    addr.sin_family = AF_INET;
    socklen_t addr_len = sizeof(addr);

    status = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
    printf("Bind Status = %d\n", status);

    char buffer[2048];
    while (1)
    {
		status = recvfrom(sock, buffer, sizeof(buffer), 0,
				(struct sockaddr *)&addr, &addr_len);
		printf("recvfrom Status = %d\n", status);
		printf("from %08x\n", addr.sin_addr.s_addr);
		if (addr.sin_addr.s_addr == ip_addr)
		{
			printf("Message is from self. Dropping.\n");
		}
		else if (status > 0)
		{
			printf("received over UDP: ");
			for (int i = 0; i < status; i++)
				printf("%02x ", buffer[i]);
			printf("\n");

			// Create the netlink message
			nl_msg* msg = nlmsg_alloc();

			/*
			nl80211_commands cmd = NL80211_CMD_GET_INTERFACE;
			int ifIndex = if_nametoindex("wlan0");
			int flags = 0;
			 */
			// setup the message
			genlmsg_put(msg, NL_AUTO_PORT, NL_AUTO_SEQ,
					expectedId, 0, 0, HWSIM_CMD_FRAME, 0);

			//add message attributes
			if (nla_put(msg, HWSIM_ATTR_ADDR_RECEIVER, sizeof(mac_addr), mac_addr) < 0)
				printf("put receiver failed\n");
			if (nla_put(msg, HWSIM_ATTR_FRAME, status, buffer) < 0)
				printf("put frame failed\n");
			if (nla_put_u32(msg, HWSIM_ATTR_RX_RATE, 0) < 0)
				printf("put rx rate failed\n");
			if (nla_put_u32(msg, HWSIM_ATTR_SIGNAL, 0) < 0)
				printf("put signal failed\n");

			//send the messge (this frees it)
			int ret = nl_send_auto(sk, msg);
			printf("Send frame to interface: %d\n", ret);
		}
    }
}

int main(int argc, char** argv)
{
    int ret;

    // Read the mac address for the device we want to use
    // from the command line
    if (argc != 3)
    {
    	printf ("Usage: program <ethernet interface> <ip address of eth>.\n");
    	printf ("Example: user_driver eth0 192.168.2.165");
    	exit(0);
    }

    strcpy(eth_interface, argv[1]);

    ip_addr = inet_addr(argv[2]);
    printf("Using IP address %08x\n", ip_addr);

    mac_addr[0] = 0;
    mac_addr[1] = 0;
    mac_addr[2] = 0;
    mac_addr[3] = 0;
    mac_addr[4] = 0;
    mac_addr[5] = 0;

    // Create the socket to transmit on
	struct sockaddr_in addr;
	int status;
	tx_sock = socket (AF_INET, SOCK_DGRAM, 0);
	printf("TX sock: %d\n", tx_sock);

    int yes = 1;
    status = setsockopt(tx_sock, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int) );
    status = setsockopt(tx_sock, SOL_SOCKET, SO_BINDTODEVICE, eth_interface, 
        strlen(eth_interface));
    printf("TX Setsockopt Status = %d\n", status);

    /* set up destination address */
    memset(&addr,0,sizeof(addr));
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port=htons(0);
    addr.sin_family = AF_INET;
    socklen_t addr_len = sizeof(addr);

    status = bind(tx_sock, (struct sockaddr *)&addr, sizeof(addr));
    printf("Bind Status = %d\n", status);

    //allocate socket
    sk = nl_socket_alloc();

    //connect to generic netlink
    genl_connect(sk);

    //find the nl80211 driver ID
    expectedId = genl_ctrl_resolve(sk, "MAC80211_HWSIM");

    //attach a callback
    nl_socket_modify_cb(sk, NL_CB_MSG_IN, NL_CB_CUSTOM,
            nlCallback, NULL);

    //allocate a message
    nl_msg* msg = nlmsg_alloc();

    /*
    nl80211_commands cmd = NL80211_CMD_GET_INTERFACE;
    int ifIndex = if_nametoindex("wlan0");
    int flags = 0;
     */
    // setup the message
    genlmsg_put(msg, NL_AUTO_PORT, NL_AUTO_SEQ,
    		expectedId, 0, 0, HWSIM_CMD_REGISTER, 0);

    //add message attributes
    //NLA_PUT_U32(msg, NL80211_ATTR_IFINDEX, ifIndex);

    //send the messge (this frees it)
    ret = nl_send_auto(sk, msg);

    /* Create the thread to listen over UDP */
    pthread_t listen_udp_thread;
    int err = pthread_create(&listen_udp_thread, NULL, &listen_to_udp, NULL);
	if (err != 0)
		printf("\ncan't create thread :[%s]", strerror(err));
	else
		printf("\n Thread created successfully\n");


    //block for message to return
    while (1) {
    	nl_recvmsgs_default(sk);
    }

    return 0;

nla_put_failure:
    nlmsg_free(msg);
    return 1;
}
